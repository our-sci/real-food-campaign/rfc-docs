---
title: "Testing the Performance of Quantile Random Forests"
author: "Real Food Campaign"
date: "05/28/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---




```
## Registered S3 method overwritten by 'GGally':
##   method from   
##   +.gg   ggplot2
```

```
## Loading required package: lattice
```

```
## Loading required package: ggplot2
```

```
## Loading required package: foreach
```

```
## Loading required package: iterators
```

```
## Loading required package: parallel
```

```
## Loading required package: randomForest
```

```
## randomForest 4.6-14
```

```
## Type rfNews() to see new features/changes/bug fixes.
```

```
## 
## Attaching package: 'randomForest'
```

```
## The following object is masked from 'package:ggplot2':
## 
##     margin
```

```
## Loading required package: RColorBrewer
```

```
## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.0 ──
```

```
## ✔ tibble  3.0.1     ✔ dplyr   0.8.5
## ✔ tidyr   1.0.2     ✔ stringr 1.4.0
## ✔ readr   1.3.1     ✔ forcats 0.5.0
## ✔ purrr   0.3.4
```

```
## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ purrr::accumulate()    masks foreach::accumulate()
## ✖ dplyr::combine()       masks randomForest::combine()
## ✖ dplyr::filter()        masks stats::filter()
## ✖ dplyr::lag()           masks stats::lag()
## ✖ purrr::lift()          masks caret::lift()
## ✖ randomForest::margin() masks ggplot2::margin()
## ✖ purrr::when()          masks foreach::when()
```

```
## Loading required package: future
```

```
## 
## Attaching package: 'future'
```

```
## The following object is masked from 'package:caret':
## 
##     cluster
```

```
## 
## Attaching package: 'kableExtra'
```

```
## The following object is masked from 'package:dplyr':
## 
##     group_rows
```

# Method

We have chosen some typical models over several variables, to see how the observed $R^2$ value translates to a model with prediction intervals.

We will look at several of our pre existing models and train the same model we knew, using *Quantile Random Forest Regression* insteado of *Random Forest Regression*. 

## Targets and Means

1. We are going to study the representative size of the prediction intervals, relative to the range of the variable ( that is, the median of the interval sizes, in ratio agains the difference between the maximum and minimum observed values ).

1. We are going to look at the prediction performance, using a plot of **actual vs. predicted values** with the intervals superimposed, colored according to their success (that is, if the actual value belongs to their range).

1. We are going to have a look at the distribution of the prediction interval width, by plotting the errors and bars and looking at the area encompassed at each region, indexed by the **observed value**.


# A Model of High R², *c-min-vis-who* for BQI in wheat

* $R^2$ is 0.7628359. 
* Typical relative size of interval is 14% of the range observed for this variable.
* The interval misses the value in 25.7575758% of the occasions. 

## Performance


```r
plotQQ2
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

## Width Distribution


```r
plotArea2
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

# A Model of Medium R², *f-max-vis-pro* for Antioxidants in wheat

* $R^2$ is 0.7742138. 
* Typical relative size of interval is 22% of the range observed for this variable.
* The interval misses the value in 18.1395349% of the occasions. 

## Performance


```r
plotQQ1
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

## Width Distribution


```r
plotArea1
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-5-1.png)<!-- -->



# A Model of Medium Low R², *f-max-vis-who* for Antioxidants in oats

* $R^2$ is 0.7067854. 
* Typical relative size of interval is 16% of the range observed for this variable.
* The interval misses the value in 18.018018% of the occasions. 

## Performance


```r
plotQQ3
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

## Width Distribution


```r
plotArea3
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

# A Model of Low R², *f-max-vis-who* for Proteins in wheat

* $R^2$ is 0.5475766. 
* Typical relative size of interval is 24% of the range observed for this variable.
* The interval misses the value in 22.3255814% of the occasions. 

## Performance


```r
plotQQ4
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

## Width Distribution


```r
plotArea4
```

```
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
## `geom_smooth()` using method = 'loess' and formula 'y ~ x'
```

![](quantileRegressionsEvaluation_files/figure-html/unnamed-chunk-9-1.png)<!-- -->
