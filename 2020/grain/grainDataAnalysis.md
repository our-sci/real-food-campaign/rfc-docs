---
title: "2020 Survey, Grain Samples Data Analysis"
author: "Real Food Campaign"
date: "12/27/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---








![](grainDataAnalysis_files/figure-html/medianShiftsLib-1.png)<!-- -->

# Some Summary Statistics

## Frequency of Each Farm Practice

<!--html_preserve--><div id="htmlwidget-505d2a15c38bc00d3081" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-505d2a15c38bc00d3081">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26"],["farm_practices.covercrops","Tillage Only","farm_practices.organic","farm_practices.regenerative","farm_practices.transitioning","farm_practices.notill","amendments.none","amendments.synth_fertilizer","amendments.organic_amendment","seed_treatment.none","seed_treatment.biological","seed_treatment.fungicide","seed_treatment.insecticide","seed_treatment.other","seed_treatment.gold_package, wire_worm","seed_treatment.cruser_max_wire_worm","seed_treatment.gold_package","seed_treatment.standard_package","tillage.light_tillage","tillage.heavy_tillage","tillage.none","land_prep.tillage","land_prep.notill","land_prep.none","companion_cropping.used","companion_cropping.none"],[97,17,190,159,186,90,146,147,71,134,54,105,33,3,6,3,12,15,148,56,90,215,90,90,49,65],[28,6,103,84,96,0,71,36,21,65,33,3,0,0,0,0,0,0,79,20,0,99,0,0,49,65],[69,11,87,75,90,90,75,111,50,69,21,102,33,3,6,3,12,15,69,36,90,116,90,90,0,0]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>practice<\/th>\n      <th>amount<\/th>\n      <th>amount_oats<\/th>\n      <th>amount_wheat<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## General: Variation and ratios between maximum and mimimum value





### Censoring The More Evident Outliers





* The following table has separate values for the outliers stripped values, when outliers exists. If the column is empty, that means there's no outliers.
* This table is saved as a file for further processing by any user: `BFA-Extremes-Table.csv`.


<!--html_preserve--><div id="htmlwidget-bbae3a3dbbfff5a9c819" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-bbae3a3dbbfff5a9c819">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102"],["Antioxidants_max","Antioxidants_median","Antioxidants_min","Antioxidants_range","Antioxidants_ratio","Antioxidants_sd","Polyphenols_max","Polyphenols_median","Polyphenols_min","Polyphenols_range","Polyphenols_ratio","Polyphenols_sd","produce_xrf_Al_max","produce_xrf_Al_median","produce_xrf_Al_min","produce_xrf_Al_range","produce_xrf_Al_ratio","produce_xrf_Al_sd","produce_xrf_Ca_max","produce_xrf_Ca_median","produce_xrf_Ca_min","produce_xrf_Ca_range","produce_xrf_Ca_ratio","produce_xrf_Ca_sd","produce_xrf_Cu_max","produce_xrf_Cu_median","produce_xrf_Cu_min","produce_xrf_Cu_range","produce_xrf_Cu_ratio","produce_xrf_Cu_sd","produce_xrf_Fe_max","produce_xrf_Fe_median","produce_xrf_Fe_min","produce_xrf_Fe_range","produce_xrf_Fe_ratio","produce_xrf_Fe_sd","produce_xrf_K_max","produce_xrf_K_median","produce_xrf_K_min","produce_xrf_K_range","produce_xrf_K_ratio","produce_xrf_K_sd","produce_xrf_Mg_max","produce_xrf_Mg_median","produce_xrf_Mg_min","produce_xrf_Mg_range","produce_xrf_Mg_ratio","produce_xrf_Mg_sd","produce_xrf_Mn_max","produce_xrf_Mn_median","produce_xrf_Mn_min","produce_xrf_Mn_range","produce_xrf_Mn_ratio","produce_xrf_Mn_sd","produce_xrf_Mo_max","produce_xrf_Mo_median","produce_xrf_Mo_min","produce_xrf_Mo_range","produce_xrf_Mo_ratio","produce_xrf_Mo_sd","produce_xrf_Na_max","produce_xrf_Na_median","produce_xrf_Na_min","produce_xrf_Na_range","produce_xrf_Na_ratio","produce_xrf_Na_sd","produce_xrf_Ni_max","produce_xrf_Ni_median","produce_xrf_Ni_min","produce_xrf_Ni_range","produce_xrf_Ni_ratio","produce_xrf_Ni_sd","produce_xrf_P_max","produce_xrf_P_median","produce_xrf_P_min","produce_xrf_P_range","produce_xrf_P_ratio","produce_xrf_P_sd","produce_xrf_S_max","produce_xrf_S_median","produce_xrf_S_min","produce_xrf_S_range","produce_xrf_S_ratio","produce_xrf_S_sd","produce_xrf_Si_max","produce_xrf_Si_median","produce_xrf_Si_min","produce_xrf_Si_range","produce_xrf_Si_ratio","produce_xrf_Si_sd","produce_xrf_Zn_max","produce_xrf_Zn_median","produce_xrf_Zn_min","produce_xrf_Zn_range","produce_xrf_Zn_ratio","produce_xrf_Zn_sd","Proteins_max","Proteins_median","Proteins_min","Proteins_range","Proteins_ratio","Proteins_sd"],[7640,2911.885,1166.14,6473.86,6.55152897593771,1574.51508817605,329.93,122.125,26.65,303.28,12.3801125703565,38.4670892741848,5.96,3.61,3.08,2.88,1.93506493506493,0.656925530082164,108.5,81.01,67.79,40.71,1.60053105177755,9.84499393244881,1.33,0.74,0.47,0.86,2.82978723404255,0.235680970197557,7.8,5.64,3.43,4.37,2.27405247813411,0.930829866104078,600.39,263.53,182.2,418.19,3.29522502744237,112.136133681158,90.79,45.645,9.36,81.43,9.69978632478633,17.8149574455094,5.56,4.07,3.39,2.17,1.64011799410029,0.480623048869867,null,null,null,null,null,null,26.36,19.51,16.01,10.35,1.64647095565272,2.82582245639097,null,null,null,null,null,null,387.07,232.44,133.86,253.21,2.89160316748842,54.7067462138885,182.51,152.84,122.15,60.36,1.49414654113795,16.077551758838,354.35,148.9,110.71,243.64,3.20070454340168,42.9868063589532,3.63,1.25,0.06,3.57,60.5,1.10134763020769,16.54,10.365,5.58,10.96,2.96415770609319,2.49189503118745],[6853.93,3255.3,628.38,6225.55,10.9073013144912,1718.44474930054,268.77,106.2,27.86,240.91,9.64716439339555,38.2575451533675,6.28,4.28,2.81,3.47,2.23487544483986,0.784694855478679,148.77,89.73,43.47,105.3,3.42236024844721,26.6144927482661,1.38,1.16,0.32,1.06,4.3125,0.265948980654651,12.34,7.36,3.24,9.1,3.80864197530864,2.21325766383725,843.27,459.09,132.11,711.16,6.38308984936795,189.062600556894,139.09,60.33,3.24,135.85,42.929012345679,31.8749254673776,10.54,5.87,3.08,7.46,3.42207792207792,1.86810718193629,null,null,null,null,null,null,25.94,21.77,14.31,11.63,1.81271837875611,2.98083685422483,null,null,null,null,null,null,451.93,260.65,8.45,443.48,53.4828402366864,119.22209312863,193.35,147.89,81.89,111.46,2.36109415068995,28.9943300277532,238.28,138.71,83.69,154.59,2.84717409487394,30.6939732903225,4.07,2.07,0.02,4.05,203.5,0.956632549623592,14.6,8.485,2.79,11.81,5.23297491039427,2.5669503588255],[null,null,null,null,null,null,193.48,121.54,null,166.83,7.26003752345216,33.9048023670255,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,143.49,105.88,null,115.63,5.15039483129935,32.2106698479182,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>variable<\/th>\n      <th>oats<\/th>\n      <th>wheat<\/th>\n      <th>oats_w/o_outliers<\/th>\n      <th>wheat_w/o_outliers<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


![](grainDataAnalysis_files/figure-html/preliminaryhistograms-1.png)<!-- -->

# Farm Practices: Median Shifts Comparisons







# Synthesis Median Shift Tables.

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is intersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting magnitudes are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.




## Color Scale

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>



# Percentual Shift Over Crop's Median Values for Farm Practices


* The reference category used to define the neutral median is defined as *Farms that are not organic, transitioning, regenerative, no till, or cover crops and that indicated they are using tillage*. It us called *Tillage Only* (and, inside the code and functions, `farm_practices.none`).
* There's plenty of columns prefixed by *farm_practices* on the dataset, but they are not mutually related in a sound logical way, that's why calculations can't be easily performed over the whole set unless a good reference is defined. 




## Visualizing some clusterization

![](grainDataAnalysis_files/figure-html/practicesClusters-1.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-2.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-3.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-4.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-5.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-6.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-7.png)<!-- -->![](grainDataAnalysis_files/figure-html/practicesClusters-8.png)<!-- -->

# Percentual Shift Over Crop's Median Values for each Soil Amendment


* The reference category is farms that don't mention any soil amendment on the submitted information ( `FALSE` value on every dataset column prefixed by *amendments* ).


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">33.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">13.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">2.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">5.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-7.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">0.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-29.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-0.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-0.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">-21.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">22.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">-24.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-40.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-7.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-22.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-5.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-15.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-31.31</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Synth Fertilizer </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">15.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">10.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">7.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 111">43.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 106">45.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 111">-5.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-9.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">8.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">39.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">33.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">17.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">49.82</span> </td>
  </tr>
</tbody>
</table>


# Percentual Shift Over Crop's Median Values for each Seed Treatment


* The reference category is farms that don't mention any seed treatment on the submitted information ( `FALSE` value on every dataset column prefixed by *seed_treatment* ).


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">16.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">11.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-1.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-58.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">9.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-20.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Fungicide </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 102">65.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 97">52.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 102">1.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">30.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">20.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">108.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">58.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">43.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">69.93</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Gold Package </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">113.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">84.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">19.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Insecticide </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-33.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">18.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-18.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-19.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">7.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">90.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">41.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">29.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">25.2</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Standard Package </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">35.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">10.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">46.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">22.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">108.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">44.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">48.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">85.54</span> </td>
  </tr>
</tbody>
</table>


# Percentual Shift Over Crop's Median Values for each Tillage Practice


* The reference category is farms that inform *notill* as a practice.
* *Light tillage* is confirmed tillage, with an informed depth of less than 6 inches.
* *Heavy tillage* is tillage with a depth superior or equal to 6 inches.


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-72.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-35.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-34.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-40.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-56.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-38.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-41.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-43.06</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 79">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 79">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 79">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-45.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-42.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-10.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-48.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-21.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-57.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-38.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-29.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-63.35</span> </td>
  </tr>
</tbody>
</table>

# Percentual Shift Over Crop's Median Values for Companion Cropping (Just in Oats)

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Used </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-0.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-4.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-8.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-3.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-0.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-0.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-23.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-9.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-41.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

# Soil Suborder Influence {.tabset}


## Antioxidants

### Analysis of Variance
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 7631.000 </td>
   <td style="text-align:right;"> 846.753 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 4599.726 </td>
   <td style="text-align:right;"> 399.163 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 3486.672 </td>
   <td style="text-align:right;"> 205.368 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 3384.344 </td>
   <td style="text-align:right;"> 199.582 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 2278.243 </td>
   <td style="text-align:right;"> 691.371 </td>
   <td style="text-align:right;"> 0.001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 1586.971 </td>
   <td style="text-align:right;"> 261.314 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 4647.778 </td>
   <td style="text-align:right;"> 107.862 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 3102.041 </td>
   <td style="text-align:right;"> 142.688 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 2506.171 </td>
   <td style="text-align:right;"> 329.524 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 1383.947 </td>
   <td style="text-align:right;"> 329.524 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 1345.735 </td>
   <td style="text-align:right;"> 403.583 </td>
   <td style="text-align:right;"> 0.001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 1198.487 </td>
   <td style="text-align:right;"> 255.249 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 935.859 </td>
   <td style="text-align:right;"> 201.792 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

## Polyphenols
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 179.865 </td>
   <td style="text-align:right;"> 18.987 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 154.150 </td>
   <td style="text-align:right;"> 8.951 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 127.379 </td>
   <td style="text-align:right;"> 4.475 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 125.805 </td>
   <td style="text-align:right;"> 4.605 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 102.910 </td>
   <td style="text-align:right;"> 15.503 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 91.780 </td>
   <td style="text-align:right;"> 5.860 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 126.522 </td>
   <td style="text-align:right;"> 2.246 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 106.424 </td>
   <td style="text-align:right;"> 6.654 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 83.138 </td>
   <td style="text-align:right;"> 6.654 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 78.308 </td>
   <td style="text-align:right;"> 8.149 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 77.847 </td>
   <td style="text-align:right;"> 5.154 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 76.403 </td>
   <td style="text-align:right;"> 4.075 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 75.450 </td>
   <td style="text-align:right;"> 2.881 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>


## Proteins
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 12.439 </td>
   <td style="text-align:right;"> 0.468 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 10.877 </td>
   <td style="text-align:right;"> 1.238 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 10.647 </td>
   <td style="text-align:right;"> 0.357 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 8.891 </td>
   <td style="text-align:right;"> 0.368 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 8.805 </td>
   <td style="text-align:right;"> 1.517 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 8.734 </td>
   <td style="text-align:right;"> 0.715 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 9.858 </td>
   <td style="text-align:right;"> 0.260 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 9.297 </td>
   <td style="text-align:right;"> 0.196 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 8.497 </td>
   <td style="text-align:right;"> 0.600 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 5.983 </td>
   <td style="text-align:right;"> 0.367 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 4.643 </td>
   <td style="text-align:right;"> 0.600 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 4.527 </td>
   <td style="text-align:right;"> 0.465 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 3.490 </td>
   <td style="text-align:right;"> 0.734 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

 
# Climate Region Influence { .tabset }

## Antioxidants

### Analysis of Variance
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Climate Region in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 4508.300 </td>
   <td style="text-align:right;"> 802.974 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 3770.527 </td>
   <td style="text-align:right;"> 161.676 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 3646.389 </td>
   <td style="text-align:right;"> 439.807 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 2278.243 </td>
   <td style="text-align:right;"> 802.974 </td>
   <td style="text-align:right;"> 0.005 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 1586.971 </td>
   <td style="text-align:right;"> 303.496 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 4392.334 </td>
   <td style="text-align:right;"> 114.394 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 3934.676 </td>
   <td style="text-align:right;"> 383.216 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 2906.534 </td>
   <td style="text-align:right;"> 177.394 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 2506.171 </td>
   <td style="text-align:right;"> 383.216 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 1323.144 </td>
   <td style="text-align:right;"> 270.974 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 1116.050 </td>
   <td style="text-align:right;"> 663.749 </td>
   <td style="text-align:right;"> 0.094 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 1024.336 </td>
   <td style="text-align:right;"> 200.128 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>


## Polyphenols
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 135.697 </td>
   <td style="text-align:right;"> 16.635 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 133.212 </td>
   <td style="text-align:right;"> 3.372 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 124.963 </td>
   <td style="text-align:right;"> 9.111 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 102.910 </td>
   <td style="text-align:right;"> 16.635 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 91.780 </td>
   <td style="text-align:right;"> 6.287 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 116.929 </td>
   <td style="text-align:right;"> 8.620 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 116.630 </td>
   <td style="text-align:right;"> 2.639 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 106.424 </td>
   <td style="text-align:right;"> 8.620 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 81.030 </td>
   <td style="text-align:right;"> 6.095 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 77.050 </td>
   <td style="text-align:right;"> 4.502 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 72.750 </td>
   <td style="text-align:right;"> 14.931 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 69.934 </td>
   <td style="text-align:right;"> 3.990 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>


## Proteins
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 12.439 </td>
   <td style="text-align:right;"> 0.502 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 11.153 </td>
   <td style="text-align:right;"> 1.328 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 10.877 </td>
   <td style="text-align:right;"> 1.328 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 9.647 </td>
   <td style="text-align:right;"> 0.728 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 9.502 </td>
   <td style="text-align:right;"> 0.267 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 9.976 </td>
   <td style="text-align:right;"> 0.281 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 9.167 </td>
   <td style="text-align:right;"> 0.181 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 8.611 </td>
   <td style="text-align:right;"> 0.608 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 8.497 </td>
   <td style="text-align:right;"> 0.608 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 6.370 </td>
   <td style="text-align:right;"> 1.052 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 5.275 </td>
   <td style="text-align:right;"> 0.317 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 4.502 </td>
   <td style="text-align:right;"> 0.430 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

# Clusterization for Climate Regions and Soil Suborders { .tabset }

### by Climate Region

![](grainDataAnalysis_files/figure-html/climateClusters-1.png)<!-- -->![](grainDataAnalysis_files/figure-html/climateClusters-2.png)<!-- -->

### by Soil Suborder

![](grainDataAnalysis_files/figure-html/suborderClusters-1.png)<!-- -->![](grainDataAnalysis_files/figure-html/suborderClusters-2.png)<!-- -->




# Random Forest on Antioxidants, Polyphenols and Proteins


  These regressions are performed using a *Random Forest* algorithm. The only tuned parameter besides the selection of initial variables is `mtry`.
  For the classifications, we are using *Accuracy* as a precission metric, while for regressions the custom random forest $R^2$ is used, which is a special metric comparable to the homonym in the context of linear models.


### Compared Datasets


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-vis-who </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole </td>
   <td style="text-align:left;"> metadata     , farmPractices </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed       , nir_whole       , nir_processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices </td>
   <td style="text-align:left;"> color    , whole    , nir_whole </td>
   <td style="text-align:left;"> color, whole </td>
  </tr>
</tbody>
</table>


  


## Random Forest Regression




<!--html_preserve--><div id="htmlwidget-61a19f7ed72bca7c9b9b" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-61a19f7ed72bca7c9b9b">{"x":{"filter":"none","extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"],["Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins"],["c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.664694108668798,0.607064444787639,0.587629210867517,0.264807479372174,0.428473261995967,0.753494059525675,0.633294190642412,0.539935882613446,0.683672878087116,0.746350244127651,0.709730122386867,0.615808370765423,0.52150631526598,0.598249263464066,0.630850967351468,0.545581938346194,0.452685176306501,0.656423299457839,0.540003378856756,0.619652065298864,0.587818876939709,0.372103757490902,0.636428391545095,0.259563615084347,0.318958996516456,0.784309123256852,0.475114291635929,0.734287665029493,0.565476415480264,0.664382288764481,0.775172714825585,0.642023467482318,0.521271650235398,null,null,null,null,null,0.613318284722616,0.789567928979804,0.46760048239894,0.57674497833519,0.64405936010945,0.499594272722676,0.462661363090697,0.596902805103147,0.625384446301487,0.438965575500956,0.574420179102534,0.509182289065813,0.544490559093701,0.527734335113185,0.320051647944878,0.614700740688441,0.631595161888338,0.555782513169787,0.665991918917038,0.412078372648525,0.574076352327284,0.703147020262467,0.35918949278223,0.4151136628259,0.429256246728084,0.185578866269475,0.240564659791205,0.430407016604876,0.55379994792293,0.521386596457097,0.583906123560467,0.497461114321019,0.503726596774162,0.426969407218491,0.365767531913719,0.366045596761218,0.391920795372622,0.435264342674756,0.519514146860739,0.453842467603578,0.510448171745976,0.416137755105626],[111,111,111,363,366,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111,52,52,52,299,302,52,52,52,52,52,52,52,52,null,null,null,null,null,52,52,110,110,110,367,370,110,109,110,110,110,110,109,110,101,100,101,101,101,110,110,111,111,111,370,373,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111],[0.846846846846847,0.846846846846847,0.855855855855856,0.848484848484849,0.814207650273224,0.828828828828829,0.809090909090909,0.747747747747748,0.882882882882883,0.738738738738739,0.720720720720721,0.845454545454545,0.828828828828829,0.833333333333333,0.851485148514851,0.823529411764706,0.833333333333333,0.813725490196078,0.882882882882883,0.864864864864865,0.769230769230769,0.769230769230769,0.711538461538462,0.712374581939799,0.764900662251656,0.769230769230769,0.788461538461538,0.826923076923077,0.788461538461538,0.884615384615385,0.807692307692308,0.75,0.711538461538462,null,null,null,null,null,0.826923076923077,0.788461538461538,0.818181818181818,0.772727272727273,0.809090909090909,0.825613079019074,0.772972972972973,0.763636363636364,0.834862385321101,0.827272727272727,0.827272727272727,0.854545454545454,0.772727272727273,0.825688073394495,0.809090909090909,0.762376237623762,0.83,0.811881188118812,0.871287128712871,0.811881188118812,0.818181818181818,0.845454545454545,0.756756756756757,0.747747747747748,0.846846846846847,0.751351351351351,0.723860589812332,0.720720720720721,0.781818181818182,0.774774774774775,0.774774774774775,0.792792792792793,0.765765765765766,0.781818181818182,0.738738738738739,0.696078431372549,0.762376237623762,0.705882352941177,0.833333333333333,0.745098039215686,0.783783783783784,0.792792792792793],[0.542954688212431,0.316530464532709,0.367226036023684,0.364587955635796,0.261706856207363,0.484731544159145,0.319206413988592,0.192982428171044,0.474264811384224,0.18807819479828,0.33227687667197,0.319935464739962,0.289668937942205,0.450073016672672,0.324253013930561,0.31686908333785,0.366888852043988,0.271685872070963,0.36189645661885,0.619192544009285,0.528113881668862,0.367234598252825,0.184551966786054,0.135497286673313,0.194236399727401,0.346872003642635,0.401326754027517,0.474248933517874,0.243731573681802,0.542750604363169,0.559310747741206,0.395908363389447,0.217526303924192,null,null,null,null,null,0.381704243733236,0.314247033441981,0.555444075461033,0.361972726630396,0.344630113756801,0.364086021505376,0.232762096774193,0.38398219458772,0.379714548152335,0.319260934077581,0.333286229068042,0.551663958171413,0.386702465908288,0.383473468522575,0.440139899667915,0.330601285946443,0.395583975128948,0.413622553522221,0.479968911184908,0.307567300219035,0.329788737370169,0.543418356532184,0.555449330783939,0.397705544933079,0.551625239005736,0.430180533751962,0.373037676609105,0.363288718929254,0.41655437921078,0.391395793499044,0.413001912045889,0.44321223709369,0.363288718929254,0.432290664100096,0.40057361376673,0.371653919694073,0.409432146294514,0.380975143403442,0.441300191204589,0.369933078393882,0.427342256214149,0.434990439770555],[0.611523895326922,0.615009623088878,0.798621242980543,0.47233030173222,0.617354758802696,0.630696324462758,0.76979591299627,0.617256650009171,0.807889211594146,0.788586503392446,0.686466570209358,0.8189059834876,0.704312628629324,0.697401265309698,0.725752237489599,0.660385138990888,0.714144490761945,0.651938903370292,0.798811669882891,0.706867412876095,0.812514044818144,0.796153189110311,0.864332258992651,0.652791549456052,0.814845780541134,0.752212223936074,0.898092701128969,0.867239091952186,0.818337733404491,0.917250171897863,0.807268072327783,0.772193397320353,0.902197393103352,0.951130692006233,0.845508666568491,0.762857751308809,0.817481344024987,0.944485421984189,0.792214863693998,0.9185351257177,0.622509086120791,0.614520244155414,0.561185686910857,0.538853685723063,0.490708814481367,0.688968006434908,0.686997101804441,0.643487608273267,0.716815783491312,0.680348049815266,0.610085590267676,0.667987101577001,0.574867646410617,0.695615871480423,0.590649200963526,0.571038462476399,0.607387503661287,0.52536918659589,0.64861357423681,0.596001276643394,0.634450295791533,0.58985014698139,0.501831708089707,0.463270137701194,0.538079843499079,0.5093107615956,0.600859361534426,0.479758483292773,0.626833784883333,0.614856808209088,0.515093000856841,0.669336132507819,0.646843613698134,0.476678134558795,0.550985596682534,0.43579884548823,0.534052752162387,0.633195514046245,0.558040810355556,0.641467006207193],[215,210,215,225,230,194,187,189,194,194,194,187,189,141,139,141,141,141,194,194,132,130,132,145,147,111,108,109,111,111,111,108,109,87,86,87,87,87,111,111,210,206,210,221,225,190,184,186,190,190,190,184,186,140,138,140,140,140,190,190,215,210,215,225,230,194,187,189,194,194,194,187,189,141,139,141,141,141,194,194],[0.73953488372093,0.771428571428571,0.767441860465116,0.817777777777778,0.78695652173913,0.819587628865979,0.828877005347594,0.793650793650794,0.819587628865979,0.824742268041237,0.84020618556701,0.807486631016043,0.82010582010582,0.879432624113475,0.820143884892086,0.822695035460993,0.773049645390071,0.702127659574468,0.768041237113402,0.77319587628866,0.863636363636364,0.784615384615385,0.886363636363636,0.820689655172414,0.850340136054422,0.747747747747748,0.87037037037037,0.880733944954128,0.774774774774775,0.891891891891892,0.882882882882883,0.833333333333333,0.844036697247706,0.747126436781609,0.837209302325581,0.816091954022989,0.758620689655172,0.747126436781609,0.756756756756757,0.765765765765766,0.838095238095238,0.830097087378641,0.89047619047619,0.823529411764706,0.795555555555556,0.878947368421053,0.815217391304348,0.833333333333333,0.768421052631579,0.773684210526316,0.878947368421053,0.793478260869565,0.790322580645161,0.742857142857143,0.804347826086957,0.807142857142857,0.757142857142857,0.792857142857143,0.868421052631579,0.789473684210526,0.767441860465116,0.776190476190476,0.762790697674419,0.755555555555556,0.782608695652174,0.731958762886598,0.759358288770054,0.761904761904762,0.731958762886598,0.778350515463918,0.824742268041237,0.748663101604278,0.767195767195767,0.815602836879433,0.776978417266187,0.801418439716312,0.737588652482269,0.815602836879433,0.731958762886598,0.809278350515464],[0.444586097903241,0.256735171496534,0.220667515906211,0.298628198241979,0.226126207441518,0.463777612175923,0.271093611629586,0.264645124592978,0.213868004759827,0.31064069244963,0.52656761110649,0.252804874725814,0.267596459079824,0.560000817449829,0.272912948405292,0.30725725146119,0.190627937710323,0.174862736549544,0.19439939432725,0.207012003116634,0.603670789347069,0.202026434211644,0.276591288597062,0.214841649449523,0.241802281153596,0.241162676239831,0.279553284502475,0.283913320401841,0.174604093685765,0.786537737760922,0.633965432384702,0.21578269303147,0.219767812165253,0.232229365345358,0.230294628439109,0.224597056762438,0.135547362968831,0.15039151762591,0.144755507947077,0.158824295764454,0.73190659773942,0.228493822833611,0.669412073950758,0.290686059756418,0.312871287128713,0.46650311048804,0.229694208358889,0.238605099447998,0.21295890651012,0.204304302111627,0.666341014632437,0.218421974940857,0.230027162008236,0.193428546394462,0.281266976255148,0.25287610619469,0.225400858669938,0.245360553754491,0.3433934986419,0.202330675545431,0.41030534351145,0.288835877862595,0.269274809160305,0.272996183206107,0.278148854961832,0.296718322698268,0.282588878760255,0.269006381039198,0.252415679124886,0.276959890610757,0.404922515952598,0.272470373746582,0.27260711030082,0.410344827586207,0.316847290640394,0.317980295566503,0.300492610837439,0.387487684729064,0.252962625341841,0.322561531449407]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_oats<\/th>\n      <th>N_oats<\/th>\n      <th>covers_oats<\/th>\n      <th>width_oats<\/th>\n      <th>cvRsquared_wheat<\/th>\n      <th>N_wheat<\/th>\n      <th>covers_wheat<\/th>\n      <th>width_wheat<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Bfrtip","lengthMenu":[10,20,40,80],"buttons":["colvis"],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->


### Plots Comparing Actual and Predicted Values, for All Crops, Variables and Models { .tabset }


#### Example: One of the highly accurate regressions

![](grainDataAnalysis_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


#### Antioxidants { .tabset }


##### farmerWithSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-consumerModels-fitPlot.png)

#### Polyphenols { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-consumerModels-fitPlot.png)

#### Proteins { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-consumerModels-fitPlot.png)


#### Proteins { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-consumerModels-fitPlot.png)

#### BQI

![](grainDataAnalysis_files/figure-html/allBQIRegs-1.png)<!-- -->


### Importance Metrics Plots { .tabset }

* This information is stored as `./models/allRFImportancesGrouped.csv` in the gitlab project folder for this analysis.

#### Wheat { .tabset }

##### Farmers with soil data

![](grainDataAnalysis_files/figure-html/importanceRegWheatFS-1.png)<!-- -->

##### Farmers without soil data

![](grainDataAnalysis_files/figure-html/importanceRegWheatF-1.png)<!-- -->

##### Farmer-Consumer models 

![](grainDataAnalysis_files/figure-html/importanceRegWheatFC-1.png)<!-- -->

##### Consumer models 

![](grainDataAnalysis_files/figure-html/importanceRegWheatC-1.png)<!-- -->

#### Oats { .tabset }


##### Farmers with soil data




##### Farmers without soil data


![](grainDataAnalysis_files/figure-html/importanceRegOatsF-1.png)<!-- -->

##### Farmer-Consumer models 


![](grainDataAnalysis_files/figure-html/importanceRegOatsFC-1.png)<!-- -->

##### Consumer models 


![](grainDataAnalysis_files/figure-html/importanceRegOatsC-1.png)<!-- -->


#### Relations between climate region / soil suborder

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Population by climate region / soil suborder combination</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> soilSuborder </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> East North Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Northeast </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Northwest </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> South </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Southeast </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> West North Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> NA </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 60 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 84 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

## Random Forest Classification of The Percentile on the Explained Variable


Two targets are generated for each explained variable (Antioxidants, Polyphenols, Proteins). The objective is predicting the quantile were the sample contents belong. Instead of using each quantile, the central ones are meged into a center category.

* Quartiles is three categories, *lower* for the first quartile, *center* for the second and third and *higher* for the fourth.
* Quintiles is also three categories, with *center* composed of the second, third and fourth quintiles.





<!--html_preserve--><div id="htmlwidget-5ff0e468ac0050a480f3" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-5ff0e468ac0050a480f3">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"],["antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile"],["c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.780733885997044,0.616514041514042,0.648611111111111,0.606599446858914,0.581428871994676,0.743809523809524,0.593751803751804,0.710468103325246,0.69751875109018,0.748611111111111,0.771541950113379,0.587060657596372,0.666727716727717,0.781308506308506,0.730385487528345,0.693096784168213,0.758080808080808,0.672962962962963,0.725396825396825,0.710731728588871,0.621212121212121,0.538690476190476,0.546536796536797,0.523426209912886,0.519395551503195,0.551851851851852,0.603333333333333,0.576704545454545,0.681385281385281,0.56547619047619,0.505886243386243,0.614603174603175,0.390873015873016,0,0,0,0,0,0.678787878787879,0.594444444444444,0.667037327751614,0.643588950731808,0.822764490411549,0.61123099696413,0.629735240360341,0.747435897435897,0.678333333333333,0.644337778971238,0.845709845709846,0.774859943977591,0.78993783993784,0.736549707602339,0.642126623376623,0.708488138845282,0.693295196866625,0.738095238095238,0.775661375661376,0.79537037037037,0.817250233426704,0.782635882635883,0.603592333592334,0.529040404040404,0.553004535147392,0.495544186894058,0.550065739069132,0.68338011195154,0.574603174603175,0.516304727019013,0.594928148774303,0.609217171717172,0.563133163133163,0.591150793650794,0.595473574045003,0.597936507936508,0.576556776556777,0.696581196581197,0.633730158730159,0.576887464387464,0.63216824111561,0.522311022311022],[111,111,111,363,366,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111,52,52,52,299,302,52,52,52,52,52,52,52,52,0,0,0,0,0,52,52,110,110,110,367,370,110,109,110,110,110,110,109,110,101,100,101,101,101,110,110,111,111,111,370,373,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111],[0.710882138295931,0.750123456790123,0.833862433862434,0.654691358024691,0.751613756613757,0.655220977279801,0.787087332956898,0.649107744107744,0.825185185185185,0.806481481481481,0.690108932461874,0.774482379219221,0.741719576719577,0.734708994708995,0.78973544973545,0.634550264550265,0.777777777777778,0.778597883597884,0.832461873638344,0.777018633540373,0.656462585034014,0.729861111111111,0.804074074074074,0.722222222222222,0.66672335600907,0.635555555555556,0.809494949494949,0.823809523809524,0.812698412698413,0.724074074074074,0.701111111111111,0.850740740740741,0.806292517006803,0.711538461538462,0.868484848484849,0.682905982905983,0.777777777777778,0.75,0.785185185185185,0.757936507936508,0.67546342111048,0.608844058715298,0.654956187603246,0.618173200160778,0.591112455083043,0.729126984126984,0.699408671514583,0.660438808373591,0.707282254782255,0.651733865263277,0.704455861598719,0.748695054945055,0.743062678062678,0.661262626262626,0.668153175800235,0.491616161616162,0.673115079365079,0.672777777777778,0.715315425315425,0.653469294150409,0.66237135879993,0.705362415362415,0.679873475960432,0.686429249762583,0.576290071584189,0.713386243386243,0.738739974622328,0.652323232323232,0.669656084656085,0.67858753163101,0.672022792022792,0.729187109187109,0.646335403726708,0.700396825396825,0.703677248677249,0.655343915343915,0.791216931216931,0.753514739229025,0.644872325741891,0.628730158730159],[215,210,215,225,230,215,208,210,215,215,215,208,210,159,157,159,159,159,215,215,132,130,132,145,147,132,129,130,132,132,132,129,130,105,104,105,105,105,132,132,210,206,210,221,225,210,204,206,210,210,210,204,206,157,155,157,157,157,210,210,215,210,215,225,230,215,208,210,215,215,215,208,210,159,157,159,159,159,215,215]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>Accuracy_oats<\/th>\n      <th>N_oats<\/th>\n      <th>Accuracy_wheat<\/th>\n      <th>N_wheat<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[5]; $(this.api().cell(row, 5).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->


### Confusion Matrix for a Succesful Model





An immediately obvious issue while looking at the prediction models is class waning. 

We've already observed that almost all farm practices have noticeably positive impacts on our studied variables. When filtering the data to define our different datasets, we often end up with subselections which lack any element in one of the classes, typically asking for deep information on practices leaves us with 0 entries in the `lower` category.

![](grainDataAnalysis_files/figure-html/farmer2 confusion matrix-1.png)<!-- -->![](grainDataAnalysis_files/figure-html/farmer2 confusion matrix-2.png)<!-- -->

The only way to work around this tendency is to work over bigger populations. We could define differently the categories according to the model, but it would destroy all comparability and the regression would be meaningless.

As a consequence of this, models that are not top performers but retain bigger sections of the total population increase their relevance. One of the more interesting results is **consumer 2** on Antioxidants for Oats, which has a 75% Accuracy metric. Looking at the matrix illustrates very well that this number is better than just "15% better than assigning everything to the center tier".



# Agregation of Farm Practices Analysis Over Crops

  In this section, we'll try to find meaningful results for farm practices **across** crops.
  Our strategy to achieve a meaningful comparison is replacing on each point the actual variable value by the crop's percentile reached. The goal of this study is checking if a practices is able to push each crop far from its *median*.
  This means that if a tomatto that is bigger than 60% of all other tomato observations has a value of 0.1 for a certain variable and for Kale you need to reach 10 to be above 60% of all subjects, we are giving the same score to a a 0.1 tomato sample and a kale sample with a value of 10. We are measuring the relative scarcity of a value among it's crop peers, rather than the absolute value.



## Agregation by Percentile Transformation


Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.




<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4006339 </td>
   <td style="text-align:right;"> 0.3455571 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.29,-0.02] </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.0477400 </td>
   <td style="text-align:right;"> -0.1727240 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4708299 </td>
   <td style="text-align:right;"> 0.4213793 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.22,-0.03] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0477400 </td>
   <td style="text-align:right;"> -0.1454698 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6072320 </td>
   <td style="text-align:right;"> 0.6579690 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.05,0.46] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.46 </td>
   <td style="text-align:right;"> 0.0966543 </td>
   <td style="text-align:right;"> 0.2559529 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5766654 </td>
   <td style="text-align:right;"> 0.5944828 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.12,0.44] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> 0.0966543 </td>
   <td style="text-align:right;"> 0.3027423 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4618019 </td>
   <td style="text-align:right;"> 0.4866008 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.18,-0.02] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.1016715 </td>
   <td style="text-align:right;"> -0.1187757 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4998450 </td>
   <td style="text-align:right;"> 0.4788434 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.22,0.01] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.1238432 </td>
   <td style="text-align:right;"> -0.1049391 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4040465 </td>
   <td style="text-align:right;"> 0.3565891 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.06,0.19] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.1276700 </td>
   <td style="text-align:right;"> 0.0598357 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4771308 </td>
   <td style="text-align:right;"> 0.4866008 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.28,-0.12] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.2414067 </td>
   <td style="text-align:right;"> -0.2120086 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6134085 </td>
   <td style="text-align:right;"> 0.6600846 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.31,-0.15] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.2414067 </td>
   <td style="text-align:right;"> -0.2221187 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4670330 </td>
   <td style="text-align:right;"> 0.4055007 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.33,-0.09] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.2414067 </td>
   <td style="text-align:right;"> -0.2148701 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5237139 </td>
   <td style="text-align:right;"> 0.5197461 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.03,0.32] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.2722020 </td>
   <td style="text-align:right;"> 0.1777498 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6236453 </td>
   <td style="text-align:right;"> 0.6903448 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.19,0.45] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 0.2722020 </td>
   <td style="text-align:right;"> 0.3076036 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6613943 </td>
   <td style="text-align:right;"> 0.7482370 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.14,0.11] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.2941671 </td>
   <td style="text-align:right;"> -0.0190124 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5186063 </td>
   <td style="text-align:right;"> 0.5503876 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.19,0.08] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.3397849 </td>
   <td style="text-align:right;"> -0.0555268 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5186063 </td>
   <td style="text-align:right;"> 0.5503876 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.19,0.08] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.3397849 </td>
   <td style="text-align:right;"> -0.0555268 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4878253 </td>
   <td style="text-align:right;"> 0.4379408 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.31,-0.08] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.3801673 </td>
   <td style="text-align:right;"> -0.1961017 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4629708 </td>
   <td style="text-align:right;"> 0.4144828 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.04,0.2] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.3801673 </td>
   <td style="text-align:right;"> 0.1283028 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4884653 </td>
   <td style="text-align:right;"> 0.4931034 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.16,0.04] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.4004014 </td>
   <td style="text-align:right;"> -0.0463625 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5096249 </td>
   <td style="text-align:right;"> 0.4655172 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.02,0.3] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.5110712 </td>
   <td style="text-align:right;"> 0.1353626 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.1962941 </td>
   <td style="text-align:right;"> 0.1448276 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.4,-0.05] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.5110712 </td>
   <td style="text-align:right;"> -0.0805883 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6408345 </td>
   <td style="text-align:right;"> 0.6812412 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.24,0.43] </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.5269428 </td>
   <td style="text-align:right;"> 0.3301033 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.1888513 </td>
   <td style="text-align:right;"> 0.1346968 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.4,-0.06] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.5594234 </td>
   <td style="text-align:right;"> -0.1075313 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6512012 </td>
   <td style="text-align:right;"> 0.7579310 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.24,-0.12] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.5760202 </td>
   <td style="text-align:right;"> -0.1699359 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4945737 </td>
   <td style="text-align:right;"> 0.4565517 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.29,-0.06] </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.6622904 </td>
   <td style="text-align:right;"> -0.1716654 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4357244 </td>
   <td style="text-align:right;"> 0.4083216 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.2,-0.08] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.6801472 </td>
   <td style="text-align:right;"> -0.1368681 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6139901 </td>
   <td style="text-align:right;"> 0.6544828 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.08,0.17] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.6801472 </td>
   <td style="text-align:right;"> 0.0434813 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4778670 </td>
   <td style="text-align:right;"> 0.4217207 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.02,0.11] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.7719630 </td>
   <td style="text-align:right;"> 0.0345708 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4342706 </td>
   <td style="text-align:right;"> 0.4068966 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.1,0.04] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.7719630 </td>
   <td style="text-align:right;"> -0.0335024 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5512717 </td>
   <td style="text-align:right;"> 0.5571227 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.08,0.37] </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.8287261 </td>
   <td style="text-align:right;"> 0.2364750 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5185980 </td>
   <td style="text-align:right;"> 0.4924138 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.21,0.03] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.8863982 </td>
   <td style="text-align:right;"> -0.1197368 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4532076 </td>
   <td style="text-align:right;"> 0.4418605 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.1,0.11] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.9018693 </td>
   <td style="text-align:right;"> -0.0103324 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.3981053 </td>
   <td style="text-align:right;"> 0.3455172 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.2,-0.05] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.9252033 </td>
   <td style="text-align:right;"> -0.1145199 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4642552 </td>
   <td style="text-align:right;"> 0.4666667 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.17,0.66] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.66 </td>
   <td style="text-align:right;"> 0.9261236 </td>
   <td style="text-align:right;"> 0.4333747 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 2.5294017 </td>
   <td style="text-align:right;"> 2.1400000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.62,1.68] </td>
   <td style="text-align:right;"> -0.62 </td>
   <td style="text-align:right;"> 1.68 </td>
   <td style="text-align:right;"> 0.9704053 </td>
   <td style="text-align:right;"> 0.2400110 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.3514225 </td>
   <td style="text-align:right;"> 0.3266667 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.33,0.06] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.9704053 </td>
   <td style="text-align:right;"> -0.1067170 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.3927778 </td>
   <td style="text-align:right;"> 0.3000000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.28,0.21] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.9704058 </td>
   <td style="text-align:right;"> -0.1465937 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4443590 </td>
   <td style="text-align:right;"> 0.4266667 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.23,0.33] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.9704058 </td>
   <td style="text-align:right;"> 0.0466818 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4057204 </td>
   <td style="text-align:right;"> 0.3133333 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.08,0.47] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.47 </td>
   <td style="text-align:right;"> 0.9704058 </td>
   <td style="text-align:right;"> 0.0666970 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4767460 </td>
   <td style="text-align:right;"> 0.4600000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.08,0.53] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.53 </td>
   <td style="text-align:right;"> 0.9704058 </td>
   <td style="text-align:right;"> 0.2267121 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4815873 </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.31,0.31] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.9704058 </td>
   <td style="text-align:right;"> 0.0333666 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.8572541 </td>
   <td style="text-align:right;"> 0.7948718 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.17,0.23] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.9714441 </td>
   <td style="text-align:right;"> 0.0527940 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.3294164 </td>
   <td style="text-align:right;"> 0.3004630 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.13,0.16] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.9852151 </td>
   <td style="text-align:right;"> 0.0110287 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4398565 </td>
   <td style="text-align:right;"> 0.4489958 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.21,0.13] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0108623 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4083639 </td>
   <td style="text-align:right;"> 0.3900000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.13,0.36] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1091079 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4273504 </td>
   <td style="text-align:right;"> 0.3984127 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.01,0.5] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2192440 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4750427 </td>
   <td style="text-align:right;"> 0.5200000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.09,0.74] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.74 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4779611 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.3987118 </td>
   <td style="text-align:right;"> 0.3933333 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.3,0.13] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0878146 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4583761 </td>
   <td style="text-align:right;"> 0.4366667 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.15,0.45] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2665004 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5402577 </td>
   <td style="text-align:right;"> 0.5434483 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.04,0.31] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1486489 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4204148 </td>
   <td style="text-align:right;"> 0.3963329 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.53,-0.4] </td>
   <td style="text-align:right;"> -0.53 </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> 0.0504823 </td>
   <td style="text-align:right;"> -0.4705327 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4283811 </td>
   <td style="text-align:right;"> 0.4520451 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.01,0.25] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0871171 </td>
   <td style="text-align:right;"> 0.1617473 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4333333 </td>
   <td style="text-align:right;"> 0.4406897 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.12,0.29] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0871171 </td>
   <td style="text-align:right;"> 0.2025898 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5217573 </td>
   <td style="text-align:right;"> 0.5380818 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.09,0.03] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.1032872 </td>
   <td style="text-align:right;"> -0.0336653 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6415005 </td>
   <td style="text-align:right;"> 0.6987179 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.03,0.18] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.1584273 </td>
   <td style="text-align:right;"> 0.0811632 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6415005 </td>
   <td style="text-align:right;"> 0.6987179 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.03,0.18] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.1584273 </td>
   <td style="text-align:right;"> 0.0811632 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6148914 </td>
   <td style="text-align:right;"> 0.6117241 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.12,0.01] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.1663361 </td>
   <td style="text-align:right;"> -0.0761749 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5295931 </td>
   <td style="text-align:right;"> 0.5112835 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.14,0.03] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.2070733 </td>
   <td style="text-align:right;"> -0.0845849 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 1.5304843 </td>
   <td style="text-align:right;"> 1.6175214 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.54,0.96] </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.96 </td>
   <td style="text-align:right;"> 0.2345075 </td>
   <td style="text-align:right;"> 0.7008242 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5816858 </td>
   <td style="text-align:right;"> 0.5572414 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.22,0.31] </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.2380144 </td>
   <td style="text-align:right;"> 0.2509258 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.3885232 </td>
   <td style="text-align:right;"> 0.3801128 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.36,-0.22] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.3713352 </td>
   <td style="text-align:right;"> -0.2937282 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4196169 </td>
   <td style="text-align:right;"> 0.4213793 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.52,-0.4] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> 0.4166240 </td>
   <td style="text-align:right;"> -0.4730815 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5409451 </td>
   <td style="text-align:right;"> 0.5689655 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.1,0.25] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.4650180 </td>
   <td style="text-align:right;"> 0.1553669 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5738912 </td>
   <td style="text-align:right;"> 0.5740480 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.22,-0.07] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.5163861 </td>
   <td style="text-align:right;"> -0.1507711 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5945515 </td>
   <td style="text-align:right;"> 0.5895628 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.16,0.29] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.5705490 </td>
   <td style="text-align:right;"> 0.2176686 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6545735 </td>
   <td style="text-align:right;"> 0.6354020 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.23,0.44] </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> 0.6272791 </td>
   <td style="text-align:right;"> 0.3554809 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.2938933 </td>
   <td style="text-align:right;"> 0.2764457 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.51,-0.28] </td>
   <td style="text-align:right;"> -0.51 </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> 0.6272791 </td>
   <td style="text-align:right;"> -0.3905585 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.3990294 </td>
   <td style="text-align:right;"> 0.4117241 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.27,-0.1] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.6272791 </td>
   <td style="text-align:right;"> -0.2063884 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7535138 </td>
   <td style="text-align:right;"> 0.8012821 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.2,0.43] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.6607525 </td>
   <td style="text-align:right;"> 0.3248020 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5016858 </td>
   <td style="text-align:right;"> 0.4841379 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.28,-0.06] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.6863031 </td>
   <td style="text-align:right;"> -0.1583776 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4719741 </td>
   <td style="text-align:right;"> 0.4668547 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.38,-0.06] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.7473057 </td>
   <td style="text-align:right;"> -0.2033755 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4683014 </td>
   <td style="text-align:right;"> 0.4655172 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.12,0.26] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.7473057 </td>
   <td style="text-align:right;"> 0.1719005 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4965006 </td>
   <td style="text-align:right;"> 0.5117241 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0,0.12] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.7473057 </td>
   <td style="text-align:right;"> 0.0680380 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7769706 </td>
   <td style="text-align:right;"> 0.8055556 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.3,0.57] </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.57 </td>
   <td style="text-align:right;"> 0.7752753 </td>
   <td style="text-align:right;"> 0.4273743 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5372460 </td>
   <td style="text-align:right;"> 0.5246827 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.08,0.03] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.8099341 </td>
   <td style="text-align:right;"> -0.0329282 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7180843 </td>
   <td style="text-align:right;"> 0.7262069 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.03,0.24] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.8099341 </td>
   <td style="text-align:right;"> 0.1130845 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5562085 </td>
   <td style="text-align:right;"> 0.5486601 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.16,0.34] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.8738043 </td>
   <td style="text-align:right;"> 0.2666615 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6449042 </td>
   <td style="text-align:right;"> 0.6606897 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.19,0.48] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.8738043 </td>
   <td style="text-align:right;"> 0.3834429 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.2745849 </td>
   <td style="text-align:right;"> 0.2758621 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.47,-0.14] </td>
   <td style="text-align:right;"> -0.47 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.8738043 </td>
   <td style="text-align:right;"> -0.2807864 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7060213 </td>
   <td style="text-align:right;"> 0.7335328 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.14,0.38] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.8898227 </td>
   <td style="text-align:right;"> 0.2717939 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7598942 </td>
   <td style="text-align:right;"> 0.7733333 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.47,0.73] </td>
   <td style="text-align:right;"> 0.47 </td>
   <td style="text-align:right;"> 0.73 </td>
   <td style="text-align:right;"> 0.9019817 </td>
   <td style="text-align:right;"> 0.5933660 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5232184 </td>
   <td style="text-align:right;"> 0.4993103 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.01,0.09] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.9060705 </td>
   <td style="text-align:right;"> 0.0276615 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5372688 </td>
   <td style="text-align:right;"> 0.5498006 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.18,0.32] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.9141984 </td>
   <td style="text-align:right;"> 0.2541432 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4905710 </td>
   <td style="text-align:right;"> 0.4943583 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.1,0.31] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.9710379 </td>
   <td style="text-align:right;"> 0.2417871 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5726984 </td>
   <td style="text-align:right;"> 0.5666667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.33,0.8] </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.80 </td>
   <td style="text-align:right;"> 0.9754111 </td>
   <td style="text-align:right;"> 0.5332635 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6935450 </td>
   <td style="text-align:right;"> 0.7466667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.41,0.71] </td>
   <td style="text-align:right;"> 0.41 </td>
   <td style="text-align:right;"> 0.71 </td>
   <td style="text-align:right;"> 0.9754361 </td>
   <td style="text-align:right;"> 0.5866441 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 4.2724868 </td>
   <td style="text-align:right;"> 4.2666667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [1.93,2.93] </td>
   <td style="text-align:right;"> 1.93 </td>
   <td style="text-align:right;"> 2.93 </td>
   <td style="text-align:right;"> 0.9754391 </td>
   <td style="text-align:right;"> 2.3666428 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5401058 </td>
   <td style="text-align:right;"> 0.5200000 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.33,0.72] </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.72 </td>
   <td style="text-align:right;"> 0.9754396 </td>
   <td style="text-align:right;"> 0.4933347 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7744974 </td>
   <td style="text-align:right;"> 0.7800000 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.39,0.67] </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.67 </td>
   <td style="text-align:right;"> 0.9754399 </td>
   <td style="text-align:right;"> 0.5333923 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7548148 </td>
   <td style="text-align:right;"> 0.7666667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.33,0.6] </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.60 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4799676 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7482540 </td>
   <td style="text-align:right;"> 0.7600000 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.15,0.47] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.47 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.3132872 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7082540 </td>
   <td style="text-align:right;"> 0.7266667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.15,0.49] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.49 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.3466591 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6473016 </td>
   <td style="text-align:right;"> 0.6933333 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.23,0.6] </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.60 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4599892 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6856085 </td>
   <td style="text-align:right;"> 0.7066667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.01,0.38] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2200786 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7006349 </td>
   <td style="text-align:right;"> 0.7066667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.07,0.47] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.47 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2733302 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6863492 </td>
   <td style="text-align:right;"> 0.7266667 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.07,0.41] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.41 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2599570 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7335057 </td>
   <td style="text-align:right;"> 0.7334274 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.01,0.15] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0475310 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.1883456 </td>
   <td style="text-align:right;"> 0.1770099 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.42,0] </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.2811043 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.2277395 </td>
   <td style="text-align:right;"> 0.2172414 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.38,0.06] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.2034483 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6875065 </td>
   <td style="text-align:right;"> 0.7510345 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [0.25,0.56] </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.56 </td>
   <td style="text-align:right;"> 0.0412636 </td>
   <td style="text-align:right;"> 0.4414242 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.7103315 </td>
   <td style="text-align:right;"> 0.7637518 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.2,0.54] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.0547549 </td>
   <td style="text-align:right;"> 0.4372500 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5928180 </td>
   <td style="text-align:right;"> 0.6868829 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.15,0.39] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.0684809 </td>
   <td style="text-align:right;"> 0.2629654 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3970868 </td>
   <td style="text-align:right;"> 0.3333333 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [-0.05,0.17] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0686354 </td>
   <td style="text-align:right;"> 0.0641064 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5732567 </td>
   <td style="text-align:right;"> 0.6368124 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.14,0.38] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.0823901 </td>
   <td style="text-align:right;"> 0.2480560 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3934088 </td>
   <td style="text-align:right;"> 0.3399154 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.37,-0.21] </td>
   <td style="text-align:right;"> -0.37 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.0901589 </td>
   <td style="text-align:right;"> -0.2771882 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3669029 </td>
   <td style="text-align:right;"> 0.3011283 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.45,-0.21] </td>
   <td style="text-align:right;"> -0.45 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.0928758 </td>
   <td style="text-align:right;"> -0.3378687 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3949396 </td>
   <td style="text-align:right;"> 0.3381523 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.34,-0.11] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0956580 </td>
   <td style="text-align:right;"> -0.2418837 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3658709 </td>
   <td style="text-align:right;"> 0.2983075 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.34,-0.05] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0985066 </td>
   <td style="text-align:right;"> -0.2566996 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4164481 </td>
   <td style="text-align:right;"> 0.3400000 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.38,-0.06] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.1043365 </td>
   <td style="text-align:right;"> -0.2069529 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6058989 </td>
   <td style="text-align:right;"> 0.6382228 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.32,-0.15] </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.1382218 </td>
   <td style="text-align:right;"> -0.2437576 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4758377 </td>
   <td style="text-align:right;"> 0.3977433 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.33,-0.08] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.1622333 </td>
   <td style="text-align:right;"> -0.1918056 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4460938 </td>
   <td style="text-align:right;"> 0.4668547 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.19,-0.03] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.1753612 </td>
   <td style="text-align:right;"> -0.1268923 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4795974 </td>
   <td style="text-align:right;"> 0.3862069 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.23,-0.04] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.1796245 </td>
   <td style="text-align:right;"> -0.1483375 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6706319 </td>
   <td style="text-align:right;"> 0.7344828 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [0.25,0.5] </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.1976951 </td>
   <td style="text-align:right;"> 0.3462301 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3741559 </td>
   <td style="text-align:right;"> 0.3262069 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.31,-0.08] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.2120964 </td>
   <td style="text-align:right;"> -0.1779320 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6944769 </td>
   <td style="text-align:right;"> 0.7764457 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.29,0.5] </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.2194815 </td>
   <td style="text-align:right;"> 0.3624533 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4116564 </td>
   <td style="text-align:right;"> 0.3524138 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.36,-0.17] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.2246592 </td>
   <td style="text-align:right;"> -0.2626880 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5920789 </td>
   <td style="text-align:right;"> 0.6068966 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.35,-0.18] </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.3479020 </td>
   <td style="text-align:right;"> -0.2662318 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6995814 </td>
   <td style="text-align:right;"> 0.7710345 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [0.02,0.25] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.3983477 </td>
   <td style="text-align:right;"> 0.1147799 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5717760 </td>
   <td style="text-align:right;"> 0.6158621 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [0.13,0.4] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.4369416 </td>
   <td style="text-align:right;"> 0.2731578 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3950289 </td>
   <td style="text-align:right;"> 0.3537931 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.01,0.1] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.4610958 </td>
   <td style="text-align:right;"> 0.0461700 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6062906 </td>
   <td style="text-align:right;"> 0.7075862 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [0.09,0.35] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.35 </td>
   <td style="text-align:right;"> 0.4859787 </td>
   <td style="text-align:right;"> 0.2407660 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4029380 </td>
   <td style="text-align:right;"> 0.3600000 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.14,0.02] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.5739091 </td>
   <td style="text-align:right;"> -0.0612004 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3364640 </td>
   <td style="text-align:right;"> 0.3027586 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.24,-0.12] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.5831000 </td>
   <td style="text-align:right;"> -0.1876545 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.1809687 </td>
   <td style="text-align:right;"> 0.1372414 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:left;"> [-0.43,-0.04] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.6016848 </td>
   <td style="text-align:right;"> -0.0800380 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.1875065 </td>
   <td style="text-align:right;"> 0.1671368 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.39,-0.07] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.7043541 </td>
   <td style="text-align:right;"> -0.1099203 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4782719 </td>
   <td style="text-align:right;"> 0.4341085 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [-0.25,0.02] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.7144802 </td>
   <td style="text-align:right;"> -0.1238938 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4782719 </td>
   <td style="text-align:right;"> 0.4341085 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [-0.25,0.02] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.7144802 </td>
   <td style="text-align:right;"> -0.1238938 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3715771 </td>
   <td style="text-align:right;"> 0.3353315 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.28,-0.16] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.7899915 </td>
   <td style="text-align:right;"> -0.2206940 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4396694 </td>
   <td style="text-align:right;"> 0.4217207 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.01,0.08] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.8228361 </td>
   <td style="text-align:right;"> 0.0295849 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4034301 </td>
   <td style="text-align:right;"> 0.3720930 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [-0.18,0.04] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.8581601 </td>
   <td style="text-align:right;"> -0.0832180 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4191521 </td>
   <td style="text-align:right;"> 0.4654547 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.22,0.12] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.8731878 </td>
   <td style="text-align:right;"> 0.0170671 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.7147391 </td>
   <td style="text-align:right;"> 0.7722144 </td>
   <td style="text-align:right;"> 164 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.04,0.15] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.9230202 </td>
   <td style="text-align:right;"> 0.0585995 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.8005169 </td>
   <td style="text-align:right;"> 0.6976744 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [-0.24,0.16] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.9446832 </td>
   <td style="text-align:right;"> -0.0299904 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5227502 </td>
   <td style="text-align:right;"> 0.5396825 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.21,0.8] </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.80 </td>
   <td style="text-align:right;"> 0.9575592 </td>
   <td style="text-align:right;"> 0.5130847 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3144038 </td>
   <td style="text-align:right;"> 0.3516058 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.13,0.12] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.9575694 </td>
   <td style="text-align:right;"> 0.0570764 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4712319 </td>
   <td style="text-align:right;"> 0.3733333 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.16,0.51] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.51 </td>
   <td style="text-align:right;"> 0.9829243 </td>
   <td style="text-align:right;"> 0.2133141 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4195204 </td>
   <td style="text-align:right;"> 0.3866667 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.33,0.21] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.9830156 </td>
   <td style="text-align:right;"> -0.1000624 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 2.5312043 </td>
   <td style="text-align:right;"> 2.5555556 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.62,1.69] </td>
   <td style="text-align:right;"> -0.62 </td>
   <td style="text-align:right;"> 1.69 </td>
   <td style="text-align:right;"> 0.9830159 </td>
   <td style="text-align:right;"> 0.6555005 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4059213 </td>
   <td style="text-align:right;"> 0.3266667 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.24,0.25] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.9830159 </td>
   <td style="text-align:right;"> -0.0532873 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4190787 </td>
   <td style="text-align:right;"> 0.3800000 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.08,0.43] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.9830159 </td>
   <td style="text-align:right;"> 0.1332905 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3957937 </td>
   <td style="text-align:right;"> 0.3492063 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.27,0.2] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.9830159 </td>
   <td style="text-align:right;"> -0.0841314 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4194720 </td>
   <td style="text-align:right;"> 0.3733333 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.33,0.21] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.9830159 </td>
   <td style="text-align:right;"> -0.0933592 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4185921 </td>
   <td style="text-align:right;"> 0.3412698 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.14,0.43] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0533806 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4205763 </td>
   <td style="text-align:right;"> 0.3512698 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.24,0.22] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0945477 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4947999 </td>
   <td style="text-align:right;"> 0.4603175 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.19,0.73] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.73 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4437770 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4186025 </td>
   <td style="text-align:right;"> 0.3888889 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.09,0.43] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1684638 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3861042 </td>
   <td style="text-align:right;"> 0.3479365 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0.36] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1675445 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.7367542 </td>
   <td style="text-align:right;"> 0.8028914 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.31,0.57] </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.57 </td>
   <td style="text-align:right;"> 0.0362279 </td>
   <td style="text-align:right;"> 0.4845506 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3486028 </td>
   <td style="text-align:right;"> 0.2722144 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.41,-0.24] </td>
   <td style="text-align:right;"> -0.41 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.0409113 </td>
   <td style="text-align:right;"> -0.3187121 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.7126339 </td>
   <td style="text-align:right;"> 0.7510345 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0.3,0.58] </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.58 </td>
   <td style="text-align:right;"> 0.0506342 </td>
   <td style="text-align:right;"> 0.4791372 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4022238 </td>
   <td style="text-align:right;"> 0.3461538 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.05,0.18] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.0826804 </td>
   <td style="text-align:right;"> 0.0620601 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3569968 </td>
   <td style="text-align:right;"> 0.2662069 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.41,-0.23] </td>
   <td style="text-align:right;"> -0.41 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0843084 </td>
   <td style="text-align:right;"> -0.3164981 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3443255 </td>
   <td style="text-align:right;"> 0.2873766 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.47,-0.26] </td>
   <td style="text-align:right;"> -0.47 </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> 0.0999752 </td>
   <td style="text-align:right;"> -0.3645880 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6138724 </td>
   <td style="text-align:right;"> 0.6995769 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.17,0.4] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.0999752 </td>
   <td style="text-align:right;"> 0.2778313 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3955357 </td>
   <td style="text-align:right;"> 0.3293371 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.36,-0.1] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.1034549 </td>
   <td style="text-align:right;"> -0.2150928 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6189484 </td>
   <td style="text-align:right;"> 0.7103448 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0.16,0.45] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 0.1069528 </td>
   <td style="text-align:right;"> 0.3345581 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6148127 </td>
   <td style="text-align:right;"> 0.6731312 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.17,0.42] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> 0.1107028 </td>
   <td style="text-align:right;"> 0.3071628 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3453016 </td>
   <td style="text-align:right;"> 0.2842031 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.36,-0.08] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.1144741 </td>
   <td style="text-align:right;"> -0.2956457 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4402494 </td>
   <td style="text-align:right;"> 0.3275862 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.29,-0.09] </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.1258959 </td>
   <td style="text-align:right;"> -0.1890300 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5859498 </td>
   <td style="text-align:right;"> 0.6078984 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.34,-0.18] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.1348678 </td>
   <td style="text-align:right;"> -0.2595937 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4395761 </td>
   <td style="text-align:right;"> 0.3469676 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.37,-0.11] </td>
   <td style="text-align:right;"> -0.37 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.1392647 </td>
   <td style="text-align:right;"> -0.2475311 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3641330 </td>
   <td style="text-align:right;"> 0.3006897 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.33,-0.07] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.1615125 </td>
   <td style="text-align:right;"> -0.1778640 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3995745 </td>
   <td style="text-align:right;"> 0.3317241 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.4,-0.08] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.1766729 </td>
   <td style="text-align:right;"> -0.2908001 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4649639 </td>
   <td style="text-align:right;"> 0.4841326 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.17,0] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.2191183 </td>
   <td style="text-align:right;"> -0.0903514 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.7017804 </td>
   <td style="text-align:right;"> 0.7717241 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0.28,0.55] </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.55 </td>
   <td style="text-align:right;"> 0.2615562 </td>
   <td style="text-align:right;"> 0.3796413 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6385424 </td>
   <td style="text-align:right;"> 0.7434483 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0.12,0.37] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.2977981 </td>
   <td style="text-align:right;"> 0.2875389 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.7300495 </td>
   <td style="text-align:right;"> 0.7930183 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.31,0.54] </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.3702540 </td>
   <td style="text-align:right;"> 0.4028268 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5156158 </td>
   <td style="text-align:right;"> 0.5341880 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.21,0.07] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.3931591 </td>
   <td style="text-align:right;"> -0.0717572 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5156158 </td>
   <td style="text-align:right;"> 0.5341880 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.21,0.07] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.3931591 </td>
   <td style="text-align:right;"> -0.0717572 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.1865884 </td>
   <td style="text-align:right;"> 0.1234483 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.4,-0.04] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.4950294 </td>
   <td style="text-align:right;"> -0.0773770 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6883297 </td>
   <td style="text-align:right;"> 0.7503448 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0.01,0.25] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.5699288 </td>
   <td style="text-align:right;"> 0.1076150 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.1918808 </td>
   <td style="text-align:right;"> 0.1442172 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.39,-0.07] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.5847392 </td>
   <td style="text-align:right;"> -0.1093293 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5770164 </td>
   <td style="text-align:right;"> 0.6068966 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.36,-0.18] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.5922800 </td>
   <td style="text-align:right;"> -0.2905114 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4122128 </td>
   <td style="text-align:right;"> 0.3953488 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.18,0.07] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.6431794 </td>
   <td style="text-align:right;"> -0.0845539 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.7117036 </td>
   <td style="text-align:right;"> 0.7856135 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.04,0.15] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.7039186 </td>
   <td style="text-align:right;"> 0.0521601 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4091612 </td>
   <td style="text-align:right;"> 0.3793103 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [0,0.12] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.7340023 </td>
   <td style="text-align:right;"> 0.0586335 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3395549 </td>
   <td style="text-align:right;"> 0.3048276 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.24,-0.11] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.7524984 </td>
   <td style="text-align:right;"> -0.1820665 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3825913 </td>
   <td style="text-align:right;"> 0.3455571 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-0.27,-0.14] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.7666066 </td>
   <td style="text-align:right;"> -0.2018551 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4134165 </td>
   <td style="text-align:right;"> 0.3848276 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-0.12,0.03] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.7773879 </td>
   <td style="text-align:right;"> -0.0530871 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4694559 </td>
   <td style="text-align:right;"> 0.4449929 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.01,0.1] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.8438643 </td>
   <td style="text-align:right;"> 0.0521290 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4563263 </td>
   <td style="text-align:right;"> 0.4914483 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.16,0.15] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.9676504 </td>
   <td style="text-align:right;"> 0.0516426 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4679101 </td>
   <td style="text-align:right;"> 0.4444444 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.1,0.48] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.9784183 </td>
   <td style="text-align:right;"> 0.1577398 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4419797 </td>
   <td style="text-align:right;"> 0.3650794 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.19,0.43] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.9784191 </td>
   <td style="text-align:right;"> -0.0149501 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4297972 </td>
   <td style="text-align:right;"> 0.3733333 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.33,0.31] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.9784192 </td>
   <td style="text-align:right;"> -0.0934059 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.8144367 </td>
   <td style="text-align:right;"> 0.7094017 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.25,0.18] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.9860147 </td>
   <td style="text-align:right;"> -0.0283292 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3445523 </td>
   <td style="text-align:right;"> 0.3748308 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.11,0.15] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0836933 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 2.8166446 </td>
   <td style="text-align:right;"> 3.2142857 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.58,2.05] </td>
   <td style="text-align:right;"> -0.58 </td>
   <td style="text-align:right;"> 2.05 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 1.2516237 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4284215 </td>
   <td style="text-align:right;"> 0.3512698 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.21,0.24] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0943456 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5109039 </td>
   <td style="text-align:right;"> 0.5158730 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [0.21,0.73] </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.73 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4885280 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4689771 </td>
   <td style="text-align:right;"> 0.4000000 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.05,0.53] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.53 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1507345 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4862522 </td>
   <td style="text-align:right;"> 0.4761905 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.07,0.59] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.59 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2311902 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3957011 </td>
   <td style="text-align:right;"> 0.3792063 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.01,0.39] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1884427 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5114286 </td>
   <td style="text-align:right;"> 0.5490476 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [0.18,0.78] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.78 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.5033333 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4732981 </td>
   <td style="text-align:right;"> 0.4444444 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.37,0.33] </td>
   <td style="text-align:right;"> -0.37 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0348657 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4710891 </td>
   <td style="text-align:right;"> 0.4682540 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.26,0.33] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0290935 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5199250 </td>
   <td style="text-align:right;"> 0.4209524 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [0.19,0.59] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.59 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.2657020 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Sheet Mulching </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6887737 </td>
   <td style="text-align:right;"> 0.7510345 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.25,0.57] </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.57 </td>
   <td style="text-align:right;"> 0.0356859 </td>
   <td style="text-align:right;"> 0.4516752 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4141352 </td>
   <td style="text-align:right;"> 0.3420310 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.34,-0.2] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.0509593 </td>
   <td style="text-align:right;"> -0.2707660 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5486689 </td>
   <td style="text-align:right;"> 0.6205924 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.11,0.34] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.0685248 </td>
   <td style="text-align:right;"> 0.2192990 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3476507 </td>
   <td style="text-align:right;"> 0.2891396 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.46,-0.24] </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.0853944 </td>
   <td style="text-align:right;"> -0.3385345 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7198960 </td>
   <td style="text-align:right;"> 0.7482370 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.22,0.55] </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.55 </td>
   <td style="text-align:right;"> 0.0853944 </td>
   <td style="text-align:right;"> 0.4626165 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4031305 </td>
   <td style="text-align:right;"> 0.3565891 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [-0.04,0.17] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0904859 </td>
   <td style="text-align:right;"> 0.0682272 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6351772 </td>
   <td style="text-align:right;"> 0.6466855 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.3,-0.14] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.1185296 </td>
   <td style="text-align:right;"> -0.2322415 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4921412 </td>
   <td style="text-align:right;"> 0.3977433 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.31,-0.04] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.1185296 </td>
   <td style="text-align:right;"> -0.1621787 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6473169 </td>
   <td style="text-align:right;"> 0.7241379 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.22,0.45] </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 0.1276950 </td>
   <td style="text-align:right;"> 0.3283372 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5908895 </td>
   <td style="text-align:right;"> 0.6593794 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.14,0.38] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.1404492 </td>
   <td style="text-align:right;"> 0.2373501 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3599656 </td>
   <td style="text-align:right;"> 0.3032440 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.35,-0.07] </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.1567120 </td>
   <td style="text-align:right;"> -0.2823283 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4102102 </td>
   <td style="text-align:right;"> 0.3496552 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.38,-0.07] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.1971852 </td>
   <td style="text-align:right;"> -0.2823593 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3551393 </td>
   <td style="text-align:right;"> 0.3321580 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.38,-0.16] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.2194352 </td>
   <td style="text-align:right;"> -0.2827856 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4431300 </td>
   <td style="text-align:right;"> 0.3868966 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.33,-0.14] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.2909816 </td>
   <td style="text-align:right;"> -0.2385665 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6120057 </td>
   <td style="text-align:right;"> 0.6137931 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.32,-0.18] </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.3371735 </td>
   <td style="text-align:right;"> -0.2539065 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6782132 </td>
   <td style="text-align:right;"> 0.7313117 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.27,0.49] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.49 </td>
   <td style="text-align:right;"> 0.3405946 </td>
   <td style="text-align:right;"> 0.3470373 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3453377 </td>
   <td style="text-align:right;"> 0.3227586 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.33,-0.11] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.4031143 </td>
   <td style="text-align:right;"> -0.2131549 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7272638 </td>
   <td style="text-align:right;"> 0.7834483 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.04,0.27] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.4593809 </td>
   <td style="text-align:right;"> 0.1441824 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5889288 </td>
   <td style="text-align:right;"> 0.6572414 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.08,0.34] </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.4847797 </td>
   <td style="text-align:right;"> 0.2096531 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3073454 </td>
   <td style="text-align:right;"> 0.2862069 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.25,-0.13] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.5197952 </td>
   <td style="text-align:right;"> -0.2020048 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4395046 </td>
   <td style="text-align:right;"> 0.3650794 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.13,0.41] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.41 </td>
   <td style="text-align:right;"> 0.5437915 </td>
   <td style="text-align:right;"> 0.2050961 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5376862 </td>
   <td style="text-align:right;"> 0.5386207 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.06,0.36] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 0.6223794 </td>
   <td style="text-align:right;"> 0.2097438 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4085552 </td>
   <td style="text-align:right;"> 0.3702398 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.23,-0.06] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.6243002 </td>
   <td style="text-align:right;"> -0.1614831 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4117179 </td>
   <td style="text-align:right;"> 0.4031008 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [-0.18,0.06] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.6292742 </td>
   <td style="text-align:right;"> -0.0744229 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3871342 </td>
   <td style="text-align:right;"> 0.3854020 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.05,0.04] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.6345809 </td>
   <td style="text-align:right;"> -0.0034661 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3734258 </td>
   <td style="text-align:right;"> 0.3331034 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.17,-0.02] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.6717389 </td>
   <td style="text-align:right;"> -0.0952350 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5196980 </td>
   <td style="text-align:right;"> 0.5220690 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.19,0.02] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.6919019 </td>
   <td style="text-align:right;"> -0.0979190 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3376190 </td>
   <td style="text-align:right;"> 0.3166432 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.3,-0.18] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.7409987 </td>
   <td style="text-align:right;"> -0.2369151 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.1491900 </td>
   <td style="text-align:right;"> 0.1206897 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.45,-0.05] </td>
   <td style="text-align:right;"> -0.45 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.8063828 </td>
   <td style="text-align:right;"> -0.0952000 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4621045 </td>
   <td style="text-align:right;"> 0.3953488 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [-0.27,-0.01] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.8267691 </td>
   <td style="text-align:right;"> -0.1484435 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4621045 </td>
   <td style="text-align:right;"> 0.3953488 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [-0.27,-0.01] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.8267691 </td>
   <td style="text-align:right;"> -0.1484435 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7446359 </td>
   <td style="text-align:right;"> 0.7722144 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.03,0.17] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.8639901 </td>
   <td style="text-align:right;"> 0.0762247 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.1497091 </td>
   <td style="text-align:right;"> 0.1100141 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.44,-0.08] </td>
   <td style="text-align:right;"> -0.44 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.8639901 </td>
   <td style="text-align:right;"> -0.1641947 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4704274 </td>
   <td style="text-align:right;"> 0.4666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.19,0.66] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.66 </td>
   <td style="text-align:right;"> 0.8802228 </td>
   <td style="text-align:right;"> 0.4333204 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3424566 </td>
   <td style="text-align:right;"> 0.3420690 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.03,0.06] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.9088162 </td>
   <td style="text-align:right;"> 0.0130919 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.8148485 </td>
   <td style="text-align:right;"> 0.7286822 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [-0.23,0.18] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.9316074 </td>
   <td style="text-align:right;"> -0.0184797 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4048157 </td>
   <td style="text-align:right;"> 0.4366720 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.23,0.11] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.9399868 </td>
   <td style="text-align:right;"> -0.0070936 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4181127 </td>
   <td style="text-align:right;"> 0.4000000 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.09,0.43] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.9570889 </td>
   <td style="text-align:right;"> 0.1666662 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4140310 </td>
   <td style="text-align:right;"> 0.3333333 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.07,0.48] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.9571007 </td>
   <td style="text-align:right;"> 0.0866256 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3067497 </td>
   <td style="text-align:right;"> 0.3396702 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.14,0.12] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.9742600 </td>
   <td style="text-align:right;"> 0.0470315 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4944253 </td>
   <td style="text-align:right;"> 0.3968254 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.21,0.77] </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.77 </td>
   <td style="text-align:right;"> 0.9828340 </td>
   <td style="text-align:right;"> 0.3701705 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIMinerals </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 2.4697645 </td>
   <td style="text-align:right;"> 2.4444444 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.62,1.69] </td>
   <td style="text-align:right;"> -0.62 </td>
   <td style="text-align:right;"> 1.69 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.5444186 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4185906 </td>
   <td style="text-align:right;"> 0.3492063 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.09,0.45] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.0625906 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4012768 </td>
   <td style="text-align:right;"> 0.3492063 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.27,0.19] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0974285 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4170905 </td>
   <td style="text-align:right;"> 0.3600000 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.21,0.27] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0200311 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3889412 </td>
   <td style="text-align:right;"> 0.3533333 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.01,0.37] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.1733890 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3998221 </td>
   <td style="text-align:right;"> 0.2866667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.35,0.18] </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.2000012 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3797488 </td>
   <td style="text-align:right;"> 0.3066667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.29,0.18] </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.1266045 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4443468 </td>
   <td style="text-align:right;"> 0.4466667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.33,0.25] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -0.0200062 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> NaN </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>



## BQI Shifts

![](grainDataAnalysis_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

### Individual Farm Practices

![](grainDataAnalysis_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


![](grainDataAnalysis_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](grainDataAnalysis_files/figure-html/bqi1bqi2-1.png)<!-- -->







