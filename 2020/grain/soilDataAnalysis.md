---
title: "2020 Survey, Grain Soils Quality Relationships"
author: "Real Food Campaign"
date: "12/27/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: true
---





# Context and Purpose of this Brief

  This document concentrates our analysis around soil helath indicators. Our main intentios are **measuring impact of soil helath over our crop quality variables**, **assesment of the potential of this variables to add predictive power to our machine learning models** and **summarising and testing the effects of management practices over soil health indicators**.


#### Crop Nutritional Density Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Nutritional </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
  </tr>
</tbody>
</table>

### Soil Health Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Soil Health Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Soil </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> PH_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> PH_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_20cm </td>
  </tr>
</tbody>
</table>

* The following table is available as a CSV file for manipulation, filename `./BFA-Soil-Extremes-Table.csv`.

<!--html_preserve--><div id="htmlwidget-0bb205dc129bfd920ba2" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-0bb205dc129bfd920ba2">{"x":{"filter":"none","data":[["1","2"],["oats","wheat"],[2.47,3.29],[19.48,20.91],[17.67,7.27],[55.86,47.81],[0,1.15],[6.72,4.52],[17.67,6.12],[49.14,43.29],[3.7843922933524,1.178902811007],[11.1601455991651,10.921117628786],[null,6.32173913043478],[8.3125,10.5774336283186],[2.31,2.72],[14.75,12.02],[17.18,6.94],[35.17,36.41],[1.12,0.99],[3.75,3.3],[16.06,5.95],[31.42,33.11],[3.5579691772136,1.22225964417177],[7.93947965925032,7.3805605094588],[15.3392857142857,7.01010101010101],[9.37866666666667,11.0333333333333]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>Type<\/th>\n      <th>median_10cm_organic_carbon_percentage<\/th>\n      <th>median_10cm_respiration_soil<\/th>\n      <th>max_10cm_organic_carbon_percentage<\/th>\n      <th>max_10cm_respiration_soil<\/th>\n      <th>min_10cm_organic_carbon_percentage<\/th>\n      <th>min_10cm_respiration_soil<\/th>\n      <th>range_10cm_organic_carbon_percentage<\/th>\n      <th>range_10cm_respiration_soil<\/th>\n      <th>sd_10cm_organic_carbon_percentage<\/th>\n      <th>sd_10cm_respiration_soil<\/th>\n      <th>ratio_10cm_organic_carbon_percentage<\/th>\n      <th>ratio_10cm_respiration_soil<\/th>\n      <th>median_20cm_organic_carbon_percentage<\/th>\n      <th>median_20cm_respiration_soil<\/th>\n      <th>max_20cm_organic_carbon_percentage<\/th>\n      <th>max_20cm_respiration_soil<\/th>\n      <th>min_20cm_organic_carbon_percentage<\/th>\n      <th>min_20cm_respiration_soil<\/th>\n      <th>range_20cm_organic_carbon_percentage<\/th>\n      <th>range_20cm_respiration_soil<\/th>\n      <th>sd_20cm_organic_carbon_percentage<\/th>\n      <th>sd_20cm_respiration_soil<\/th>\n      <th>ratio_20cm_organic_carbon_percentage<\/th>\n      <th>ratio_20cm_respiration_soil<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## Frequency of Each Farm Practice

<!--html_preserve--><div id="htmlwidget-7907cdd8c22aebfa18dc" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-7907cdd8c22aebfa18dc">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28"],["farm_practices.covercrops","Tillage Only","farm_practices.organic","farm_practices.regenerative","farm_practices.transitioning","farm_practices.notill","amendments.none","amendments.synth_fertilizer","amendments.organic_amendment","seed_treatment.none","seed_treatment.biological","seed_treatment.fungicide","seed_treatment.insecticide","seed_treatment.other","seed_treatment.gold_package, wire_worm","seed_treatment.cruser_max_wire_worm","seed_treatment.gold_package","seed_treatment.standard_package","tillage.light_tillage","tillage.heavy_tillage","tillage.none","land_prep.tillage","land_prep.notill","land_prep.none","companion_cropping.used","companion_cropping.none","covercrops.true","covercrops.none"],[97,17,190,159,186,90,146,147,71,134,54,105,33,3,6,3,12,15,148,56,90,215,90,90,49,65,97,43]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>practice<\/th>\n      <th>amount<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":2},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


## Methodologies

  We've performed an extensive medians comparisons in the past for crop nutritional quality indicators ( *antioxidants*, *polyphenols* and *brix* ). Methods, code implementations and a detailed guide on how we are interpreting results are all detailed on a previous brief available on the same site where this one is offered, *2019 Survey, Quality Relationships Analysis*.

  The predictive potential and impact of soil health indicators over nutritional density indicators will be measured by a non parametrical measure of correlation, *Spearman's Rank Correlation Coefficient*, in line with the rank tests employed for median shifts on our *Quality Relationships* synthesis tables.








## Boxplots for Soil Health Variables Over Climate Regions


![](soilDataAnalysis_files/figure-html/unnamed-chunk-3-1.png)<!-- -->![](soilDataAnalysis_files/figure-html/unnamed-chunk-3-2.png)<!-- -->


# Cuantification of Soil Quality Variables over Produce Quality Variables

  As stated, we are measuring correlation between soil and nutritional variables for each crop.
  Two linear regressions will be adjusted for each crop, explaining a nutritional variable with all six soil variables.
  
  On a first step, a **first linear model** is adjusted for all the explicative variables and all data points available. Then an **$\alpha$ trimming** is performed for the $15%$ biggest residuals and a **second model** is adjusted with this high residuals trimmmed dataset.
  
  We don't expect such a simple regression to yield a complete prediction of this variables, which have proven remarkably complex to predict in a regressive context. Good $R^2$ results will inform that this variables are good material to increase the predictive power of our current complex models, like Random Forests which already employ spectrometry, regions, varieties, etc.

## Fit Quality on Each Crop and Variable

* **Trimmed** models are thos were an alfa trimming has been performed: after a first run, the 15% worst residual samples are removed and a new model is trained over the clean dataset.
* The **F Test** p value is offered for both models.
* The **T test** p values are offered just for the trimmed models. No Bonferroni correction has been applied, so the reader should bear in mind that for 6 variables to give the desired confidence, the threshold p value should be divided by 6 ( that is, to satisfy a level 0.05 we need to see 0.083 on the t test ).

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Samples </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> R2 </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> F test p-value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> trimmed R2 </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> F test p-value, trimmed </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val (Intercept) </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val PH_soil_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val PH_soil_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val respiration_soil_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val respiration_soil_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val organic_carbon_percentage_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val organic_carbon_percentage_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_Intercept </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_PH_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_PH_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_respiration_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_respiration_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_carbon_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_carbon_20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 0.3237056 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.5304825 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.1292 </td>
   <td style="text-align:right;"> 0.0079 </td>
   <td style="text-align:right;"> 0.0487 </td>
   <td style="text-align:right;"> 0.6188 </td>
   <td style="text-align:right;"> 0.0232 </td>
   <td style="text-align:right;"> 23544.40 </td>
   <td style="text-align:right;"> -4896.85 </td>
   <td style="text-align:right;"> 1032.36 </td>
   <td style="text-align:right;"> 26.60 </td>
   <td style="text-align:right;"> 29.78 </td>
   <td style="text-align:right;"> -32.74 </td>
   <td style="text-align:right;"> 167.49 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 0.2213846 </td>
   <td style="text-align:right;"> 0.0000149 </td>
   <td style="text-align:right;"> 0.4936021 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0364 </td>
   <td style="text-align:right;"> 0.0046 </td>
   <td style="text-align:right;"> 0.5333 </td>
   <td style="text-align:right;"> 0.6571 </td>
   <td style="text-align:right;"> 0.0100 </td>
   <td style="text-align:right;"> 386.66 </td>
   <td style="text-align:right;"> -82.52 </td>
   <td style="text-align:right;"> 31.18 </td>
   <td style="text-align:right;"> 0.63 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> -0.61 </td>
   <td style="text-align:right;"> 3.94 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0593321 </td>
   <td style="text-align:right;"> 0.0187955 </td>
   <td style="text-align:right;"> 0.1953265 </td>
   <td style="text-align:right;"> 0.0000083 </td>
   <td style="text-align:right;"> 0.0347 </td>
   <td style="text-align:right;"> 0.1301 </td>
   <td style="text-align:right;"> 0.7720 </td>
   <td style="text-align:right;"> 0.0800 </td>
   <td style="text-align:right;"> 0.0326 </td>
   <td style="text-align:right;"> 0.0010 </td>
   <td style="text-align:right;"> 0.2591 </td>
   <td style="text-align:right;"> 279.38 </td>
   <td style="text-align:right;"> -32.29 </td>
   <td style="text-align:right;"> -5.38 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> -0.91 </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> -3.57 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 0.1042241 </td>
   <td style="text-align:right;"> 0.0076261 </td>
   <td style="text-align:right;"> 0.1731663 </td>
   <td style="text-align:right;"> 0.0009213 </td>
   <td style="text-align:right;"> 0.7672 </td>
   <td style="text-align:right;"> 0.0335 </td>
   <td style="text-align:right;"> 0.0001 </td>
   <td style="text-align:right;"> 0.1790 </td>
   <td style="text-align:right;"> 0.7054 </td>
   <td style="text-align:right;"> 0.0015 </td>
   <td style="text-align:right;"> 0.0024 </td>
   <td style="text-align:right;"> -1.74 </td>
   <td style="text-align:right;"> -3.14 </td>
   <td style="text-align:right;"> 5.40 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> -0.42 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0581945 </td>
   <td style="text-align:right;"> 0.0201620 </td>
   <td style="text-align:right;"> 0.0931013 </td>
   <td style="text-align:right;"> 0.0053923 </td>
   <td style="text-align:right;"> 0.9654 </td>
   <td style="text-align:right;"> 0.7534 </td>
   <td style="text-align:right;"> 0.5215 </td>
   <td style="text-align:right;"> 0.0088 </td>
   <td style="text-align:right;"> 0.1919 </td>
   <td style="text-align:right;"> 0.2600 </td>
   <td style="text-align:right;"> 0.4511 </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 0.55 </td>
   <td style="text-align:right;"> 0.94 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> -0.18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0074932 </td>
   <td style="text-align:right;"> 0.3120372 </td>
   <td style="text-align:right;"> 0.0362460 </td>
   <td style="text-align:right;"> 0.1001291 </td>
   <td style="text-align:right;"> 0.6498 </td>
   <td style="text-align:right;"> 0.0893 </td>
   <td style="text-align:right;"> 0.2137 </td>
   <td style="text-align:right;"> 0.2740 </td>
   <td style="text-align:right;"> 0.0250 </td>
   <td style="text-align:right;"> 0.4899 </td>
   <td style="text-align:right;"> 0.1031 </td>
   <td style="text-align:right;"> 3739.13 </td>
   <td style="text-align:right;"> -2084.70 </td>
   <td style="text-align:right;"> 1901.64 </td>
   <td style="text-align:right;"> 15.82 </td>
   <td style="text-align:right;"> -47.45 </td>
   <td style="text-align:right;"> -139.87 </td>
   <td style="text-align:right;"> 292.13 </td>
  </tr>
</tbody>
</table>

### Residuals Graphics for the Most Promissing Regressions

#### Antioxidants on Oats

![](soilDataAnalysis_files/figure-html/oatsAnti-1.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-2.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-3.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-4.png)<!-- -->

## Random Forest Regressions

This random forest models are cross validated and include several other variables sets, to help better compare the importance of our soil predictors in comparison with other available information, which in this case is the hand spectrometer scans.

### Datasets

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>composition of datasets</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> name </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variables </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
</tbody>
</table>


### Observed Results


<!--html_preserve--><div id="htmlwidget-038a33aa21be4371f9d1" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-038a33aa21be4371f9d1">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"],["Antioxidants","Antioxidants","Antioxidants","Antioxidants","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Proteins","Proteins","Proteins","Proteins","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Proteins","Proteins","Proteins","Proteins"],["oats","oats","oats","oats","oats","oats","oats","oats","oats","oats","oats","oats","wheat","wheat","wheat","wheat","wheat","wheat","wheat","wheat","wheat","wheat","wheat","wheat"],["set4","set2","set3","set1","set4","set1","set2","set3","set3","set2","set4","set1","set4","set1","set3","set2","set3","set2","set4","set1","set2","set4","set3","set1"],[0.592202544482384,0.747478639069116,0.813064293287065,0.899876738887654,0.656019331670237,0.728945965753679,0.744104920305534,0.751089803565089,0.297899702702664,0.525862220670323,0.616450542620182,0.774639193584156,0.305772121645821,0.425956652067584,0.560101035839076,0.586695351807806,0.372050267834655,0.391001690340251,0.543669078906627,0.592945558632785,0.331937378338385,0.507548951868985,0.555506636915007,0.576042234769133],[13,18,1,3,1,1,1,4,4,37,39,7,1,2,2,2,35,3,18,1,29,34,2,36],[62,62,62,62,62,62,62,62,62,62,62,62,108,108,108,108,108,108,108,108,108,108,108,108]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>mtry<\/th>\n      <th>N<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

#### Quality of fit plots for all Random Forest Regressions on Soil Health

![](soilDataAnalysis_files/figure-html/allImportance-1.png)<!-- -->

![](soilDataAnalysis_files/figure-html/allRegressions-1.png)<!-- -->


# Quality Relationship Tables

  We offer separatedly the synthesis of this tables on the same site were this brief is available, showing the shifts as percentages of deviation over the median and the sample sizes. 
  
  This variables have shown interesting behaviours. As we are not accounting for the crop factor, populations are robust for most evaluated practices, and most of them have shown shifts that our test could detect as significant. 
  
  Tables offered below are much deeper so the recommended procedure is searching for interesting data on the synthesis tables, and then getting a deeper insight by looking at the complete summaries. 
  
  Besites the medians shift analysis, we've performed a much more informal summary for the frequency of factors on certaing percentiles. The intuitive idea behind this procedure is that if the frequency of *Practice X* over the whole dataset is $30%$ but while looking at the "10%" biggest values for the vaiable it doubles to $60%$, it is probably *concentrated* among the most succesfull crops. There are tables for the best and worst percentiles showing this behaviour. 
  
  It's main advantage is that it spares the reader looking at the enormuos amount of graphics that would have been involved in comparint all these factors for each of the three variables and that it shows patterns even while not detected as relevant by the test, could be interesting for certain investigators or worth an additional exploration.

## Percentual Shift Over the Median Value for Farm Practices

### p.values Code Key

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>

### Tables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Covercrops </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-12.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-14.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">-14.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-16.84</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">6.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">-23.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">2.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">-32.9</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 169">-18.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 163">-24.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 175">-16.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 175">-25.37</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 141">-19.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 135">-22.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 144">-19.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 144">-22.35</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Transitioning </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 165">-18.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 159">-25.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">-18.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">-30.12</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Soil Amendments

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 68">18.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 68">2.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">11.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-10.54</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Synth Fertilizer </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 114">7.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 132">-18.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 114">-4.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 138">-30.16</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Tillage Intensity

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-18.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">21.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-22.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">28.06</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 127">-26.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 127">0.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 133">-15.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 133">8.83</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over Crop's Median Values for Companion Cropping (Just in Oats)

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Used </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">19.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">63.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">30.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">36.99</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over Crop's Median Values for Cover Cropping

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> True </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-23.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-15.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">-31.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-35.8</span> </td>
  </tr>
</tbody>
</table>


<!-- ## Quality Replationships for PH at 10cm -->

<!-- ### Median Shifts -->



<!-- ### Density Variation on Certain Percentiles -->

<!-- On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable.  -->

<!-- We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values. -->




<!-- ## Quality Replationships for PH at 20cm -->

<!-- ### Median Shifts -->



<!-- ### Density Variation on Certain Percentiles -->

<!-- On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable.  -->

<!-- We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values. -->




## Quality Replationships for respiration at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 23.02294 </td>
   <td style="text-align:right;"> 21.88 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [-7.96,0.16] </td>
   <td style="text-align:right;"> -7.96 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0837590 </td>
   <td style="text-align:right;"> -4.220064 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 27.60412 </td>
   <td style="text-align:right;"> 21.67 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [21.98,33.64] </td>
   <td style="text-align:right;"> 21.98 </td>
   <td style="text-align:right;"> 33.64 </td>
   <td style="text-align:right;"> 0.0506692 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 28.985 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 21.49890 </td>
   <td style="text-align:right;"> 20.91 </td>
   <td style="text-align:right;"> 163 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-10.41,-2.22] </td>
   <td style="text-align:right;"> -10.41 </td>
   <td style="text-align:right;"> -2.22 </td>
   <td style="text-align:right;"> 0.0609745 </td>
   <td style="text-align:right;"> -7.210039 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 22.13800 </td>
   <td style="text-align:right;"> 21.21 </td>
   <td style="text-align:right;"> 135 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [-10.05,-0.58] </td>
   <td style="text-align:right;"> -10.05 </td>
   <td style="text-align:right;"> -0.58 </td>
   <td style="text-align:right;"> 0.0894012 </td>
   <td style="text-align:right;"> -6.429969 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 20.70208 </td>
   <td style="text-align:right;"> 18.18 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-10.67,-2.8] </td>
   <td style="text-align:right;"> -10.67 </td>
   <td style="text-align:right;"> -2.80 </td>
   <td style="text-align:right;"> 0.1541486 </td>
   <td style="text-align:right;"> -7.259999 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 20.96185 </td>
   <td style="text-align:right;"> 18.32 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-11.09,-2.73] </td>
   <td style="text-align:right;"> -11.09 </td>
   <td style="text-align:right;"> -2.73 </td>
   <td style="text-align:right;"> 0.1968082 </td>
   <td style="text-align:right;"> -6.779974 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 23.54018 </td>
   <td style="text-align:right;"> 23.00 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [21.84,24.56] </td>
   <td style="text-align:right;"> 21.84 </td>
   <td style="text-align:right;"> 24.56 </td>
   <td style="text-align:right;"> 0.9047460 </td>
   <td style="text-align:right;"> 23.06996 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 19.91955 </td>
   <td style="text-align:right;"> 16.40 </td>
   <td style="text-align:right;"> 132 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> [-6.68,-2.25] </td>
   <td style="text-align:right;"> -6.68 </td>
   <td style="text-align:right;"> -2.25 </td>
   <td style="text-align:right;"> 0.1242371 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -4.3100132 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 23.95956 </td>
   <td style="text-align:right;"> 23.25 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-3.72,4.15] </td>
   <td style="text-align:right;"> -3.72 </td>
   <td style="text-align:right;"> 4.15 </td>
   <td style="text-align:right;"> 0.9284270 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.4700359 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

## Quality Replationships for Soil Respiration at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 14.89568 </td>
   <td style="text-align:right;"> 12.28 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [-6.1,0.54] </td>
   <td style="text-align:right;"> -6.10 </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.3946678 </td>
   <td style="text-align:right;"> -2.840036 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 16.95400 </td>
   <td style="text-align:right;"> 17.36 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [14.18,19.73] </td>
   <td style="text-align:right;"> 14.18 </td>
   <td style="text-align:right;"> 19.73 </td>
   <td style="text-align:right;"> 0.5022610 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 16.86764 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 14.26394 </td>
   <td style="text-align:right;"> 11.74 </td>
   <td style="text-align:right;"> 175 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> [-7.21,-0.5] </td>
   <td style="text-align:right;"> -7.21 </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.4734863 </td>
   <td style="text-align:right;"> -4.279978 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 14.31840 </td>
   <td style="text-align:right;"> 12.15 </td>
   <td style="text-align:right;"> 144 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-6.58,-0.09] </td>
   <td style="text-align:right;"> -6.58 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.4228079 </td>
   <td style="text-align:right;"> -3.769988 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 13.07550 </td>
   <td style="text-align:right;"> 11.48 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:left;"> [-7.75,-2.08] </td>
   <td style="text-align:right;"> -7.75 </td>
   <td style="text-align:right;"> -2.08 </td>
   <td style="text-align:right;"> 0.5907329 </td>
   <td style="text-align:right;"> -5.080092 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 12.22815 </td>
   <td style="text-align:right;"> 11.54 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-7.71,-3.33] </td>
   <td style="text-align:right;"> -7.71 </td>
   <td style="text-align:right;"> -3.33 </td>
   <td style="text-align:right;"> 0.7890524 </td>
   <td style="text-align:right;"> -5.550030 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 17.03911 </td>
   <td style="text-align:right;"> 15.280 </td>
   <td style="text-align:right;"> 113 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [14.94,18.08] </td>
   <td style="text-align:right;"> 14.94 </td>
   <td style="text-align:right;"> 18.08 </td>
   <td style="text-align:right;"> 0.1447082 </td>
   <td style="text-align:right;"> 16.51501 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 11.29217 </td>
   <td style="text-align:right;"> 9.885 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-6.56,-3.32] </td>
   <td style="text-align:right;"> -6.56 </td>
   <td style="text-align:right;"> -3.32 </td>
   <td style="text-align:right;"> 0.6339162 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -4.981547 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 15.12478 </td>
   <td style="text-align:right;"> 14.720 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-3.66,0.26] </td>
   <td style="text-align:right;"> -3.66 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.3392274 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -1.740047 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

## Quality Replationships for Carbon Mass Percentage at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 3.071412 </td>
   <td style="text-align:right;"> 2.80 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-1,-0.12] </td>
   <td style="text-align:right;"> -1.00 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.1370556 </td>
   <td style="text-align:right;"> -0.4900340 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.841176 </td>
   <td style="text-align:right;"> 2.94 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [2.88,5] </td>
   <td style="text-align:right;"> 2.88 </td>
   <td style="text-align:right;"> 5.00 </td>
   <td style="text-align:right;"> 0.1654716 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.980044 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 3.364142 </td>
   <td style="text-align:right;"> 2.48 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-1.1,-0.35] </td>
   <td style="text-align:right;"> -1.10 </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> 0.2439312 </td>
   <td style="text-align:right;"> -0.7200017 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 3.412979 </td>
   <td style="text-align:right;"> 2.46 </td>
   <td style="text-align:right;"> 141 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:left;"> [-1.11,-0.44] </td>
   <td style="text-align:right;"> -1.11 </td>
   <td style="text-align:right;"> -0.44 </td>
   <td style="text-align:right;"> 0.1555672 </td>
   <td style="text-align:right;"> -0.7599821 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 3.367818 </td>
   <td style="text-align:right;"> 2.48 </td>
   <td style="text-align:right;"> 165 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-1.15,-0.36] </td>
   <td style="text-align:right;"> -1.15 </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> 0.2077195 </td>
   <td style="text-align:right;"> -0.7399635 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 3.658421 </td>
   <td style="text-align:right;"> 3.39 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [-0.18,0.59] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.59 </td>
   <td style="text-align:right;"> 0.2828455 </td>
   <td style="text-align:right;"> 0.2499630 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.060091 </td>
   <td style="text-align:right;"> 2.67 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [2.84,3.23] </td>
   <td style="text-align:right;"> 2.84 </td>
   <td style="text-align:right;"> 3.23 </td>
   <td style="text-align:right;"> 0.0076886 </td>
   <td style="text-align:right;"> 3.050039 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 4.039211 </td>
   <td style="text-align:right;"> 2.96 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:left;"> [-0.04,0.48] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.7020118 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.2200557 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 3.511765 </td>
   <td style="text-align:right;"> 3.51 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> [0.17,0.87] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.87 </td>
   <td style="text-align:right;"> 0.1593340 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.5699873 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

## Quality Replationships for Soil Carbon Mass Percentage at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 2.677439 </td>
   <td style="text-align:right;"> 2.28 </td>
   <td style="text-align:right;"> 82 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [-0.96,-0.12] </td>
   <td style="text-align:right;"> -0.96 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.3253346 </td>
   <td style="text-align:right;"> -0.4800449 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.100000 </td>
   <td style="text-align:right;"> 2.50 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [2.49,4.03] </td>
   <td style="text-align:right;"> 2.49 </td>
   <td style="text-align:right;"> 4.03 </td>
   <td style="text-align:right;"> 0.1671745 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.255371 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 3.225200 </td>
   <td style="text-align:right;"> 2.22 </td>
   <td style="text-align:right;"> 175 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> [-0.92,-0.19] </td>
   <td style="text-align:right;"> -0.92 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.2412402 </td>
   <td style="text-align:right;"> -0.5399739 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 3.231250 </td>
   <td style="text-align:right;"> 2.11 </td>
   <td style="text-align:right;"> 144 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-1.04,-0.31] </td>
   <td style="text-align:right;"> -1.04 </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> 0.2443995 </td>
   <td style="text-align:right;"> -0.6306680 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 3.129357 </td>
   <td style="text-align:right;"> 2.16 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:left;"> [-0.95,-0.27] </td>
   <td style="text-align:right;"> -0.95 </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> 0.2645853 </td>
   <td style="text-align:right;"> -0.5899915 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 3.055263 </td>
   <td style="text-align:right;"> 2.82 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [-0.23,0.37] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.1715822 </td>
   <td style="text-align:right;"> 0.0799740 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 2.881681 </td>
   <td style="text-align:right;"> 2.500 </td>
   <td style="text-align:right;"> 113 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [2.63,2.98] </td>
   <td style="text-align:right;"> 2.63 </td>
   <td style="text-align:right;"> 2.98 </td>
   <td style="text-align:right;"> 0.0045183 </td>
   <td style="text-align:right;"> 2.810023 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 3.624474 </td>
   <td style="text-align:right;"> 2.455 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:left;"> [-0.37,0.14] </td>
   <td style="text-align:right;"> -0.37 </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.5858901 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -0.1299501 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 3.186812 </td>
   <td style="text-align:right;"> 3.020 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.01,0.68] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.68 </td>
   <td style="text-align:right;"> 0.3004371 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.3299551 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable.

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>

  </tr>
</tbody>
</table>

# Predicting Soil Organic Carbon Content

 In an attempt of having as much reassurance as possible, both Linear Models and Random Forest Models have been trained for this experiment. 
 
 
 As it is to be expected when mixing diverse sets of variables which can't be expected to seek for an intrinsical mathematical model such as a physical equation, random forest yields an increase in precission. 
 
 The behaviour of this increase in observed precission points to future enquiry paths about the informing power of each variable, especially considering the most powerful model replaces the 10 bionutrient meter scan variables by just `climateRegion` achieving an apparent improvement. This result is probably the result of an overfit over the climate region and probably shouldn't be considered relevant. I wouldn't attribute this to a mathematical overfit, but to an under representation of the internal variance of each climatic region, so a further check is also seeing how many actual locations represent each climatic region and how far from each other they are.
 
 Overall, the prediction seems feasible but probably requires some further variables to be useful, besides the checks we considered above. 
 
 This experiment will without doubt benefit from a comparison with all the other soil samples we have, even considering that `crop` as factor for sure has a high influence on the results both a as a consequence of the physiological action of each species on it and because it is to be expected that the crop has been chosen because of preexisting qualities of the soil. 
 
 A proposed strategy to face the possible overfit over regions is to clusterize the residulas/actual vs. predicted plots and search for evident clusters.
 
 
## Grain Soils
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> bionutrientMeter10cm </td>
   <td style="text-align:left;"> bionutrientMeter10cm, scsMetadata10cm </td>
   <td style="text-align:left;"> bionutrientMeter10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm </td>
   <td style="text-align:left;"> scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm, scsMetadata20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm </td>
   <td style="text-align:left;"> scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm </td>
   <td style="text-align:left;"> scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm </td>
  </tr>
</tbody>
</table>

<!--html_preserve--><div id="htmlwidget-8923d529858de1fef4ea" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-8923d529858de1fef4ea">{"x":{"filter":"none","caption":"<caption>Random Forest on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","20cm","10cm","10cm","20cm","20cm","10cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["1-10cm","1-20cm","3-10cm","2-20cm","2-10cm","3-20cm","4-20cm","5-20cm","4-10cm","5-10cm","6-20cm","7-20cm","7-10cm","6-10cm"],[0.4015,0.4573,0.4631,0.5245,0.5656,0.5718,0.6072,0.6727,0.6818,0.6835,0.6898,0.6919,0.721,0.7386],[281,290,267,249,243,276,238,238,232,234,238,235,229,234],[21,2,22,1,14,29,2,4,2,37,15,5,14,29],[0.98932384341637,0.986206896551724,0.98876404494382,1,0.979423868312757,0.981884057971015,0.983193277310924,0.974789915966387,0.987068965517241,0.961538461538462,0.953781512605042,0.987234042553191,0.96943231441048,0.957264957264957],[0.0290556900726392,0.039715873996294,0.024818401937046,0.10772104607721,0.0181598062953995,0.0206609017912292,0.124221668742217,0.133063511830635,0.122881355932203,0.0441888619854721,0.0485678704856787,0.108343711083437,0.0472154963680387,0.0334745762711866],[null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>mtry<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n      <th>model<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[4,5,6,7,8]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[4]; $(this.api().cell(row, 4).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-c08fa5c641e7a02783d2" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-c08fa5c641e7a02783d2">{"x":{"filter":"none","caption":"<caption>Linear Model on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","10cm","20cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm","bionutrientMeter10cm"],[0.4627,0.3905,0.3712,0.3296,0.3248,0.2496,0.2137,0.191,0.1877,0.1599]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

### Organic content percentage at 10cm, model '2-10cm'

* Saved as `./graphics/grainSoilHealRegressionModel210cm.png`.

![](soilDataAnalysis_files/figure-html/unnamed-chunk-9-1.png)<!-- -->


## Soil Models for All Crops

<!--html_preserve--><div id="htmlwidget-bfa080e9174075fa6d5e" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-bfa080e9174075fa6d5e">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["20cm","20cm","10cm","10cm","20cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm"],["bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices"],[0.2936,0.3353,0.4109,0.4808,0.534,0.5418,0.5731,0.5782,0.5884,0.5971,0.5974,0.6039,0.6061,0.6124],[0.854471069549971,0.869598180439727,0.865060240963855,0.884615384615385,0.90622009569378,0.836325237592397,0.818833162743091,0.869670152855994,0.793466807165437,0.792452830188679,0.781488549618321,0.789778206364513,0.892233009708738,0.862745098039216],[0.0496023138105568,0.043916109202676,0.137126865671642,0.13715103793844,0.0817780794027825,0.0735400144196107,0.0666906993511175,0.0610583446404342,0.0655270655270655,0.0678062678062678,0.0393620631150322,0.0590551181102362,0.120204008589835,0.0868690434168726]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-9cc58f19b74eee456343" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-9cc58f19b74eee456343">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","10cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm","20cm","20cm","20cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm"],[0.2909,0.2883,0.2808,0.2647,0.205,0.2035,0.1889,0.1387,0.1221,0.1009,0.097,0.0966,0.0241,0.0208]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## Depth as a variable


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-10cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> bionutrientMeter </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata </td>
   <td style="text-align:left;"> bionutrientMeter, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata, climateRegion, medFarmPractices, PH_soil </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil </td>
  </tr>
</tbody>
</table>

<!--html_preserve--><div id="htmlwidget-7b24e8d1357a4b93768f" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-7b24e8d1357a4b93768f">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils, depth as variable.<\/caption>","data":[["1","2","3","4","5","6","7"],["organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage"],["bionutrientMeter","bionutrientMeter, scsMetadata","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices"],[0.4283,0.4791,0.5368,0.5533,0.5916,0.6018,0.6319],[1660,1274,974,1030,977,1224,1037],[0.859638554216868,0.884615384615385,0.910677618069815,0.901941747572816,0.817809621289662,0.893790849673203,0.832208293153327],[0.127332089552239,0.137437365783822,0.127054794520548,0.132211882605584,0.0634462869502523,0.117984468761031,0.0809949892627057]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-f28399a20378cfb6f167" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-f28399a20378cfb6f167">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils, depth as a parameter.<\/caption>","data":[["1","2","3","4","5","6","7"],["soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","scsMetadata, climateRegion, medFarmPractices","bionutrientMeter","bionutrientMeter, scsMetadata"],[0.3628,0.3045,0.2839,0.2744,0.1472,0.1028,0.0987]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":3},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

# Agnostic on Depth Soil Model Review

![](soilDataAnalysis_files/figure-html/soilDepthReview-1.png)<!-- -->![](soilDataAnalysis_files/figure-html/soilDepthReview-2.png)<!-- -->![](soilDataAnalysis_files/figure-html/soilDepthReview-3.png)<!-- -->




