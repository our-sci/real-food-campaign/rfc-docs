---
title: "2020 Survey, Grain Nutrients ANOVA"
author: "Real Food Campaign"
date: "05/27/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: true
---




# Crop Mineral on Soil Mineral Regressions

* These regressions have been attempted for both the observed X values and for logarithm and root of these values.

## Oats

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Fits for the models that compare amount of mineral in soil with amount of mineral in crop.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquared </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquaredRoot </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquaredLog </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:right;"> 0.0066375 </td>
   <td style="text-align:right;"> 0.0122512 </td>
   <td style="text-align:right;"> 0.0210372 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:right;"> 0.0292207 </td>
   <td style="text-align:right;"> 0.0464670 </td>
   <td style="text-align:right;"> 0.0997980 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:right;"> 0.1283980 </td>
   <td style="text-align:right;"> 0.1292431 </td>
   <td style="text-align:right;"> 0.1291704 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:right;"> 0.1850232 </td>
   <td style="text-align:right;"> 0.1699049 </td>
   <td style="text-align:right;"> 0.1550031 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P </td>
   <td style="text-align:left;"> P </td>
   <td style="text-align:right;"> 0.0114123 </td>
   <td style="text-align:right;"> 0.0159208 </td>
   <td style="text-align:right;"> 0.0211175 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> S </td>
   <td style="text-align:left;"> S </td>
   <td style="text-align:right;"> 0.1308031 </td>
   <td style="text-align:right;"> 0.1160629 </td>
   <td style="text-align:right;"> 0.1012943 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> K </td>
   <td style="text-align:left;"> K </td>
   <td style="text-align:right;"> 0.1768984 </td>
   <td style="text-align:right;"> 0.1943457 </td>
   <td style="text-align:right;"> 0.2086432 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:right;"> 0.0784921 </td>
   <td style="text-align:right;"> 0.0813456 </td>
   <td style="text-align:right;"> 0.0769821 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:right;"> 0.0548709 </td>
   <td style="text-align:right;"> 0.0442825 </td>
   <td style="text-align:right;"> 0.0344776 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:right;"> 0.0006837 </td>
   <td style="text-align:right;"> 0.0026728 </td>
   <td style="text-align:right;"> 0.0059193 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:right;"> 0.0118012 </td>
   <td style="text-align:right;"> 0.0221662 </td>
   <td style="text-align:right;"> 0.0376148 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:right;"> 0.0247666 </td>
   <td style="text-align:right;"> 0.0517991 </td>
   <td style="text-align:right;"> 0.1143441 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:right;"> 0.1214372 </td>
   <td style="text-align:right;"> 0.1194051 </td>
   <td style="text-align:right;"> 0.1167519 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:right;"> 0.2413252 </td>
   <td style="text-align:right;"> 0.2248470 </td>
   <td style="text-align:right;"> 0.2074322 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P </td>
   <td style="text-align:left;"> P </td>
   <td style="text-align:right;"> 0.0056794 </td>
   <td style="text-align:right;"> 0.0019351 </td>
   <td style="text-align:right;"> 0.0002388 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> S </td>
   <td style="text-align:left;"> S </td>
   <td style="text-align:right;"> 0.2128540 </td>
   <td style="text-align:right;"> 0.2081135 </td>
   <td style="text-align:right;"> 0.2014065 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> K </td>
   <td style="text-align:left;"> K </td>
   <td style="text-align:right;"> 0.2802688 </td>
   <td style="text-align:right;"> 0.2939386 </td>
   <td style="text-align:right;"> 0.3043788 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:right;"> 0.0547330 </td>
   <td style="text-align:right;"> 0.0539081 </td>
   <td style="text-align:right;"> 0.0479923 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:right;"> 0.0333582 </td>
   <td style="text-align:right;"> 0.0406222 </td>
   <td style="text-align:right;"> 0.0474507 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:right;"> 0.0007760 </td>
   <td style="text-align:right;"> 0.0027673 </td>
   <td style="text-align:right;"> 0.0060445 </td>
  </tr>
</tbody>
</table>

## Wheat

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Fits for the models that compare amount of mineral in soil with amount of mineral in crop.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquared </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquaredRoot </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> rSquaredLog </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 10cm_Na </td>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:right;"> 0.0058116 </td>
   <td style="text-align:right;"> 0.0071375 </td>
   <td style="text-align:right;"> 0.0086775 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Mg </td>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:right;"> 0.2149312 </td>
   <td style="text-align:right;"> 0.2813030 </td>
   <td style="text-align:right;"> 0.3649370 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Al </td>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:right;"> 0.0013769 </td>
   <td style="text-align:right;"> 0.0006535 </td>
   <td style="text-align:right;"> 0.0002109 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Si </td>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:right;"> 0.1819682 </td>
   <td style="text-align:right;"> 0.1697615 </td>
   <td style="text-align:right;"> 0.1568182 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_P </td>
   <td style="text-align:left;"> P </td>
   <td style="text-align:right;"> 0.0083190 </td>
   <td style="text-align:right;"> 0.0057970 </td>
   <td style="text-align:right;"> 0.0033930 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_S </td>
   <td style="text-align:left;"> S </td>
   <td style="text-align:right;"> 0.0049929 </td>
   <td style="text-align:right;"> 0.0000411 </td>
   <td style="text-align:right;"> 0.0037251 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_K </td>
   <td style="text-align:left;"> K </td>
   <td style="text-align:right;"> 0.1712746 </td>
   <td style="text-align:right;"> 0.1699691 </td>
   <td style="text-align:right;"> 0.1677281 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Ca </td>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:right;"> 0.4050613 </td>
   <td style="text-align:right;"> 0.4385893 </td>
   <td style="text-align:right;"> 0.4539869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Mn </td>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:right;"> 0.0652509 </td>
   <td style="text-align:right;"> 0.0553668 </td>
   <td style="text-align:right;"> 0.0440852 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 10cm_Fe </td>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:right;"> 0.0540364 </td>
   <td style="text-align:right;"> 0.0688273 </td>
   <td style="text-align:right;"> 0.0820709 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Na </td>
   <td style="text-align:left;"> Na </td>
   <td style="text-align:right;"> 0.0016957 </td>
   <td style="text-align:right;"> 0.0011650 </td>
   <td style="text-align:right;"> 0.0007461 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Mg </td>
   <td style="text-align:left;"> Mg </td>
   <td style="text-align:right;"> 0.6601667 </td>
   <td style="text-align:right;"> 0.6792224 </td>
   <td style="text-align:right;"> 0.6218438 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Al </td>
   <td style="text-align:left;"> Al </td>
   <td style="text-align:right;"> 0.0018142 </td>
   <td style="text-align:right;"> 0.0040667 </td>
   <td style="text-align:right;"> 0.0071389 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Si </td>
   <td style="text-align:left;"> Si </td>
   <td style="text-align:right;"> 0.2044355 </td>
   <td style="text-align:right;"> 0.1977323 </td>
   <td style="text-align:right;"> 0.1899385 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_P </td>
   <td style="text-align:left;"> P </td>
   <td style="text-align:right;"> 0.0349192 </td>
   <td style="text-align:right;"> 0.0334635 </td>
   <td style="text-align:right;"> 0.0291080 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_S </td>
   <td style="text-align:left;"> S </td>
   <td style="text-align:right;"> 0.0356053 </td>
   <td style="text-align:right;"> 0.0194195 </td>
   <td style="text-align:right;"> 0.0073531 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_K </td>
   <td style="text-align:left;"> K </td>
   <td style="text-align:right;"> 0.1199973 </td>
   <td style="text-align:right;"> 0.1187245 </td>
   <td style="text-align:right;"> 0.1166824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Ca </td>
   <td style="text-align:left;"> Ca </td>
   <td style="text-align:right;"> 0.5068322 </td>
   <td style="text-align:right;"> 0.5034467 </td>
   <td style="text-align:right;"> 0.4887772 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Mn </td>
   <td style="text-align:left;"> Mn </td>
   <td style="text-align:right;"> 0.0855827 </td>
   <td style="text-align:right;"> 0.0767459 </td>
   <td style="text-align:right;"> 0.0641920 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 20cm_Fe </td>
   <td style="text-align:left;"> Fe </td>
   <td style="text-align:right;"> 0.0481747 </td>
   <td style="text-align:right;"> 0.0671057 </td>
   <td style="text-align:right;"> 0.0823577 </td>
  </tr>
</tbody>
</table>


# Results by Explained Variable

##  Antioxidants

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for Antioxidants</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 3406.862 </td>
   <td style="text-align:right;"> 146.287 </td>
   <td style="text-align:right;"> 23.289 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -1213.572 </td>
   <td style="text-align:right;"> 218.001 </td>
   <td style="text-align:right;"> -5.567 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 2505.297 </td>
   <td style="text-align:right;"> 235.113 </td>
   <td style="text-align:right;"> 10.656 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -564.186 </td>
   <td style="text-align:right;"> 305.356 </td>
   <td style="text-align:right;"> -1.848 </td>
   <td style="text-align:right;"> 0.066 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 691.988 </td>
   <td style="text-align:right;"> 395.751 </td>
   <td style="text-align:right;"> 1.749 </td>
   <td style="text-align:right;"> 0.081 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -412.332 </td>
   <td style="text-align:right;"> 476.063 </td>
   <td style="text-align:right;"> -0.866 </td>
   <td style="text-align:right;"> 0.387 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-3-1.png)<!-- -->


##  Antioxidants

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for Antioxidants</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 3406.862 </td>
   <td style="text-align:right;"> 146.287 </td>
   <td style="text-align:right;"> 23.289 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -1213.572 </td>
   <td style="text-align:right;"> 218.001 </td>
   <td style="text-align:right;"> -5.567 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 2505.297 </td>
   <td style="text-align:right;"> 235.113 </td>
   <td style="text-align:right;"> 10.656 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -564.186 </td>
   <td style="text-align:right;"> 305.356 </td>
   <td style="text-align:right;"> -1.848 </td>
   <td style="text-align:right;"> 0.066 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 691.988 </td>
   <td style="text-align:right;"> 395.751 </td>
   <td style="text-align:right;"> 1.749 </td>
   <td style="text-align:right;"> 0.081 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -412.332 </td>
   <td style="text-align:right;"> 476.063 </td>
   <td style="text-align:right;"> -0.866 </td>
   <td style="text-align:right;"> 0.387 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-4-1.png)<!-- -->


##  Proteins

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for Proteins</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 10.035 </td>
   <td style="text-align:right;"> 0.255 </td>
   <td style="text-align:right;"> 39.377 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -2.753 </td>
   <td style="text-align:right;"> 0.380 </td>
   <td style="text-align:right;"> -7.249 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 2.237 </td>
   <td style="text-align:right;"> 0.410 </td>
   <td style="text-align:right;"> 5.461 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> 0.834 </td>
   <td style="text-align:right;"> 0.532 </td>
   <td style="text-align:right;"> 1.567 </td>
   <td style="text-align:right;"> 0.118 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -0.128 </td>
   <td style="text-align:right;"> 0.689 </td>
   <td style="text-align:right;"> -0.186 </td>
   <td style="text-align:right;"> 0.853 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -1.950 </td>
   <td style="text-align:right;"> 0.829 </td>
   <td style="text-align:right;"> -2.351 </td>
   <td style="text-align:right;"> 0.019 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-5-1.png)<!-- -->


##  Polyphenols

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for Polyphenols</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 128.563 </td>
   <td style="text-align:right;"> 3.273 </td>
   <td style="text-align:right;"> 39.281 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -45.837 </td>
   <td style="text-align:right;"> 4.877 </td>
   <td style="text-align:right;"> -9.398 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 50.358 </td>
   <td style="text-align:right;"> 5.260 </td>
   <td style="text-align:right;"> 9.573 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -15.551 </td>
   <td style="text-align:right;"> 6.832 </td>
   <td style="text-align:right;"> -2.276 </td>
   <td style="text-align:right;"> 0.023 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 6.865 </td>
   <td style="text-align:right;"> 8.854 </td>
   <td style="text-align:right;"> 0.775 </td>
   <td style="text-align:right;"> 0.439 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 10.089 </td>
   <td style="text-align:right;"> 10.651 </td>
   <td style="text-align:right;"> 0.947 </td>
   <td style="text-align:right;"> 0.344 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-6-1.png)<!-- -->


##  organic_carbon_pecentage_10cm


```
## NULL
```

```
## NULL
```


##  organic_carbon_pecentage_20cm


```
## NULL
```

```
## NULL
```

##  respiration_soil_10cm

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for respiration_soil_10cm</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 20.325 </td>
   <td style="text-align:right;"> 1.192 </td>
   <td style="text-align:right;"> 17.048 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 3.908 </td>
   <td style="text-align:right;"> 1.767 </td>
   <td style="text-align:right;"> 2.211 </td>
   <td style="text-align:right;"> 0.028 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> -2.868 </td>
   <td style="text-align:right;"> 1.928 </td>
   <td style="text-align:right;"> -1.488 </td>
   <td style="text-align:right;"> 0.138 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> 3.279 </td>
   <td style="text-align:right;"> 2.395 </td>
   <td style="text-align:right;"> 1.369 </td>
   <td style="text-align:right;"> 0.172 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -3.383 </td>
   <td style="text-align:right;"> 3.212 </td>
   <td style="text-align:right;"> -1.053 </td>
   <td style="text-align:right;"> 0.293 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -2.414 </td>
   <td style="text-align:right;"> 3.827 </td>
   <td style="text-align:right;"> -0.631 </td>
   <td style="text-align:right;"> 0.529 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-9-1.png)<!-- -->


##  respiration_soil_20cm

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for respiration_soil_20cm</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 14.783 </td>
   <td style="text-align:right;"> 0.785 </td>
   <td style="text-align:right;"> 18.820 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -0.660 </td>
   <td style="text-align:right;"> 1.182 </td>
   <td style="text-align:right;"> -0.558 </td>
   <td style="text-align:right;"> 0.577 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> -2.058 </td>
   <td style="text-align:right;"> 1.310 </td>
   <td style="text-align:right;"> -1.571 </td>
   <td style="text-align:right;"> 0.117 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> 1.553 </td>
   <td style="text-align:right;"> 1.619 </td>
   <td style="text-align:right;"> 0.959 </td>
   <td style="text-align:right;"> 0.338 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 0.278 </td>
   <td style="text-align:right;"> 2.156 </td>
   <td style="text-align:right;"> 0.129 </td>
   <td style="text-align:right;"> 0.898 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -4.864 </td>
   <td style="text-align:right;"> 2.590 </td>
   <td style="text-align:right;"> -1.878 </td>
   <td style="text-align:right;"> 0.061 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-10-1.png)<!-- -->


##  produce_xrf_Na

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Na</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 223.817 </td>
   <td style="text-align:right;"> 4.893 </td>
   <td style="text-align:right;"> 45.738 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 22.424 </td>
   <td style="text-align:right;"> 6.884 </td>
   <td style="text-align:right;"> 3.257 </td>
   <td style="text-align:right;"> 0.001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 3.155 </td>
   <td style="text-align:right;"> 7.088 </td>
   <td style="text-align:right;"> 0.445 </td>
   <td style="text-align:right;"> 0.657 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -16.828 </td>
   <td style="text-align:right;"> 11.683 </td>
   <td style="text-align:right;"> -1.440 </td>
   <td style="text-align:right;"> 0.151 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 12.222 </td>
   <td style="text-align:right;"> 14.199 </td>
   <td style="text-align:right;"> 0.861 </td>
   <td style="text-align:right;"> 0.391 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 4.568 </td>
   <td style="text-align:right;"> 12.921 </td>
   <td style="text-align:right;"> 0.354 </td>
   <td style="text-align:right;"> 0.724 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-11-1.png)<!-- -->


##  produce_xrf_Mg

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Mg</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 485.527 </td>
   <td style="text-align:right;"> 48.416 </td>
   <td style="text-align:right;"> 10.028 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -11.672 </td>
   <td style="text-align:right;"> 68.114 </td>
   <td style="text-align:right;"> -0.171 </td>
   <td style="text-align:right;"> 0.864 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 388.514 </td>
   <td style="text-align:right;"> 70.132 </td>
   <td style="text-align:right;"> 5.540 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -3.746 </td>
   <td style="text-align:right;"> 115.593 </td>
   <td style="text-align:right;"> -0.032 </td>
   <td style="text-align:right;"> 0.974 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 62.727 </td>
   <td style="text-align:right;"> 140.490 </td>
   <td style="text-align:right;"> 0.446 </td>
   <td style="text-align:right;"> 0.656 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -160.321 </td>
   <td style="text-align:right;"> 127.844 </td>
   <td style="text-align:right;"> -1.254 </td>
   <td style="text-align:right;"> 0.211 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

##  produce_xrf_Al

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Al</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 42.284 </td>
   <td style="text-align:right;"> 0.884 </td>
   <td style="text-align:right;"> 47.827 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 0.102 </td>
   <td style="text-align:right;"> 1.244 </td>
   <td style="text-align:right;"> 0.082 </td>
   <td style="text-align:right;"> 0.935 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 13.161 </td>
   <td style="text-align:right;"> 1.281 </td>
   <td style="text-align:right;"> 10.277 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -4.120 </td>
   <td style="text-align:right;"> 2.111 </td>
   <td style="text-align:right;"> -1.952 </td>
   <td style="text-align:right;"> 0.052 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 1.470 </td>
   <td style="text-align:right;"> 2.565 </td>
   <td style="text-align:right;"> 0.573 </td>
   <td style="text-align:right;"> 0.567 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 1.116 </td>
   <td style="text-align:right;"> 2.334 </td>
   <td style="text-align:right;"> 0.478 </td>
   <td style="text-align:right;"> 0.633 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-13-1.png)<!-- -->


##  produce_xrf_Si

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Si</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 1750.274 </td>
   <td style="text-align:right;"> 50.656 </td>
   <td style="text-align:right;"> 34.552 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -277.745 </td>
   <td style="text-align:right;"> 71.264 </td>
   <td style="text-align:right;"> -3.897 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 371.141 </td>
   <td style="text-align:right;"> 73.376 </td>
   <td style="text-align:right;"> 5.058 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> 125.912 </td>
   <td style="text-align:right;"> 120.939 </td>
   <td style="text-align:right;"> 1.041 </td>
   <td style="text-align:right;"> 0.299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -161.341 </td>
   <td style="text-align:right;"> 146.988 </td>
   <td style="text-align:right;"> -1.098 </td>
   <td style="text-align:right;"> 0.274 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 73.830 </td>
   <td style="text-align:right;"> 133.757 </td>
   <td style="text-align:right;"> 0.552 </td>
   <td style="text-align:right;"> 0.582 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-14-1.png)<!-- -->


##  produce_xrf_P

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_P</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 2527.191 </td>
   <td style="text-align:right;"> 152.202 </td>
   <td style="text-align:right;"> 16.604 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -495.632 </td>
   <td style="text-align:right;"> 214.123 </td>
   <td style="text-align:right;"> -2.315 </td>
   <td style="text-align:right;"> 0.022 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 1721.638 </td>
   <td style="text-align:right;"> 220.469 </td>
   <td style="text-align:right;"> 7.809 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -287.893 </td>
   <td style="text-align:right;"> 363.378 </td>
   <td style="text-align:right;"> -0.792 </td>
   <td style="text-align:right;"> 0.429 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 137.912 </td>
   <td style="text-align:right;"> 441.647 </td>
   <td style="text-align:right;"> 0.312 </td>
   <td style="text-align:right;"> 0.755 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -566.416 </td>
   <td style="text-align:right;"> 401.892 </td>
   <td style="text-align:right;"> -1.409 </td>
   <td style="text-align:right;"> 0.160 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-15-1.png)<!-- -->

##  produce_xrf_S

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_S</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 1719.066 </td>
   <td style="text-align:right;"> 34.199 </td>
   <td style="text-align:right;"> 50.267 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -211.046 </td>
   <td style="text-align:right;"> 48.112 </td>
   <td style="text-align:right;"> -4.387 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 373.162 </td>
   <td style="text-align:right;"> 49.538 </td>
   <td style="text-align:right;"> 7.533 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -73.074 </td>
   <td style="text-align:right;"> 81.649 </td>
   <td style="text-align:right;"> -0.895 </td>
   <td style="text-align:right;"> 0.372 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -151.455 </td>
   <td style="text-align:right;"> 99.235 </td>
   <td style="text-align:right;"> -1.526 </td>
   <td style="text-align:right;"> 0.129 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 75.947 </td>
   <td style="text-align:right;"> 90.302 </td>
   <td style="text-align:right;"> 0.841 </td>
   <td style="text-align:right;"> 0.401 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-16-1.png)<!-- -->


##  Antioxidants

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_K</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 3848.637 </td>
   <td style="text-align:right;"> 195.956 </td>
   <td style="text-align:right;"> 19.640 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -76.680 </td>
   <td style="text-align:right;"> 275.677 </td>
   <td style="text-align:right;"> -0.278 </td>
   <td style="text-align:right;"> 0.781 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 3277.660 </td>
   <td style="text-align:right;"> 283.847 </td>
   <td style="text-align:right;"> 11.547 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -1205.145 </td>
   <td style="text-align:right;"> 467.839 </td>
   <td style="text-align:right;"> -2.576 </td>
   <td style="text-align:right;"> 0.011 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 342.469 </td>
   <td style="text-align:right;"> 568.608 </td>
   <td style="text-align:right;"> 0.602 </td>
   <td style="text-align:right;"> 0.548 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 966.183 </td>
   <td style="text-align:right;"> 517.425 </td>
   <td style="text-align:right;"> 1.867 </td>
   <td style="text-align:right;"> 0.063 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-17-1.png)<!-- -->


##  produce_xrf_Ca

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Ca</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 942.778 </td>
   <td style="text-align:right;"> 26.307 </td>
   <td style="text-align:right;"> 35.837 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> -69.204 </td>
   <td style="text-align:right;"> 37.009 </td>
   <td style="text-align:right;"> -1.870 </td>
   <td style="text-align:right;"> 0.063 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 456.242 </td>
   <td style="text-align:right;"> 38.106 </td>
   <td style="text-align:right;"> 11.973 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -68.950 </td>
   <td style="text-align:right;"> 62.807 </td>
   <td style="text-align:right;"> -1.098 </td>
   <td style="text-align:right;"> 0.274 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -70.588 </td>
   <td style="text-align:right;"> 76.335 </td>
   <td style="text-align:right;"> -0.925 </td>
   <td style="text-align:right;"> 0.356 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 8.395 </td>
   <td style="text-align:right;"> 69.464 </td>
   <td style="text-align:right;"> 0.121 </td>
   <td style="text-align:right;"> 0.904 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-18-1.png)<!-- -->


##  produce_xrf_Mn

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Mn</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 46.402 </td>
   <td style="text-align:right;"> 1.814 </td>
   <td style="text-align:right;"> 25.581 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 9.296 </td>
   <td style="text-align:right;"> 2.552 </td>
   <td style="text-align:right;"> 3.643 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 33.542 </td>
   <td style="text-align:right;"> 2.628 </td>
   <td style="text-align:right;"> 12.765 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -4.466 </td>
   <td style="text-align:right;"> 4.331 </td>
   <td style="text-align:right;"> -1.031 </td>
   <td style="text-align:right;"> 0.304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 2.675 </td>
   <td style="text-align:right;"> 5.264 </td>
   <td style="text-align:right;"> 0.508 </td>
   <td style="text-align:right;"> 0.612 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 1.104 </td>
   <td style="text-align:right;"> 4.790 </td>
   <td style="text-align:right;"> 0.230 </td>
   <td style="text-align:right;"> 0.818 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-19-1.png)<!-- -->


##  produce_xrf_Fe

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Fe</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 61.425 </td>
   <td style="text-align:right;"> 2.539 </td>
   <td style="text-align:right;"> 24.197 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 8.664 </td>
   <td style="text-align:right;"> 3.571 </td>
   <td style="text-align:right;"> 2.426 </td>
   <td style="text-align:right;"> 0.016 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 32.819 </td>
   <td style="text-align:right;"> 3.677 </td>
   <td style="text-align:right;"> 8.925 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -3.620 </td>
   <td style="text-align:right;"> 6.061 </td>
   <td style="text-align:right;"> -0.597 </td>
   <td style="text-align:right;"> 0.551 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> -2.552 </td>
   <td style="text-align:right;"> 7.366 </td>
   <td style="text-align:right;"> -0.346 </td>
   <td style="text-align:right;"> 0.729 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> 0.313 </td>
   <td style="text-align:right;"> 6.703 </td>
   <td style="text-align:right;"> 0.047 </td>
   <td style="text-align:right;"> 0.963 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-20-1.png)<!-- -->


##  produce_xrf_Cu

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Cu</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 9.173 </td>
   <td style="text-align:right;"> 0.422 </td>
   <td style="text-align:right;"> 21.747 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 2.298 </td>
   <td style="text-align:right;"> 0.593 </td>
   <td style="text-align:right;"> 3.873 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 1.427 </td>
   <td style="text-align:right;"> 0.611 </td>
   <td style="text-align:right;"> 2.335 </td>
   <td style="text-align:right;"> 0.021 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -0.679 </td>
   <td style="text-align:right;"> 1.007 </td>
   <td style="text-align:right;"> -0.675 </td>
   <td style="text-align:right;"> 0.501 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 1.151 </td>
   <td style="text-align:right;"> 1.224 </td>
   <td style="text-align:right;"> 0.940 </td>
   <td style="text-align:right;"> 0.348 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -0.521 </td>
   <td style="text-align:right;"> 1.114 </td>
   <td style="text-align:right;"> -0.468 </td>
   <td style="text-align:right;"> 0.640 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-21-1.png)<!-- -->

##  produce_xrf_Zn

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA over tillage, covercrops and species for produce_xrf_Zn</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> statistic </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> (Intercept) </td>
   <td style="text-align:right;"> 9.011 </td>
   <td style="text-align:right;"> 1.672 </td>
   <td style="text-align:right;"> 5.391 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat </td>
   <td style="text-align:right;"> 5.304 </td>
   <td style="text-align:right;"> 2.352 </td>
   <td style="text-align:right;"> 2.256 </td>
   <td style="text-align:right;"> 0.025 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE </td>
   <td style="text-align:right;"> 13.484 </td>
   <td style="text-align:right;"> 2.421 </td>
   <td style="text-align:right;"> 5.569 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercropsTRUE </td>
   <td style="text-align:right;"> -3.676 </td>
   <td style="text-align:right;"> 3.991 </td>
   <td style="text-align:right;"> -0.921 </td>
   <td style="text-align:right;"> 0.358 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Typewheat:covercropsTRUE </td>
   <td style="text-align:right;"> 0.520 </td>
   <td style="text-align:right;"> 4.850 </td>
   <td style="text-align:right;"> 0.107 </td>
   <td style="text-align:right;"> 0.915 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notillTRUE:covercropsTRUE </td>
   <td style="text-align:right;"> -0.832 </td>
   <td style="text-align:right;"> 4.414 </td>
   <td style="text-align:right;"> -0.189 </td>
   <td style="text-align:right;"> 0.851 </td>
  </tr>
</tbody>
</table>

![](nutrientsANOVA_files/figure-html/unnamed-chunk-22-1.png)<!-- -->
