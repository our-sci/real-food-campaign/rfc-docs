---
title: "2020 Survey, Grain Samples Data Analysis"
author: "Real Food Campaign"
date: "05/30/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---








![](grainBOAFinalReport_files/figure-html/medianShiftsLib-1.png)<!-- -->

# Some Summary Statistics

## General: Variation and ratios between maximum and mimimum value





### Censoring The More Evident Outliers





* The following table has separate values for the outliers stripped values, when outliers exists. If the column is empty, that means there's no outliers.
* This table is saved as a file for further processing by any user: `BFA-Extremes-Table.csv`.


<!--html_preserve--><div id="htmlwidget-f9016a2e485bd0a20511" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-f9016a2e485bd0a20511">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102"],["Antioxidants_max","Antioxidants_median","Antioxidants_min","Antioxidants_range","Antioxidants_ratio","Antioxidants_sd","Polyphenols_max","Polyphenols_median","Polyphenols_min","Polyphenols_range","Polyphenols_ratio","Polyphenols_sd","produce_xrf_Al_max","produce_xrf_Al_median","produce_xrf_Al_min","produce_xrf_Al_range","produce_xrf_Al_ratio","produce_xrf_Al_sd","produce_xrf_Ca_max","produce_xrf_Ca_median","produce_xrf_Ca_min","produce_xrf_Ca_range","produce_xrf_Ca_ratio","produce_xrf_Ca_sd","produce_xrf_Cu_max","produce_xrf_Cu_median","produce_xrf_Cu_min","produce_xrf_Cu_range","produce_xrf_Cu_ratio","produce_xrf_Cu_sd","produce_xrf_Fe_max","produce_xrf_Fe_median","produce_xrf_Fe_min","produce_xrf_Fe_range","produce_xrf_Fe_ratio","produce_xrf_Fe_sd","produce_xrf_K_max","produce_xrf_K_median","produce_xrf_K_min","produce_xrf_K_range","produce_xrf_K_ratio","produce_xrf_K_sd","produce_xrf_Mg_max","produce_xrf_Mg_median","produce_xrf_Mg_min","produce_xrf_Mg_range","produce_xrf_Mg_ratio","produce_xrf_Mg_sd","produce_xrf_Mn_max","produce_xrf_Mn_median","produce_xrf_Mn_min","produce_xrf_Mn_range","produce_xrf_Mn_ratio","produce_xrf_Mn_sd","produce_xrf_Mo_max","produce_xrf_Mo_median","produce_xrf_Mo_min","produce_xrf_Mo_range","produce_xrf_Mo_ratio","produce_xrf_Mo_sd","produce_xrf_Na_max","produce_xrf_Na_median","produce_xrf_Na_min","produce_xrf_Na_range","produce_xrf_Na_ratio","produce_xrf_Na_sd","produce_xrf_Ni_max","produce_xrf_Ni_median","produce_xrf_Ni_min","produce_xrf_Ni_range","produce_xrf_Ni_ratio","produce_xrf_Ni_sd","produce_xrf_P_max","produce_xrf_P_median","produce_xrf_P_min","produce_xrf_P_range","produce_xrf_P_ratio","produce_xrf_P_sd","produce_xrf_S_max","produce_xrf_S_median","produce_xrf_S_min","produce_xrf_S_range","produce_xrf_S_ratio","produce_xrf_S_sd","produce_xrf_Si_max","produce_xrf_Si_median","produce_xrf_Si_min","produce_xrf_Si_range","produce_xrf_Si_ratio","produce_xrf_Si_sd","produce_xrf_Zn_max","produce_xrf_Zn_median","produce_xrf_Zn_min","produce_xrf_Zn_range","produce_xrf_Zn_ratio","produce_xrf_Zn_sd","Proteins_max","Proteins_median","Proteins_min","Proteins_range","Proteins_ratio","Proteins_sd"],[13158.11,2842.48,962.17,12195.94,13.6754523628881,2596.04842588298,329.93,91.85,18.2,311.73,18.128021978022,45.3392367204366,73.5345159916,45.7972402563,31.8450797546,41.689436237,2.30913273128098,9.01806247510855,1518.4811165076,941.817737777,644.685110345,873.7960061626,2.35538419011258,202.080743505092,15.531011201,10.1998689661,4.7487291383,10.7822820627,3.27056160683866,2.37116238528127,165.1656704476,70.9060859296,38.4797311312,126.6859393164,4.29227714415294,13.5803011050823,7932.1156932099,4230.7314762795,1799.7784833613,6132.3372098486,4.40727332087876,1389.54667599063,1844.084031052,755.5765141705,30.5909136514,1813.4931174006,60.2820841530375,429.420719415745,75.4078308262,46.7101992102,26.6972285607,48.7106022655,2.82455651360026,9.30870933525405,0.1873060934,0.11369788455,0.0374958905,0.1498102029,4.99537658400192,0.0616770712896581,298.6505337227,230.7819571888,118.3219060637,180.328627659,2.52405107099879,28.051364789672,9.900820767,9.26628407895,8.1121160573,1.7887047097,1.2204979190467,0.757327246506314,9494.3872860148,3573.2075847688,71.9577767204,9422.4295092944,131.943866510861,1718.44925257936,2929.2867601458,1912.6620819342,905.6372774157,2023.6494827301,3.2345032975064,269.275224841047,8055.2020829213,1830.8612126596,1024.7041133945,7030.4979695268,7.86100297405573,977.463947709581,40.7190817746,19.7828492737,0.701834763,40.0172470116,58.018046299881,9.18126831206841,17.18,9.97,3.16,14.02,5.43670886075949,2.96730973547032],[6853.93,3255.3,628.38,6225.55,10.9073013144912,1718.44474930054,268.77,106.2,27.86,240.91,9.64716439339555,38.2575451533675,72.9789257694,49.7446677655,32.6810732241,40.2978525453,2.23306392874464,9.12295788055022,1729.8377747815,1043.3924990518,505.4450739196,1224.3927008619,3.42240505257484,309.47508971795,15.9891360591,13.5305128746,3.6906714528,12.2984646063,4.33231087177092,3.08890731298958,143.5440368943,85.5364557877,37.6232995653,105.920737329,3.81529633372961,25.7343700179946,9805.4171911303,5338.2299173502,1536.138163797,8269.2790273333,6.38316098266411,2198.40137401694,1617.3618424186,701.5369457086,37.7114733734,1579.6503690452,42.8877924339974,370.645116467101,122.5885288315,68.242959154,35.7995190672,86.7890097643,3.42430658359925,21.723881480476,null,null,null,null,null,null,301.5874540607,253.1955260521,166.3451656693,135.2422883914,1.81302205475731,34.6656616336635,null,null,null,null,null,null,5255.0519155474,3030.7911570521,98.2047956505,5156.8471198969,53.511153714423,1386.30609569001,2248.2619835045,1719.6809732582,952.1925708541,1296.0694126504,2.3611421180149,337.14493155841,2770.703459548,1612.8883507943,973.1345211333,1797.5689384147,2.84719470882738,356.90766465001,47.2993717174,24.0384770443,0.2042105481,47.0951611693,231.62060999042,11.1242766805053,14.6,8.485,2.79,11.81,5.23297491039427,2.5669503588255],[10395.66,2806.97,null,9433.49,10.8043900765977,2361.51102940993,193.48,91.64,null,175.28,10.6307692307692,43.5712912110939,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,5720.7017021529,1821.5893466497,null,4695.9975887584,5.58278397380698,782.071072176051,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,143.49,105.88,null,115.63,5.15039483129935,32.2106698479182,null,null,null,null,null,null,1588.9896399931,1041.1231085477,null,1083.5445660735,3.14374344905745,305.011438729013,null,null,null,null,null,null,null,null,null,null,null,null,8653.115023274,5313.6619990733,null,7116.976859477,5.63303173321687,2169.7505355373,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>variable<\/th>\n      <th>oats<\/th>\n      <th>wheat<\/th>\n      <th>oats_w/o_outliers<\/th>\n      <th>wheat_w/o_outliers<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


![](grainBOAFinalReport_files/figure-html/preliminaryhistograms-1.png)<!-- -->

# Farm Practices: Median Shifts Comparisons







# Synthesis Median Shift Tables.

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is intersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting magnitudes are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.




## Color Scale

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>



# Percentual Shift Over Crop's Median Values for Farm Practices


* The reference category used to define the neutral median is defined as *Farms t
hat are neither organic, transitioning, regenerative, no till or sowing cover crops*.
* There's plenty of columns prefixed by *farm_practices* on the dataset, but they are not mutually related in a sound logical way, that's why calculations can't be easily performed over the whole set. 


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-14.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">16.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-3.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">3.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-63.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-15.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-13.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">55.17</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Covercrops </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">-25.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">-11.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">6.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-10.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-26.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-4.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">2.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-8.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-49.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">-9.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 68">0.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">-10.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-23.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-15.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-16.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-21.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-8.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-23.73</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 84">49.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 79">38.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 84">10.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 57">11.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 57">4.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 56">10.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 56">18.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 57">21.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 57">35.35</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 100">-20.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 100">-7.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 100">3.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-31.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-16.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-6.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">9.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-3.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-49.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-35.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-9.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-15.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-42.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-23.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-55.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-24.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-24.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-48.12</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 82">-20.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 82">-5.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 82">6.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">-21.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">-17.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">-6.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">51.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">2.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">-49.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 75">-34.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 75">-10.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 75">-11.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-33.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-16.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-55.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-21.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-23.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-26.59</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 97">-24.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 97">-9.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 97">6.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">-26.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">-14.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">-5.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">33.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">0.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">-49.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 105">-28.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 105">-5.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 105">-14.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-39.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-16.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-55.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-25.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-18.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 60">-41.82</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Transitioning </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 96">-20.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 96">-4.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 96">3.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">-27.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">-16.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">-6.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">13.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">0.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">-49.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">-33.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">-9.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">-20.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-42.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-28.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-57.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-27.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-20.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-57.55</span> </td>
  </tr>
</tbody>
</table>

## Visualizing some clusterization

![](grainBOAFinalReport_files/figure-html/practicesClusters-1.png)<!-- -->![](grainBOAFinalReport_files/figure-html/practicesClusters-2.png)<!-- -->![](grainBOAFinalReport_files/figure-html/practicesClusters-3.png)<!-- -->![](grainBOAFinalReport_files/figure-html/practicesClusters-4.png)<!-- -->![](grainBOAFinalReport_files/figure-html/practicesClusters-5.png)<!-- -->![](grainBOAFinalReport_files/figure-html/practicesClusters-6.png)<!-- -->

# Percentual Shift Over Crop's Median Values for each Soil Amendment


* The reference category is farms that don't mention any soil amendment on the submitted information ( `FALSE` value on every dataset column prefixed by *amendments* ).


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">32.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">12.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">2.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">7.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-9.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">1.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-29.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">1.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-19.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">32.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-27.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-35.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-5.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-10.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">1.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-11.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-37.69</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Synth Fertilizer </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 36">14.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 36">8.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 36">7.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 111">44.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 106">66.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 111">-8.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">-5.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">13.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 68">51.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 68">48.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">26.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">49.29</span> </td>
  </tr>
</tbody>
</table>


# Percentual Shift Over Crop's Median Values for each Seed Treatment


* The reference category is farms that don't mention any seed treatment on the submitted information ( `FALSE` value on every dataset column prefixed by *seed_treatment* ).


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">16.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">11.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">-1.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">-58.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">9.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">-20.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Fungicide </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 102">65.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 97">52.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 102">1.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 63">30.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 63">20.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 62">107.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 62">57.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 63">43.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 63">69.81</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Gold Package </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">113.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">84.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">19.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Insecticide </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">-33.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">18.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">-18.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">-19.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">7.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">77.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">41.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">29.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">25.16</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Standard Package </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">35.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">10.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">46.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">22.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">108.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">44.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">48.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">85.97</span> </td>
  </tr>
</tbody>
</table>


# Percentual Shift Over Crop's Median Values for each Tillage Practice


* The reference category is farms that inform *notill* as a practice.
* *Light tillage* is exiting tillage, with an informed depth of less than 6 inches.
* *Heavy tillage* is tillage with a depth superior or equal to 6 inches.


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-73.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-37.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-37.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-63.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-25.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-62.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-43.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-45.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-57.22</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-53.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-42.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-17.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-47.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-21.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-56.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-38.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-29.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 45">-67.93</span> </td>
  </tr>
</tbody>
</table>

# Percentual Shift Over Crop's Median Values for Companion Cropping (Just in Oats)

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Oats</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="9"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Wheat</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Oats Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Proteins </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Wheat Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> True </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 49">-0.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 49">-4.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 49">-8.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-3.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-0.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-0.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-23.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-9.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">-41.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

# Soil Suborder Influence {.tabset}


## Antioxidants

### Analysis of Variance
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 7631.000 </td>
   <td style="text-align:right;"> 846.753 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 4599.726 </td>
   <td style="text-align:right;"> 399.163 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 3486.672 </td>
   <td style="text-align:right;"> 205.368 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 3384.344 </td>
   <td style="text-align:right;"> 199.582 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 2278.243 </td>
   <td style="text-align:right;"> 691.371 </td>
   <td style="text-align:right;"> 0.001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 1586.971 </td>
   <td style="text-align:right;"> 261.314 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 4647.778 </td>
   <td style="text-align:right;"> 107.862 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 3102.041 </td>
   <td style="text-align:right;"> 142.688 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 2506.171 </td>
   <td style="text-align:right;"> 329.524 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 1383.947 </td>
   <td style="text-align:right;"> 329.524 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 1345.735 </td>
   <td style="text-align:right;"> 403.583 </td>
   <td style="text-align:right;"> 0.001 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 1198.487 </td>
   <td style="text-align:right;"> 255.249 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 935.859 </td>
   <td style="text-align:right;"> 201.792 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

## Polyphenols
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 179.865 </td>
   <td style="text-align:right;"> 18.987 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 154.150 </td>
   <td style="text-align:right;"> 8.951 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 127.379 </td>
   <td style="text-align:right;"> 4.475 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 125.805 </td>
   <td style="text-align:right;"> 4.605 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 102.910 </td>
   <td style="text-align:right;"> 15.503 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 91.780 </td>
   <td style="text-align:right;"> 5.860 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 126.522 </td>
   <td style="text-align:right;"> 2.246 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 106.424 </td>
   <td style="text-align:right;"> 6.654 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 83.138 </td>
   <td style="text-align:right;"> 6.654 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 78.308 </td>
   <td style="text-align:right;"> 8.149 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 77.847 </td>
   <td style="text-align:right;"> 5.154 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 76.403 </td>
   <td style="text-align:right;"> 4.075 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 75.450 </td>
   <td style="text-align:right;"> 2.881 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>


## Proteins
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Soil Suborder in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 12.439 </td>
   <td style="text-align:right;"> 0.468 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 10.877 </td>
   <td style="text-align:right;"> 1.238 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 10.647 </td>
   <td style="text-align:right;"> 0.357 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 8.891 </td>
   <td style="text-align:right;"> 0.368 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 8.805 </td>
   <td style="text-align:right;"> 1.517 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 8.734 </td>
   <td style="text-align:right;"> 0.715 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Soil Suborder in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> 9.858 </td>
   <td style="text-align:right;"> 0.260 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> 9.297 </td>
   <td style="text-align:right;"> 0.196 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> 8.497 </td>
   <td style="text-align:right;"> 0.600 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> 5.983 </td>
   <td style="text-align:right;"> 0.367 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 4.643 </td>
   <td style="text-align:right;"> 0.600 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 4.527 </td>
   <td style="text-align:right;"> 0.465 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> 3.490 </td>
   <td style="text-align:right;"> 0.734 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

 
# Climate Region Influence { .tabset }

## Antioxidants

### Analysis of Variance
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Climate Region in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 4508.300 </td>
   <td style="text-align:right;"> 802.974 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 3770.527 </td>
   <td style="text-align:right;"> 161.676 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 3646.389 </td>
   <td style="text-align:right;"> 439.807 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 2278.243 </td>
   <td style="text-align:right;"> 802.974 </td>
   <td style="text-align:right;"> 0.005 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 1586.971 </td>
   <td style="text-align:right;"> 303.496 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Antioxidants over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 4392.334 </td>
   <td style="text-align:right;"> 114.394 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 3934.676 </td>
   <td style="text-align:right;"> 383.216 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 2906.534 </td>
   <td style="text-align:right;"> 177.394 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 2506.171 </td>
   <td style="text-align:right;"> 383.216 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 1323.144 </td>
   <td style="text-align:right;"> 270.974 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 1116.050 </td>
   <td style="text-align:right;"> 663.749 </td>
   <td style="text-align:right;"> 0.094 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 1024.336 </td>
   <td style="text-align:right;"> 200.128 </td>
   <td style="text-align:right;"> 0.000 </td>
  </tr>
</tbody>
</table>


## Polyphenols
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Oats</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 135.697 </td>
   <td style="text-align:right;"> 16.635 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 133.212 </td>
   <td style="text-align:right;"> 3.372 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 124.963 </td>
   <td style="text-align:right;"> 9.111 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 102.910 </td>
   <td style="text-align:right;"> 16.635 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 91.780 </td>
   <td style="text-align:right;"> 6.287 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 116.929 </td>
   <td style="text-align:right;"> 8.620 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 116.630 </td>
   <td style="text-align:right;"> 2.639 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 106.424 </td>
   <td style="text-align:right;"> 8.620 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 81.030 </td>
   <td style="text-align:right;"> 6.095 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 77.050 </td>
   <td style="text-align:right;"> 4.502 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 72.750 </td>
   <td style="text-align:right;"> 14.931 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 69.934 </td>
   <td style="text-align:right;"> 3.990 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>


## Proteins
 
### Analysis of Variance 
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Polyphenols over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 12.439 </td>
   <td style="text-align:right;"> 0.502 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 11.153 </td>
   <td style="text-align:right;"> 1.328 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 10.877 </td>
   <td style="text-align:right;"> 1.328 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 9.647 </td>
   <td style="text-align:right;"> 0.728 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 9.502 </td>
   <td style="text-align:right;"> 0.267 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>ANOVA for Proteins over Climate Region in Wheat</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> term </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> std.error </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> West North Central </td>
   <td style="text-align:right;"> 9.976 </td>
   <td style="text-align:right;"> 0.281 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northwest </td>
   <td style="text-align:right;"> 9.167 </td>
   <td style="text-align:right;"> 0.181 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> South </td>
   <td style="text-align:right;"> 8.611 </td>
   <td style="text-align:right;"> 0.608 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Southeast </td>
   <td style="text-align:right;"> 8.497 </td>
   <td style="text-align:right;"> 0.608 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Northeast </td>
   <td style="text-align:right;"> 6.370 </td>
   <td style="text-align:right;"> 1.052 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> East North Central </td>
   <td style="text-align:right;"> 5.275 </td>
   <td style="text-align:right;"> 0.317 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Central </td>
   <td style="text-align:right;"> 4.502 </td>
   <td style="text-align:right;"> 0.430 </td>
   <td style="text-align:right;"> 0 </td>
  </tr>
</tbody>
</table>

# Clusterization for Climate Regions and Soil Suborders { .tabset }

### by Climate Region

![](grainBOAFinalReport_files/figure-html/climateClusters-1.png)<!-- -->![](grainBOAFinalReport_files/figure-html/climateClusters-2.png)<!-- -->

### by Soil Suborder

![](grainBOAFinalReport_files/figure-html/suborderClusters-1.png)<!-- -->![](grainBOAFinalReport_files/figure-html/suborderClusters-2.png)<!-- -->




# Random Forest on Antioxidants, Polyphenols and Proteins


  These regressions are performed using a *Random Forest* algorithm. The only tuned parameter besides the selection of initial variables is `mtry`.
  For the classifications, we are using *Accuracy* as a precission metric, while for regressions the custom random forest $R^2$ is used, which is a special metric comparable to the homonym in the context of linear models.


### Compared Datasets


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-vis-who </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> c("metadata", "farmPractices", "soil", "whole", "processed", "nir_whole", "nir_processed") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "soil", "whole", "processed") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "soil", "whole", "nir_whole") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "soil", "whole") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "soil") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "whole", "processed", "nir_whole", "nir_processed") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "whole", "processed") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "whole", "nir_whole") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices", "whole") </td>
   <td style="text-align:left;"> c("metadata", "farmPractices") </td>
   <td style="text-align:left;"> c("metadata", "medFarmPractices", "whole", "processed", "nir_whole", "nir_processed") </td>
   <td style="text-align:left;"> c("metadata", "medFarmPractices", "whole", "processed") </td>
   <td style="text-align:left;"> c("metadata", "medFarmPractices", "whole", "nir_whole") </td>
   <td style="text-align:left;"> c("metadata", "medFarmPractices", "whole") </td>
   <td style="text-align:left;"> c("metadata", "medFarmPractices") </td>
   <td style="text-align:left;"> c("color", "region", "minFarmPractices", "whole", "nir_whole") </td>
   <td style="text-align:left;"> c("color", "region", "minFarmPractices", "whole") </td>
   <td style="text-align:left;"> c("color", "region", "minFarmPractices") </td>
   <td style="text-align:left;"> c("color", "whole", "nir_whole") </td>
   <td style="text-align:left;"> c("color", "whole") </td>
  </tr>
</tbody>
</table>


  


## Random Forest Regression




<!--html_preserve--><div id="htmlwidget-336931f1fbbdf71b49c8" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-336931f1fbbdf71b49c8">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"],["Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins","Proteins"],["c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.664694108668798,0.607064444787639,0.587629210867517,0.264807479372174,0.428473261995967,0.753494059525675,0.633294190642412,0.539935882613446,0.683672878087116,0.746350244127651,0.709730122386867,0.615808370765423,0.52150631526598,0.598249263464066,0.630850967351468,0.545581938346194,0.452685176306501,0.656423299457839,0.540003378856756,0.619652065298864,0.587818876939709,0.372103757490902,0.636428391545095,0.259563615084347,0.318958996516456,0.784309123256852,0.475114291635929,0.734287665029493,0.565476415480264,0.664382288764481,0.775172714825585,0.642023467482318,0.521271650235398,null,null,null,null,null,0.613318284722616,0.789567928979804,0.46760048239894,0.57674497833519,0.64405936010945,0.499594272722676,0.462661363090697,0.596902805103147,0.625384446301487,0.438965575500956,0.574420179102534,0.509182289065813,0.544490559093701,0.527734335113185,0.320051647944878,0.614700740688441,0.631595161888338,0.555782513169787,0.665991918917038,0.412078372648525,0.574076352327284,0.703147020262467,0.35918949278223,0.4151136628259,0.429256246728084,0.185578866269475,0.240564659791205,0.430407016604876,0.55379994792293,0.521386596457097,0.583906123560467,0.497461114321019,0.503726596774162,0.426969407218491,0.365767531913719,0.366045596761218,0.391920795372622,0.435264342674756,0.519514146860739,0.453842467603578,0.510448171745976,0.416137755105626],[111,111,111,363,366,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111,52,52,52,299,302,52,52,52,52,52,52,52,52,null,null,null,null,null,52,52,110,110,110,367,370,110,109,110,110,110,110,109,110,101,100,101,101,101,110,110,111,111,111,370,373,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111],[0.846846846846847,0.846846846846847,0.855855855855856,0.848484848484849,0.814207650273224,0.828828828828829,0.809090909090909,0.747747747747748,0.882882882882883,0.738738738738739,0.720720720720721,0.845454545454545,0.828828828828829,0.833333333333333,0.851485148514851,0.823529411764706,0.833333333333333,0.813725490196078,0.882882882882883,0.864864864864865,0.769230769230769,0.769230769230769,0.711538461538462,0.712374581939799,0.764900662251656,0.769230769230769,0.788461538461538,0.826923076923077,0.788461538461538,0.884615384615385,0.807692307692308,0.75,0.711538461538462,null,null,null,null,null,0.826923076923077,0.788461538461538,0.818181818181818,0.772727272727273,0.809090909090909,0.825613079019074,0.772972972972973,0.763636363636364,0.834862385321101,0.827272727272727,0.827272727272727,0.854545454545454,0.772727272727273,0.825688073394495,0.809090909090909,0.762376237623762,0.83,0.811881188118812,0.871287128712871,0.811881188118812,0.818181818181818,0.845454545454545,0.756756756756757,0.747747747747748,0.846846846846847,0.751351351351351,0.723860589812332,0.720720720720721,0.781818181818182,0.774774774774775,0.774774774774775,0.792792792792793,0.765765765765766,0.781818181818182,0.738738738738739,0.696078431372549,0.762376237623762,0.705882352941177,0.833333333333333,0.745098039215686,0.783783783783784,0.792792792792793],[0.526070227036111,0.306687200526425,0.355806273228028,0.312327781128723,0.224193697136478,0.469657669458407,0.309279935000139,0.186981182787394,0.459516424513351,0.182229458159429,0.321943940709252,0.30998631419277,0.280660996685131,0.436076930919112,0.314169598971865,0.307015289178326,0.355479574782278,0.263237156812165,0.350642429709632,0.599937286255804,0.490850900384439,0.341323035545091,0.171530236581537,0.128275496922309,0.183883908727302,0.32239719730166,0.373009696204669,0.440786588089668,0.226534212688699,0.504454876274023,0.519846558957914,0.367973619677147,0.202177950333518,null,null,null,null,null,0.354771722956714,0.292074199641758,0.523661071143085,0.341260325073274,0.324910071942446,0.309082610680055,0.197598128708352,0.362010391686651,0.357986943778311,0.300992539301892,0.314215294431122,0.52009725552891,0.364575006661338,0.361530775379696,0.414954702904343,0.311683986144418,0.37294830802025,0.389954702904343,0.452504662936318,0.289968025579536,0.31091793232081,0.51232347455369,0.530109489051095,0.379562043795621,0.526459854014598,0.390905848787446,0.33898002853067,0.346715328467153,0.394890510948905,0.373540145985401,0.394160583941606,0.422992700729927,0.346715328467153,0.409808394160584,0.382299270072993,0.354698905109489,0.388138686131387,0.363594890510949,0.421167883211679,0.353056569343066,0.407846715328467,0.41514598540146],[0.611523895326922,0.615009623088878,0.798621242980543,0.47233030173222,0.617354758802696,0.630696324462758,0.76979591299627,0.617256650009171,0.807889211594146,0.788586503392446,0.686466570209358,0.8189059834876,0.704312628629324,0.697401265309698,0.725752237489599,0.660385138990888,0.714144490761945,0.651938903370292,0.798811669882891,0.706867412876095,0.812514044818144,0.796153189110311,0.864332258992651,0.652791549456052,0.814845780541134,0.752212223936074,0.898092701128969,0.867239091952186,0.818337733404491,0.917250171897863,0.807268072327783,0.772193397320353,0.902197393103352,0.951130692006233,0.845508666568491,0.762857751308809,0.817481344024987,0.944485421984189,0.792214863693998,0.9185351257177,0.622509086120791,0.614520244155414,0.561185686910857,0.538853685723063,0.490708814481367,0.688968006434908,0.686997101804441,0.643487608273267,0.716815783491312,0.680348049815266,0.610085590267676,0.667987101577001,0.574867646410617,0.695615871480423,0.590649200963526,0.571038462476399,0.607387503661287,0.52536918659589,0.64861357423681,0.596001276643394,0.634450295791533,0.58985014698139,0.501831708089707,0.463270137701194,0.538079843499079,0.5093107615956,0.600859361534426,0.479758483292773,0.626833784883333,0.614856808209088,0.515093000856841,0.669336132507819,0.646843613698134,0.476678134558795,0.550985596682534,0.43579884548823,0.534052752162387,0.633195514046245,0.558040810355556,0.641467006207193],[215,210,215,225,230,194,187,189,194,194,194,187,189,141,139,141,141,141,194,194,132,130,132,145,147,111,108,109,111,111,111,108,109,87,86,87,87,87,111,111,210,206,210,221,225,190,184,186,190,190,190,184,186,140,138,140,140,140,190,190,215,210,215,225,230,194,187,189,194,194,194,187,189,141,139,141,141,141,194,194],[0.73953488372093,0.771428571428571,0.767441860465116,0.817777777777778,0.78695652173913,0.819587628865979,0.828877005347594,0.793650793650794,0.819587628865979,0.824742268041237,0.84020618556701,0.807486631016043,0.82010582010582,0.879432624113475,0.820143884892086,0.822695035460993,0.773049645390071,0.702127659574468,0.768041237113402,0.77319587628866,0.863636363636364,0.784615384615385,0.886363636363636,0.820689655172414,0.850340136054422,0.747747747747748,0.87037037037037,0.880733944954128,0.774774774774775,0.891891891891892,0.882882882882883,0.833333333333333,0.844036697247706,0.747126436781609,0.837209302325581,0.816091954022989,0.758620689655172,0.747126436781609,0.756756756756757,0.765765765765766,0.838095238095238,0.830097087378641,0.89047619047619,0.823529411764706,0.795555555555556,0.878947368421053,0.815217391304348,0.833333333333333,0.768421052631579,0.773684210526316,0.878947368421053,0.793478260869565,0.790322580645161,0.742857142857143,0.804347826086957,0.807142857142857,0.757142857142857,0.792857142857143,0.868421052631579,0.789473684210526,0.767441860465116,0.776190476190476,0.762790697674419,0.755555555555556,0.782608695652174,0.731958762886598,0.759358288770054,0.761904761904762,0.731958762886598,0.778350515463918,0.824742268041237,0.748663101604278,0.767195767195767,0.815602836879433,0.776978417266187,0.801418439716312,0.737588652482269,0.815602836879433,0.731958762886598,0.809278350515464],[0.419332267831758,0.241847909020087,0.208132936045811,0.281311691336508,0.213281557452755,0.438852792122784,0.255694677578688,0.249612484037555,0.202374087430026,0.293945916425055,0.498268265454458,0.238444796042117,0.252396173832031,0.528191083518725,0.257410670543165,0.289804113692765,0.179799696412365,0.164930006184193,0.18395177936086,0.195886548176466,0.566490491872942,0.189583554658451,0.259555933283756,0.201609476256241,0.226909593115904,0.229179849781275,0.265662915783971,0.26980631138746,0.165928412248604,0.747456461331059,0.602465127795967,0.205060940380224,0.208848047981952,0.225743395332306,0.223862693993603,0.218324250681199,0.131761639616159,0.146191209572325,0.137562935042782,0.15093267669959,0.722412003805241,0.225529706823489,0.660728184727147,0.286915160425495,0.308812591887918,0.460451439937732,0.22671452045317,0.23550981579175,0.210196315834991,0.201653982530485,0.657696964455591,0.215588515091239,0.227043154890599,0.190919311597336,0.277618265156102,0.249595693159215,0.22247686586526,0.242177635561706,0.33893885669809,0.199705958661247,0.364098221845893,0.256308213378493,0.238950042337003,0.24225232853514,0.246824724809483,0.275613886536833,0.262489415749365,0.249872988992379,0.234462320067739,0.257260795935648,0.376121930567316,0.253090601185436,0.253217612193057,0.378292461398728,0.292098092643052,0.29314259763851,0.277020890099909,0.357220708446866,0.234970364098222,0.299618966977138]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_oats<\/th>\n      <th>N_oats<\/th>\n      <th>covers_oats<\/th>\n      <th>width_oats<\/th>\n      <th>cvRsquared_wheat<\/th>\n      <th>N_wheat<\/th>\n      <th>covers_wheat<\/th>\n      <th>width_wheat<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->


### Plots Comparing Actual and Predicted Values, for All Crops, Variables and Models { .tabset }


#### Example: One of the highly accurate regressions

![](grainBOAFinalReport_files/figure-html/unnamed-chunk-8-1.png)<!-- -->


#### Antioxidants { .tabset }


##### farmerWithSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Grain-consumerModels-fitPlot.png)

#### Polyphenols { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Grain-consumerModels-fitPlot.png)

#### Proteins { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-consumerModels-fitPlot.png)


#### Proteins { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Proteins.](./graphics/fitPlots/Proteins-Grain-consumerModels-fitPlot.png)

#### BQI

![](grainBOAFinalReport_files/figure-html/allBQIRegs-1.png)<!-- -->


### Importance Metrics Plots { .tabset }

* This information is stored as `./models/allRFImportancesGrouped.csv` in the gitlab project folder for this analysis.

#### Wheat { .tabset }

##### Farmers with soil data

![](grainBOAFinalReport_files/figure-html/importanceRegWheatFS-1.png)<!-- -->

##### Farmers without soil data

![](grainBOAFinalReport_files/figure-html/importanceRegWheatF-1.png)<!-- -->

##### Farmer-Consumer models 

![](grainBOAFinalReport_files/figure-html/importanceRegWheatFC-1.png)<!-- -->

##### Consumer models 

![](grainBOAFinalReport_files/figure-html/importanceRegWheatC-1.png)<!-- -->

#### Oats { .tabset }


##### Farmers with soil data




##### Farmers without soil data


![](grainBOAFinalReport_files/figure-html/importanceRegOatsF-1.png)<!-- -->

##### Farmer-Consumer models 


![](grainBOAFinalReport_files/figure-html/importanceRegOatsFC-1.png)<!-- -->

##### Consumer models 


![](grainBOAFinalReport_files/figure-html/importanceRegOatsC-1.png)<!-- -->


#### Relations between climate region / soil suborder

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Population by climate region / soil suborder combination</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> soilSuborder </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> East North Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Northeast </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Northwest </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> South </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Southeast </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> West North Central </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> NA </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Udalfs </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udolls </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aqualfs </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Psamments </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 60 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 277 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Xerolls </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 84 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ustolls </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Udults </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

## Random Forest Classification of The Percentile on the Explained Variable


Two targets are generated for each explained variable (Antioxidants, Polyphenols, Proteins). The objective is predicting the quantile were the sample contents belong. Instead of using each quantile, the central ones are meged into a center category.

* Quartiles is three categories, *lower* for the first quartile, *center* for the second and third and *higher* for the fourth.
* Quintiles is also three categories, with *center* composed of the second, third and fourth quintiles.





<!--html_preserve--><div id="htmlwidget-f84ce48565673e6677e2" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-f84ce48565673e6677e2">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"],["antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile","proteinsQuartile"],["c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.780733885997044,0.616514041514042,0.648611111111111,0.606599446858914,0.581428871994676,0.743809523809524,0.593751803751804,0.710468103325246,0.69751875109018,0.748611111111111,0.771541950113379,0.587060657596372,0.666727716727717,0.781308506308506,0.730385487528345,0.693096784168213,0.758080808080808,0.672962962962963,0.725396825396825,0.710731728588871,0.621212121212121,0.538690476190476,0.546536796536797,0.523426209912886,0.519395551503195,0.551851851851852,0.603333333333333,0.576704545454545,0.681385281385281,0.56547619047619,0.505886243386243,0.614603174603175,0.390873015873016,0,0,0,0,0,0.678787878787879,0.594444444444444,0.667037327751614,0.643588950731808,0.822764490411549,0.61123099696413,0.629735240360341,0.747435897435897,0.678333333333333,0.644337778971238,0.845709845709846,0.774859943977591,0.78993783993784,0.736549707602339,0.642126623376623,0.708488138845282,0.693295196866625,0.738095238095238,0.775661375661376,0.79537037037037,0.817250233426704,0.782635882635883,0.603592333592334,0.529040404040404,0.553004535147392,0.495544186894058,0.550065739069132,0.68338011195154,0.574603174603175,0.516304727019013,0.594928148774303,0.609217171717172,0.563133163133163,0.591150793650794,0.595473574045003,0.597936507936508,0.576556776556777,0.696581196581197,0.633730158730159,0.576887464387464,0.63216824111561,0.522311022311022],[111,111,111,363,366,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111,52,52,52,299,302,52,52,52,52,52,52,52,52,0,0,0,0,0,52,52,110,110,110,367,370,110,109,110,110,110,110,109,110,101,100,101,101,101,110,110,111,111,111,370,373,111,110,111,111,111,111,110,111,102,101,102,102,102,111,111],[0.710882138295931,0.750123456790123,0.833862433862434,0.654691358024691,0.751613756613757,0.655220977279801,0.787087332956898,0.649107744107744,0.825185185185185,0.806481481481481,0.690108932461874,0.774482379219221,0.741719576719577,0.734708994708995,0.78973544973545,0.634550264550265,0.777777777777778,0.778597883597884,0.832461873638344,0.777018633540373,0.656462585034014,0.729861111111111,0.804074074074074,0.722222222222222,0.66672335600907,0.635555555555556,0.809494949494949,0.823809523809524,0.812698412698413,0.724074074074074,0.701111111111111,0.850740740740741,0.806292517006803,0.711538461538462,0.868484848484849,0.682905982905983,0.777777777777778,0.75,0.785185185185185,0.757936507936508,0.67546342111048,0.608844058715298,0.654956187603246,0.618173200160778,0.591112455083043,0.729126984126984,0.699408671514583,0.660438808373591,0.707282254782255,0.651733865263277,0.704455861598719,0.748695054945055,0.743062678062678,0.661262626262626,0.668153175800235,0.491616161616162,0.673115079365079,0.672777777777778,0.715315425315425,0.653469294150409,0.66237135879993,0.705362415362415,0.679873475960432,0.686429249762583,0.576290071584189,0.713386243386243,0.738739974622328,0.652323232323232,0.669656084656085,0.67858753163101,0.672022792022792,0.729187109187109,0.646335403726708,0.700396825396825,0.703677248677249,0.655343915343915,0.791216931216931,0.753514739229025,0.644872325741891,0.628730158730159],[215,210,215,225,230,215,208,210,215,215,215,208,210,159,157,159,159,159,215,215,132,130,132,145,147,132,129,130,132,132,132,129,130,105,104,105,105,105,132,132,210,206,210,221,225,210,204,206,210,210,210,204,206,157,155,157,157,157,210,210,215,210,215,225,230,215,208,210,215,215,215,208,210,159,157,159,159,159,215,215]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>Accuracy_oats<\/th>\n      <th>N_oats<\/th>\n      <th>Accuracy_wheat<\/th>\n      <th>N_wheat<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[5]; $(this.api().cell(row, 5).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->


### Confusion Matrix for a Succesful Model





An immediately obvious issue while looking at the prediction models is class waning. 

We've already observed that almost all farm practices have noticeably positive impacts on our studied variables. When filtering the data to define our different datasets, we often end up with subselections which lack any element in one of the classes, typically asking for deep information on practices leaves us with 0 entries in the `lower` category.

![](grainBOAFinalReport_files/figure-html/farmer2 confusion matrix-1.png)<!-- -->![](grainBOAFinalReport_files/figure-html/farmer2 confusion matrix-2.png)<!-- -->

The only way to work around this tendency is to work over bigger populations. We could define differently the categories according to the model, but it would destroy all comparability and the regression would be meaningless.

As a consequence of this, models that are not top performers but retain bigger sections of the total population increase their relevance. One of the more interesting results is **consumer 2** on Antioxidants for Oats, which has a 75% Accuracy metric. Looking at the matrix illustrates very well that this number is better than just "15% better than assigning everything to the center tier".



# Agregation of Farm Practices Analysis Over Crops

  In this section, we'll try to find meaningful results for farm practices **across** crops.
  Our strategy to achieve a meaningful comparison is replacing on each point the actual variable value by the crop's percentile reached. The goal of this study is checking if a practices is able to push each crop far from its *median*.
  This means that if a tomatto that is bigger than 60% of all other tomato observations has a value of 0.1 for a certain variable and for Kale you need to reach 10 to be above 60% of all subjects, we are giving the same score to a a 0.1 tomato sample and a kale sample with a value of 10. We are measuring the relative scarcity of a value among it's crop peers, rather than the absolute value.



## Agregation by Percentile Transformation


Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.




<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.2653481 </td>
   <td style="text-align:right;"> 0.3233333 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0.01,0.15] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0618885 </td>
   <td style="text-align:right;"> 0.0826814 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6613943 </td>
   <td style="text-align:right;"> 0.7482370 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.01,0.16] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0114632 </td>
   <td style="text-align:right;"> 0.0818356 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6139901 </td>
   <td style="text-align:right;"> 0.6544828 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.01,0.2] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.0334248 </td>
   <td style="text-align:right;"> 0.0958761 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4708299 </td>
   <td style="text-align:right;"> 0.4213793 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.26,-0.11] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0433910 </td>
   <td style="text-align:right;"> -0.1790403 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4354572 </td>
   <td style="text-align:right;"> 0.4400000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.39,-0.24] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.0438834 </td>
   <td style="text-align:right;"> -0.3129746 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4006339 </td>
   <td style="text-align:right;"> 0.3455571 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.28,-0.12] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.0514427 </td>
   <td style="text-align:right;"> -0.2045888 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4745397 </td>
   <td style="text-align:right;"> 0.5189558 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.09,0.29] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0554442 </td>
   <td style="text-align:right;"> 0.1999539 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4162357 </td>
   <td style="text-align:right;"> 0.3833333 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.45,-0.29] </td>
   <td style="text-align:right;"> -0.45 </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.0581478 </td>
   <td style="text-align:right;"> -0.3764619 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4629708 </td>
   <td style="text-align:right;"> 0.4144828 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.1,0.21] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0652779 </td>
   <td style="text-align:right;"> 0.1558468 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.1962941 </td>
   <td style="text-align:right;"> 0.1448276 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-0.14,-0.05] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0738991 </td>
   <td style="text-align:right;"> -0.0807215 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4853599 </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.18,0.35] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.35 </td>
   <td style="text-align:right;"> 0.0862115 </td>
   <td style="text-align:right;"> 0.2650686 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4807069 </td>
   <td style="text-align:right;"> 0.5053533 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.12,0.23] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0985107 </td>
   <td style="text-align:right;"> 0.1770468 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4124683 </td>
   <td style="text-align:right;"> 0.3730606 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.48,-0.36] </td>
   <td style="text-align:right;"> -0.48 </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> 0.0028477 </td>
   <td style="text-align:right;"> -0.4330296 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6020187 </td>
   <td style="text-align:right;"> 0.6266667 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [-0.19,-0.06] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0188362 </td>
   <td style="text-align:right;"> -0.1161500 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6851938 </td>
   <td style="text-align:right;"> 0.7756410 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.35,0.48] </td>
   <td style="text-align:right;"> 0.35 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.0193100 </td>
   <td style="text-align:right;"> 0.4208845 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6114191 </td>
   <td style="text-align:right;"> 0.6673804 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.01,0.13] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0411579 </td>
   <td style="text-align:right;"> 0.0693448 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5512075 </td>
   <td style="text-align:right;"> 0.6000000 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [0.24,0.4] </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.0440073 </td>
   <td style="text-align:right;"> 0.3137929 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5701665 </td>
   <td style="text-align:right;"> 0.6200000 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [0.32,0.45] </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 0.0469456 </td>
   <td style="text-align:right;"> 0.3824172 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.7042889 </td>
   <td style="text-align:right;"> 0.7756410 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.27,0.41] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.41 </td>
   <td style="text-align:right;"> 0.0588067 </td>
   <td style="text-align:right;"> 0.3439722 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 1.3894827 </td>
   <td style="text-align:right;"> 1.5555556 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.64,0.91] </td>
   <td style="text-align:right;"> 0.64 </td>
   <td style="text-align:right;"> 0.91 </td>
   <td style="text-align:right;"> 0.0650195 </td>
   <td style="text-align:right;"> 0.7777322 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6080878 </td>
   <td style="text-align:right;"> 0.6600000 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [0.15,0.29] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0786878 </td>
   <td style="text-align:right;"> 0.2235661 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3946841 </td>
   <td style="text-align:right;"> 0.2589157 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [-0.49,-0.26] </td>
   <td style="text-align:right;"> -0.49 </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> 0.0007732 </td>
   <td style="text-align:right;"> -0.3599575 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4228031 </td>
   <td style="text-align:right;"> 0.4748714 </td>
   <td style="text-align:right;"> 194 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [0.04,0.16] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0048299 </td>
   <td style="text-align:right;"> 0.0963831 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5900534 </td>
   <td style="text-align:right;"> 0.6868829 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [0.02,0.19] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.0155615 </td>
   <td style="text-align:right;"> 0.0952521 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.7046613 </td>
   <td style="text-align:right;"> 0.7834483 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [0.13,0.32] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0168451 </td>
   <td style="text-align:right;"> 0.2220283 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4738368 </td>
   <td style="text-align:right;"> 0.3871650 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-0.16,0.04] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0384741 </td>
   <td style="text-align:right;"> -0.0465486 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3387164 </td>
   <td style="text-align:right;"> 0.3180723 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [-0.52,-0.43] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> 0.0418518 </td>
   <td style="text-align:right;"> -0.4770686 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3665385 </td>
   <td style="text-align:right;"> 0.2891396 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-0.27,-0.11] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0447834 </td>
   <td style="text-align:right;"> -0.1918190 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6029331 </td>
   <td style="text-align:right;"> 0.6800000 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [0.02,0.21] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0513677 </td>
   <td style="text-align:right;"> 0.0909918 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5720725 </td>
   <td style="text-align:right;"> 0.6622003 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [0.02,0.22] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0620378 </td>
   <td style="text-align:right;"> 0.1284026 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5420357 </td>
   <td style="text-align:right;"> 0.5662393 </td>
   <td style="text-align:right;"> 194 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:left;"> [0.08,0.21] </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0727550 </td>
   <td style="text-align:right;"> 0.1410567 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3595345 </td>
   <td style="text-align:right;"> 0.2961918 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-0.34,-0.17] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.0737100 </td>
   <td style="text-align:right;"> -0.2799946 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3433557 </td>
   <td style="text-align:right;"> 0.2200000 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [-0.05,0.06] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0758817 </td>
   <td style="text-align:right;"> 0.0103064 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4106045 </td>
   <td style="text-align:right;"> 0.3400000 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.36,-0.17] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.0780426 </td>
   <td style="text-align:right;"> -0.2859586 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.1817404 </td>
   <td style="text-align:right;"> 0.1448276 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [-0.14,-0.05] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0910584 </td>
   <td style="text-align:right;"> -0.0800497 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3990304 </td>
   <td style="text-align:right;"> 0.3555172 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [0.02,0.13] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0931419 </td>
   <td style="text-align:right;"> 0.0717675 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6011047 </td>
   <td style="text-align:right;"> 0.6354020 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:left;"> [-0.26,-0.13] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0935781 </td>
   <td style="text-align:right;"> -0.1847552 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4248059 </td>
   <td style="text-align:right;"> 0.4860814 </td>
   <td style="text-align:right;"> 157 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:left;"> [0.03,0.16] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0042083 </td>
   <td style="text-align:right;"> 0.0941666 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3723959 </td>
   <td style="text-align:right;"> 0.2433333 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [-0.54,-0.29] </td>
   <td style="text-align:right;"> -0.54 </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.0053069 </td>
   <td style="text-align:right;"> -0.3936177 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6842620 </td>
   <td style="text-align:right;"> 0.7503448 </td>
   <td style="text-align:right;"> 139 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.1,0.3] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0205275 </td>
   <td style="text-align:right;"> 0.2076227 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4240918 </td>
   <td style="text-align:right;"> 0.4349398 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [0.09,0.21] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0207269 </td>
   <td style="text-align:right;"> 0.1469912 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5889042 </td>
   <td style="text-align:right;"> 0.6107193 </td>
   <td style="text-align:right;"> 136 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> [-0.27,-0.13] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0320437 </td>
   <td style="text-align:right;"> -0.1953971 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6433937 </td>
   <td style="text-align:right;"> 0.7434483 </td>
   <td style="text-align:right;"> 139 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:left;"> [0.05,0.26] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0411951 </td>
   <td style="text-align:right;"> 0.1510600 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6194464 </td>
   <td style="text-align:right;"> 0.7122708 </td>
   <td style="text-align:right;"> 136 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> [0.05,0.23] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0416891 </td>
   <td style="text-align:right;"> 0.1269024 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4350058 </td>
   <td style="text-align:right;"> 0.3455571 </td>
   <td style="text-align:right;"> 136 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0472662 </td>
   <td style="text-align:right;"> -0.0980616 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4880964 </td>
   <td style="text-align:right;"> 0.4891566 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [0.11,0.26] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0496720 </td>
   <td style="text-align:right;"> 0.1850343 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3979818 </td>
   <td style="text-align:right;"> 0.3307475 </td>
   <td style="text-align:right;"> 136 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> [-0.22,-0.04] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.0676021 </td>
   <td style="text-align:right;"> -0.1248310 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.3733998 </td>
   <td style="text-align:right;"> 0.2500000 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [-0.52,-0.29] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.0010053 </td>
   <td style="text-align:right;"> -0.3866370 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.3018891 </td>
   <td style="text-align:right;"> 0.2866667 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [-0.54,-0.44] </td>
   <td style="text-align:right;"> -0.54 </td>
   <td style="text-align:right;"> -0.44 </td>
   <td style="text-align:right;"> 0.0124448 </td>
   <td style="text-align:right;"> -0.4897199 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.4239150 </td>
   <td style="text-align:right;"> 0.4646681 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:left;"> [0.05,0.16] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0137885 </td>
   <td style="text-align:right;"> 0.1070501 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.6836567 </td>
   <td style="text-align:right;"> 0.7462069 </td>
   <td style="text-align:right;"> 182 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> [0.1,0.29] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0163098 </td>
   <td style="text-align:right;"> 0.1979597 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.3901437 </td>
   <td style="text-align:right;"> 0.3975904 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [0.08,0.19] </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.0165307 </td>
   <td style="text-align:right;"> 0.1297947 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.4688481 </td>
   <td style="text-align:right;"> 0.3871650 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> [-0.16,0.02] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.0449174 </td>
   <td style="text-align:right;"> -0.0473229 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.5425935 </td>
   <td style="text-align:right;"> 0.5769231 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:left;"> [0.09,0.21] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0510317 </td>
   <td style="text-align:right;"> 0.1459376 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.6967764 </td>
   <td style="text-align:right;"> 0.7595205 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> [0.06,0.22] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0661682 </td>
   <td style="text-align:right;"> 0.1474178 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.3716584 </td>
   <td style="text-align:right;"> 0.3060649 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> [-0.33,-0.16] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> 0.0766211 </td>
   <td style="text-align:right;"> -0.2707226 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.4221296 </td>
   <td style="text-align:right;"> 0.3524138 </td>
   <td style="text-align:right;"> 182 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> [-0.35,-0.15] </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.0829485 </td>
   <td style="text-align:right;"> -0.2765185 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.4519733 </td>
   <td style="text-align:right;"> 0.4666667 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [0.2,0.32] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0864287 </td>
   <td style="text-align:right;"> 0.2627504 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.3372467 </td>
   <td style="text-align:right;"> 0.2192771 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [-0.15,-0.05] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0999954 </td>
   <td style="text-align:right;"> -0.1003005 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3517719 </td>
   <td style="text-align:right;"> 0.2409639 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.54,-0.32] </td>
   <td style="text-align:right;"> -0.54 </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> 0.0026589 </td>
   <td style="text-align:right;"> -0.4333150 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4319462 </td>
   <td style="text-align:right;"> 0.4914530 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [0.04,0.17] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0039222 </td>
   <td style="text-align:right;"> 0.1068481 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7272638 </td>
   <td style="text-align:right;"> 0.7834483 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0.16,0.32] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0171080 </td>
   <td style="text-align:right;"> 0.2379694 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4921412 </td>
   <td style="text-align:right;"> 0.3977433 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.13,0.06] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0185382 </td>
   <td style="text-align:right;"> -0.0268240 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4488159 </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.21,0.34] </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.0253715 </td>
   <td style="text-align:right;"> 0.2771592 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5908895 </td>
   <td style="text-align:right;"> 0.6593794 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.02,0.19] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.0347926 </td>
   <td style="text-align:right;"> 0.0951619 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6351772 </td>
   <td style="text-align:right;"> 0.6466855 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.24,-0.11] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0541818 </td>
   <td style="text-align:right;"> -0.1622232 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3064831 </td>
   <td style="text-align:right;"> 0.3066667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-0.53,-0.44] </td>
   <td style="text-align:right;"> -0.53 </td>
   <td style="text-align:right;"> -0.44 </td>
   <td style="text-align:right;"> 0.0593747 </td>
   <td style="text-align:right;"> -0.4872259 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3476507 </td>
   <td style="text-align:right;"> 0.2891396 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [-0.28,-0.13] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0612485 </td>
   <td style="text-align:right;"> -0.2044999 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5889288 </td>
   <td style="text-align:right;"> 0.6572414 </td>
   <td style="text-align:right;"> 169 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [0,0.19] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.0702415 </td>
   <td style="text-align:right;"> 0.0724224 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3739406 </td>
   <td style="text-align:right;"> 0.3590361 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [0.06,0.17] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0756971 </td>
   <td style="text-align:right;"> 0.1153190 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>



## BQI Shifts

![](grainBOAFinalReport_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

### Individual Farm Practices

![](grainBOAFinalReport_files/figure-html/unnamed-chunk-11-1.png)<!-- -->




#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](grainBOAFinalReport_files/figure-html/bqi1bqi2-1.png)<!-- -->





