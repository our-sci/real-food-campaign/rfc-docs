---
title: "2020 Survey, Leafy Greens Samples Data Analysis"
author: "Real Food Campaign"
date: "04/09/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---













# Some Summary Statistics

## General: Variation and ratios between maximum and mimimum value



```
## # A tibble: 105 x 1
##    Antioxidants
##           <dbl>
##  1       33138.
##  2       26613.
##  3       25499.
##  4       18583.
##  5        9842.
##  6        9567.
##  7        7640.
##  8        7314.
##  9        6511.
## 10        5825.
## # … with 95 more rows
```

```
## # A tibble: 105 x 1
##    Polyphenols
##          <dbl>
##  1        281.
##  2        251.
##  3        205.
##  4        190.
##  5        171.
##  6        154.
##  7        152.
##  8        150.
##  9        150.
## 10        146.
## # … with 95 more rows
```

```
## # A tibble: 105 x 1
##     Brix
##    <dbl>
##  1   9.9
##  2   7.3
##  3   7.3
##  4   6.9
##  5   6.6
##  6   6.5
##  7   6.4
##  8   6.3
##  9   5.9
## 10   5.9
## # … with 95 more rows
```

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Extremes and their ratios for studied variables, by crop.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Type </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Sample Size </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Max </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Min </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Max </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Min </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Max </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Min </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Max non Outlier </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Max non Outlier </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Max non Outlier </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Antioxidants Ratio W/O outliers </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Polyphenols Ratio W/O outliers </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Brix Ratio W/O outliers </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 281.02 </td>
   <td style="text-align:right;"> 22.55 </td>
   <td style="text-align:right;"> 85.13327 </td>
   <td style="text-align:right;"> 33137.89 </td>
   <td style="text-align:right;"> 64.95 </td>
   <td style="text-align:right;"> 3325.9656 </td>
   <td style="text-align:right;"> 9.9 </td>
   <td style="text-align:right;"> 1.8 </td>
   <td style="text-align:right;"> 4.203093 </td>
   <td style="text-align:right;"> 12.462084 </td>
   <td style="text-align:right;"> 510.20616 </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 8750 </td>
   <td style="text-align:right;"> 220 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 134.719015 </td>
   <td style="text-align:right;"> 9.756098 </td>
   <td style="text-align:right;"> 4.444444 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:right;"> 192 </td>
   <td style="text-align:right;"> 954.55 </td>
   <td style="text-align:right;"> 59.96 </td>
   <td style="text-align:right;"> 264.11172 </td>
   <td style="text-align:right;"> 25368.73 </td>
   <td style="text-align:right;"> 1648.31 </td>
   <td style="text-align:right;"> 9707.4371 </td>
   <td style="text-align:right;"> 17.8 </td>
   <td style="text-align:right;"> 3.8 </td>
   <td style="text-align:right;"> 10.974834 </td>
   <td style="text-align:right;"> 15.919780 </td>
   <td style="text-align:right;"> 15.39075 </td>
   <td style="text-align:right;"> 4.684210 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 684 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 11.407605 </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 233.26 </td>
   <td style="text-align:right;"> 32.61 </td>
   <td style="text-align:right;"> 95.84889 </td>
   <td style="text-align:right;"> 10452.75 </td>
   <td style="text-align:right;"> 126.22 </td>
   <td style="text-align:right;"> 689.8573 </td>
   <td style="text-align:right;"> 17.6 </td>
   <td style="text-align:right;"> 3.0 </td>
   <td style="text-align:right;"> 8.809836 </td>
   <td style="text-align:right;"> 7.153021 </td>
   <td style="text-align:right;"> 82.81374 </td>
   <td style="text-align:right;"> 5.866667 </td>
   <td style="text-align:right;"> 1500 </td>
   <td style="text-align:right;"> 190 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 11.884012 </td>
   <td style="text-align:right;"> 5.826434 </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:right;"> 166 </td>
   <td style="text-align:right;"> 222.37 </td>
   <td style="text-align:right;"> 1.98 </td>
   <td style="text-align:right;"> 36.85755 </td>
   <td style="text-align:right;"> 8636.10 </td>
   <td style="text-align:right;"> 75.20 </td>
   <td style="text-align:right;"> 938.1960 </td>
   <td style="text-align:right;"> 11.2 </td>
   <td style="text-align:right;"> 1.7 </td>
   <td style="text-align:right;"> 3.625926 </td>
   <td style="text-align:right;"> 112.308081 </td>
   <td style="text-align:right;"> 114.84176 </td>
   <td style="text-align:right;"> 6.588235 </td>
   <td style="text-align:right;"> 7300 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 97.074468 </td>
   <td style="text-align:right;"> 101.010101 </td>
   <td style="text-align:right;"> 3.529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 500.71 </td>
   <td style="text-align:right;"> 63.85 </td>
   <td style="text-align:right;"> 155.88886 </td>
   <td style="text-align:right;"> 30080.74 </td>
   <td style="text-align:right;"> 2138.90 </td>
   <td style="text-align:right;"> 10142.3785 </td>
   <td style="text-align:right;"> 11.3 </td>
   <td style="text-align:right;"> 3.5 </td>
   <td style="text-align:right;"> 8.965476 </td>
   <td style="text-align:right;"> 7.841973 </td>
   <td style="text-align:right;"> 14.06365 </td>
   <td style="text-align:right;"> 3.228571 </td>
   <td style="text-align:right;"> 19000 </td>
   <td style="text-align:right;"> 300 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 8.883071 </td>
   <td style="text-align:right;"> 4.698512 </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 719.59 </td>
   <td style="text-align:right;"> 72.19 </td>
   <td style="text-align:right;"> 234.38679 </td>
   <td style="text-align:right;"> 49808.46 </td>
   <td style="text-align:right;"> 1861.12 </td>
   <td style="text-align:right;"> 8263.4713 </td>
   <td style="text-align:right;"> 13.1 </td>
   <td style="text-align:right;"> 4.0 </td>
   <td style="text-align:right;"> 8.380952 </td>
   <td style="text-align:right;"> 9.968001 </td>
   <td style="text-align:right;"> 26.76263 </td>
   <td style="text-align:right;"> 3.275000 </td>
   <td style="text-align:right;"> 28000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 15.044704 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 3.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:right;"> 65 </td>
   <td style="text-align:right;"> 490.98 </td>
   <td style="text-align:right;"> 103.81 </td>
   <td style="text-align:right;"> 253.92903 </td>
   <td style="text-align:right;"> 15596.32 </td>
   <td style="text-align:right;"> 20.53 </td>
   <td style="text-align:right;"> 853.7592 </td>
   <td style="text-align:right;"> 13.9 </td>
   <td style="text-align:right;"> 2.3 </td>
   <td style="text-align:right;"> 6.131481 </td>
   <td style="text-align:right;"> 4.729602 </td>
   <td style="text-align:right;"> 759.68436 </td>
   <td style="text-align:right;"> 6.043478 </td>
   <td style="text-align:right;"> 1500 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 73.063809 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> 4.347826 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 702.27 </td>
   <td style="text-align:right;"> 36.46 </td>
   <td style="text-align:right;"> 214.97000 </td>
   <td style="text-align:right;"> 14863.74 </td>
   <td style="text-align:right;"> 88.53 </td>
   <td style="text-align:right;"> 1406.6679 </td>
   <td style="text-align:right;"> 13.2 </td>
   <td style="text-align:right;"> 1.4 </td>
   <td style="text-align:right;"> 6.697826 </td>
   <td style="text-align:right;"> 19.261382 </td>
   <td style="text-align:right;"> 167.89495 </td>
   <td style="text-align:right;"> 9.428571 </td>
   <td style="text-align:right;"> 4000 </td>
   <td style="text-align:right;"> 450 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 45.182424 </td>
   <td style="text-align:right;"> 12.342293 </td>
   <td style="text-align:right;"> 7.857143 </td>
  </tr>
</tbody>
</table>

### Censoring The More Evident Outliers




### Histograms of the Main Nutrients by Crop



![](dataAnalysis_files/figure-html/preliminaryhistograms-1.png)<!-- -->



## Wheat

### Antioxidants

### Poliphenols

### Brix

## Oats


# Farm Practices: Median Shifts Comparisons







# Synthesis Median Shift Tables.

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is intersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting magnitudes are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.




## Color Scale

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 5%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 5%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;">Between 5% and 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;">Between 5% and 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;">10% to 20%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;">10% to 20%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.4) !important;">20% to 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.4) !important;">20% to 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.3) !important;">50% to 90%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.3) !important;">50% to 90%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;">90% or more</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;">90% or more</span> </td>
  </tr>
</tbody>
</table>


# Percentual Shift Over Crop's Median Values for Farm Practices

* The reference category used to define the neutral median is defined as *Farms that are neither organic, transitioning, regenerative, no till or sowing cover crops*.
* There's plenty of columns prefixed by *farm_practices* on the dataset, but they are not mutually related in a sound logical way, that's why calculations can't be easily performed over the whole set. 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Antioxidants</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Polyphenols</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Brix</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biodynamic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-7.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">21.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">187.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">2.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-17.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">70.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">97.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">177.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-11.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-12.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">-20.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-22.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 5">-23.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">32.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-4.51</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">43.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-21.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">-15.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">155.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">56.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">34.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">-2.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">20.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Covercrops </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">0.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">3.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">20.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">232.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">13.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 29">27.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 22">119.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">13.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">83.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">209.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">25.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">32.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">2.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-22.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">2.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">20.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 22">1.5</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Greenhouse </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">8.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">6.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">29.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">16.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">-9.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">60.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">2.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">109.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-7.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">-3.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">39.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-39.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">36.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">1.5</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nonchlorine Water </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">-26.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">55.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">72.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-24.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-8.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">74.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-18.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">66.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-4.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">81.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-31.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-11.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-4.51</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nospray </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 52">-8.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">39.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-27.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">27.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">205.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">-26.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">10.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">61.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">5.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">-25.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">89.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">244.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">31.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">14.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">24.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-24.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">37.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-11.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">10.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-1.5</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 37">0.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">10.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">13.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">205.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">-1.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 36">67.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">7.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">105.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">244.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">12.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">12.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-23.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">22.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">30.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">7.52</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 41">-7.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">55.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">36.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">1.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 32">3.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">52.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">-4.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">117.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-12.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 29">49.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">37.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 26">-21.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">22.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">32.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 27">7.52</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Certified </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-1.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">2.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">55.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">100.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">51.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">19.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-9.02</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 54">-17.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-12.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 34">-6.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 41">-0.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 46">10.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">49.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 53">3.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">23.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 34">17.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 41">-0.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 47">-0.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">50.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">37.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">7.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 34">8.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 41">-4.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-1.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">4.51</span> </td>
  </tr>
</tbody>
</table>

# Percentual Shift Over Crop's Median Values for each Soil Amendment

* The reference category is farms that don't mention any soil amendment on the submitted information ( `FALSE` value on every dataset column prefixed by *amendments* ).

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Antioxidants</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Polyphenols</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Brix</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Mulch </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-5.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">68.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 113">-35.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 38">31.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">-31.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 82">-29.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 83">-1.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 86">-15.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">-4.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-13.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 111">2.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">21.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-10.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 82">32.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 81">7.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 87">-4.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">9.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 48">46.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 32">5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 91">30.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">21.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 79">18.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 80">-0.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 81">19.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">36.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 44">9.45</span> </td>
  </tr>
</tbody>
</table>

# Percentual Shift Over Crop's Median Values for each Seed Treatment

* The reference category is farms that don't mention any seed treatment on the submitted information ( `FALSE` value on every dataset column prefixed by *seed_treatment* ).

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Antioxidants</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Polyphenols</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Brix</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-32.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">48.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-31.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

# Percentual Shift Over Crop's Median Values for each Tillage Practice

* The reference category is farms that inform *notill* as a practice.
* *Light tillage* is exiting tillage, with an informed depth of less than 6 inches.
* *Heavy tillage* is tillage with a depth superior or equal to 6 inches.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Antioxidants</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Polyphenols</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="8"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Brix</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Leeks Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mizuna Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Spinach Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 51">-22.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-22.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 42">-52.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">58.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-36.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">18.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 43">-71.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">46.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">8.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">40.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">38.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 8">-20.03</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 34">1.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">26.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">4.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">-20.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">-4.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">-4.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">-2.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-4.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">-0.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">-20.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">-0.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">4.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-5.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">10.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-1.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 28">-1.41</span> </td>
  </tr>
</tbody>
</table>



# Random Forest on Antioxidants, Polyphenols and Brix

  These regressions are performed using a *Random Forest* algorithm. The only tuned parameter besides the selection of initial variables is `mtry`.
  For the classifications, we are using *Accuracy* as a precission metric, while for regressions the custom random forest $R^2$ is used, which is a special metric comparable to the homonym in the context of linear models.

### Compared Datasets

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Composition of employed datasets</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> name </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> composition </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:left;"> OurSci Spectrometer measurements over surface crop (9 bands). </td>
  </tr>
  <tr>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:left;"> All 'surface' variables, plus crop variety </td>
  </tr>
  <tr>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:left;"> All 'surface' variables, plus climate region </td>
  </tr>
  <tr>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:left;"> All 'surface' variables, climate region and crop variety </td>
  </tr>
  <tr>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:left;"> OurSci Spectrometer measurements over juice crop surface (9 bands). </td>
  </tr>
  <tr>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:left;"> All 'juice' variables, plus crop variety </td>
  </tr>
  <tr>
   <td style="text-align:left;"> juiceRegion </td>
   <td style="text-align:left;"> All 'juice' variables, plus climate region </td>
  </tr>
  <tr>
   <td style="text-align:left;"> juiceWithMetadata </td>
   <td style="text-align:left;"> All 'juice' variables, climate region and crop variety </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> All 'juice' and 'surface' variables, climate region and crop variety </td>
  </tr>
  <tr>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:left;"> Climate Region and Crop Variety, no scans </td>
  </tr>
</tbody>
</table>



## Random Forest Regression



<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Random Forest Regressions</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> species </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> dataset </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> cvRsquared </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mtry </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5313639 </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4562024 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4379098 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.3957250 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3948807 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3024950 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7747486 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6744531 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6638706 </td>
   <td style="text-align:right;"> 19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6135748 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5992882 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5267851 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4750724 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4571865 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4266125 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4236955 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.4234658 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.4022275 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.3875676 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.2201758 </td>
   <td style="text-align:right;"> 30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6235348 </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5859201 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5613360 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5334356 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4423526 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3704498 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5929216 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5030173 </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.4895067 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4310796 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4159864 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4114273 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.3711074 </td>
   <td style="text-align:right;"> 30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.2981344 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.2728920 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.2712179 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.2469468 </td>
   <td style="text-align:right;"> 15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.2426205 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5776176 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4688396 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4460222 </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4140195 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3928503 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3018109 </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6699608 </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6252527 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6193477 </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6091538 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6082740 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5492081 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5370579 </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5245681 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4320773 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3950817 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.3843315 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.3589927 </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6335891 </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6157044 </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6010670 </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5876875 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5056669 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3924193 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6810095 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6234291 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.6097566 </td>
   <td style="text-align:right;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceRegion </td>
   <td style="text-align:right;"> 0.5707949 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5658702 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5561500 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceRegion </td>
   <td style="text-align:right;"> 0.5340633 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5111693 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceRegion </td>
   <td style="text-align:right;"> 0.4853199 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4709660 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.4299748 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4113469 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.3708169 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.3686610 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.3581223 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.3247745 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.2751545 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.2649158 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
</tbody>
</table>

## Random Forest Classification of The Percentile on the Explained Variable


Two targets are generated for each explained variable (Antioxidants, Polyphenols, Brix). The objective is predicting the quantile were the sample contents belong. Instead of using each quantile, the central ones are meged into a center category.

* Quartiles is three categories, *lower* for the first quartile, *center* for the second and third and *higher* for the fourth.
* Quintiles is also three categories, with *center* composed of the second, third and fourth quintiles.




<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Random Forest Classifications</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> species </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> dataset </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Accuracy </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mtry </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7976190 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7619048 </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7222222 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7125926 </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7100000 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6988889 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6855556 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6688889 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6581746 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6499186 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6488095 </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6472222 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6380952 </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6325397 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6166667 </td>
   <td style="text-align:right;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6155556 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6125283 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6111111 </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5973016 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5944444 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5865079 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5756410 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5533333 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5525510 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5416667 </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5357143 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5263228 </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5208466 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5201587 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5139927 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5077778 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.4944444 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4930159 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.4555556 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4115344 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> bok_choi </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.3902778 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.8000000 </td>
   <td style="text-align:right;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7976190 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7307692 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7115385 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7114286 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.7051282 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6911111 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6602564 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6596825 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6555556 </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6441270 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6330159 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6295918 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6261905 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6211111 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6120748 </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6074074 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6011111 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5869048 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5782313 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5731217 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5320513 </td>
   <td style="text-align:right;"> 33 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5285714 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5246032 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5188889 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5166667 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5166667 </td>
   <td style="text-align:right;"> 39 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5153846 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5141270 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5138095 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4955026 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4846825 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4749471 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4742063 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4494757 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4454365 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.8012821 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7626190 </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7615385 </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7500000 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7400000 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.7371795 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7266667 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6944444 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6547619 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6519048 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6500000 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6430272 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6380952 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6372222 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6258730 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6180952 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5833333 </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5764652 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5574603 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5535714 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5486111 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5465079 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5365873 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5177778 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5166667 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5090476 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5033333 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4939153 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4878211 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4726190 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4714286 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4500000 </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.4423077 </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4295238 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4289418 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> leeks </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4200000 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7123810 </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6902778 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6880952 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6869048 </td>
   <td style="text-align:right;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6858974 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6805556 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6703175 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6699206 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6666667 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6666667 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6635185 </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6309524 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6292063 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6100000 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6054545 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5952381 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5935498 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5933333 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5827513 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5760073 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5606349 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5509524 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5507816 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5320513 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5100000 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5099490 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4969312 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.4966553 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4946441 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4805026 </td>
   <td style="text-align:right;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4786869 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4743590 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.4743590 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4666667 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4602564 </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4564286 </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7638889 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7222222 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7120635 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7082011 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7023810 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6875000 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6726190 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6714815 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.6369048 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6246032 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6244444 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6226190 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6152910 </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6130952 </td>
   <td style="text-align:right;"> 20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6130952 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6119048 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6054762 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6023810 </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5988095 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5963553 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.5883333 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5691534 </td>
   <td style="text-align:right;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5458913 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5427850 </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5403608 </td>
   <td style="text-align:right;"> 31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5277778 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5218615 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5132395 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5070635 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4791667 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4759788 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4692857 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4666667 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mizuna </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.3869048 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.8416667 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.8333333 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7416667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7069841 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6953419 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6785714 </td>
   <td style="text-align:right;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6761905 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6742424 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6741497 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6676190 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6466667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6437729 </td>
   <td style="text-align:right;"> 18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6400000 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6296825 </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6277411 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6236508 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6230769 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6134354 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6047619 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5866859 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5700000 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5691270 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5666667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5642857 </td>
   <td style="text-align:right;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5620635 </td>
   <td style="text-align:right;"> 36 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5334921 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5270635 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5082865 </td>
   <td style="text-align:right;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4853297 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4757576 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4717460 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4708333 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.4615385 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4071429 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> mustard_greens </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.3782051 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7986111 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7651515 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7480952 </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7234921 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7116667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.7025641 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6977778 </td>
   <td style="text-align:right;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6863492 </td>
   <td style="text-align:right;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6755556 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6733333 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6515152 </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6471429 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6346154 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6346154 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6232540 </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.6216402 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6166667 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6126455 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5946128 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5872711 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5722222 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5584127 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5565512 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5533761 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5527211 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5193407 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5128205 </td>
   <td style="text-align:right;"> 30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5059524 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4944444 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4633862 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4555123 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4384615 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4226190 </td>
   <td style="text-align:right;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4175397 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.3930556 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.8194444 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.7119048 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.7077778 </td>
   <td style="text-align:right;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6944444 </td>
   <td style="text-align:right;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6914286 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.6875000 </td>
   <td style="text-align:right;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6833333 </td>
   <td style="text-align:right;"> 34 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6766667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.6602564 </td>
   <td style="text-align:right;"> 23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6600000 </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6587302 </td>
   <td style="text-align:right;"> 37 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.6164966 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.6123647 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6065873 </td>
   <td style="text-align:right;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.6051282 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5966667 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> justMetadata </td>
   <td style="text-align:right;"> 0.5961538 </td>
   <td style="text-align:right;"> 25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.5961538 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5829630 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5788889 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceVariety </td>
   <td style="text-align:right;"> 0.5733333 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juiceVariety </td>
   <td style="text-align:right;"> 0.5702381 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5666667 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.5660847 </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5650529 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5572222 </td>
   <td style="text-align:right;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5499747 </td>
   <td style="text-align:right;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brixQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5421164 </td>
   <td style="text-align:right;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5226190 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.5144274 </td>
   <td style="text-align:right;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.5059524 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surface </td>
   <td style="text-align:right;"> 0.4961905 </td>
   <td style="text-align:right;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4918519 </td>
   <td style="text-align:right;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceWithMetadata </td>
   <td style="text-align:right;"> 0.4844444 </td>
   <td style="text-align:right;"> 19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyQuartile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> juice </td>
   <td style="text-align:right;"> 0.4782540 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antiQuintile </td>
   <td style="text-align:left;"> swiss_chard </td>
   <td style="text-align:left;"> surfaceRegion </td>
   <td style="text-align:right;"> 0.4782051 </td>
   <td style="text-align:right;"> 29 </td>
  </tr>
</tbody>
</table>

### Confusion Matrix for a Succesful Model

![](dataAnalysis_files/figure-html/confusionMatrix-1.png)<!-- -->


# Agregation of Farm Practices Analysis Over Crops

  In this section, we'll try to find meaningful results for farm practices **across** crops.
  Our strategy to achieve a meaningful comparison is replacing on each point the actual variable value by the crop's percentile reached. The goal of this study is checking if a practices is able to push each crop far from its *median*.
  This means that if a tomatto that is bigger than 60% of all other tomato observations has a value of 0.1 for a certain variable and for Kale you need to reach 10 to be above 60% of all subjects, we are giving the same score to a a 0.1 tomato sample and a kale sample with a value of 10. We are measuring the relative scarcity of a value among it's crop peers, rather than the absolute value.



## Agregation by Percentile Transformation


Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.




<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ni-Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4357510 </td>
   <td style="text-align:right;"> 0.3537736 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> [-0.16,-0.02] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.0365487 </td>
   <td style="text-align:right;"> -0.0904157 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Na-Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4390790 </td>
   <td style="text-align:right;"> 0.3636498 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> [-0.16,-0.02] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.0587169 </td>
   <td style="text-align:right;"> -0.0888595 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> S-Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4313660 </td>
   <td style="text-align:right;"> 0.3519630 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> [-0.15,-0.01] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0735227 </td>
   <td style="text-align:right;"> -0.0800748 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.6581336 </td>
   <td style="text-align:right;"> 0.7717391 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:left;"> [0.13,0.3] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0318964 </td>
   <td style="text-align:right;"> 0.2133389 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ni-Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.4666074 </td>
   <td style="text-align:right;"> 0.3977877 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:left;"> [-0.13,0.02] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.0729418 </td>
   <td style="text-align:right;"> -0.0566146 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Si-Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.7010690 </td>
   <td style="text-align:right;"> 0.7774175 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:left;"> [0.18,0.34] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.0876081 </td>
   <td style="text-align:right;"> 0.2595696 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Si-Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6758523 </td>
   <td style="text-align:right;"> 0.7500000 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:left;"> [0.18,0.29] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0227289 </td>
   <td style="text-align:right;"> 0.2356042 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants-Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5820530 </td>
   <td style="text-align:right;"> 0.6524032 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:left;"> [0.05,0.15] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0328668 </td>
   <td style="text-align:right;"> 0.0972129 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6928051 </td>
   <td style="text-align:right;"> 0.7663043 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:left;"> [0.19,0.3] </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0342232 </td>
   <td style="text-align:right;"> 0.2469097 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.5366034 </td>
   <td style="text-align:right;"> 0.6314042 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:left;"> [0.01,0.15] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0163336 </td>
   <td style="text-align:right;"> 0.0744875 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mg-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.4967579 </td>
   <td style="text-align:right;"> 0.4056604 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.07,0.07] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.0296763 </td>
   <td style="text-align:right;"> 0.0000489 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Cl-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.3945326 </td>
   <td style="text-align:right;"> 0.2926829 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.21,-0.06] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0357323 </td>
   <td style="text-align:right;"> -0.1329298 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mn-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.4292685 </td>
   <td style="text-align:right;"> 0.3396226 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.13,0.01] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0484224 </td>
   <td style="text-align:right;"> -0.0554686 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ni-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.4006304 </td>
   <td style="text-align:right;"> 0.3281250 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.2,-0.06] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0531756 </td>
   <td style="text-align:right;"> -0.1250095 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Cu-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.4810088 </td>
   <td style="text-align:right;"> 0.4056604 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.11,0.03] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0897875 </td>
   <td style="text-align:right;"> -0.0385440 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Na-Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.4031737 </td>
   <td style="text-align:right;"> 0.3396226 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-0.2,-0.05] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0966599 </td>
   <td style="text-align:right;"> -0.1226956 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Cu-Percentile </td>
   <td style="text-align:left;"> Nonchlorine Water </td>
   <td style="text-align:right;"> 0.4893134 </td>
   <td style="text-align:right;"> 0.3829761 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:left;"> [-0.09,0.03] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0067627 </td>
   <td style="text-align:right;"> -0.0296008 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Nonchlorine Water </td>
   <td style="text-align:right;"> 0.5700401 </td>
   <td style="text-align:right;"> 0.6630435 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:left;"> [0.05,0.18] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.0194980 </td>
   <td style="text-align:right;"> 0.1169235 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Zn-Percentile </td>
   <td style="text-align:left;"> Nonchlorine Water </td>
   <td style="text-align:right;"> 0.5713943 </td>
   <td style="text-align:right;"> 0.6345335 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:left;"> [0.03,0.16] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0612172 </td>
   <td style="text-align:right;"> 0.0985985 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6105459 </td>
   <td style="text-align:right;"> 0.6833440 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:left;"> [0.11,0.2] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.0065425 </td>
   <td style="text-align:right;"> 0.1558606 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6118733 </td>
   <td style="text-align:right;"> 0.6882460 </td>
   <td style="text-align:right;"> 136 </td>
   <td style="text-align:left;"> [0.11,0.21] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0068489 </td>
   <td style="text-align:right;"> 0.1569066 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols-Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6264502 </td>
   <td style="text-align:right;"> 0.6960784 </td>
   <td style="text-align:right;"> 121 </td>
   <td style="text-align:left;"> [0.12,0.23] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0263368 </td>
   <td style="text-align:right;"> 0.1757588 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Organic Certified </td>
   <td style="text-align:right;"> 0.4307511 </td>
   <td style="text-align:right;"> 0.2555556 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.23,0.01] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0322634 </td>
   <td style="text-align:right;"> -0.1018541 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix-Percentile </td>
   <td style="text-align:left;"> Organic Certified </td>
   <td style="text-align:right;"> 0.4307511 </td>
   <td style="text-align:right;"> 0.2555556 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.23,0.01] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0322634 </td>
   <td style="text-align:right;"> -0.1018541 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P-Percentile </td>
   <td style="text-align:left;"> Organic Certified </td>
   <td style="text-align:right;"> 0.4641100 </td>
   <td style="text-align:right;"> 0.5625000 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [-0.14,0.04] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0785788 </td>
   <td style="text-align:right;"> -0.0515031 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Fe-Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.6955719 </td>
   <td style="text-align:right;"> 0.8125000 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:left;"> [0.14,0.26] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0038245 </td>
   <td style="text-align:right;"> 0.2017191 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mo-Percentile </td>
   <td style="text-align:left;"> Tillage </td>
   <td style="text-align:right;"> 0.5869337 </td>
   <td style="text-align:right;"> 0.6562500 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:left;"> [0.03,0.16] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0947683 </td>
   <td style="text-align:right;"> 0.0971473 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ni-Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4200323 </td>
   <td style="text-align:right;"> 0.2656250 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.22,0] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0194429 </td>
   <td style="text-align:right;"> -0.1062340 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Al-Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4349358 </td>
   <td style="text-align:right;"> 0.3018868 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.22,0.01] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0625482 </td>
   <td style="text-align:right;"> -0.1049048 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mg-Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5803236 </td>
   <td style="text-align:right;"> 0.6923077 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-0.01,0.2] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.0997257 </td>
   <td style="text-align:right;"> 0.0865698 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>

![](dataAnalysis_files/figure-html/percentileShiftsTable-1.png)<!-- -->

## BQI Shifts

![](dataAnalysis_files/figure-html/unnamed-chunk-1-1.png)<!-- -->



#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](dataAnalysis_files/figure-html/bqi1bqi2-1.png)<!-- -->


