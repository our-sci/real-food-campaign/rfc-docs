---
title: "Shifts for Calcium in Kale"
author: "Real Food Campaign"
date: "07/13/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---



# Farm  Practices

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biodynamic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">6.99</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-0.66</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">6.22</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Hydroponic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">-0.56</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Irrigation </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">8.13</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Local </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-14.28</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nospray </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-6.9</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-11.11</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">0.86</span> </td>
  </tr>
</tbody>
</table>

# Soil Amendments

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Mulch </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">7.05</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">-6.32</span> </td>
  </tr>
</tbody>
</table>

# Tillage Practices

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-28.56</span> </td>
  </tr>
</tbody>
</table>

# Land Preparation

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Broadforking </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">19.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sheet Mulching </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">32.37</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Solarization </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">16.22</span> </td>
  </tr>
</tbody>
</table>
