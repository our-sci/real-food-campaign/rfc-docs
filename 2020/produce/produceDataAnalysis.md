---
title: "2020 Survey, Produce Samples Data Analysis"
author: "Real Food Campaign"
date: "01/07/2022"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---













# Some Summary Statistics

## Frequency of Each Farm Practice

* The amount of *missing* samples refers to those samples for which we lack enought information about farm practices. This category will add to the total together with the *reference*  samples and *not reference* samples, which respectively are those samples avoiding all the management practices we are going to study or those practiging at least one of those practices.


<!--html_preserve--><div id="htmlwidget-d93f6147c052e39954a3" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-d93f6147c052e39954a3">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53"],["Total","Not Reference","farm_practices.hydroponic","farm_practices.biodynamic","Reference","farm_practices.nospray","farm_practices.herbicide","farm_practices.biological","farm_practices.certified_organic","farm_practices.regenerative","farm_practices.organic","farm_practices.greenhouse","farm_practices.local","farm_practices.transitioning","farm_practices.irrigation","farm_practices.notill","amendments.none","amendments.organic_amendment","amendments.mulch","amendments.synth_fertilizer","amendments.lime","amendments.inoculant","amendments.compost","amendments.Manure","amendments.foliar_spray","amendments.org_fertilizer","amendments.synth_fertlizer","seed_treatment.none","seed_treatment.homéopathie pour gasteropode 'helix tosca'","seed_treatment.biological","seed_treatment.fungicide","seed_treatment.insecticide","seed_treatment.metam_sodium_fumigation","seed_treatment.enrobage argile bentonite","seed_treatment.green_sprouting","seed_treatment.bd_500 bd_barrel_compound bd_507_valerian","seed_treatment.inoculum","seed_treatment.bark_dust","seed_treatment.forest_imo","seed_treatment.pelleted_seeds","land_prep.sheet_mulching","land_prep.solarization","land_prep.notill","land_prep.broadforking","land_prep.none","farm_irrigation.none","farm_irrigation.nonchlorine_water","farm_irrigation.chlorinated","covercropping.practiced","covercropping.none","tillage.light_tillage","tillage.heavy_tillage","tillage.none"],[2081,1563,15,124,518,494,98,113,416,234,257,9,51,47,360,450,1125,707,208,160,49,53,18,21,18,18,48,606,6,42,86,83,25,1,5,3,9,16,3,3,80,128,450,163,452,1734,311,49,370,589,169,213,450]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>practice<\/th>\n      <th>amount<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":2},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


## Censoring The More Evident Outliers




## General: Variation and ratios between maximum and mimimum value


<!--html_preserve--><div id="htmlwidget-464ef4292b32cf49dc56" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-464ef4292b32cf49dc56">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108"],["Antioxidants_max","Antioxidants_median","Antioxidants_min","Antioxidants_range","Antioxidants_ratio","Antioxidants_sd","Brix_max","Brix_median","Brix_min","Brix_range","Brix_ratio","Brix_sd","Polyphenols_max","Polyphenols_median","Polyphenols_min","Polyphenols_range","Polyphenols_ratio","Polyphenols_sd","produce_xrf_Al_max","produce_xrf_Al_median","produce_xrf_Al_min","produce_xrf_Al_range","produce_xrf_Al_ratio","produce_xrf_Al_sd","produce_xrf_Ca_max","produce_xrf_Ca_median","produce_xrf_Ca_min","produce_xrf_Ca_range","produce_xrf_Ca_ratio","produce_xrf_Ca_sd","produce_xrf_Cl_max","produce_xrf_Cl_median","produce_xrf_Cl_min","produce_xrf_Cl_range","produce_xrf_Cl_ratio","produce_xrf_Cl_sd","produce_xrf_Cu_max","produce_xrf_Cu_median","produce_xrf_Cu_min","produce_xrf_Cu_range","produce_xrf_Cu_ratio","produce_xrf_Cu_sd","produce_xrf_Fe_max","produce_xrf_Fe_median","produce_xrf_Fe_min","produce_xrf_Fe_range","produce_xrf_Fe_ratio","produce_xrf_Fe_sd","produce_xrf_K_max","produce_xrf_K_median","produce_xrf_K_min","produce_xrf_K_range","produce_xrf_K_ratio","produce_xrf_K_sd","produce_xrf_Mg_max","produce_xrf_Mg_median","produce_xrf_Mg_min","produce_xrf_Mg_range","produce_xrf_Mg_ratio","produce_xrf_Mg_sd","produce_xrf_Mn_max","produce_xrf_Mn_median","produce_xrf_Mn_min","produce_xrf_Mn_range","produce_xrf_Mn_ratio","produce_xrf_Mn_sd","produce_xrf_Mo_max","produce_xrf_Mo_median","produce_xrf_Mo_min","produce_xrf_Mo_range","produce_xrf_Mo_ratio","produce_xrf_Mo_sd","produce_xrf_Na_max","produce_xrf_Na_median","produce_xrf_Na_min","produce_xrf_Na_range","produce_xrf_Na_ratio","produce_xrf_Na_sd","produce_xrf_Ni_max","produce_xrf_Ni_median","produce_xrf_Ni_min","produce_xrf_Ni_range","produce_xrf_Ni_ratio","produce_xrf_Ni_sd","produce_xrf_P_max","produce_xrf_P_median","produce_xrf_P_min","produce_xrf_P_range","produce_xrf_P_ratio","produce_xrf_P_sd","produce_xrf_S_max","produce_xrf_S_median","produce_xrf_S_min","produce_xrf_S_range","produce_xrf_S_ratio","produce_xrf_S_sd","produce_xrf_Si_max","produce_xrf_Si_median","produce_xrf_Si_min","produce_xrf_Si_range","produce_xrf_Si_ratio","produce_xrf_Si_sd","produce_xrf_Zn_max","produce_xrf_Zn_median","produce_xrf_Zn_min","produce_xrf_Zn_range","produce_xrf_Zn_ratio","produce_xrf_Zn_sd"],[22477.4,12530.17,5249.02,17228.38,4.28220886946516,3729.60771690923,18.2,14.9,11.3,6.9,1.61061946902655,1.69876500164164,121.28,59.655,24.41,96.87,4.96845555100369,20.3313878412611,0.31,0.195,0.14,0.17,2.21428571428571,0.0357682599668006,8.99,2.175,0.26,8.73,34.5769230769231,2.22694993208199,0.38,0.32,0.01,0.37,38,0.174427635425124,0.16,0.13,0.08,0.08,2,0.0173367068242147,1.55,1.225,1.05,0.5,1.47619047619048,0.0959773365061097,172.03,119.845,87.16,84.87,1.97372648003671,19.0274401427508,13.18,7.25,0.74,12.44,17.8108108108108,3.31313133526641,0.68,0.46,0.09,0.59,7.55555555555556,0.108006822396599,null,null,null,null,null,null,7.02,2.005,0.24,6.78,29.25,1.83754748932108,0.12,0.09,0.04,0.08,3,0.0173887440907224,13.22,8.48,4.01,9.21,3.29675810473816,2.01383453735893,5.42,1.62,0.1,5.32,54.2,1.14571723306498,41.3,33.255,28.6,12.7,1.44405594405594,2.51910251353219,0.12,0.04,0.01,0.11,12,0.0290287214094363],[33301.27,9632.65,247.95,33053.32,134.306392417826,7880.49474604138,17.2,10.9,3.8,13.4,4.52631578947368,2.55483492804338,328.96,118.33,11.23,317.73,29.2929652715939,71.8758171669189,0.85,0.45,0.01,0.84,85,0.118203199453851,93.99,12,0.16,93.83,587.4375,11.7659640779809,272.81,53.37,1,271.81,272.81,50.8965535727023,0.16,0.12,0.02,0.14,8,0.0181125161451058,3.89,1.07,0.07,3.82,55.5714285714286,0.309457620802453,641.74,307.59,8.49,633.25,75.5877502944641,92.7021755514908,78.41,27.35,2.79,75.62,28.1039426523297,13.2774581344038,4.73,0.79,0.04,4.69,118.25,0.747134906437312,0.02,0.01,0.01,0.01,2,0.00150755672288882,26.73,6.07,0.11,26.62,243,5.66670062734922,0.12,0.06,0.01,0.11,12,0.0205481069978531,68.44,33.49,0.01,68.43,6844,13.3266750873763,34.88,11.875,0.09,34.79,387.555555555556,4.78677545827331,202.49,28.345,2.81,199.68,72.0604982206406,13.1488872872839,5.06,0.48,0.11,4.95,46,0.642792482684124],[45536.13,18063.54,1984.89,43551.24,22.941387180146,16732.5987554077,15.8,12.2,8,7.8,1.975,1.86859381812711,567.65,410.365,112.04,455.61,5.0664941092467,149.023930630738,0.27,0.19,0.13,0.14,2.07692307692308,0.0303204177262194,22.75,6.35,0.5,22.25,45.5,6.00579178443469,2.4,1.2,0.04,2.36,60,0.757326607048498,0.23,0.17,0.13,0.1,1.76923076923077,0.0232992949004287,1.87,1.48,1.24,0.63,1.50806451612903,0.146892174483087,108.22,81.67,62.85,45.37,1.72187748607796,10.2583668495608,14.55,7.505,0.2,14.35,72.75,3.18204829485765,1.59,0.82,0.52,1.07,3.05769230769231,0.278440434617985,0.01,0.01,0.01,0,1,0,29.28,19.59,0.41,28.87,71.4146341463415,6.59986099930022,0.16,0.12,0.09,0.07,1.77777777777778,0.0162852719975324,13.65,8.14,4.3,9.35,3.17441860465116,2.02204954680836,8.46,5.14,0.93,7.53,9.09677419354839,1.85089517293024,58.89,42.76,32.81,26.08,1.79487960987504,6.31617078975689,0.5,0.21,0.06,0.44,8.33333333333333,0.0967514358350017],[242.74,47.015,0.05,242.69,4854.8,39.3443559487833,13.2,8.3,4.2,9,3.14285714285714,1.62060264752853,49.85,6.28,0.44,49.41,113.295454545455,6.16579987706788,0.73,0.39,0.02,0.71,36.5,0.113622901827925,68.09,39.76,24.84,43.25,2.74114331723027,10.0994051854857,85.03,15.355,0.58,84.45,146.603448275862,20.8730214396199,0.17,0.1,0.01,0.16,17,0.0210129572198164,2.85,1.04,0.05,2.8,57,0.39246534318623,480.58,261.79,6.61,473.97,72.7049924357035,84.5438327800703,43.33,16.83,4.83,38.5,8.97101449275362,7.06837358817015,0.83,0.45,0.02,0.81,41.5,0.147221015419232,null,null,null,null,null,null,18.02,5.77,0.13,17.89,138.615384615385,3.32850735148021,0.09,0.05,0.02,0.07,4.5,0.0149583232227312,82.62,35.71,20.36,62.26,4.05795677799607,12.1730225533193,29.25,11.61,5.07,24.18,5.76923076923077,4.23236406739768,119.76,28.15,0.19,119.57,630.315789473684,16.7578618813601,1.5,0.34,0.04,1.46,37.5,0.192660922515984],[4492.26,1170.505,501.92,3990.34,8.95015141855276,863.598058907301,24.6,17.9,10.5,14.1,2.34285714285714,3.4185280231945,272.7,90.535,32.99,239.71,8.26614125492573,61.2853822617454,0.69,0.34,0.23,0.46,3,0.1032444894567,66.47,6.87,0.07,66.4,949.571428571428,13.1471657921906,14.87,2.185,0.02,14.85,743.5,2.52460033723065,0.29,0.21,0.14,0.15,2.07142857142857,0.0349702254305955,3.59,1.64,1.04,2.55,3.45192307692308,0.314624997240984,453.44,209.735,137.08,316.36,3.3078494309892,58.2428226961067,26.81,9.86,0.35,26.46,76.6,6.37962797626535,1.35,0.555,0.28,1.07,4.82142857142857,0.189886923327526,0.05,0.03,0.01,0.04,5,0.00979449862881355,56.23,36.715,3.34,52.89,16.8353293413174,10.8792364570266,0.18,0.125,0.04,0.14,4.5,0.0304655510632469,46.37,17.85,8.5,37.87,5.45529411764706,6.83853437374942,21.71,6.155,1.01,20.7,21.4950495049505,3.89176622438169,94.15,41.33,25.37,68.78,3.71107607410327,11.9026940550805,1.03,0.12,0.02,1.01,51.5,0.164705251854065],[14005.18,8106.46,508.16,13497.02,27.5605714735516,3784.84994339669,10.7,6.5,1.8,8.9,5.94444444444444,2.16055271724577,215.79,76.8,23.59,192.2,9.1475201356507,39.9340779532298,0.56,0.21,0.12,0.44,4.66666666666667,0.0598750674044085,17.94,4.28,0.02,17.92,897,3.4065058688687,23.83,5.82,0.25,23.58,95.32,3.79880422989533,0.08,0.06,0.03,0.05,2.66666666666667,0.00845447938198454,4.71,0.66,0.46,4.25,10.2391304347826,0.473444586592916,231.78,145.33,88.66,143.12,2.6142567110309,27.4514007416946,18.31,8.72,2.51,15.8,7.29482071713147,3.27361145681797,0.85,0.26,0.11,0.74,7.72727272727273,0.0730622695931028,0.01,0.01,0.01,0,1,0,4.34,1.455,0.02,4.32,217,0.973498819925358,0.04,0.03,0.01,0.03,4,0.00744982463945662,31.86,15.7,8.84,23.02,3.60407239819005,4.24636483669705,19.27,10.24,6.13,13.14,3.14355628058728,2.52042164924884,102.91,14.53,7.64,95.27,13.4698952879581,11.4203420699561,0.5,0.19,0.08,0.42,6.25,0.076376409326456],[989.59,97.49,12.21,977.38,81.0475020475021,106.435732836696,11.9,5.8,2.8,9.1,4.25,1.17205314525521,165.62,35.085,9.13,156.49,18.1401971522453,32.433119561949,1.61,0.66,0.19,1.42,8.47368421052632,0.134488520416901,86.29,8.57,0.05,86.24,1725.8,10.6171297569093,151.86,51.65,19.21,132.65,7.90525767829256,21.3116771066207,0.33,0.21,0.13,0.2,2.53846153846154,0.0300712944544303,13.74,1.9,0.83,12.91,16.5542168674699,1.1254127108256,790.5,459.25,221.33,569.17,3.57158993358334,106.610345665025,62.29,26.575,3.9,58.39,15.9717948717949,9.93278986335391,2.57,0.69,0.33,2.24,7.78787878787879,0.208143455361914,0.03,0.01,0.01,0.02,3,0.00296360820978467,26.14,9.275,0.18,25.96,145.222222222222,3.84762606662497,0.19,0.14,0.08,0.11,2.375,0.0187400449853013,104.83,37.45,11.45,93.38,9.15545851528384,13.9834010500969,59.3,18.89,1.54,57.76,38.5064935064935,7.72416616844898,298.98,64.24,16.53,282.45,18.0871143375681,24.6109598453242,1.23,0.21,0.01,1.22,123,0.181260209011488],[297.82,57.79,28.89,268.93,10.3087573554863,42.0209587943151,11.6,6.65,3.5,8.1,3.31428571428571,1.40819269712169,52.58,27.35,3.29,49.29,15.9817629179331,11.7217037917422,3.31,0.52,0.26,3.05,12.7307692307692,0.298831478225068,74.37,27.52,0.34,74.03,218.735294117647,13.2184502177659,157.22,80.705,2.97,154.25,52.9360269360269,33.0002350645811,0.25,0.13,0.06,0.19,4.16666666666667,0.0230656217816293,45.54,1.11,0.78,44.76,58.3846153846154,4.41274842698606,636.17,361.615,175.8,460.37,3.61871444823663,81.0988020456179,54.23,27.77,12.3,41.93,4.40894308943089,7.06440101884081,0.7,0.42,0.16,0.54,4.375,0.100566489024986,0.01,0.01,0.01,0,1,0,13.2,8.77,4.9,8.3,2.69387755102041,2.24731004295131,0.11,0.05,0.03,0.08,3.66666666666667,0.0172536885614902,95.86,44.705,10.81,85.05,8.8677150786309,20.2289357847106,50.45,24.85,7.07,43.38,7.13578500707214,7.18549483420765,939.62,73.025,6.67,932.95,140.872563718141,88.0506508402167,1.15,0.48,0.2,0.95,5.75,0.1567869235313],[6438.57,3416.185,1814.46,4624.11,3.54847723289574,1195.60831761916,7,3.65,2.2,4.8,3.18181818181818,0.917913412731744,152.67,93.73,44.98,107.69,3.39417518897288,23.1023928091792,0.35,0.25,0.18,0.17,1.94444444444444,0.03086174901796,14.43,5.815,0.03,14.4,481,3.36422559737847,50.76,32.725,6.21,44.55,8.17391304347826,11.4021415687717,0.06,0.04,0.03,0.03,2,0.00720657560390037,0.89,0.41,0.29,0.6,3.06896551724138,0.112869026971164,242.56,188.495,123.44,119.12,1.9650032404407,23.8720969911578,15.3,8.835,4.52,10.78,3.38495575221239,2.50589981698668,0.38,0.22,0.12,0.26,3.16666666666667,0.0569343055768238,null,null,null,null,null,null,4.32,0.85,0.06,4.26,72,1.02838034987965,0.03,0.02,0.01,0.02,3,0.00532618985469898,30.46,15.86,8.23,22.23,3.70109356014581,5.99095150845757,13.87,9.13,4.85,9.02,2.85979381443299,2.21195596869811,25.66,10.945,8.79,16.87,2.91922639362912,2.12958078871508,0.52,0.15,0.07,0.45,7.42857142857143,0.0698177781408299],[494.58,150.48,6.31,488.27,78.3803486529319,84.0243512965391,8,3.7,2,6,4,1.05522222275402,72.42,26.57,1.82,70.6,39.7912087912088,12.8463593563334,0.58,0.25,0.01,0.57,58,0.0671166248613754,43.59,16.2,1.02,42.57,42.7352941176471,8.16033932322741,73.2,20.84,2.34,70.86,31.2820512820513,14.0566535920123,0.08,0.04,0.01,0.07,8,0.00988693134809362,2.77,0.47,0.02,2.75,138.5,0.283618620090225,282.26,188.86,4.83,277.43,58.4389233954451,43.5909253783356,36.08,19.17,3.27,32.81,11.0336391437309,5.96571856589236,0.95,0.25,0.01,0.94,95,0.118985679429301,0.01,0.01,0.01,0,1,0,4.54,1.27,0.04,4.5,113.5,1.06125822338279,0.04,0.02,0.01,0.03,4,0.00657121575463962,49.36,31.22,0.08,49.28,617,8.5563501216742,46.78,10.37,3.05,43.73,15.3377049180328,3.2149439185866,108.29,26.93,4.5,103.79,24.0644444444444,15.53128950407,0.67,0.31,0.02,0.65,33.5,0.106501550657582]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>variable<\/th>\n      <th>apple<\/th>\n      <th>beet<\/th>\n      <th>blueberry<\/th>\n      <th>carrot<\/th>\n      <th>grapes<\/th>\n      <th>peppers<\/th>\n      <th>potato<\/th>\n      <th>squash_butternut<\/th>\n      <th>tomato<\/th>\n      <th>zucchini<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5,6,7,8,9,10,11]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


### Histograms of the Main Nutrients by Crop



![](produceDataAnalysis_files/figure-html/preliminaryhistograms-1.png)<!-- -->



# Farm Practices: Median Shifts Comparisons







# Synthesis Median Shift Tables.

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is intersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting magnitudes are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.




## Color Scale

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Farm Practices 

* The reference category used to define the neutral median is defined as *Farms that are not organic, transitioning, regenerative, no till, hydroponic, coming from a greenhouse, or cover crops and that indicated they are using tillage or samples from stores or farm markets that have been clearly identified as fulfiling these same conditions*. It is called *Reference* (and, inside the code and functions, `farm_practices.none`).

* There's plenty of columns prefixed by *farm_practices* on the dataset, but they are not mutually related in a sound logical way, that's why calculations can't be easily performed over the whole set unless a good reference is defined. 


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Bqi </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biodynamic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-32.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-31.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-12.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-4.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-40.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-8.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">7.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-8.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-3.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-3.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-5.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-9.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-0.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-5.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 31">-1.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">1.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">22.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-33.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-12.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">13.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 26">-4.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-11.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">14.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-8.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">23.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">1.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-21.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-10.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-13.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">11.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 26">-14.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">4.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-8.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-8.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">16.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 26">-17.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 70">-6.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-16.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">-3.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">1.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-9.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-0.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 51">-12.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 72">-25.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">10.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">-3.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">23.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-3.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-14.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 53">-4.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 72">0.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-8.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 83">5.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">21.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 89">5.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">9.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-12.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 68">-9.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-8.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">-4.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-13.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 89">11.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-1.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">-25.58</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Greenhouse </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-10.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">34.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">18.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-3.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Hydroponic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">2.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-11.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-19.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-32.58</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Irrigation </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 53">-4.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 127">-23.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-6.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-8.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">29.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">-11.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 137">-42.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">6.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">40.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">-12.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">0.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 140">-2.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">8.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-3.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">-9.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">4.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-12.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">1.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-5.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">-13.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Local </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">4.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-0.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-23.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">22.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-10.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-2.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">26.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">6.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-5.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-8.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-18.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-2.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-10.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-11.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">5.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">4.59</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nospray </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 76">-6.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 120">-16.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-10.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 71">-11.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 96">-18.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 72">-31.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 120">-22.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 83">-1.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">27.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 95">-4.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 70">0.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 119">-4.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-5.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 70">1.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 94">-12.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 68">3.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-14.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 86">-5.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-9.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 93">1.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-15.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 136">-23.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">-10.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-17.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-25.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-22.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 145">-26.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">6.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">24.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-25.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-13.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 137">-2.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 54">-5.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-6.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-21.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">9.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">-4.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">-6.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-10.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-7.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-39.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-25.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-14.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-28.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 48">-35.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-48.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">-16.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">11.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">27.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 48">-5.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-16.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 47">-5.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-19.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">-21.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-5.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-12.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-5.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-2.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-2.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-38.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 47">-30.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 60">-15.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">-40.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-27.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-49.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">2.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-2.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-8.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-15.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 51">-5.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 60">-8.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-24.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 40">-21.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-1.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 47">-4.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 59">-8.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 31">-2.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-6.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Transitioning </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">33.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">26.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-17.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-44.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">5.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-1.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">2.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-23.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-15.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-2.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-32.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>



## Percentual Shift Over the Median for Soil Amendments

The reference category are farms that informed they do not use soil amendments.The size of that reference sample is 1057 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Bqi </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Inoculant </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">58.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-26.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-15.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">50.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">14.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">12.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-7.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-10.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">18.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">20.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-8.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">1.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Lime </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-45.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-10.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">7.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-42.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-12.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">20.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-4.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">40</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-4.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Mulch </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">16.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-22.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-8.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">19.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-23.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">27.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-24.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">7.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">12.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">-12.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">1.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 42">-5.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">10.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">10.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">-24.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">2.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-21.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-3.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">-11.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-5.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 98">-5.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 211">-12.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 124">-5.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 79">-7.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 103">-7.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 95">-10.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 223">-14.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 112">7.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">15.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 102">3.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">1.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 211">-5.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 126">2.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 78">-10.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 103">-14.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 86">0.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 72">-11.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 120">0.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">-2.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 99">-0.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Tillage Intensity

The reference category are farms informing they avoid tillage. The size of that reference sample is 438 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Bqi </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">47.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">21.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">2.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">41.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 118">20.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-26.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-5.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-22.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">6.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 117">30.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">11.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">2.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">5.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">52.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 118">30.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-11.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-1.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">2.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-1.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 117">10.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 48">2.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-1.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 22">19.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-1.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 55">38.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">10.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">1.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">29.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 55">63.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">14.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-12.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 22">5.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">5.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 55">6.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">-7.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-18.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 22">12.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-1.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 55">0.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Irrigation


The reference category are farms informing no irrigation. The size of that reference sample is 1640 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Bqi </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Chlorinated </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">48.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">9.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-5.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-65.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">52.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-27.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">14.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">51.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">8.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-4.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-2.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-12.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">10.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-2.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">3.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nonchlorine Water </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">-0.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 115">-9.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-6.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">14.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">47.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">6.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 122">-28.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">3.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">16.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-16.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">2.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 125">-1.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">10.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-3.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-13.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">9.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-1.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">8.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">0.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-20.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Land Preparation Techniques


The reference category are farms informing that they use any form of tillage. The size of that reference sample is 427 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Beet Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Carrot Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Zucchini Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Peppers Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Potato Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Grapes Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Tomato Bqi </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Broadforking </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">-14.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">13.89</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-14.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-2.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-38.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">24.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">2.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-18.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-23.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-18.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 48">7.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-2.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-8.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 22">-15.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-0.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">17.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-4.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-0.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-6.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">-9.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 136">-5.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">-8.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-4.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-23.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 41">5.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 145">-1.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 45">17.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-9.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">-24.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-14.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 137">5.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 54">-5.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">-19.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">-16.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">8.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">15.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">-4.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-2.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-7.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sheet Mulching </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-10.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">1.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">6.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">44.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">37.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-25.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">32.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-35.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-6.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">7.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-18.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-9.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">19.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-13.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Solarization </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-0.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 58">-8.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">19.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-10.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">2.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">-7.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-2.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-8.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">-9.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 63">6.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-47.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">14.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">8.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-12.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">18.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>



# Random Forest on Antioxidants, Polyphenols and Brix

  These regressions are performed using a *Random Forest* algorithm. The only tuned parameter besides the selection of initial variables is `mtry`.
  For the classifications, we are using *Accuracy* as a precission metric, while for regressions the custom random forest $R^2$ is used, which is a special metric comparable to the homonym in the context of linear models.

### Compared Datasets

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-vis-who </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole </td>
   <td style="text-align:left;"> metadata     , farmPractices </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed       , nir_whole       , nir_processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices </td>
   <td style="text-align:left;"> color    , whole    , nir_whole </td>
   <td style="text-align:left;"> color, whole </td>
  </tr>
</tbody>
</table>



## Random Forest Regression



<!--html_preserve--><div id="htmlwidget-6ddd5083d37ee1de58d7" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-6ddd5083d37ee1de58d7">{"x":{"filter":"none","extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"],["Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols"],["c-min","c-min","c-min-nir-who","c-min-nir-who","c-min-vis-who","c-min-vis-who","c-no-nir-who","c-no-nir-who","c-no-vis-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min","c-min-nir-who","c-min-nir-who","c-min-vis-who","c-min-vis-who","c-no-nir-who","c-no-nir-who","c-no-vis-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min","c-min-nir-who","c-min-nir-who","c-min-vis-who","c-min-vis-who","c-no-nir-who","c-no-nir-who","c-no-vis-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min","c-min-nir-who","c-min-nir-who","c-min-vis-who","c-min-vis-who","c-no-nir-who","c-no-nir-who","c-no-vis-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[null,null,null,null,null,null,0.38954384805563,0.38954384805563,0.46940476818587,0.46940476818587,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.357533416269244,0.357533416269244,0.365224576618038,0.365224576618038,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.404988734724249,0.404988734724249,0.402223738753445,0.402223738753445,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.48104194552488,0.48104194552488,0.474290654375583,0.474290654375583,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,78,78,78,78,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,76,76,76,76,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,78,78,78,78,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,78,78,78,78,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,0.743589743589744,0.743589743589744,0.717948717948718,0.717948717948718,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.736842105263158,0.736842105263158,0.802631578947368,0.802631578947368,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.743589743589744,0.743589743589744,0.756410256410256,0.756410256410256,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.833333333333333,0.833333333333333,0.794871794871795,0.794871794871795,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,0.45632162835944,0.45632162835944,0.446017860209071,0.446017860209071,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.515868865647626,0.515868865647626,0.548094328238133,0.548094328238133,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.617647058823529,0.617647058823529,0.573529411764706,0.573529411764706,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.591784154709919,0.591784154709919,0.522258265751715,0.522258265751715,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[0.198210366057401,0.198210366057401,0.329120606624133,0.329120606624133,0.299066514011918,0.299066514011918,0.347369833022536,0.347369833022536,0.235272046994972,0.235272046994972,0.692754840177878,0.778951503792731,0.683505957414017,0.717262959426181,0.638151581485662,0.714701072342357,0.798215194900896,0.548655761954536,0.671759114155789,0.763227196355535,0.71393816632979,0.847399169748222,0.781143846778918,0.772342064029088,0.635184084468067,0.222712799508267,0.222712799508267,0.304959931674576,0.304959931674576,0.254152122998851,0.254152122998851,0.23285111934136,0.23285111934136,0.192499365563586,0.192499365563586,0.51897983089734,0.608687593233914,0.442307337986161,0.607623877008534,0.522286051061285,0.513871378507569,0.686738413403687,0.406483604233711,0.522325621492787,0.683793939164291,0.508836248389668,0.706876041469749,0.574337542562162,0.700185367746315,0.568822134710413,0.339428246989849,0.339428246989849,0.214629650872701,0.214629650872701,0.247028426175222,0.247028426175222,0.1272048550003,0.1272048550003,0.12400476257645,0.12400476257645,0.698289999635334,0.511181059198589,0.495118078595538,0.606514370252035,0.606151346611465,0.753923613275882,0.534473273884891,0.662840453990084,0.604643281912947,0.755048393067587,0.596085753628629,0.709321544355303,0.673208042753117,0.554943334346482,0.643565210621315,0.323910254809532,0.323910254809532,0.354455685034762,0.354455685034762,0.380006881768482,0.380006881768482,0.241921873478441,0.241921873478441,0.240010819380966,0.240010819380966,0.672417330456989,0.686914708748591,0.489382564508868,0.692091836553481,0.443643304913961,0.656898370003339,0.575042929210782,0.61598574013107,0.655621373508105,0.655647548512702,0.567011260168233,0.689818877982078,0.635416631875158,0.549608131429528,0.543292612841776],[182,182,182,182,182,182,254,254,254,254,86,80,86,80,86,86,80,86,78,72,78,72,78,80,86,167,167,167,167,167,167,235,235,235,235,77,77,77,77,77,77,77,77,71,71,71,71,71,77,77,175,175,175,175,175,175,243,243,243,243,80,80,80,80,80,80,80,80,72,72,72,72,72,80,80,184,184,184,184,184,184,256,256,256,256,86,80,86,80,86,86,80,86,78,72,78,72,78,80,86],[0.774725274725275,0.774725274725275,0.82967032967033,0.82967032967033,0.802197802197802,0.802197802197802,0.755905511811024,0.755905511811024,0.771653543307087,0.771653543307087,0.686046511627907,0.8375,0.848837209302326,0.85,0.837209302325581,0.720930232558139,0.875,0.837209302325581,0.82051282051282,0.861111111111111,0.871794871794872,0.861111111111111,0.846153846153846,0.875,0.779069767441861,0.796407185628742,0.796407185628742,0.74251497005988,0.74251497005988,0.748502994011976,0.748502994011976,0.795744680851064,0.795744680851064,0.748936170212766,0.748936170212766,0.675324675324675,0.818181818181818,0.792207792207792,0.74025974025974,0.792207792207792,0.636363636363636,0.818181818181818,0.779220779220779,0.845070422535211,0.774647887323944,0.816901408450704,0.830985915492958,0.788732394366197,0.818181818181818,0.805194805194805,0.754285714285714,0.754285714285714,0.771428571428571,0.771428571428571,0.76,0.76,0.765432098765432,0.765432098765432,0.757201646090535,0.757201646090535,0.6875,0.775,0.825,0.8,0.825,0.675,0.8125,0.7875,0.819444444444444,0.777777777777778,0.833333333333333,0.791666666666667,0.819444444444444,0.825,0.8,0.701086956521739,0.701086956521739,0.755434782608696,0.755434782608696,0.760869565217391,0.760869565217391,0.75390625,0.75390625,0.7578125,0.7578125,0.697674418604651,0.8375,0.790697674418605,0.7875,0.813953488372093,0.697674418604651,0.7875,0.802325581395349,0.756410256410256,0.791666666666667,0.756410256410256,0.805555555555556,0.756410256410256,0.775,0.813953488372093],[0.70587580448323,0.70587580448323,0.680305961809742,0.680305961809742,0.677609205579404,0.677609205579404,0.662108321084273,0.662108321084273,0.64340637175534,0.64340637175534,0.370610472006822,0.544404672297481,0.676695875288982,0.611950102274285,0.668585980476963,0.448529807878786,0.55880728672791,0.658802644013951,0.537962733195592,0.59058620288936,0.640214673607673,0.493584037254461,0.634888028145516,0.644135057558371,0.587033472680985,0.533050900754018,0.533050900754018,0.517873181066536,0.517873181066536,0.518657484646035,0.518657484646035,0.52468947616592,0.52468947616592,0.496350940184249,0.496350940184249,0.45795281791516,0.675512730882536,0.671439093731439,0.492725355970791,0.652006326578694,0.442352895111342,0.537486192323464,0.687037450140634,0.645460523543105,0.480903205787892,0.643379929922916,0.533410511969524,0.593707466915336,0.501902443323176,0.634141923461198,0.440944881889764,0.440944881889764,0.448818897637795,0.448818897637795,0.448818897637795,0.448818897637795,0.488,0.488,0.4856,0.4856,0.328125,0.53125,0.547767857142857,0.526785714285714,0.547321428571429,0.331026785714286,0.558035714285714,0.542857142857143,0.471491228070175,0.501754385964912,0.543859649122807,0.530263157894737,0.543859649122807,0.531696428571429,0.500892857142858,0.369492703266157,0.369492703266157,0.447977762334955,0.447977762334955,0.457817929117443,0.457817929117443,0.434115357887422,0.434115357887422,0.422507296733843,0.422507296733843,0.353269269538337,0.531767752773598,0.573251125596398,0.527543485108319,0.560012767959142,0.350808077414152,0.521142337171456,0.529635105167663,0.352730337786741,0.580406052480368,0.495264053440887,0.576537061865543,0.478617343080413,0.516326602036888,0.571233452052954],[0.494423039624665,0.494423039624665,0.459208390392635,0.459208390392635,0.530937682319727,0.530937682319727,0.216402644911553,0.216402644911553,0.185458452955471,0.185458452955471,0.467367601352791,0.50876979896972,0.482445936841565,0.52730171757827,0.528366214892588,0.389635363995378,0.537665421921201,0.473632982815739,0.628514106979457,0.491290750716353,0.526252928238101,0.687513007466911,0.472494365426367,0.619215284295954,0.612900163835504,0.357232595726504,0.357232595726504,0.247700092682416,0.247700092682416,0.342095901810009,0.342095901810009,0.321881095626986,0.321881095626986,0.307452901887909,0.307452901887909,0.459980138809543,0.521943891082256,0.547880348219344,0.496837101726126,0.409285990257998,0.411807167092017,0.496572459421388,0.470377751778757,0.431827655591391,0.517125649220005,0.556145714881392,0.426749105900046,0.501181671161311,0.315998817195142,0.457161229349276,0.405660939397746,0.405660939397746,0.430035466465401,0.430035466465401,0.373670512973708,0.373670512973708,0.185634603539209,0.185634603539209,0.187225990405471,0.187225990405471,0.497497983583094,0.475320414721665,0.534669456287281,0.505555188603119,0.4339080124492,0.54258216653085,0.512879912466767,0.443572695120929,0.50236152399417,0.507068615298265,0.606492748903167,0.480922772455063,0.46163365373719,0.591867532217927,0.450163243810492,0.61213295107286,0.61213295107286,0.512942664526123,0.512942664526123,0.670209769746326,0.670209769746326,0.625251897035155,0.625251897035155,0.565342043244439,0.565342043244439,0.671022220459046,0.793477578726778,0.577801624951626,0.666135811777293,0.689439623382303,0.722688459815905,0.760506197232139,0.718903587779577,0.723906459644809,0.598057573706057,0.588859166358297,0.767305470656182,0.648732187542038,0.770045042791449,0.612586172391806],[146,146,146,146,146,146,406,406,406,406,111,101,111,101,111,111,101,111,106,98,106,98,106,101,111,135,135,135,135,135,135,151,151,151,151,101,101,101,101,101,101,101,101,98,98,98,98,98,101,101,139,139,139,139,139,139,403,403,403,403,103,103,103,103,103,103,103,103,100,100,100,100,100,103,103,151,151,151,151,151,151,416,416,416,416,114,103,114,103,114,114,103,114,109,100,109,100,109,103,114],[0.76027397260274,0.76027397260274,0.828767123287671,0.828767123287671,0.842465753424658,0.842465753424658,0.748768472906404,0.748768472906404,0.753694581280788,0.753694581280788,0.747747747747748,0.821782178217822,0.864864864864865,0.831683168316832,0.864864864864865,0.720720720720721,0.831683168316832,0.855855855855856,0.707547169811321,0.846938775510204,0.830188679245283,0.857142857142857,0.820754716981132,0.811881188118812,0.828828828828829,0.822222222222222,0.822222222222222,0.859259259259259,0.859259259259259,0.866666666666667,0.866666666666667,0.834437086092715,0.834437086092715,0.847682119205298,0.847682119205298,0.801980198019802,0.891089108910891,0.871287128712871,0.871287128712871,0.871287128712871,0.782178217821782,0.910891089108911,0.910891089108911,0.846938775510204,0.897959183673469,0.887755102040816,0.857142857142857,0.897959183673469,0.861386138613861,0.861386138613861,0.798561151079137,0.798561151079137,0.892086330935252,0.892086330935252,0.877697841726619,0.877697841726619,0.776674937965261,0.776674937965261,0.784119106699752,0.784119106699752,0.83495145631068,0.87378640776699,0.883495145631068,0.902912621359223,0.883495145631068,0.766990291262136,0.932038834951456,0.902912621359223,0.89,0.91,0.89,0.91,0.88,0.87378640776699,0.883495145631068,0.821192052980132,0.821192052980132,0.854304635761589,0.854304635761589,0.867549668874172,0.867549668874172,0.788461538461538,0.788461538461538,0.807692307692308,0.807692307692308,0.754385964912281,0.902912621359223,0.912280701754386,0.932038834951456,0.921052631578947,0.771929824561403,0.912621359223301,0.894736842105263,0.880733944954128,0.92,0.926605504587156,0.93,0.908256880733945,0.902912621359223,0.903508771929825],[0.0380273572784425,0.0380273572784425,0.0379401143147197,0.0379401143147197,0.0493321947685948,0.0493321947685948,0.468331404001984,0.468331404001984,0.460454219723279,0.460454219723279,0.309748427672956,0.379716981132076,0.379716981132076,0.364414869721474,0.340745732255166,0.309748427672956,0.377414645103324,0.378077268643307,0.309748427672956,0.363502358490566,0.34597371967655,0.300426774483378,0.37533692722372,0.378335579514825,0.373259209344115,0.53289407430228,0.53289407430228,0.538737255352493,0.538737255352493,0.495557073755558,0.495557073755558,0.501525483491608,0.501525483491608,0.483966112994714,0.483966112994714,0.391757722616873,0.441877416323157,0.410079361414319,0.598764358345131,0.631534682236225,0.336690418497931,0.579327405870581,0.523798682154964,0.438958157891775,0.590358181403449,0.461133292479038,0.431698326994893,0.421472991960518,0.400593257047106,0.414004737487891,0.344262295081967,0.344262295081967,0.39344262295082,0.39344262295082,0.491803278688525,0.491803278688525,0.423076923076923,0.423076923076923,0.417948717948718,0.417948717948718,0.387755102040816,0.346938775510204,0.322448979591836,0.502040816326531,0.408163265306122,0.204081632653061,0.448979591836735,0.489795918367347,0.469387755102041,0.346938775510204,0.346938775510204,0.469387755102041,0.316326530612245,0.36734693877551,0.326530612244898,0.0989813064482252,0.0989813064482252,0.103969754253308,0.103969754253308,0.102772526780088,0.102772526780088,0.170135135135135,0.170135135135135,0.17722972972973,0.17722972972973,0.065982942891072,0.0983914498542589,0.0979596243117781,0.118104285868509,0.0880384324732808,0.0663931771564288,0.0925186224765195,0.0921947533196589,0.0771888157184498,0.0935496059591925,0.0918708841627982,0.0912825218611681,0.0879844542804707,0.0904674511497355,0.0846971823383354],[0.427432800561351,0.427432800561351,0.466191781180637,0.466191781180637,0.393690545134587,0.393690545134587,0.332094409771261,0.332094409771261,0.371720104518774,0.371720104518774,0.653102207634116,0.547609449186513,0.591244742230559,0.596422718750826,0.610754942214727,0.576351672947838,0.641616832544201,0.575220499488732,0.539996258298947,0.589335410715993,0.544752023496536,0.519685598486492,0.521675156252118,0.530975164016569,0.637941156765071,0.175775838722336,0.175775838722336,0.38400574409317,0.38400574409317,0.333805318023626,0.333805318023626,0.199557614046648,0.199557614046648,0.218851715863755,0.218851715863755,0.545714695497353,0.451301384647127,0.524492831779685,0.527842072392621,0.55199112175677,0.612550501309072,0.425677876251711,0.479443403565221,0.526965481696231,0.400651194485875,0.688137356736627,0.658802160630153,0.557009769093899,0.546223807405618,0.641990478837694,0.70373969987588,0.70373969987588,0.695356796310632,0.695356796310632,0.688569470441101,0.688569470441101,0.773654940840146,0.773654940840146,0.717618951253392,0.717618951253392,0.702052686316556,0.905258406934029,0.84107105825005,0.778986423445465,0.766599514932621,0.774788163604189,0.867685859449143,0.856634457068236,0.835072450521021,0.952625842132337,0.787987755248241,0.930524673172704,0.866426276397265,0.933383330675342,0.763906087677568,0.239878423102729,0.239878423102729,0.405739518587445,0.405739518587445,0.374825895304961,0.374825895304961,0.22702743447942,0.22702743447942,0.238463102718326,0.238463102718326,0.45425421507616,0.519611649707949,0.544044589314398,0.575532828747621,0.503800021437791,0.457207738933204,0.514982634488526,0.502710393067619,0.438519536712902,0.454174384031716,0.533309490359608,0.421881370308579,0.677012066818285,0.488301500311383,0.523946029474314],[141,141,141,141,141,141,212,212,212,212,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,136,136,136,136,136,136,203,203,203,203,78,78,78,78,78,78,78,78,69,69,69,69,69,78,78,138,138,138,138,138,138,205,205,205,205,78,78,78,78,78,78,78,78,69,69,69,69,69,78,78,141,141,141,141,141,141,212,212,212,212,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81],[0.75177304964539,0.75177304964539,0.815602836879433,0.815602836879433,0.737588652482269,0.737588652482269,0.773584905660377,0.773584905660377,0.778301886792453,0.778301886792453,0.703703703703704,0.833333333333333,0.790123456790123,0.807692307692308,0.839506172839506,0.765432098765432,0.82051282051282,0.839506172839506,0.791666666666667,0.826086956521739,0.736111111111111,0.811594202898551,0.777777777777778,0.833333333333333,0.753086419753086,0.764705882352941,0.764705882352941,0.794117647058823,0.794117647058823,0.786764705882353,0.786764705882353,0.778325123152709,0.778325123152709,0.822660098522167,0.822660098522167,0.743589743589744,0.743589743589744,0.82051282051282,0.782051282051282,0.794871794871795,0.769230769230769,0.807692307692308,0.833333333333333,0.768115942028985,0.840579710144927,0.797101449275362,0.840579710144927,0.797101449275362,0.833333333333333,0.794871794871795,0.717391304347826,0.717391304347826,0.847826086956522,0.847826086956522,0.818840579710145,0.818840579710145,0.84390243902439,0.84390243902439,0.814634146341463,0.814634146341463,0.82051282051282,0.884615384615385,0.782051282051282,0.884615384615385,0.871794871794872,0.705128205128205,0.897435897435897,0.846153846153846,0.710144927536232,0.898550724637681,0.855072463768116,0.898550724637681,0.811594202898551,0.91025641025641,0.858974358974359,0.709219858156028,0.709219858156028,0.780141843971631,0.780141843971631,0.765957446808511,0.765957446808511,0.740566037735849,0.740566037735849,0.75,0.75,0.703703703703704,0.794871794871795,0.790123456790123,0.82051282051282,0.765432098765432,0.716049382716049,0.794871794871795,0.765432098765432,0.791666666666667,0.768115942028985,0.708333333333333,0.782608695652174,0.763888888888889,0.794871794871795,0.740740740740741],[0.609261733471665,0.609261733471665,0.752753579475225,0.752753579475225,0.518943765881517,0.518943765881517,0.548184078670685,0.548184078670685,0.537739060169846,0.537739060169846,0.629902287852368,0.590220680131193,0.70030602606701,0.557188848610392,0.780292517681299,0.752706485590683,0.529942723977214,0.765309690179359,0.650315898498187,0.594230139823925,0.63092266528569,0.63605523908165,0.593996892801657,0.695776626963577,0.577051847003367,0.592989793355529,0.592989793355529,0.679287126845198,0.679287126845198,0.65330218332663,0.65330218332663,0.610708369783493,0.610708369783493,0.587497240263895,0.587497240263895,0.500574707170245,0.629368300516271,0.661159922607153,0.612215461326915,0.624293035568473,0.522436180607746,0.609476462628848,0.586351484742553,0.574493344296817,0.708216694917305,0.583889848971933,0.716396562831952,0.648339226134861,0.741737879488487,0.56608322660154,0.30472972972973,0.30472972972973,0.554054054054054,0.554054054054054,0.418918918918919,0.418918918918919,0.476190476190476,0.476190476190476,0.45,0.45,0.532191780821918,0.438356164383562,0.424657534246575,0.36027397260274,0.643835616438356,0.164383561643836,0.553424657534247,0.479452054794521,0.202898550724638,0.301449275362318,0.555072463768116,0.576811594202898,0.37536231884058,0.554109589041096,0.65068493150685,0.397331845389064,0.397331845389064,0.510388556355889,0.510388556355889,0.478441716546617,0.478441716546617,0.450136296917593,0.450136296917593,0.445101174250367,0.445101174250367,0.439240362811792,0.66309970221683,0.683956916099774,0.738028013675968,0.658956916099773,0.444047619047619,0.687597882430794,0.645181405895692,0.538963220063819,0.673794995241561,0.594382242624419,0.677193080669541,0.63561551811006,0.67954119333848,0.615646258503401],[0.274077702554816,0.274077702554816,0.336471549331749,0.336471549331749,0.299801000788368,0.299801000788368,0.250319286806855,0.250319286806855,0.249765082951136,0.249765082951136,0.439166359068808,0.365052913296798,0.392498279738515,0.389773949526584,0.416362569525645,0.502094333675331,0.455679510912703,0.463262320695924,0.529589212877344,0.503417790799114,0.511899908867477,0.503711118437394,0.510675325632394,0.376479588838435,0.41906991387708,0.264382622388219,0.264382622388219,0.234255134357227,0.234255134357227,0.228801835374425,0.228801835374425,0.102611599288918,0.102611599288918,0.142300507985176,0.142300507985176,0.35530432105259,0.347097311291518,0.366644018930812,0.383192378941351,0.357443448850441,0.356301302638895,0.38751334735458,0.367817971018417,0.387535568346334,0.401172933411188,0.346271609099938,0.314421005502506,0.380237970497422,0.373770427648435,0.35187414288097,0.241850567661656,0.241850567661656,0.376047865305815,0.376047865305815,0.408521924893411,0.408521924893411,0.232684086796971,0.232684086796971,0.205221265633042,0.205221265633042,0.562888935524684,0.571733732951118,0.552182994452127,0.524884682354034,0.536062140851166,0.679808565448609,0.58377985270284,0.60173735175029,0.688361456896136,0.62030084862089,0.653311045972332,0.63835285193895,0.663497759094399,0.651648784552172,0.563988387669635,0.231460651635883,0.231460651635883,0.22737769100335,0.22737769100335,0.213355744664675,0.213355744664675,0.181815591469604,0.181815591469604,0.170058380925648,0.170058380925648,0.509891783408793,0.475564820120697,0.328804439108405,0.441777604824887,0.367007783611378,0.49454478581555,0.471387975428787,0.319373347403633,0.601954342118591,0.607290496182243,0.491680846267839,0.542634687389846,0.439432121930105,0.528087811589655,0.347901300647572],[322,322,322,322,322,322,477,477,477,477,249,247,249,247,249,249,247,249,200,198,200,198,200,247,249,316,316,316,316,316,316,453,453,453,453,244,244,244,244,244,244,244,244,195,195,195,195,195,244,244,321,321,321,321,321,321,477,477,477,477,247,247,247,247,247,247,247,247,198,198,198,198,198,247,247,322,322,322,322,322,322,480,480,480,480,249,247,249,247,249,249,247,249,200,198,200,198,200,247,249],[0.770186335403727,0.770186335403727,0.791925465838509,0.791925465838509,0.801242236024845,0.801242236024845,0.756813417190776,0.756813417190776,0.750524109014675,0.750524109014675,0.755020080321285,0.825910931174089,0.799196787148594,0.805668016194332,0.811244979919679,0.763052208835341,0.821862348178138,0.815261044176707,0.76,0.782828282828283,0.84,0.777777777777778,0.82,0.850202429149798,0.803212851405622,0.75,0.75,0.787974683544304,0.787974683544304,0.759493670886076,0.759493670886076,0.759381898454746,0.759381898454746,0.759381898454746,0.759381898454746,0.799180327868853,0.758196721311475,0.795081967213115,0.790983606557377,0.790983606557377,0.717213114754098,0.790983606557377,0.799180327868853,0.81025641025641,0.8,0.82051282051282,0.815384615384615,0.779487179487179,0.766393442622951,0.745901639344262,0.763239875389408,0.763239875389408,0.809968847352025,0.809968847352025,0.81619937694704,0.81619937694704,0.794549266247379,0.794549266247379,0.821802935010482,0.821802935010482,0.740890688259109,0.797570850202429,0.805668016194332,0.813765182186235,0.781376518218624,0.769230769230769,0.793522267206478,0.777327935222672,0.787878787878788,0.853535353535353,0.792929292929293,0.848484848484849,0.772727272727273,0.809716599190283,0.777327935222672,0.729813664596273,0.729813664596273,0.804347826086957,0.804347826086957,0.813664596273292,0.813664596273292,0.802083333333333,0.802083333333333,0.802083333333333,0.802083333333333,0.78714859437751,0.825910931174089,0.827309236947791,0.825910931174089,0.823293172690763,0.771084337349398,0.813765182186235,0.835341365461847,0.815,0.823232323232323,0.765,0.797979797979798,0.75,0.821862348178138,0.823293172690763],[0.534165968147527,0.534165968147527,0.509245599329422,0.509245599329422,0.512375523889355,0.512375523889355,0.434212170436086,0.434212170436086,0.445058103211296,0.445058103211296,0.444271990701173,0.560151789682404,0.492393422447096,0.535127004204984,0.477932378380226,0.445112987590168,0.535947488974736,0.484325322211206,0.45483133640553,0.473464155071621,0.572320737327189,0.43813886704728,0.522348387096774,0.562305562203001,0.514071313801237,0.527930240144285,0.527930240144285,0.548030547883272,0.548030547883272,0.546462247147234,0.546462247147234,0.561008010830063,0.561008010830063,0.555144008561276,0.555144008561276,0.533118528484848,0.515281948140332,0.545770042635733,0.545720328960222,0.547947439449842,0.460822430378934,0.538241120343877,0.54329973875786,0.511158808562183,0.543558089195479,0.562711866801313,0.597367431432012,0.526295394566766,0.510583108664854,0.517297601925607,0.42,0.42,0.46,0.46,0.48,0.48,0.5,0.5,0.515384615384615,0.515384615384615,0.326530612244898,0.424489795918367,0.4,0.428571428571428,0.408163265306122,0.318367346938776,0.387755102040816,0.346938775510204,0.333333333333333,0.411764705882353,0.352941176470588,0.411764705882353,0.333333333333333,0.387755102040816,0.359183673469388,0.468869074836153,0.468869074836153,0.522008840115836,0.522008840115836,0.523159579332419,0.523159579332419,0.461094596379971,0.461094596379971,0.492185229224468,0.492185229224468,0.484681793872717,0.496676598670639,0.509970203988082,0.460302544121018,0.523111009244404,0.462709145083658,0.469172587669035,0.530598212239285,0.499413449104009,0.472552717298132,0.457278773323481,0.434039087947883,0.416497321263625,0.484146993658797,0.509420123768049],[null,null,null,null,null,null,0.71491445715335,0.71491445715335,0.618272033103249,0.618272033103249,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.425285413773367,0.425285413773367,0.433886088556721,0.433886088556721,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.595168499200404,0.595168499200404,0.612875986381023,0.612875986381023,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,109,109,109,109,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,115,115,115,115,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,109,109,109,109,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,0.798165137614679,0.798165137614679,0.761467889908257,0.761467889908257,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.730434782608696,0.730434782608696,0.782608695652174,0.782608695652174,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.788990825688073,0.788990825688073,0.798165137614679,0.798165137614679,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,0.0697742474916388,0.0697742474916388,0.0562904124860647,0.0562904124860647,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.236363636363636,0.236363636363636,0.264545454545455,0.264545454545455,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.453235950497058,0.453235950497058,0.467194157029824,0.467194157029824,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[0.280003466139848,0.216313439502487,0.540556000705849,0.181492327790136,0.446477134664303,0.162972950098277,0.340568636684874,0.130460370379087,0.305034633283512,0.130225537471296,0.431476700344633,0.509484525203376,0.469509378883451,0.517578292284378,0.420427793634076,0.462058502111032,0.538698892267273,0.539745690230094,0.606281066617813,0.467547409507626,0.423372080859683,0.466840942656947,0.516976101536356,0.631081879886908,0.410835882142823,0.328862496092811,0.222909316669016,0.414207688556715,0.245394176784682,0.343419711080366,0.170574453523738,0.208009542142821,0.207959856925579,0.24992183744477,0.185973179575802,0.414469822152669,0.394005959816777,0.329644024184141,0.366072436019656,0.357980616524527,0.267191239806364,0.367718153847088,0.452900044536807,0.387559195972803,0.420390326321036,0.438139518825236,0.431855633195281,0.484166041732637,0.366428696502672,0.421670877969337,0.391085695406517,0.186478666483466,0.233453699997601,0.216335790792872,0.32246254027705,0.263922493644716,0.149198450268124,0.148904210113619,0.232382737288543,0.199022486309674,0.479021944728988,0.640621363362168,0.429535346696191,0.679081873175516,0.415049735303953,0.482819532530603,0.662166364900412,0.356270110497871,0.641976436590979,0.603273756375901,0.498475891659737,0.670983127253425,0.524942088540453,0.530996271291531,0.416752953217886,0.294876011445004,0.286351148842413,0.305984995352294,0.265082607736329,0.453625136055116,0.193543560275125,0.320985388470505,0.130297680126621,0.130151481780031,0.106223435545067,0.419772399574696,0.439930770175825,0.396407957766228,0.333999514226262,0.392793247140377,0.410574867949609,0.323173519478167,0.563267926838472,0.481713366187819,0.497709597979625,0.351475810649077,0.414586704316121,0.501925306612275,0.335887477256284,0.433296085750069],[103,224,103,224,103,224,150,275,150,275,103,102,103,102,103,103,102,103,87,87,87,87,87,102,103,101,219,101,219,101,219,268,147,147,268,101,100,101,100,101,101,100,101,86,86,86,86,86,100,101,102,223,102,223,102,223,151,276,151,276,103,102,103,102,103,103,102,103,87,87,87,87,87,102,103,103,222,103,222,103,222,150,273,273,150,101,100,101,100,101,101,100,101,86,86,86,86,86,100,101],[0.776699029126214,0.709821428571429,0.728155339805825,0.741071428571429,0.747572815533981,0.709821428571429,0.7,0.723636363636364,0.733333333333333,0.730909090909091,0.786407766990291,0.774509803921569,0.776699029126214,0.803921568627451,0.766990291262136,0.766990291262136,0.784313725490196,0.786407766990291,0.724137931034483,0.804597701149425,0.816091954022989,0.839080459770115,0.804597701149425,0.774509803921569,0.83495145631068,0.811881188118812,0.789954337899543,0.732673267326733,0.821917808219178,0.792079207920792,0.776255707762557,0.813432835820896,0.782312925170068,0.761904761904762,0.791044776119403,0.752475247524752,0.81,0.782178217821782,0.78,0.811881188118812,0.782178217821782,0.77,0.782178217821782,0.813953488372093,0.779069767441861,0.767441860465116,0.790697674418605,0.755813953488372,0.8,0.792079207920792,0.794117647058823,0.798206278026906,0.774509803921569,0.807174887892377,0.754901960784314,0.766816143497758,0.754966887417219,0.778985507246377,0.821192052980132,0.833333333333333,0.776699029126214,0.803921568627451,0.864077669902913,0.833333333333333,0.815533980582524,0.747572815533981,0.823529411764706,0.805825242718447,0.839080459770115,0.758620689655172,0.793103448275862,0.758620689655172,0.816091954022989,0.862745098039216,0.805825242718447,0.70873786407767,0.761261261261261,0.757281553398058,0.738738738738739,0.776699029126214,0.824324324324324,0.766666666666667,0.754578754578755,0.73992673992674,0.726666666666667,0.732673267326733,0.74,0.782178217821782,0.75,0.821782178217822,0.722772277227723,0.71,0.732673267326733,0.825581395348837,0.732558139534884,0.732558139534884,0.744186046511628,0.755813953488372,0.73,0.821782178217822],[0.626962790981854,0.437559448927618,0.520681859839922,0.552907244331257,0.514419258263579,0.527234512595502,0.366213365555943,0.453693411479386,0.36425133635079,0.458329122877931,0.544874657141035,0.484738789309179,0.568922625502328,0.572925304586337,0.558652803470052,0.459850417809529,0.524787905849334,0.558525228041079,0.399948969828411,0.570644893793456,0.568922625502328,0.569401033360975,0.582860241117561,0.543868405945015,0.572909357657715,0.502893214613085,0.540090427838239,0.448088994075992,0.526067224037827,0.482633554629091,0.478467838683783,0.514179650725481,0.486747288221643,0.462064193811115,0.515218851783606,0.490070886815597,0.544500391730207,0.473035966614849,0.491348889524773,0.54070079457096,0.514449488242528,0.473133538172183,0.491552115964985,0.524504804940586,0.477292484550902,0.478004135544991,0.492367934706777,0.485089942601566,0.507481077811488,0.507568187707781,0.642857142857143,0.482926829268293,0.607142857142857,0.560975609756097,0.607142857142857,0.48780487804878,0.5,0.5,0.558823529411765,0.518181818181818,0.539285714285714,0.5,0.666666666666667,0.5,0.580952380952382,0.521428571428571,0.455952380952381,0.585714285714286,0.495121951219512,0.365853658536585,0.512195121951219,0.341463414634146,0.48780487804878,0.595238095238095,0.571428571428571,0.245338744222748,0.216665162047486,0.276631721339195,0.245626072041166,0.302051628903168,0.39566669675905,0.360370634354954,0.251688693098385,0.247577092511013,0.34464005702067,0.217754847891834,0.224359544565024,0.366118128446896,0.2580056929372,0.328624799857677,0.217754847891834,0.241549546344067,0.197295854830101,0.246308486034514,0.211261341398328,0.204145169898595,0.200120085394058,0.200507027219356,0.224826543319694,0.38187155310443],[0.785123966942149,0.785123966942149,0.801652892561983,0.801652892561983,0.818181818181818,0.818181818181818,0.736,0.736,0.824,0.824,0.779661016949153,0.793103448275862,0.745762711864407,0.775862068965517,0.796610169491525,0.779661016949153,0.775862068965517,0.796610169491525,null,null,null,null,null,0.775862068965517,0.779661016949153,0.703389830508475,0.703389830508475,0.771186440677966,0.771186440677966,0.779661016949153,0.779661016949153,0.760330578512397,0.760330578512397,0.776859504132231,0.776859504132231,0.771929824561403,0.785714285714286,0.807017543859649,0.803571428571429,0.754385964912281,0.771929824561403,0.821428571428571,0.736842105263158,null,null,null,null,null,0.732142857142857,0.736842105263158,0.818181818181818,0.818181818181818,0.760330578512397,0.760330578512397,0.834710743801653,0.834710743801653,0.832,0.832,0.824,0.824,0.76271186440678,0.793103448275862,0.796610169491525,0.879310344827586,0.813559322033898,0.76271186440678,0.844827586206897,0.813559322033898,null,null,null,null,null,0.810344827586207,0.847457627118644,0.789915966386555,0.789915966386555,0.781512605042017,0.781512605042017,0.823529411764706,0.823529411764706,0.772357723577236,0.772357723577236,0.764227642276423,0.764227642276423,0.719298245614035,0.803571428571429,0.807017543859649,0.767857142857143,0.807017543859649,0.736842105263158,0.767857142857143,0.789473684210526,null,null,null,null,null,0.839285714285714,0.824561403508772],[0.613513416687565,0.613513416687565,0.734388994375381,0.734388994375381,0.639021244581378,0.639021244581378,0.576956133681531,0.576956133681531,0.698522362897094,0.698522362897094,0.612258329905389,0.554915672562731,0.617064308240779,0.446671465789115,0.631667352255588,0.628479363773481,0.556766762649116,0.613653503359386,null,null,null,null,null,0.444672974084739,0.626628273687097,0.460651722847716,0.460651722847716,0.45889315963128,0.45889315963128,0.492780366679139,0.492780366679139,0.44405866712411,0.44405866712411,0.435437001458101,0.435437001458101,0.561146857250645,0.508813111730556,0.651528933414373,0.501697650535494,0.534785069058748,0.57796826860604,0.57167857816528,0.489535346760841,null,null,null,null,null,0.477308071175571,0.498249952843041,0.700000000000001,0.700000000000001,0.565217391304348,0.565217391304348,0.704347826086957,0.704347826086957,0.68695652173913,0.68695652173913,0.695652173913043,0.695652173913043,0.545652173913044,0.515217391304349,0.621739130434783,0.630434782608696,0.608695652173913,0.5,0.56413043478261,0.630434782608696,null,null,null,null,null,0.554347826086956,0.669565217391304,0.269957234497505,0.269957234497505,0.338542409123309,0.338542409123309,0.482270135424091,0.482270135424091,0.461023027557569,0.461023027557569,0.452246130615327,0.452246130615327,0.232611174458381,0.253088179399468,0.236982136069935,0.246702774610414,0.232041049030787,0.232611174458381,0.237637780311669,0.235385784872672,null,null,null,null,null,0.394574306347396,0.23555682250095]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_apple<\/th>\n      <th>N_apple<\/th>\n      <th>covers_apple<\/th>\n      <th>width_apple<\/th>\n      <th>cvRsquared_beet<\/th>\n      <th>N_beet<\/th>\n      <th>covers_beet<\/th>\n      <th>width_beet<\/th>\n      <th>cvRsquared_carrot<\/th>\n      <th>N_carrot<\/th>\n      <th>covers_carrot<\/th>\n      <th>width_carrot<\/th>\n      <th>cvRsquared_peppers<\/th>\n      <th>N_peppers<\/th>\n      <th>covers_peppers<\/th>\n      <th>width_peppers<\/th>\n      <th>cvRsquared_potato<\/th>\n      <th>N_potato<\/th>\n      <th>covers_potato<\/th>\n      <th>width_potato<\/th>\n      <th>cvRsquared_squash_butternut<\/th>\n      <th>N_squash_butternut<\/th>\n      <th>covers_squash_butternut<\/th>\n      <th>width_squash_butternut<\/th>\n      <th>cvRsquared_zucchini<\/th>\n      <th>N_zucchini<\/th>\n      <th>covers_zucchini<\/th>\n      <th>width_zucchini<\/th>\n      <th>covers_squash<\/th>\n      <th>width_squash<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Bfrtipl","lengthMenu":[10,20,40,80],"buttons":["colvis"],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[11]; $(this.api().cell(row, 11).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[15]; $(this.api().cell(row, 15).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[19]; $(this.api().cell(row, 19).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[23]; $(this.api().cell(row, 23).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[27]; $(this.api().cell(row, 27).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->

## Random Forest Regression over BQI Minerals 

<!--html_preserve--><div id="htmlwidget-01ad1b26314969600cd5" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-01ad1b26314969600cd5">{"x":{"filter":"none","extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120"],["produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile"],["c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.28260930378526,0.419468515856209,0.286191667824332,0.128999072093439,0.114331175176834,0.540530196173508,0.456120430251509,0.594363171725474,0.388085561478956,0.500507068962827,0.513849201080837,0.571429188262259,0.399150395178439,0.542379024911575,0.455432194613275,0.54212263063986,0.479533209146088,0.483434140738008,0.484389192842857,0.479137506776197,0.156770533663914,0.166379014510793,0.215916907636903,0.104137140988505,0.116527679499464,0.358384928540487,0.396189709733049,0.477109272914001,0.587307146813215,0.363404720761085,0.40665937967085,0.48028207885188,0.475688260091292,0.446635503399152,0.501899355266106,0.561575219788721,0.51318761024164,0.526869772118981,0.497147700941342,0.466659492876877,0.240248722743706,0.215896757559645,0.17466099395654,0.101795604827279,0.102680496281379,0.410664109871895,0.452098806416406,0.493169762150687,0.421164587816081,0.526229145995076,0.428235192417454,0.403742371955474,0.4978044047287,0.498428711532251,0.555654602044053,0.385285923782989,0.405976471701294,0.452098144945041,0.450103282767166,0.34027906754373,0.238968082296925,0.180073952536854,0.159171361546426,0.113758386221331,0.117303321624911,0.60693247644998,0.622526957217614,0.479193520538496,0.541052858319712,0.589611573839819,0.621132677576531,0.544954396583963,0.526608233543922,0.557834949785334,0.500704524936929,0.541434000746807,0.512974743469305,0.579510203727247,0.54418823216132,0.492205891851868,0.246816655877595,0.188696056488744,0.204000772323206,0.120176074046495,0.0782675157438851,0.582456588614538,0.387595970192255,0.402949564656068,0.527592994901577,0.498627611951871,0.399232765747685,0.506085963695232,0.397118276113319,0.520676671064063,0.531474971405721,0.472962239915895,0.502892046933843,0.397224922071918,0.549843796861388,0.407177731167314,0.437656070334765,0.360191338718807,0.277459748414706,0.127097219129376,0.143896380628405,0.695231903224059,0.555779504885653,0.649303584349642,0.623646838978844,0.590720141501147,0.670330620602925,0.686546142501801,0.635115688291891,0.737814816128568,0.731175403981047,0.63302881619497,0.64352372607687,0.688708802453835,0.674871390458145,0.527326738013649],[181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83,181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83,181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83,181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83,181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83,181,181,181,253,253,83,77,83,77,83,83,77,83,77,71,77,71,77,77,83],[0.707182320441989,0.773480662983425,0.756906077348066,0.762845849802372,0.798418972332016,0.650602409638554,0.792207792207792,0.734939759036145,0.792207792207792,0.734939759036145,0.783132530120482,0.792207792207792,0.771084337349398,0.818181818181818,0.788732394366197,0.831168831168831,0.802816901408451,0.805194805194805,0.792207792207792,0.72289156626506,0.74585635359116,0.756906077348066,0.801104972375691,0.794466403162055,0.786561264822134,0.674698795180723,0.753246753246753,0.783132530120482,0.792207792207792,0.819277108433735,0.771084337349398,0.766233766233766,0.759036144578313,0.753246753246753,0.76056338028169,0.753246753246753,0.788732394366197,0.74025974025974,0.792207792207792,0.771084337349398,0.779005524861878,0.779005524861878,0.751381215469613,0.739130434782609,0.798418972332016,0.662650602409639,0.779220779220779,0.843373493975904,0.792207792207792,0.831325301204819,0.674698795180723,0.766233766233766,0.819277108433735,0.844155844155844,0.732394366197183,0.818181818181818,0.788732394366197,0.792207792207792,0.792207792207792,0.831325301204819,0.734806629834254,0.812154696132597,0.81767955801105,0.806324110671937,0.790513833992095,0.843373493975904,0.792207792207792,0.819277108433735,0.831168831168831,0.807228915662651,0.698795180722892,0.792207792207792,0.795180722891566,0.818181818181818,0.774647887323944,0.831168831168831,0.788732394366197,0.831168831168831,0.792207792207792,0.771084337349398,0.762430939226519,0.784530386740331,0.779005524861878,0.758893280632411,0.778656126482213,0.710843373493976,0.792207792207792,0.783132530120482,0.766233766233766,0.795180722891566,0.698795180722892,0.753246753246753,0.795180722891566,0.766233766233766,0.774647887323944,0.766233766233766,0.774647887323944,0.766233766233766,0.714285714285714,0.795180722891566,0.779005524861878,0.751381215469613,0.823204419889503,0.790513833992095,0.806324110671937,0.746987951807229,0.831168831168831,0.867469879518072,0.818181818181818,0.831325301204819,0.698795180722892,0.805194805194805,0.855421686746988,0.714285714285714,0.830985915492958,0.87012987012987,0.788732394366197,0.87012987012987,0.805194805194805,0.855421686746988],[0.676348547717842,0.742738589211618,0.738589211618257,0.790794979079498,0.832635983263598,0.567901234567901,0.75702479338843,0.703703703703704,0.78099173553719,0.723456790123457,0.681687242798354,0.739669421487603,0.744855967078189,0.733539094650206,0.73402489626556,0.744855967078189,0.742738589211618,0.724279835390946,0.760330578512397,0.720164609053498,0.679012345679012,0.776543209876543,0.792181069958848,0.794715447154472,0.808130081300813,0.555416666666667,0.709016393442623,0.728333333333333,0.753688524590164,0.804166666666667,0.793333333333333,0.709426229508196,0.70875,0.743333333333333,0.745833333333333,0.720833333333333,0.779166666666667,0.75,0.758196721311475,0.745833333333333,0.716599190283401,0.787044534412956,0.744939271255061,0.739837398373984,0.80650406504065,0.606425702811245,0.742971887550201,0.81004016064257,0.763052208835341,0.801606425702811,0.620883534136546,0.751004016064257,0.799196787148594,0.8132,0.763900414937759,0.788,0.774688796680498,0.7952,0.763052208835341,0.80722891566265,0.621276595744681,0.853191489361702,0.831914893617021,0.86137339055794,0.858798283261803,0.789029535864979,0.798319327731092,0.814345991561181,0.798319327731092,0.805907172995781,0.666666666666667,0.798319327731092,0.805907172995781,0.776371308016878,0.784810126582278,0.827004219409283,0.774683544303797,0.827004219409283,0.798319327731092,0.79831223628692,0.773728813559322,0.813983050847458,0.805932203389831,0.802521008403361,0.836974789915966,0.674137931034483,0.746218487394958,0.771551724137931,0.747899159663866,0.84051724137931,0.634698275862069,0.680672268907563,0.75,0.768965517241379,0.663318777292576,0.758620689655172,0.655021834061135,0.775862068965517,0.672268907563025,0.789655172413793,0.656387665198238,0.674008810572687,0.784581497797357,0.822222222222222,0.841777777777778,0.466375545851528,0.647826086956522,0.729257641921397,0.660869565217391,0.685589519650655,0.463318777292576,0.697826086956522,0.765065502183406,0.432314410480349,0.655021834061135,0.664628820960699,0.572052401746725,0.646288209606987,0.639130434782609,0.7117903930131],[0.443175814134273,0.422472422240855,0.436250270982565,0.307485986355852,0.303954903181348,0.639649004342903,0.606423454294817,0.525279709635836,0.59716236724519,0.523275718690818,0.679972450918739,0.638309661534827,0.480405411294141,0.615219864762714,0.808819236401924,0.608382991469905,0.678797043026717,0.546931499813149,0.660120861871527,0.472909149050196,0.389313587427774,0.352656986617791,0.36384396498046,0.269323019769958,0.277995144229946,0.484039442913298,0.469100047777908,0.477445718844116,0.460653299950842,0.404986148279217,0.515545970670708,0.398988912532634,0.415440282599868,0.517542305333651,0.379657304877249,0.466863094001217,0.445768156584426,0.505171859137519,0.479548728885254,0.430375043040509,0.283770926135478,0.333609405817383,0.291511360437989,0.128840539297096,0.323895088914102,0.468792179671008,0.557188728487304,0.42135044273182,0.549321148977601,0.482066596239076,0.55635169872768,0.497320523552303,0.440015143515035,0.520306597750013,0.538917270327663,0.526700174725117,0.511256998252299,0.411322912479641,0.530936031269256,0.433394372119199,0.472173516685771,0.392988598170304,0.419616512258322,0.265900516347639,0.227980965557801,0.696103944802782,0.641395563760275,0.482342408479822,0.594351369585421,0.543594184405812,0.707208956190922,0.579308058694479,0.473219076135223,0.651428632817264,0.617032193538333,0.615847083778286,0.665874008958361,0.43974314440472,0.7167532910673,0.647969763938715,0.380921545567138,0.322918148451852,0.378411097711411,0.186674487130627,0.211839308437126,0.590152957241963,0.539263837394039,0.433854272971045,0.484422601898145,0.43807502910496,0.541549091068271,0.497073713824875,0.573219386676577,0.465506428077202,0.570042324616255,0.471872110410425,0.577499658249888,0.518325799956781,0.459427099171554,0.468597095657821,0.466093680943354,0.468706714257397,0.558413776283949,0.241754061526873,0.274488397625459,0.575521360064994,0.682383349223794,0.602886211267236,0.550753337198783,0.60740724403488,0.456490835077474,0.604889683821028,0.561448493136085,0.575200982403783,0.609966352274412,0.589003997227562,0.641481265079883,0.622537167826587,0.632527995708738,0.576215041776525],[152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115,152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115,152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115,152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115,152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115,152,152,152,170,170,115,103,115,103,115,115,103,115,110,100,110,100,110,103,115],[0.703947368421053,0.861842105263158,0.881578947368421,0.841176470588235,0.852941176470588,0.71304347826087,0.87378640776699,0.895652173913044,0.883495145631068,0.895652173913044,0.878260869565217,0.883495145631068,0.843478260869565,0.9,0.91,0.9,0.87,0.881818181818182,0.902912621359223,0.88695652173913,0.717105263157895,0.822368421052632,0.855263157894737,0.817647058823529,0.788235294117647,0.747826086956522,0.893203883495146,0.878260869565217,0.902912621359223,0.878260869565217,0.834782608695652,0.893203883495146,0.826086956521739,0.818181818181818,0.9,0.9,0.89,0.854545454545454,0.893203883495146,0.843478260869565,0.782894736842105,0.815789473684211,0.828947368421053,0.794117647058823,0.8,0.71304347826087,0.844660194174757,0.869565217391304,0.854368932038835,0.826086956521739,0.721739130434783,0.844660194174757,0.826086956521739,0.772727272727273,0.89,0.827272727272727,0.86,0.827272727272727,0.825242718446602,0.843478260869565,0.789473684210526,0.802631578947368,0.828947368421053,0.8,0.841176470588235,0.730434782608696,0.912621359223301,0.817391304347826,0.932038834951456,0.834782608695652,0.71304347826087,0.87378640776699,0.843478260869565,0.8,0.9,0.818181818181818,0.91,0.836363636363636,0.883495145631068,0.869565217391304,0.710526315789474,0.815789473684211,0.815789473684211,0.835294117647059,0.852941176470588,0.791304347826087,0.854368932038835,0.860869565217391,0.844660194174757,0.860869565217391,0.791304347826087,0.864077669902913,0.852173913043478,0.845454545454545,0.93,0.927272727272727,0.95,0.9,0.844660194174757,0.826086956521739,0.861842105263158,0.848684210526316,0.868421052631579,0.811764705882353,0.817647058823529,0.730434782608696,0.902912621359223,0.88695652173913,0.902912621359223,0.878260869565217,0.826086956521739,0.893203883495146,0.869565217391304,0.927272727272727,0.9,0.909090909090909,0.89,0.936363636363636,0.87378640776699,0.869565217391304],[0.553191489361702,0.680851063829787,0.820921985815603,0.664285714285714,0.821428571428571,0.309219858156028,0.496402877697842,0.753191489361702,0.510791366906475,0.609929078014184,0.756028368794326,0.524460431654676,0.559574468085106,0.843262411347518,0.685611510791367,0.826241134751773,0.557553956834532,0.626241134751773,0.724460431654677,0.737588652482269,0.535714285714286,0.678571428571428,0.818214285714286,0.742857142857143,0.713928571428571,0.475177304964539,0.695035460992908,0.75886524822695,0.825531914893617,0.75886524822695,0.726950354609929,0.826950354609929,0.673758865248227,0.720567375886525,0.737588652482269,0.770212765957447,0.65709219858156,0.681560283687943,0.812765957446809,0.716312056737589,0.68768115942029,0.652173913043478,0.671739130434783,0.722627737226277,0.718248175182482,0.311594202898551,0.63768115942029,0.833333333333333,0.811594202898551,0.681884057971014,0.384057971014493,0.810144927536232,0.673913043478261,0.378623188405797,0.793478260869565,0.6,0.652173913043478,0.595652173913044,0.594202898550725,0.652173913043478,0.659420289855073,0.571014492753623,0.589492753623188,0.63768115942029,0.819565217391304,0.280575539568345,0.632374100719425,0.61294964028777,0.788489208633094,0.719424460431655,0.251798561151079,0.618705035971223,0.736690647482015,0.45431654676259,0.495323741007194,0.701438848920863,0.785611510791367,0.698201438848921,0.469064748201439,0.856115107913669,0.623188405797101,0.695289855072464,0.673913043478261,0.835766423357664,0.799270072992701,0.514492753623188,0.680434782608696,0.746376811594203,0.666666666666667,0.742753623188406,0.478260869565217,0.688405797101449,0.7,0.502536231884058,0.717753623188406,0.761594202898551,0.817391304347826,0.669927536231884,0.682608695652174,0.652173913043478,0.720247933884298,0.677685950413223,0.665702479338844,0.691666666666667,0.700833333333333,0.421487603305785,0.542148760330579,0.595041322314049,0.684297520661158,0.595041322314049,0.490909090909091,0.56198347107438,0.576859504132231,0.834710743801653,0.555371900826446,0.595041322314049,0.56198347107438,0.702479338842975,0.578512396694215,0.628099173553719],[0.459046859456816,0.475164174895856,0.407900807250786,0.427680972918608,0.428152122162832,0.602937036308844,0.624360600871077,0.593936800956186,0.693201962920573,0.694324557166823,0.71523076239396,0.569950929314794,0.522368703907893,0.727538685314663,0.596907116418117,0.645943644822627,0.6569544203859,0.701005517452276,0.614548680878697,0.691725577053782,0.309931491871971,0.291189432085669,0.308832622905011,0.167410742811021,0.129776506945395,0.504258148591377,0.513521397575695,0.544400692525168,0.457343719440451,0.542093392689845,0.457071652854556,0.545478388803581,0.530131080005759,0.458307420265427,0.571546275218333,0.508811288457569,0.664454559418733,0.637866206346596,0.585603945865536,0.574827879900552,0.308799498578669,0.248870195646281,0.323482409314523,0.387247260936185,0.353228274444022,0.595293721694702,0.644164163525831,0.470025014930647,0.435924093933571,0.556370444198931,0.574934542119259,0.546977702523522,0.629314871417041,0.653078727425701,0.501528487753186,0.571895991156885,0.536426351499115,0.587651543220618,0.539689174308555,0.556874124564576,0.463382149238223,0.537911156027615,0.506028872406339,0.302812923883882,0.362941330274759,0.687859026218946,0.634214068249587,0.720897831015508,0.65879779738402,0.534540227990513,0.761957225975086,0.662477545757511,0.653800289466061,0.822767497830296,0.704366892847906,0.707194358273465,0.784677072957391,0.61820668819683,0.612051320763996,0.719186207001471,0.557724924981958,0.617658315543326,0.653762201339137,0.534181746518876,0.507490519599375,0.654285319293644,0.737051034434821,0.810480695453525,0.737014304893618,0.827769642760722,0.649863992780767,0.730629192383901,0.725136833159719,0.773112819639756,0.689019546972714,0.79210372916472,0.716723501208854,0.727745862631008,0.764433159308963,0.717813726609083,0.31579881692152,0.467410967991372,0.331648024914716,0.298239897899783,0.310416570432241,0.474392435752852,0.504025415632534,0.521046866577921,0.51747664555939,0.457108927104138,0.643725860013311,0.662502573006402,0.525143105539962,0.59953665194416,0.724827161113058,0.599526202849248,0.581416227211277,0.595742019830351,0.485186551405395,0.553410821193795],[140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81,140,140,140,211,211,81,78,81,78,81,81,78,81,72,69,72,69,72,78,81],[0.778571428571429,0.814285714285714,0.807142857142857,0.838862559241706,0.800947867298578,0.790123456790123,0.833333333333333,0.765432098765432,0.846153846153846,0.790123456790123,0.654320987654321,0.717948717948718,0.827160493827161,0.791666666666667,0.855072463768116,0.722222222222222,0.797101449275362,0.819444444444444,0.846153846153846,0.802469135802469,0.7,0.742857142857143,0.771428571428571,0.748815165876777,0.767772511848341,0.716049382716049,0.743589743589744,0.777777777777778,0.769230769230769,0.827160493827161,0.703703703703704,0.756410256410256,0.728395061728395,0.736111111111111,0.72463768115942,0.791666666666667,0.782608695652174,0.763888888888889,0.769230769230769,0.777777777777778,0.764285714285714,0.778571428571429,0.8,0.791469194312796,0.815165876777251,0.728395061728395,0.794871794871795,0.814814814814815,0.794871794871795,0.827160493827161,0.703703703703704,0.756410256410256,0.814814814814815,0.847222222222222,0.811594202898551,0.791666666666667,0.811594202898551,0.819444444444444,0.794871794871795,0.802469135802469,0.721428571428571,0.792857142857143,0.771428571428571,0.71563981042654,0.777251184834123,0.753086419753086,0.846153846153846,0.851851851851852,0.871794871794872,0.839506172839506,0.728395061728395,0.846153846153846,0.851851851851852,0.777777777777778,0.797101449275362,0.791666666666667,0.826086956521739,0.777777777777778,0.807692307692308,0.827160493827161,0.764285714285714,0.735714285714286,0.728571428571429,0.805687203791469,0.786729857819905,0.814814814814815,0.782051282051282,0.876543209876543,0.833333333333333,0.888888888888889,0.777777777777778,0.782051282051282,0.864197530864197,0.805555555555556,0.811594202898551,0.833333333333333,0.840579710144927,0.819444444444444,0.794871794871795,0.802469135802469,0.75,0.835714285714286,0.785714285714286,0.796208530805687,0.796208530805687,0.703703703703704,0.871794871794872,0.839506172839506,0.884615384615385,0.839506172839506,0.790123456790123,0.858974358974359,0.901234567901235,0.75,0.884057971014493,0.861111111111111,0.840579710144927,0.916666666666667,0.884615384615385,0.901234567901235],[0.592682926829268,0.609756097560976,0.639024390243903,0.677073170731708,0.653658536585366,0.692682926829268,0.782843137254902,0.621463414634147,0.779411764705882,0.673170731707317,0.474878048780488,0.526960784313726,0.678048780487805,0.633333333333333,0.754901960784314,0.581159420289855,0.756862745098039,0.802657004830918,0.766666666666667,0.687804878048781,0.652657004830918,0.72512077294686,0.753623188405797,0.717073170731707,0.707317073170732,0.545918367346939,0.652261306532663,0.724489795918367,0.689949748743719,0.795918367346939,0.551020408163265,0.688442211055276,0.694897959183674,0.519849246231156,0.628140703517588,0.707537688442211,0.638190954773869,0.688693467336683,0.684924623115578,0.719387755102041,0.698492462311558,0.65175879396985,0.698492462311558,0.663414634146341,0.673170731707317,0.415384615384615,0.586458333333333,0.723076923076923,0.657552083333333,0.715897435897436,0.425641025641026,0.529947916666667,0.728205128205128,0.535175879396985,0.614583333333333,0.562814070351759,0.641145833333333,0.571859296482412,0.701041666666667,0.728205128205128,0.566626213592233,0.682038834951456,0.650970873786408,0.634146341463415,0.634146341463415,0.507317073170732,0.794902912621359,0.746341463414634,0.694174757281553,0.710243902439024,0.457560975609756,0.658980582524272,0.758048780487805,0.45320197044335,0.694581280788177,0.704433497536946,0.700492610837438,0.709852216748769,0.560679611650486,0.649756097560976,0.590686274509804,0.502941176470588,0.502450980392157,0.593658536585366,0.579512195121951,0.754411764705882,0.514563106796116,0.784313725490196,0.526456310679612,0.797058823529412,0.440196078431373,0.528398058252427,0.698039215686275,0.568627450980392,0.515686274509805,0.669117647058823,0.53921568627451,0.606617647058824,0.529126213592233,0.529411764705882,0.713450292397661,0.812280701754386,0.766374269005848,0.747126436781609,0.758620689655172,0.500000000000001,0.812121212121212,0.768292682926829,0.833939393939394,0.769512195121951,0.652439024390244,0.793939393939394,0.878048780487805,0.56939393939394,0.781212121212121,0.812121212121212,0.751515151515151,0.854545454545455,0.83030303030303,0.880487804878049],[0.248903422434873,0.25050910601193,0.183120068173363,0.0992590306689041,0.0993252521695883,0.372190907290644,0.341730756592015,0.287843572414316,0.292467436743079,0.364516619124856,0.395110760662647,0.259730859678584,0.307087582081692,0.514287925519881,0.406047078480714,0.327403602986052,0.431417056343109,0.33720849004595,0.368189257253117,0.404876971902891,0.23475707955232,0.256331756432204,0.248645396123755,0.256410188331736,0.259472175096119,0.297164944948055,0.358387767775328,0.294713533431567,0.352880654437498,0.288513351982962,0.259324480924634,0.349951017309184,0.32660986661312,0.411387804248021,0.387393701266027,0.377903110696061,0.430326983631882,0.370874499531224,0.412192232927173,0.314661332142867,0.249548058384681,0.23610985266925,0.23978187534735,0.135510690363065,0.134194108443808,0.369485369369612,0.3586337901574,0.335184356798556,0.378679153958629,0.348527916233184,0.396612976390662,0.352266581814607,0.358202827173424,0.500939074779688,0.453773311880307,0.396207491368183,0.378785863443965,0.395761626079324,0.320447146142129,0.357079198826659,0.337245601310419,0.303173928676733,0.349087705169798,0.148234434853409,0.145952405268082,0.447916191186859,0.394022088399256,0.375418922996452,0.418199601256499,0.419451793998602,0.395716494566864,0.395627703287486,0.431086612437404,0.530050489069594,0.496250193944094,0.485506211355818,0.484182112438442,0.469023381819882,0.425010340672947,0.432703842345926,0.252319810644697,0.253851558797998,0.280969031218135,0.186532942625369,0.166284363922654,0.455199927355036,0.429652770036765,0.366623306441395,0.399013500129905,0.420132143848664,0.452851850054413,0.367606461597555,0.379019950317028,0.461692852787406,0.462592123058376,0.340488782069512,0.4609178521153,0.430169906241362,0.425706111200569,0.374781066358358,0.179572832425871,0.118979254416329,0.147762218789371,0.0654148766701172,0.0775151779601589,0.352449025200954,0.264482026369388,0.265054761640078,0.236537315027295,0.248306575109383,0.343316366332784,0.252121969408475,0.270962781678467,0.397301436327484,0.405851455490217,0.376922390101546,0.433854803055277,0.39697472528815,0.277509724467677,0.288165571922243],[319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246,319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246,319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246,319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246,319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246,319,319,319,461,461,246,244,246,244,246,246,244,246,197,195,197,195,197,244,246],[0.899686520376176,0.896551724137931,0.893416927899687,0.891540130151844,0.893709327548807,0.792682926829268,0.901639344262295,0.90650406504065,0.901639344262295,0.902439024390244,0.865853658536585,0.918032786885246,0.890243902439024,0.913705583756345,0.902564102564103,0.913705583756345,0.882051282051282,0.908629441624365,0.918032786885246,0.914634146341463,0.717868338557994,0.786833855799373,0.808777429467085,0.819956616052061,0.789587852494577,0.780487804878049,0.80327868852459,0.796747967479675,0.778688524590164,0.788617886178862,0.804878048780488,0.80327868852459,0.800813008130081,0.766497461928934,0.784615384615385,0.807106598984772,0.794871794871795,0.83248730964467,0.795081967213115,0.817073170731707,0.77115987460815,0.739811912225705,0.80564263322884,0.754880694143167,0.796095444685466,0.813008130081301,0.836065573770492,0.764227642276423,0.807377049180328,0.808943089430894,0.808943089430894,0.819672131147541,0.821138211382114,0.776649746192893,0.82051282051282,0.82741116751269,0.81025641025641,0.842639593908629,0.80327868852459,0.764227642276423,0.780564263322884,0.815047021943574,0.799373040752351,0.778741865509761,0.787418655097614,0.703252032520325,0.80327868852459,0.813008130081301,0.836065573770492,0.841463414634146,0.796747967479675,0.831967213114754,0.83739837398374,0.766497461928934,0.825641025641026,0.842639593908629,0.835897435897436,0.862944162436548,0.836065573770492,0.845528455284553,0.705329153605016,0.808777429467085,0.815047021943574,0.789587852494577,0.752711496746204,0.792682926829268,0.80327868852459,0.829268292682927,0.827868852459016,0.800813008130081,0.747967479674797,0.819672131147541,0.817073170731707,0.796954314720812,0.830769230769231,0.791878172588833,0.779487179487179,0.822335025380711,0.799180327868853,0.788617886178862,0.746081504702194,0.849529780564263,0.884012539184953,0.906724511930586,0.889370932754881,0.813008130081301,0.811475409836066,0.853658536585366,0.807377049180328,0.878048780487805,0.723577235772358,0.89344262295082,0.845528455284553,0.868020304568528,0.815384615384615,0.847715736040609,0.835897435897436,0.822335025380711,0.864754098360656,0.878048780487805],[0.803347280334728,0.774476987447699,0.769874476987448,0.846280991735537,0.85206611570248,0.397489539748954,0.798326359832636,0.768828451882845,0.803138075313808,0.702719665271966,0.623012552301255,0.8,0.70836820083682,0.68974358974359,0.712393162393163,0.713675213675214,0.641025641025641,0.769230769230769,0.751255230125523,0.723849372384937,0.63355408388521,0.746578366445916,0.777924944812363,0.760356347438753,0.685968819599109,0.725501113585746,0.712694877505568,0.696547884187082,0.639198218262806,0.645879732739421,0.727616926503341,0.690423162583519,0.702004454342984,0.515486725663717,0.619469026548673,0.631858407079646,0.614159292035399,0.632743362831859,0.688864142538975,0.78195991091314,0.677551020408163,0.64625850340136,0.736507936507937,0.706458797327394,0.7728285077951,0.698522727272727,0.728181818181818,0.610795454545455,0.684090909090909,0.667727272727273,0.689772727272727,0.667045454545455,0.657386363636364,0.53778801843318,0.755760368663595,0.620506912442397,0.628110599078341,0.673732718894009,0.685795454545455,0.606590909090909,0.654788418708241,0.779064587973274,0.757238307349666,0.765701559020044,0.784855233853007,0.499773755656109,0.675339366515837,0.713800904977375,0.817873303167421,0.770361990950226,0.665158371040724,0.813122171945702,0.772285067873303,0.57,0.797272727272727,0.717727272727273,0.722727272727273,0.735909090909091,0.759049773755656,0.778054298642534,0.621923937360179,0.710738255033558,0.748545861297539,0.741255605381166,0.699551569506726,0.578409090909091,0.643636363636364,0.741590909090909,0.747727272727273,0.683068181818182,0.590909090909091,0.693636363636364,0.672272727272727,0.602727272727273,0.688636363636364,0.631818181818182,0.647727272727273,0.672727272727273,0.654772727272727,0.655113636363636,0.778735632183908,0.858908045977012,0.888505747126437,0.892753623188406,0.890434782608695,0.744927536231884,0.803478260869565,0.863768115942029,0.8,0.839710144927536,0.631884057971015,0.873623188405797,0.807246376811594,0.819596541786744,0.787031700288184,0.731988472622478,0.784726224783862,0.714697406340058,0.863478260869565,0.858260869565217],[0.358796034749317,0.375067113133227,0.442240433901932,0.205637649765838,0.233107354125687,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.372295040106041,0.417080890255852,0.420309990603877,0.316927509290783,0.187490543580945,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.243228609270776,0.350262505490584,0.22601143073495,0.247953635189126,0.184573854507342,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.327551579311619,0.264515101489882,0.336550421319491,0.208895226858376,0.23984513497336,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.521857738098426,0.317778612031539,0.342301680043445,0.256812189279453,0.211523916101805,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.331631175072789,0.29814500769265,0.321549315140215,0.22031627042673,0.141195590446693,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,102,102,102,148,148,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[0.705882352941177,0.774509803921569,0.764705882352941,0.716216216216216,0.716216216216216,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.696078431372549,0.705882352941177,0.715686274509804,0.716216216216216,0.722972972972973,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.745098039215686,0.705882352941177,0.745098039215686,0.75,0.756756756756757,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.715686274509804,0.725490196078431,0.745098039215686,0.716216216216216,0.702702702702703,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.764705882352941,0.852941176470588,0.794117647058823,0.722972972972973,0.72972972972973,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.774509803921569,0.794117647058823,0.705882352941177,0.810810810810811,0.790540540540541,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[0.802758620689655,0.779310344827586,0.778275862068966,0.756944444444445,0.756944444444445,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.7734375,0.828125,0.8125,0.842125984251969,0.820472440944882,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.848920863309353,0.81726618705036,0.81294964028777,0.773928571428572,0.77,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.878260869565217,0.913913043478261,0.960869565217391,0.934210526315789,0.929824561403509,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.753731343283582,0.911194029850746,0.923507462686567,0.759398496240601,0.757518796992481,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0.854166666666667,0.772222222222222,0.729166666666667,0.851748251748252,0.773426573426573,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[0.75,0.725,0.725,0.764227642276423,0.747967479674797,0.779661016949153,0.827586206896552,0.745762711864407,0.775862068965517,0.745762711864407,0.745762711864407,0.793103448275862,0.728813559322034,null,null,null,null,null,0.775862068965517,0.796610169491525,0.858333333333333,0.9,0.833333333333333,0.796747967479675,0.780487804878049,0.864406779661017,0.827586206896552,0.830508474576271,0.844827586206897,0.813559322033898,0.76271186440678,0.844827586206897,0.779661016949153,null,null,null,null,null,0.810344827586207,0.864406779661017,0.7,0.733333333333333,0.758333333333333,0.739837398373984,0.804878048780488,0.728813559322034,0.810344827586207,0.830508474576271,0.758620689655172,0.796610169491525,0.711864406779661,0.793103448275862,0.813559322033898,null,null,null,null,null,0.775862068965517,0.779661016949153,0.766666666666667,0.791666666666667,0.75,0.731707317073171,0.731707317073171,0.661016949152542,0.724137931034483,0.76271186440678,0.775862068965517,0.76271186440678,0.711864406779661,0.775862068965517,0.728813559322034,null,null,null,null,null,0.724137931034483,0.745762711864407,0.766666666666667,0.775,0.816666666666667,0.804878048780488,0.739837398373984,0.779661016949153,0.810344827586207,0.847457627118644,0.793103448275862,0.830508474576271,0.745762711864407,0.827586206896552,0.830508474576271,null,null,null,null,null,0.827586206896552,0.779661016949153,0.725,0.733333333333333,0.741666666666667,0.739837398373984,0.861788617886179,0.728813559322034,0.758620689655172,0.830508474576271,0.775862068965517,0.711864406779661,0.745762711864407,0.793103448275862,0.813559322033898,null,null,null,null,null,0.724137931034483,0.728813559322034],[0.711344537815126,0.702521008403362,0.710084033613445,0.805042016806723,0.76890756302521,0.692307692307692,0.82051282051282,0.752136752136752,0.802564102564103,0.717948717948718,0.564102564102564,0.805982905982906,0.717948717948718,null,null,null,null,null,0.693162393162393,0.817094017094017,0.801980198019802,0.878217821782178,0.782178217821782,0.769306930693069,0.748514851485149,0.906185567010309,0.84020618556701,0.845360824742268,0.845360824742269,0.824742268041237,0.745876288659794,0.850515463917526,0.845360824742268,null,null,null,null,null,0.834020618556701,0.907216494845361,0.651260504201681,0.71218487394958,0.71218487394958,0.697478991596639,0.797478991596639,0.608333333333333,0.656666666666667,0.7,0.666666666666667,0.708333333333333,0.466666666666667,0.661666666666667,0.800833333333333,null,null,null,null,null,0.633333333333333,0.641666666666667,0.788990825688073,0.864220183486239,0.743119266055046,0.715596330275229,0.730275229357798,0.685714285714286,0.866666666666667,0.912380952380953,0.936904761904762,0.904761904761905,0.730952380952381,0.962619047619048,0.866666666666667,null,null,null,null,null,0.834285714285715,0.866666666666667,0.69017094017094,0.707264957264957,0.796581196581197,0.769230769230769,0.683760683760684,0.554621848739496,0.591176470588235,0.747899159663866,0.638655462184874,0.791596638655462,0.495798319327731,0.714285714285714,0.715966386554622,null,null,null,null,null,0.69327731092437,0.691596638655462,0.588235294117647,0.736134453781513,0.739495798319328,0.729411764705883,0.857142857142857,0.625,0.656696428571429,0.678571428571429,0.65625,0.613392857142857,0.616071428571428,0.678571428571429,0.679464285714286,null,null,null,null,null,0.657142857142857,0.625]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_beet<\/th>\n      <th>N_beet<\/th>\n      <th>covers_beet<\/th>\n      <th>width_beet<\/th>\n      <th>cvRsquared_carrot<\/th>\n      <th>N_carrot<\/th>\n      <th>covers_carrot<\/th>\n      <th>width_carrot<\/th>\n      <th>cvRsquared_peppers<\/th>\n      <th>N_peppers<\/th>\n      <th>covers_peppers<\/th>\n      <th>width_peppers<\/th>\n      <th>cvRsquared_potato<\/th>\n      <th>N_potato<\/th>\n      <th>covers_potato<\/th>\n      <th>width_potato<\/th>\n      <th>cvRsquared_zucchini<\/th>\n      <th>N_zucchini<\/th>\n      <th>covers_zucchini<\/th>\n      <th>width_zucchini<\/th>\n      <th>covers_squash<\/th>\n      <th>width_squash<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Bfrtipl","lengthMenu":[10,20,40,80],"buttons":["colvis"],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[11]; $(this.api().cell(row, 11).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[15]; $(this.api().cell(row, 15).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[19]; $(this.api().cell(row, 19).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->



```
## [1] "nutrient fitplots"
```





<!-- ## Fit Plots For Nutrients { .tabset } -->

<!-- ### Antioxidants { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-consumerModels-fitPlot.png) -->

<!-- ### Polyphenols { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-consumerModels-fitPlot.png) -->


<!-- ### Brix { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-consumerModels-fitPlot.png) -->



<!-- ### BQI { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-consumerModels-fitPlot.png) -->

<!-- ## Importance for Nutrients { .tabset } -->

<!-- ### Beet { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-Farmerwithsoilmodels.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-Farmerwithoutsoilmodels.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-Farmerconsumermodels.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-Consumermodels.png ) -->


<!-- ### Carrot { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-Farmerwithsoilmodels.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-Farmerwithoutsoilmodels.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-Farmerconsumermodels.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-Consumermodels.png ) -->


<!-- ### Potato { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-Farmerwithsoilmodels.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-Farmerwithoutsoilmodels.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-Farmerconsumermodels.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-Consumermodels.png ) -->


<!-- ### Peppers { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-Farmerwithsoilmodels.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-Farmerwithoutsoilmodels.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-Farmerconsumermodels.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-Consumermodels.png ) -->


<!-- ### Squash { .tabset } -->

<!-- <\!-- #### farmerWithSoilModels -\-> -->

<!-- <\!-- ![importance plog for squash.]( ) -\-> -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for squash.]( ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for squash.]( ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for squash.]( ) -->


<!-- ### Zucchini { .tabset } -->

<!-- <\!-- #### farmerWithSoilModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.](./graphics/importancePlots/allImportanceRegressions-Zucchini-Farmerwithsoilmodels.png ) -\-> -->

<!-- <\!-- #### farmerWithoutSoilModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.](./graphics/importancePlots/allImportanceRegressions-Zucchini-Farmerwithoutsoilmodels.png ) -\-> -->

<!-- <\!-- #### farmerConsumerModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.](./graphics/importancePlots/allImportanceRegressions-Zucchini-Farmerconsumermodels.png ) -\-> -->

<!-- #### consumerModels -->

<!-- ![importance plog for zucchini.](./graphics/importancePlots/allImportanceRegressions-Zucchini-Consumermodels.png ) -->


<!-- ## Importance for Minerals { .tabset } -->

<!-- ### Beet { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-FarmerwithsoilmodelsMinerals.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-FarmerwithoutsoilmodelsMinerals.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-FarmerconsumermodelsMinerals.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for beet.](./graphics/importancePlots/allImportanceRegressions-Beet-ConsumermodelsMinerals.png ) -->


<!-- ### Carrot { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-FarmerwithsoilmodelsMinerals.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-FarmerwithoutsoilmodelsMinerals.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-FarmerconsumermodelsMinerals.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for carrot.](./graphics/importancePlots/allImportanceRegressions-Carrot-ConsumermodelsMinerals.png ) -->


<!-- ### Potato { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-FarmerwithsoilmodelsMinerals.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-FarmerwithoutsoilmodelsMinerals.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-FarmerconsumermodelsMinerals.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for potato.](./graphics/importancePlots/allImportanceRegressions-Potato-ConsumermodelsMinerals.png ) -->


<!-- ### Peppers { .tabset } -->

<!-- #### farmerWithSoilModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-FarmerwithsoilmodelsMinerals.png ) -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-FarmerwithoutsoilmodelsMinerals.png ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-FarmerconsumermodelsMinerals.png ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for peppers.](./graphics/importancePlots/allImportanceRegressions-Peppers-ConsumermodelsMinerals.png ) -->


<!-- ### Squash { .tabset } -->

<!-- <\!-- #### farmerWithSoilModels -\-> -->

<!-- <\!-- ![importance plog for squash.]( ) -\-> -->

<!-- #### farmerWithoutSoilModels -->

<!-- ![importance plog for squash.]( ) -->

<!-- #### farmerConsumerModels -->

<!-- ![importance plog for squash.]( ) -->

<!-- #### consumerModels -->

<!-- ![importance plog for squash.]( ) -->


<!-- ### Zucchini { .tabset } -->

<!-- <\!-- #### farmerWithSoilModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.]( ) -\-> -->

<!-- <\!-- #### farmerWithoutSoilModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.]( ) -\-> -->

<!-- <\!-- #### farmerConsumerModels -\-> -->

<!-- <\!-- ![importance plog for zucchini.]( ) -\-> -->

<!-- #### consumerModels -->

<!-- ![importance plog for zucchini.](./graphics/importancePlots/allImportanceRegressions-Zucchini-ConsumermodelsMinerals.png ) -->



<!-- ```{r message2} -->
<!-- print("minera fitplots") -->
<!-- ``` -->




<!-- ## Fit Plots for Minerals { .tabset } -->

<!-- ### Ca { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-consumerModels-fitPlot.png) -->



<!-- ### Fe { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-consumerModels-fitPlot.png) -->


<!-- ### K { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-consumerModels-fitPlot.png) -->


<!-- ### Mg { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-consumerModels-fitPlot.png) -->


<!-- ### S { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-consumerModels-fitPlot.png) -->


<!-- ### Zn { .tabset } -->

<!-- #### peppers, zucchini, apple, squash_butternut { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-consumerModels-fitPlot.png) -->


<!-- #### beet, carrot, potato { .tabset } -->

<!-- ##### farmerWithSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-farmerWithSoilModels-fitPlot.png) -->

<!-- ##### farmerWithoutSoilModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-farmerWithoutSoilModels-fitPlot.png) -->

<!-- ##### farmerConsumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-farmerConsumerModels-fitPlot.png) -->

<!-- ##### consumerModels -->

<!-- ![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-consumerModels-fitPlot.png) -->


<!-- ```{r message4} -->
<!-- print("classification") -->
<!-- ``` -->

<!-- ## Random Forest Classification of The Percentile on the Explained Variable -->


<!-- Two targets are generated for each explained variable (Antioxidants, Polyphenols, Brix). The objective is predicting the quantile were the sample contents belong. Instead of using each quantile, the central ones are meged into a center category. -->

<!-- * Quartiles is three categories, *lower* for the first quartile, *center* for the second and third and *higher* for the fourth. -->
<!-- * Quintiles is also three categories, with *center* composed of the second, third and fourth quintiles. -->


<!-- ```{r percentilesAsssignment} -->

<!-- assignQuantile <- function( quantilesVector, x, order ) { -->
<!--   i <- 0 -->
<!--   while ( x >= quantilesVector[i+1] && i < order + 1 ) { -->
<!--     i <- i + 1 -->
<!--   } -->
<!--   return( names(quantilesVector)[i] ) -->
<!-- } -->

<!-- vectorOfQuantiles <- function( vec, order ) { -->
<!--   quartiles <- quantile( x = vec, probs = seq( from = 0, to = 1, by = 1/order ) ) -->
<!--   quartilesVec <- map_chr( vec, ~ assignQuantile( quartiles, .x, order ) ) -->
<!--   return( as.factor(quartilesVec) ) -->
<!-- } -->

<!-- centerCatsQuartiles <- c( -->
<!--   "0%" = "lower", -->
<!--   "25%" = "center", -->
<!--   "50%" = "center", -->
<!--   "75%" = "higher", -->
<!--   "100%" = "higher" -->
<!-- ) -->
<!-- centerCatsQuintiles <- c( -->
<!--   "0%" = "lower", -->
<!--   "20%" = "center", -->
<!--   "40%" = "center", -->
<!--   "60%" = "center", -->
<!--   "80%" = "higher", -->
<!--   "100%" = "higher" -->
<!-- ) -->

<!-- assignQuantileMergedCenter <- function( quantilesVector, x, order, mergingList ) { -->
<!--   i <- 0 -->

<!--   if ( is.na(x) ) { return( NA ) }; -->

<!--   while ( x >= quantilesVector[i+1] && i < order + 1 ) { -->
<!--     i <- i + 1 -->
<!--   } -->

<!--   output <- mergingList[[names(quantilesVector)[i]]] -->

<!--   if ( is.na( x ) ) { output <- NA } -->

<!--   return( output ) -->
<!-- } -->

<!-- vectorOfQuantilesMergedCenter <- function( vec, order, mergingList ) { -->
<!--   quartiles <- quantile( x = vec, probs = seq( from = 0, to = 1, by = 1/order ), na.rm = TRUE ) -->
<!--   quartilesVec <- map_chr( vec, ~ assignQuantileMergedCenter( quartiles, .x, order, mergingList ) ) %>% -->
<!--     as.factor() -->
<!--   return( quartilesVec ) -->
<!-- } -->


<!-- dataframe <- dataframe %>% -->
<!--   group_by( Type ) %>% -->
<!--   mutate( -->
<!--     polyQuartile = vectorOfQuantilesMergedCenter( Polyphenols, order = 4, centerCatsQuartiles ), -->
<!--     antiQuartile = vectorOfQuantilesMergedCenter( Antioxidants, order = 4, centerCatsQuartiles ), -->
<!--     brixQuartile = vectorOfQuantilesMergedCenter( Brix, order = 4, centerCatsQuartiles ), -->
<!--     bqiQuartile = vectorOfQuantilesMergedCenter( BQI, order = 4, centerCatsQuartiles ) -->
<!--          ) %>% -->
<!--   ungroup() -->

<!-- ``` -->

<!-- ```{r runClassification} -->

<!-- allProduceModelsClas <- crossing( -->
<!--   explained = c( "antiQuartile", "polyQuartile",  "brixQuartile", "bqiQuartile" ), -->
<!--   species = cropsList %>% unlist, -->
<!--   dataset = datasets %>% names -->
<!-- ) -->

<!-- if ( trainClassification ) { -->

<!-- cluster <- makePSOCKcluster(48) -->
<!-- registerDoParallel(cluster) -->

<!-- ## testTree <- classificationTree( "antiQuintile", "carrot", "all", dataframe, kFolds = 3 ) -->

<!-- allRFClas <- allProduceModelsClas -->
<!-- allRFClas <- allRFClas %>% mutate( -->
<!--                              model = pmap(  list( allRFClas$explained, allRFClas$species, allRFClas$dataset ), ~classificationTree( ..1, ..2, ..3, dataframe ), kFolds = 15 ) -->
<!--                            ) -->

<!-- stopCluster(cluster) -->

<!-- allRFClas <- allRFClas %>% mutate( -->
<!--                                Accuracy = map_dbl( allRFClas$model, ~ .x$results %>% -->
<!--                                                                          arrange( - Accuracy ) %>% -->
<!--                                                                          slice(1) %>% -->
<!--                                                                          select( Accuracy ) %>% -->
<!--                                                                          unlist() -->
<!--                                                   ), -->
<!--                                confMatrix = map( allRFClas$model, ~ ( .x$finalModel )$confusion ), -->
<!--                              mtry = model %>% map_dbl( ~ .x["bestTune"] %>% unlist ), -->
<!--                              oobError = map( allRFClas$model, ~ ( .x$finalModel )$err.rate ), -->
<!--                              N = map_dbl( model, ~ .x$trainingData %>% tibble %>% nrow ), -->
<!--                              y = map( model, ~ .x$finalModel$y ), -->
<!--                              y_predicted = map( model, ~ .x$finalModel$predicted ), -->
<!--                              importance = map( model, ~ .x$finalModel$importance ) -->
<!--                              ) -->

<!-- write_rds( x = allRFClas, path="../bigFiles/produceRFClassification.Rds" ) -->

<!-- lightProduceRFClassification <- allRFClas %>% -->
<!--   select( -model ) %>% -->
<!--   write_rds(  "./models/lightProduceRFClassification.Rds" ) -->

<!-- lightProduceRFClassification %>% write_rds( "./models/lightProduceRFClassification.Rds" ) -->

<!-- rm( "allRFClas" ) -->

<!-- } -->

<!-- lightProduceRFClassification <- read_rds( "./models/lightProduceRFClassification.Rds" ) -->

<!-- tableRFClassification <-  lightProduceRFClassification %>% -->
<!--   ## select( - oobError, - confMatrix, -y, -y_predicted, - importance ) %>% -->
<!--   pivot_wider( names_from = species, id_cols = c(explained, dataset), values_from = c( Accuracy, N ) ) %>% -->
<!--   select( explained, dataset, contains( cropsList ) ) %>% -->
<!--   arrange( explained, dataset ) -->


<!-- write_csv( x = tableRFClassification, "./models/lightGrainRFClassificationNonGM.csv" ) -->

<!-- datatable( tableRFClassification, options = ( -->
<!--   list( -->
<!--     lengthMenu = c( 7,21, 42, 84 ) -->
<!--   ) -->
<!-- ) ) %>% -->
<!--   formatStyle( tableRFClassification %>% select( contains("Accuracy") ) %>% colnames , -->
<!--               color = styleInterval( c(0.6, 0.8), c("black","blue","red") ) -->
<!--               ) -->

<!-- ``` -->

<!-- ### Confusion Matrix for a Succesful Model -->

<!-- ```{r confusionMatrix} -->

<!-- classCompensatingIndex <- function( class, set1, set2 ) { -->
<!--   missingClass <- which( set2 == class ) -->
<!--   patch <- sample( x = missingClass, size = 1 ) -->
<!--   return( patch ) -->
<!-- }; -->

<!-- validateConfusionMatrix <- function( dataframe, dataSet, pType, classVar, mtry, folds ) { -->

<!--   explicative <- datasets[[dataSet]] -->

<!--   modelData <- dataframe %>% -->
<!--   filter( Type == pType ) %>% -->
<!--   select( .data[[classVar]] , !!explicative, id_sample )  %>% -->
<!--   na.omit() -->

<!--   modelData[[classVar]] %<>% fct_drop() -->


<!--   repetitionsFolds <- groupKFold( group = modelData$id_sample, k = folds  ) -->
<!--   modelData <- modelData %>% select( - id_sample ) -->

<!--   spX <- modelData %>% -->
<!--     select( - .data[[classVar]] ) -->

<!--   ## spY <- modelData[[classVar]] %>% fct_drop() -->
<!--   spY <- modelData[[classVar]] -->

<!--   matrices <- list() -->

<!--   for ( i in 1:length(repetitionsFolds) ) { -->
<!--     spXTest <- spX[ - repetitionsFolds[[i]],] -->
<!--     spXTrain <- spX[repetitionsFolds[[i]],] -->

<!--     spYTest <- spY[ - repetitionsFolds[[i]]] %>% fct_drop() -->
<!--     spYTrain <- spY[repetitionsFolds[[i]]] %>% fct_drop() -->

<!--     dif1 <- setdiff( levels( spYTest %>% fct_drop ), levels( spYTrain %>% fct_drop ) ) -->
<!--     dif2 <- setdiff( levels( spYTrain %>% fct_drop ), levels( spYTest %>% fct_drop ) ) -->

<!--     if ( length( dif2 ) > 0 ) { -->
<!--       compensatingIndices <- map_dbl( dif2, ~ classCompensatingIndex( .x, spYTest, spYTrain ) ) -->
<!--       for ( index in compensatingIndices ) { -->
<!--         spXTest <- spX[ - c( repetitionsFolds[[i]], index ), ] -->
<!--         spYTest <- spY[ - c( repetitionsFolds[[i]], index ) ] -->
<!--       } -->
<!--     } -->

<!--     if ( length( dif1 ) > 0 ) { -->
<!--       compensatingIndices <- map_dbl( dif1, ~ classCompensatingIndex( .x, spYTrain, spYTest ) ) -->
<!--       for ( index in compensatingIndices ) { -->
<!--         spXTrain <- spX[  c( repetitionsFolds[[i]], index ), ] -->
<!--         spYTrain <- spY[ c( repetitionsFolds[[i]], index ) ] -->

<!--       } -->
<!--     } -->

<!--     iterationModel <- randomForest( x = spXTrain, y = spYTrain, xtest = spXTest, ytest = spYTest, mtry = mtry ) -->
<!--     matrix <- iterationModel$test$confusion -->
<!--     matrices[[i]] <- matrix -->
<!--   } -->

<!--   confMat <- matrices %>% reduce( `+` ) %>% as_tibble( rownames = "class" ) -->
<!--   clasDict <- list( -->
<!--     "lower" = 0, -->
<!--     "center" = 1, -->
<!--     "higher" = 2 -->
<!--   ) -->

<!--   plotMatrix <- confMat %>% -->
<!--     gather( center, higher, lower, key = "actualClass", value = "NumberAsigned" ) %>% -->
<!--     rename( assignedClass = class ) %>% -->
<!--     mutate( x = clasDict[ actualClass ] %>% unlist, y = clasDict[ assignedClass ] %>% unlist ) -->

<!--   totalSamples <- plotMatrix$NumberAsigned %>% sum() -->

<!--   plotMatrix <- plotMatrix %>% mutate( -->
<!--                                  NumberAsigned = NumberAsigned / totalSamples, -->
<!--                                  class.error = class.error / folds, -->
<!--                                  labelNumberAsigned = ( round( NumberAsigned, 3 ) * 100  )%>% as.character() %>% paste0( ., "%" ), -->
<!--                                  actualClass = fct_relevel( actualClass, c( "lower","center", "higher" ) ), -->
<!--                                  assignedClass = fct_relevel( assignedClass, c( "lower","center", "higher" ) ) -->
<!--                                ) -->


<!--   return( plotMatrix ) -->

<!-- } -->

<!-- ## oatsAnti5ConfMatrix <- validateConfusionMatrix( dataframe = dataframe, dataSet = "surface", pType = "oats", classVar = "antiQuintile", mtry = 1, folds = 3 ) -->

<!-- ## oatsAnti5ConfMatrix %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   ggtitle( "Oats Antioxidants Quintile, Surface Sample Scan." ) -->

<!-- set.seed( 459098546 ) -->

<!-- ## bokchoiPolyphenolsSurfaceVariety <- validateConfusionMatrix( dataframe = dataframe, dataSet = "consumer2", pType = "carrot", classVar = "polyQuintile", mtry = 1, folds = 8 ) -->

<!-- ## bokchoiPolyphenolsSurfaceVariety %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Bok choi Polyphenols Quintile,\nUsing Just Metadata." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Oats-AntioxidantsQuartiles-ConfusionMatrix-JustMetadata.png", device = "png", width = 15, height = 10 ) -->

<!-- ## oatsAntioxidants4Ground <- validateConfusionMatrix( dataframe = dataframe, dataSet = "ground", pType = "oats", classVar = "antiQuartile", mtry = 10, folds = 8 ) -->

<!-- ## oatsAntioxidants4Ground %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Oats Antioxidants Quartile,\nUsing Ground Sample Scans." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Oats-AntioxidantsQuartiles-ConfusionMatrix-Ground.png", device = "png", width = 15, height = 10 ) -->

<!-- ## oatsAntioxidants4Unground <- validateConfusionMatrix( dataframe = dataframe, dataSet = "unground", pType = "oats", classVar = "antiQuartile", mtry = 10, folds = 8 ) -->

<!-- ## oatsAntioxidants4Unground %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Oats Antioxidants Quartile,\nUsing Unground Sample Scans." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Oats-AntioxidantsQuartiles-ConfusionMatrix-Unground.png", device = "png", width = 15, height = 10 ) -->

<!-- ## wheatAntioxidants4JustMetadata <- validateConfusionMatrix( dataframe = dataframe, dataSet = "justMetadata", pType = "wheat", classVar = "antiQuartile", mtry = 1, folds = 3 ) -->

<!-- ## wheatAntioxidants4JustMetadata %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Wheat Antioxidants Quartile,\nUsing Just Metadata." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Wheat-AntioxidantsQuartiles-ConfusionMatrix-JustMetadata.png", device = "png", width = 15, height = 10 ) -->

<!-- ## wheatAntioxidants4Ground <- validateConfusionMatrix( dataframe = dataframe, dataSet = "ground", pType = "wheat", classVar = "antiQuartile", mtry = 10, folds = 8 ) -->

<!-- ## wheatAntioxidants4Ground %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Wheat Antioxidants Quartile,\nUsing Ground Sample Scans." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Wheat-AntioxidantsQuartiles-ConfusionMatrix-Ground.png", device = "png", width = 15, height = 10 ) -->

<!-- ## wheatAntioxidants4Unground <- validateConfusionMatrix( dataframe = dataframe, dataSet = "unground", pType = "wheat", classVar = "antiQuartile", mtry = 10, folds = 8 ) -->

<!-- ## wheatAntioxidants4Unground %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   ) + -->
<!-- ##   ggtitle( "Wheat Antioxidants Quartile,\nUsing Unground Sample Scans." ) -->

<!-- ## ggsave(  filename="./graphics/confusionMatrices/Oats-AntioxidantsQuartiles-ConfusionMatrix-Unground.png", device = "png", width = 15, height = 10 ) -->

<!-- ## matrixConf <- lightProduceRFClassification[[1,"confMatrix"]] -->

<!-- plotConfusionMatrixFromRFModelObj <- function( object ) { -->

<!--   confMat <- object %>% as_tibble( rownames = "class" ) -->
<!--   clasDict <- list( -->
<!--     "lower" = 0, -->
<!--     "center" = 1, -->
<!--     "higher" = 2 -->
<!--   ) -->

<!--   plotMatrix <- confMat %>% -->
<!--     gather( center, higher, lower, key = "actualClass", value = "NumberAsigned" ) %>% -->
<!--     rename( assignedClass = class ) %>% -->
<!--     mutate( x = clasDict[ actualClass ] %>% unlist, y = clasDict[ assignedClass ] %>% unlist ) -->

<!--   totalSamples <- plotMatrix$NumberAsigned %>% sum() -->

<!--   plotMatrix <- plotMatrix %>% mutate( -->
<!--                                  NumberAsigned = NumberAsigned / totalSamples, -->
<!--                                  class.error = class.error, -->
<!--                                  labelNumberAsigned = ( round( NumberAsigned, 3 ) * 100  )%>% as.character() %>% paste0( ., "%" ), -->
<!--                                  actualClass = fct_relevel( actualClass, c( "lower","center", "higher" ) ), -->
<!--                                  assignedClass = fct_relevel( assignedClass, c( "lower","center", "higher" ) ) -->
<!--                                ) -->


<!--   return( plotMatrix ) -->

<!-- } -->

<!-- ## lightProduceRFClassification <- lightProduceRFClassification %>% arrange( - Accuracy ) -->

<!-- ## plotConfusionMatrixFromRFModelObj( ( lightProduceRFClassification[[1,"confMatrix"]] )[[1]] ) %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   )  -->

<!-- ## wheatAntioxidants4GroundConfMatrix <- ( lightProduceRFClassification %>% filter( species == "wheat" & explained == "antiQuartile" & dataset == "ground" ) )[["confMatrix"]] %>% .[[1]] -->

<!-- ## plotConfusionMatrixFromRFModelObj( wheatAntioxidants4GroundConfMatrix ) %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   )  -->

<!-- ## wheatAntioxidants4JustMetadataConfMatrix <- ( lightProduceRFClassification %>% filter( species == "wheat" & explained == "antiQuartile" & dataset == "justMetadata" ) )[["confMatrix"]] %>% .[[1]] -->

<!-- ## plotConfusionMatrixFromRFModelObj( wheatAntioxidants4JustMetadataConfMatrix ) %>% -->
<!-- ##   ggplot() + -->
<!-- ##   aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) + -->
<!-- ##   geom_tile() + -->
<!-- ##   geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) + -->
<!-- ##   scale_fill_viridis_c( "Number Asigned", option = "plasma" ) + -->
<!-- ##   theme( -->
<!-- ##     plot.title = element_text( size = 40 ) -->
<!-- ##   )  -->

<!-- ``` -->

<!-- # Clusterization for Climate Regions and Soil Suborders { .tabset } -->


<!-- ### Beet { .tabset }  -->

<!-- #### by Climate Region -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "beet" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: beet" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/beet-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "beet" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: beet" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/beet-Suborder.png", device = "png" ) -->

<!-- ``` -->

<!-- ### Carrot { .tabset }  -->

<!-- #### by Climate Region -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "carrot" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: carrot" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/carrot-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "carrot" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: carrot" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/carrot-Suborder.png", device = "png" ) -->
<!-- ``` -->


<!-- ### Peppers { .tabset }  -->

<!-- #### by Climate Region -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "peppers" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: peppers" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/peppers-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->


<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "peppers" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: peppers" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/peppers-Suborder.png", device = "png" ) -->
<!-- ``` -->


<!-- ### Potato { .tabset }  -->


<!-- #### by Climate Region -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "potato" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: potato" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/potato-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "potato" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: potato" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/beet-Suborder.png", device = "png" ) -->
<!-- ``` -->


<!-- ### Squash { .tabset }  -->

<!-- #### by Climate Region -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "squash" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: squash" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/squash-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "squash" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: squash" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/squash-Suborder.png", device = "png" ) -->
<!-- ``` -->


<!-- ### Zucchini { .tabset }  -->


<!-- #### by Climate Region -->


<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12 } -->
<!-- dataframe %>% -->
<!--   filter( Type == "zucchini" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = climateRegion ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Climate Regions over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: zucchini" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/zucchini-Region.png", device = "png" ) -->

<!-- ``` -->

<!-- #### by Soil Suborder -->

<!-- ```{r  fig.height = 10, fig.width = 12, base_size = 12} -->
<!-- dataframe %>% -->
<!--   filter( Type == "zucchini" ) %>% -->
<!--   ggplot() + -->
<!--   aes( x = Antioxidants, y = Polyphenols, color = soilSuborder ) + -->
<!--   geom_point( size = 2 ) + -->
<!--   labs( -->
<!--     title = "Clusterization of Soil Suborder over the Joint Distribution of Antioxidants and Polyphenols", -->
<!--     subtitle = "Crop: zucchini" -->
<!--   ) -->
<!-- ggsave(  filename="./graphics/clusterizations/zucchini-Suborder.png", device = "png" ) -->
<!-- ``` -->


# Aggregation of Farm Practices Analysis Over Crops

  In this section, we'll try to find meaningful results for farm practices **across** crops.
  Our strategy to achieve a meaningful comparison is replacing on each point the actual variable value by the crop's percentile reached. The goal of this study is checking if a practices is able to push each crop far from its *median*.
  This means that if a tomatto that is bigger than 60% of all other tomato observations has a value of 0.1 for a certain variable and for Kale you need to reach 10 to be above 60% of all subjects, we are giving the same score to a a 0.1 tomato sample and a kale sample with a value of 10. We are measuring the relative scarcity of a value among it's crop peers, rather than the absolute value.



## Aggregation by Percentile Transformation { .tabset }

### Tubers { .tabset }

#### Individual Variables

Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.







<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5838528 </td>
   <td style="text-align:right;"> 0.7288434 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.12,0.37] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.0016446 </td>
   <td style="text-align:right;"> 0.2291627 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6670117 </td>
   <td style="text-align:right;"> 0.8187588 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.1,0.27] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0045641 </td>
   <td style="text-align:right;"> 0.1855263 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6097525 </td>
   <td style="text-align:right;"> 0.7002821 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.17,0.33] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.0070632 </td>
   <td style="text-align:right;"> 0.2517454 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4024094 </td>
   <td style="text-align:right;"> 0.3283673 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.21,-0.06] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0076409 </td>
   <td style="text-align:right;"> -0.1323674 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5407112 </td>
   <td style="text-align:right;"> 0.6141379 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.06,0.27] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0148354 </td>
   <td style="text-align:right;"> 0.1393220 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4219368 </td>
   <td style="text-align:right;"> 0.3011283 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.43,-0.2] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.0189423 </td>
   <td style="text-align:right;"> -0.2960285 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5049030 </td>
   <td style="text-align:right;"> 0.3762069 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.2,0.07] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.0283390 </td>
   <td style="text-align:right;"> -0.0773612 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4915841 </td>
   <td style="text-align:right;"> 0.6944828 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.05,0.35] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.35 </td>
   <td style="text-align:right;"> 0.0504987 </td>
   <td style="text-align:right;"> 0.2151170 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.9141039 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0563190 </td>
   <td style="text-align:right;"> 0.0000034 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6246661 </td>
   <td style="text-align:right;"> 0.6118881 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.15] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0611486 </td>
   <td style="text-align:right;"> 0.0962080 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4701616 </td>
   <td style="text-align:right;"> 0.4182759 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.34,-0.14] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.0921740 </td>
   <td style="text-align:right;"> -0.2193456 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.5808512 </td>
   <td style="text-align:right;"> 0.7002821 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.2,0.4] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.0265622 </td>
   <td style="text-align:right;"> 0.3116706 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.8478785 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0287962 </td>
   <td style="text-align:right;"> 0.0000211 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.4057496 </td>
   <td style="text-align:right;"> 0.4809591 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.23,-0.11] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0491109 </td>
   <td style="text-align:right;"> -0.1749571 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.5652673 </td>
   <td style="text-align:right;"> 0.6727786 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.23,-0.11] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.0539897 </td>
   <td style="text-align:right;"> -0.1665001 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.5371041 </td>
   <td style="text-align:right;"> 0.6086957 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.15] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0815906 </td>
   <td style="text-align:right;"> 0.0758696 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_P_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.4699352 </td>
   <td style="text-align:right;"> 0.5500000 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.05,0.09] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.0915316 </td>
   <td style="text-align:right;"> 0.0205268 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.8993089 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0011775 </td>
   <td style="text-align:right;"> 0.0000391 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5504521 </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.05] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.0039437 </td>
   <td style="text-align:right;"> 0.0043113 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.4379310 </td>
   <td style="text-align:right;"> 0.3489655 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.36,-0.21] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.0106720 </td>
   <td style="text-align:right;"> -0.2813872 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6022942 </td>
   <td style="text-align:right;"> 0.7002821 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.21,0.39] </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.0178759 </td>
   <td style="text-align:right;"> 0.3067287 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5950170 </td>
   <td style="text-align:right;"> 0.6953456 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.17] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0231744 </td>
   <td style="text-align:right;"> 0.0944525 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.4465534 </td>
   <td style="text-align:right;"> 0.3744711 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.39,-0.23] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0291958 </td>
   <td style="text-align:right;"> -0.3139149 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6404486 </td>
   <td style="text-align:right;"> 0.7268966 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.05,0.17] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0304603 </td>
   <td style="text-align:right;"> 0.1152316 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3898493 </td>
   <td style="text-align:right;"> 0.4117241 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.02,0.15] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0360547 </td>
   <td style="text-align:right;"> 0.0668427 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5613540 </td>
   <td style="text-align:right;"> 0.5782793 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.11,0.22] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0393243 </td>
   <td style="text-align:right;"> 0.1657411 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.4885044 </td>
   <td style="text-align:right;"> 0.4859002 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.09,0] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0431014 </td>
   <td style="text-align:right;"> -0.0433185 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5830733 </td>
   <td style="text-align:right;"> 0.6600000 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.25,0.43] </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.0432614 </td>
   <td style="text-align:right;"> 0.3385782 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5567928 </td>
   <td style="text-align:right;"> 0.5641379 </td>
   <td style="text-align:right;"> 103 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.16,0.3] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0536027 </td>
   <td style="text-align:right;"> 0.2310812 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5319783 </td>
   <td style="text-align:right;"> 0.5691700 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.02,0.11] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.0631432 </td>
   <td style="text-align:right;"> 0.0653980 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3819434 </td>
   <td style="text-align:right;"> 0.3776446 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.04,0.09] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.0678591 </td>
   <td style="text-align:right;"> 0.0197371 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5988431 </td>
   <td style="text-align:right;"> 0.6685472 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.27,0.46] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.46 </td>
   <td style="text-align:right;"> 0.0781894 </td>
   <td style="text-align:right;"> 0.3601822 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3547775 </td>
   <td style="text-align:right;"> 0.3363893 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.03,0.11] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.0832264 </td>
   <td style="text-align:right;"> 0.0120594 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5489077 </td>
   <td style="text-align:right;"> 0.5338505 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.08,0.07] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.0999307 </td>
   <td style="text-align:right;"> 0.0035214 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.9057939 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 134 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0014631 </td>
   <td style="text-align:right;"> 0.0000803 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5034620 </td>
   <td style="text-align:right;"> 0.5416079 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.09,0.25] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0041787 </td>
   <td style="text-align:right;"> 0.1812688 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5033999 </td>
   <td style="text-align:right;"> 0.5379310 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.07,0.26] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0113567 </td>
   <td style="text-align:right;"> 0.1633897 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5315705 </td>
   <td style="text-align:right;"> 0.4912632 </td>
   <td style="text-align:right;"> 134 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.01,0.02] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.0159173 </td>
   <td style="text-align:right;"> 0.0000269 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4627949 </td>
   <td style="text-align:right;"> 0.4333333 </td>
   <td style="text-align:right;"> 227 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.13,-0.04] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.0194181 </td>
   <td style="text-align:right;"> -0.0848369 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5820746 </td>
   <td style="text-align:right;"> 0.6685472 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.28,0.44] </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> 0.0397822 </td>
   <td style="text-align:right;"> 0.3631871 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4265538 </td>
   <td style="text-align:right;"> 0.3775314 </td>
   <td style="text-align:right;"> 134 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.16,-0.05] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0479253 </td>
   <td style="text-align:right;"> -0.1034690 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5391446 </td>
   <td style="text-align:right;"> 0.5808822 </td>
   <td style="text-align:right;"> 134 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.02,0.13] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0731181 </td>
   <td style="text-align:right;"> 0.0716283 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.6822088 </td>
   <td style="text-align:right;"> 0.8857546 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.23,0.48] </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.0001406 </td>
   <td style="text-align:right;"> 0.3173278 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.6913392 </td>
   <td style="text-align:right;"> 0.9000000 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.24,0.46] </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.46 </td>
   <td style="text-align:right;"> 0.0007713 </td>
   <td style="text-align:right;"> 0.3353728 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.4660511 </td>
   <td style="text-align:right;"> 0.5761636 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.04,0.15] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0080007 </td>
   <td style="text-align:right;"> 0.0358932 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.3402887 </td>
   <td style="text-align:right;"> 0.4524138 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.33,-0.18] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.0083305 </td>
   <td style="text-align:right;"> -0.2792867 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.2918785 </td>
   <td style="text-align:right;"> 0.3638928 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.34,-0.23] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0158302 </td>
   <td style="text-align:right;"> -0.2915791 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.4731949 </td>
   <td style="text-align:right;"> 0.4511931 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.02,0] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0191612 </td>
   <td style="text-align:right;"> -0.0000243 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.3705842 </td>
   <td style="text-align:right;"> 0.3138223 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.43,-0.33] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.0288900 </td>
   <td style="text-align:right;"> -0.3870733 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.3772338 </td>
   <td style="text-align:right;"> 0.2440347 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.21,-0.05] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0456952 </td>
   <td style="text-align:right;"> -0.1300869 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.6165415 </td>
   <td style="text-align:right;"> 0.6671368 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.23,0.38] </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.0715138 </td>
   <td style="text-align:right;"> 0.3194682 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.4359570 </td>
   <td style="text-align:right;"> 0.4034707 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.14,0] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0950359 </td>
   <td style="text-align:right;"> -0.0737308 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4878877 </td>
   <td style="text-align:right;"> 0.3744711 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.33,-0.19] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.0005584 </td>
   <td style="text-align:right;"> -0.2714986 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4318414 </td>
   <td style="text-align:right;"> 0.3676093 </td>
   <td style="text-align:right;"> 240 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.17,-0.08] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.0012531 </td>
   <td style="text-align:right;"> -0.1231588 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5481597 </td>
   <td style="text-align:right;"> 0.6489655 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.16,0.31] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.0068462 </td>
   <td style="text-align:right;"> 0.2441439 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.3873167 </td>
   <td style="text-align:right;"> 0.3402425 </td>
   <td style="text-align:right;"> 124 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.22,-0.1] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.0155004 </td>
   <td style="text-align:right;"> -0.1625726 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4776552 </td>
   <td style="text-align:right;"> 0.3758621 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.36,-0.22] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.0169045 </td>
   <td style="text-align:right;"> -0.2946652 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5613976 </td>
   <td style="text-align:right;"> 0.6523272 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.16,0.34] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.0185535 </td>
   <td style="text-align:right;"> 0.2608826 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5716269 </td>
   <td style="text-align:right;"> 0.5761636 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.15,0.31] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.0211434 </td>
   <td style="text-align:right;"> 0.2376582 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4964864 </td>
   <td style="text-align:right;"> 0.5241379 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.05,0.21] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0231485 </td>
   <td style="text-align:right;"> 0.1110063 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5938657 </td>
   <td style="text-align:right;"> 0.6413793 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.17,0.38] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.0295983 </td>
   <td style="text-align:right;"> 0.2792657 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4877227 </td>
   <td style="text-align:right;"> 0.4511931 </td>
   <td style="text-align:right;"> 124 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.07,0] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0790832 </td>
   <td style="text-align:right;"> -0.0093628 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6097233 </td>
   <td style="text-align:right;"> 0.6431594 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.17,0.27] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0830508 </td>
   <td style="text-align:right;"> 0.2229118 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4686204 </td>
   <td style="text-align:right;"> 0.4164859 </td>
   <td style="text-align:right;"> 124 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.1,0] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0857132 </td>
   <td style="text-align:right;"> -0.0463506 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.8825143 </td>
   <td style="text-align:right;"> 0.8260870 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.09,0] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0007013 </td>
   <td style="text-align:right;"> -0.0000941 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.5748390 </td>
   <td style="text-align:right;"> 0.6482213 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.04,0.17] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0421219 </td>
   <td style="text-align:right;"> 0.0429126 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.6040762 </td>
   <td style="text-align:right;"> 0.7707510 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.01,0.29] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.0502463 </td>
   <td style="text-align:right;"> 0.1383760 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.8900455 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 237 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0005092 </td>
   <td style="text-align:right;"> 0.0000461 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4065504 </td>
   <td style="text-align:right;"> 0.3517787 </td>
   <td style="text-align:right;"> 237 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.18,-0.08] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.0008798 </td>
   <td style="text-align:right;"> -0.1332697 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5491405 </td>
   <td style="text-align:right;"> 0.5271150 </td>
   <td style="text-align:right;"> 237 </td>
   <td style="text-align:right;"> 70 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.05] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.0020951 </td>
   <td style="text-align:right;"> 0.0043525 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4371868 </td>
   <td style="text-align:right;"> 0.4503448 </td>
   <td style="text-align:right;"> 215 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.02,0.16] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0061489 </td>
   <td style="text-align:right;"> 0.0758901 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4351812 </td>
   <td style="text-align:right;"> 0.4188999 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.17] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0200821 </td>
   <td style="text-align:right;"> 0.0846238 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4363849 </td>
   <td style="text-align:right;"> 0.3800000 </td>
   <td style="text-align:right;"> 215 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.34,-0.21] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.0264901 </td>
   <td style="text-align:right;"> -0.2744758 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4561470 </td>
   <td style="text-align:right;"> 0.4125529 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.35,-0.21] </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.0272561 </td>
   <td style="text-align:right;"> -0.2785671 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5754005 </td>
   <td style="text-align:right;"> 0.6502116 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.27,0.42] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> 0.0379544 </td>
   <td style="text-align:right;"> 0.3455480 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5714417 </td>
   <td style="text-align:right;"> 0.6227080 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.15,0.27] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0481566 </td>
   <td style="text-align:right;"> 0.2094141 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5754964 </td>
   <td style="text-align:right;"> 0.6627586 </td>
   <td style="text-align:right;"> 215 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.27,0.43] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.0502044 </td>
   <td style="text-align:right;"> 0.3572688 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5622711 </td>
   <td style="text-align:right;"> 0.6006897 </td>
   <td style="text-align:right;"> 215 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.2,0.33] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.0566054 </td>
   <td style="text-align:right;"> 0.2751994 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5755163 </td>
   <td style="text-align:right;"> 0.6530324 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.2,0.36] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 0.0601844 </td>
   <td style="text-align:right;"> 0.2877471 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5355539 </td>
   <td style="text-align:right;"> 0.5930889 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.05,0.08] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.0708949 </td>
   <td style="text-align:right;"> 0.0267811 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6231859 </td>
   <td style="text-align:right;"> 0.6442688 </td>
   <td style="text-align:right;"> 135 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.06,0.16] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0010117 </td>
   <td style="text-align:right;"> 0.1152895 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4720583 </td>
   <td style="text-align:right;"> 0.4138817 </td>
   <td style="text-align:right;"> 229 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.12,-0.03] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0012533 </td>
   <td style="text-align:right;"> -0.0744730 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4128948 </td>
   <td style="text-align:right;"> 0.3158621 </td>
   <td style="text-align:right;"> 119 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.39,-0.23] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0157937 </td>
   <td style="text-align:right;"> -0.3193372 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4382556 </td>
   <td style="text-align:right;"> 0.4193103 </td>
   <td style="text-align:right;"> 119 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.01,0.15] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0252714 </td>
   <td style="text-align:right;"> 0.0648299 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Co_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5533396 </td>
   <td style="text-align:right;"> 0.6276446 </td>
   <td style="text-align:right;"> 118 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.05,0.06] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0330032 </td>
   <td style="text-align:right;"> 0.0057088 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4500072 </td>
   <td style="text-align:right;"> 0.4065585 </td>
   <td style="text-align:right;"> 118 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.17] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0509088 </td>
   <td style="text-align:right;"> 0.0855160 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4171488 </td>
   <td style="text-align:right;"> 0.3543724 </td>
   <td style="text-align:right;"> 118 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.42,-0.25] </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.0673916 </td>
   <td style="text-align:right;"> -0.3385019 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4380180 </td>
   <td style="text-align:right;"> 0.3744828 </td>
   <td style="text-align:right;"> 119 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.21,-0.09] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0693658 </td>
   <td style="text-align:right;"> -0.1517592 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5806897 </td>
   <td style="text-align:right;"> 0.6379310 </td>
   <td style="text-align:right;"> 119 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.22,0.37] </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.0707305 </td>
   <td style="text-align:right;"> 0.2986335 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5544752 </td>
   <td style="text-align:right;"> 0.6230606 </td>
   <td style="text-align:right;"> 118 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.24,0.41] </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.41 </td>
   <td style="text-align:right;"> 0.0922474 </td>
   <td style="text-align:right;"> 0.3370758 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5785222 </td>
   <td style="text-align:right;"> 0.6413793 </td>
   <td style="text-align:right;"> 119 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.27,0.43] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.0935241 </td>
   <td style="text-align:right;"> 0.3524123 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5670974 </td>
   <td style="text-align:right;"> 0.5994358 </td>
   <td style="text-align:right;"> 118 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.03,0.09] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.0939772 </td>
   <td style="text-align:right;"> 0.0367294 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.9181647 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.08] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.0000069 </td>
   <td style="text-align:right;"> 0.0021624 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5458426 </td>
   <td style="text-align:right;"> 0.5559441 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.01,0.06] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0011515 </td>
   <td style="text-align:right;"> 0.0103693 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5727102 </td>
   <td style="text-align:right;"> 0.7475862 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.17,0.35] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.35 </td>
   <td style="text-align:right;"> 0.0020635 </td>
   <td style="text-align:right;"> 0.2621113 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4622402 </td>
   <td style="text-align:right;"> 0.4083308 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.13,-0.02] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.0026320 </td>
   <td style="text-align:right;"> -0.0677964 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6502430 </td>
   <td style="text-align:right;"> 0.8092384 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.26,0.39] </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.0037713 </td>
   <td style="text-align:right;"> 0.3328974 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5598995 </td>
   <td style="text-align:right;"> 0.6265867 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.08,0.31] </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.0068805 </td>
   <td style="text-align:right;"> 0.1840162 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5490018 </td>
   <td style="text-align:right;"> 0.6358621 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.06,0.31] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.31 </td>
   <td style="text-align:right;"> 0.0074188 </td>
   <td style="text-align:right;"> 0.1674931 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5939704 </td>
   <td style="text-align:right;"> 0.7189704 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.18,0.37] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 0.0131967 </td>
   <td style="text-align:right;"> 0.2664988 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5668496 </td>
   <td style="text-align:right;"> 0.6482213 </td>
   <td style="text-align:right;"> 137 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.1] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.0233561 </td>
   <td style="text-align:right;"> 0.0498942 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4527944 </td>
   <td style="text-align:right;"> 0.3543724 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.39,-0.18] </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.0269206 </td>
   <td style="text-align:right;"> -0.2757376 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3898587 </td>
   <td style="text-align:right;"> 0.3127445 </td>
   <td style="text-align:right;"> 134 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.2,-0.09] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0283845 </td>
   <td style="text-align:right;"> -0.1488038 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5338234 </td>
   <td style="text-align:right;"> 0.6379310 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.12,0.36] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 0.0304248 </td>
   <td style="text-align:right;"> 0.2496393 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6097434 </td>
   <td style="text-align:right;"> 0.6844147 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.04,0.16] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0331195 </td>
   <td style="text-align:right;"> 0.1064954 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5341150 </td>
   <td style="text-align:right;"> 0.6290550 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.05,0.27] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0396050 </td>
   <td style="text-align:right;"> 0.1713820 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4618330 </td>
   <td style="text-align:right;"> 0.3651724 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.38,-0.17] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.0542100 </td>
   <td style="text-align:right;"> -0.2778761 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6573079 </td>
   <td style="text-align:right;"> 0.7986207 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.34,0.46] </td>
   <td style="text-align:right;"> 0.34 </td>
   <td style="text-align:right;"> 0.46 </td>
   <td style="text-align:right;"> 0.0565314 </td>
   <td style="text-align:right;"> 0.4034837 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3277121 </td>
   <td style="text-align:right;"> 0.2637627 </td>
   <td style="text-align:right;"> 132 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.22,-0.12] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.0575133 </td>
   <td style="text-align:right;"> -0.1695845 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3277121 </td>
   <td style="text-align:right;"> 0.2637627 </td>
   <td style="text-align:right;"> 132 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.22,-0.12] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.0575133 </td>
   <td style="text-align:right;"> -0.1695845 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4400816 </td>
   <td style="text-align:right;"> 0.3878702 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.01,0.17] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0824531 </td>
   <td style="text-align:right;"> 0.0726046 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.9324125 </td>
   <td style="text-align:right;"> 0.9197397 </td>
   <td style="text-align:right;"> 128 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0,0.08] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.0000004 </td>
   <td style="text-align:right;"> 0.0038909 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5954271 </td>
   <td style="text-align:right;"> 0.5914535 </td>
   <td style="text-align:right;"> 128 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.01,0.1] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.0023639 </td>
   <td style="text-align:right;"> 0.0519986 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4194752 </td>
   <td style="text-align:right;"> 0.3279267 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.42,-0.23] </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0159210 </td>
   <td style="text-align:right;"> -0.3251028 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4217126 </td>
   <td style="text-align:right;"> 0.3286207 </td>
   <td style="text-align:right;"> 120 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.38,-0.22] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.0159967 </td>
   <td style="text-align:right;"> -0.3013194 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6002524 </td>
   <td style="text-align:right;"> 0.6953456 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.04,0.17] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0243762 </td>
   <td style="text-align:right;"> 0.1023162 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3456042 </td>
   <td style="text-align:right;"> 0.2740508 </td>
   <td style="text-align:right;"> 126 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.2,-0.1] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.0356370 </td>
   <td style="text-align:right;"> -0.1529855 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3456042 </td>
   <td style="text-align:right;"> 0.2740508 </td>
   <td style="text-align:right;"> 126 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.2,-0.1] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.0356370 </td>
   <td style="text-align:right;"> -0.1529855 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4024135 </td>
   <td style="text-align:right;"> 0.3249476 </td>
   <td style="text-align:right;"> 125 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.19,-0.08] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.0399235 </td>
   <td style="text-align:right;"> -0.1365034 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5834310 </td>
   <td style="text-align:right;"> 0.6800000 </td>
   <td style="text-align:right;"> 120 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.27,0.45] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.45 </td>
   <td style="text-align:right;"> 0.0460546 </td>
   <td style="text-align:right;"> 0.3662386 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4585406 </td>
   <td style="text-align:right;"> 0.4277151 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.05,0.18] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.0500715 </td>
   <td style="text-align:right;"> 0.1057838 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5422547 </td>
   <td style="text-align:right;"> 0.5208333 </td>
   <td style="text-align:right;"> 129 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.05,0.05] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.0536420 </td>
   <td style="text-align:right;"> 0.0023783 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4496207 </td>
   <td style="text-align:right;"> 0.4124138 </td>
   <td style="text-align:right;"> 120 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.17] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0658656 </td>
   <td style="text-align:right;"> 0.0875859 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5932126 </td>
   <td style="text-align:right;"> 0.6657264 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.17,0.32] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0888017 </td>
   <td style="text-align:right;"> 0.2468253 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7405975 </td>
   <td style="text-align:right;"> 0.8841593 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.03,0.25] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0127604 </td>
   <td style="text-align:right;"> 0.1604819 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4593246 </td>
   <td style="text-align:right;"> 0.3116571 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [-0.17,0] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0233837 </td>
   <td style="text-align:right;"> -0.0720494 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.7227011 </td>
   <td style="text-align:right;"> 0.7968966 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:left;"> [0.44,0.57] </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> 0.57 </td>
   <td style="text-align:right;"> 0.0601523 </td>
   <td style="text-align:right;"> 0.5062072 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>

![](produceDataAnalysis_files/figure-html/percentileShiftsTable tubers-1.png)<!-- -->

#### BQI Shifts

![](produceDataAnalysis_files/figure-html/unnamed-chunk-1-1.png)<!-- -->



#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](produceDataAnalysis_files/figure-html/bqi1bqi2tubers-1.png)<!-- -->



### Fruits { .tabset }

#### Individual Variables

Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.





<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3940299 </td>
   <td style="text-align:right;"> 0.1791045 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.3,0.03] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0662809 </td>
   <td style="text-align:right;"> -0.1491920 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6264550 </td>
   <td style="text-align:right;"> 0.7777778 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0.03,0.32] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0946013 </td>
   <td style="text-align:right;"> 0.1712337 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6264550 </td>
   <td style="text-align:right;"> 0.7777778 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0.03,0.32] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.32 </td>
   <td style="text-align:right;"> 0.0946013 </td>
   <td style="text-align:right;"> 0.1712337 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.8178008 </td>
   <td style="text-align:right;"> 0.9285714 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [0.15,0.23] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0028831 </td>
   <td style="text-align:right;"> 0.1598427 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.6523684 </td>
   <td style="text-align:right;"> 0.7631579 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [0.04,0.26] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0078570 </td>
   <td style="text-align:right;"> 0.1356948 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.8500000 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [0.09,0.22] </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0144109 </td>
   <td style="text-align:right;"> 0.2187292 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Herbicide </td>
   <td style="text-align:right;"> 0.6167763 </td>
   <td style="text-align:right;"> 0.6381579 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.06,0.13] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0671062 </td>
   <td style="text-align:right;"> 0.0406060 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.7723214 </td>
   <td style="text-align:right;"> 0.5714286 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [0.02,0.22] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0002818 </td>
   <td style="text-align:right;"> 0.0871190 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6026786 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.02,0.24] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.0013249 </td>
   <td style="text-align:right;"> 0.1058207 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6080357 </td>
   <td style="text-align:right;"> 0.7142857 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.01,0.17] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0735231 </td>
   <td style="text-align:right;"> 0.0706378 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.8490868 </td>
   <td style="text-align:right;"> 0.9473684 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.16,0.3] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0024710 </td>
   <td style="text-align:right;"> 0.2062754 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5997365 </td>
   <td style="text-align:right;"> 0.6250000 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.06,0.12] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.0673105 </td>
   <td style="text-align:right;"> 0.0312920 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.8875903 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.22,0.43] </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.43 </td>
   <td style="text-align:right;"> 0.0676057 </td>
   <td style="text-align:right;"> 0.2187548 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>

![](produceDataAnalysis_files/figure-html/percentileShiftsTable-1.png)<!-- -->

#### BQI Shifts

![](produceDataAnalysis_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

![](produceDataAnalysis_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](produceDataAnalysis_files/figure-html/bqi1bqi2Fruits-1.png)<!-- -->

### Vegetables { .tabset }

#### Individual Variables

Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.





<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.9201938 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000062 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5351715 </td>
   <td style="text-align:right;"> 0.6363636 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.01,0.18] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.0290050 </td>
   <td style="text-align:right;"> 0.0915873 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4310813 </td>
   <td style="text-align:right;"> 0.3431734 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.24,-0.06] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0605793 </td>
   <td style="text-align:right;"> -0.1438561 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.8946809 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000002 </td>
   <td style="text-align:right;"> -0.0000180 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.6509871 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0001014 </td>
   <td style="text-align:right;"> -0.0000398 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.6405811 </td>
   <td style="text-align:right;"> 0.5461255 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.1,0] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0013617 </td>
   <td style="text-align:right;"> -0.0000356 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.3756158 </td>
   <td style="text-align:right;"> 0.2699275 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.23,-0.05] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0655543 </td>
   <td style="text-align:right;"> -0.1411674 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.3756158 </td>
   <td style="text-align:right;"> 0.2699275 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.23,-0.05] </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.0655543 </td>
   <td style="text-align:right;"> -0.1411674 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.3988976 </td>
   <td style="text-align:right;"> 0.3690037 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.33,-0.18] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.0902653 </td>
   <td style="text-align:right;"> -0.2546206 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6900883 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 168 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000707 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6590659 </td>
   <td style="text-align:right;"> 0.5606061 </td>
   <td style="text-align:right;"> 168 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.02,0] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000085 </td>
   <td style="text-align:right;"> -0.0000060 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.9457850 </td>
   <td style="text-align:right;"> 0.9857820 </td>
   <td style="text-align:right;"> 168 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0004512 </td>
   <td style="text-align:right;"> 0.0000127 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5225593 </td>
   <td style="text-align:right;"> 0.5075758 </td>
   <td style="text-align:right;"> 168 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.04,0.06] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0389182 </td>
   <td style="text-align:right;"> 0.0111451 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.8983343 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 83 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000859 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4075005 </td>
   <td style="text-align:right;"> 0.3468635 </td>
   <td style="text-align:right;"> 83 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.19,-0.04] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.0237591 </td>
   <td style="text-align:right;"> -0.1143902 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.5827580 </td>
   <td style="text-align:right;"> 0.6538462 </td>
   <td style="text-align:right;"> 82 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.07,0.21] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0348204 </td>
   <td style="text-align:right;"> 0.1355829 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Covercrops </td>
   <td style="text-align:right;"> 0.4493370 </td>
   <td style="text-align:right;"> 0.4243542 </td>
   <td style="text-align:right;"> 83 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.27,-0.13] </td>
   <td style="text-align:right;"> -0.27 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0913458 </td>
   <td style="text-align:right;"> -0.2071699 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.9123194 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0023048 </td>
   <td style="text-align:right;"> -0.0000273 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Greenhouse </td>
   <td style="text-align:right;"> 0.5235807 </td>
   <td style="text-align:right;"> 0.8118081 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.22,0.22] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.22 </td>
   <td style="text-align:right;"> 0.0557393 </td>
   <td style="text-align:right;"> 0.0000228 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> 0.7181818 </td>
   <td style="text-align:right;"> 0.4848485 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.03,0.05] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.0006485 </td>
   <td style="text-align:right;"> 0.0061259 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.01,0.13] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0196125 </td>
   <td style="text-align:right;"> 0.0142358 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.9172848 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000790 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5513671 </td>
   <td style="text-align:right;"> 0.6093286 </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0.04,0.18] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.0640655 </td>
   <td style="text-align:right;"> 0.1062708 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.6755780 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0002804 </td>
   <td style="text-align:right;"> -0.0000040 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.6827994 </td>
   <td style="text-align:right;"> 0.5606061 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.06,0.01] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0146253 </td>
   <td style="text-align:right;"> -0.0000670 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6501404 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000394 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4916564 </td>
   <td style="text-align:right;"> 0.4538745 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.14,-0.03] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0362936 </td>
   <td style="text-align:right;"> -0.0826090 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4234396 </td>
   <td style="text-align:right;"> 0.3886256 </td>
   <td style="text-align:right;"> 161 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.16,-0.04] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.0386450 </td>
   <td style="text-align:right;"> -0.0981629 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6367622 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.04,0] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000004 </td>
   <td style="text-align:right;"> -0.0061782 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5751825 </td>
   <td style="text-align:right;"> 0.6346863 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.08,0] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0130504 </td>
   <td style="text-align:right;"> -0.0177135 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4152210 </td>
   <td style="text-align:right;"> 0.3696682 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.31,-0.19] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.0202805 </td>
   <td style="text-align:right;"> -0.2398741 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.6227385 </td>
   <td style="text-align:right;"> 0.7630332 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.13,0] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0273509 </td>
   <td style="text-align:right;"> -0.0141764 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Zn_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4548258 </td>
   <td style="text-align:right;"> 0.4132841 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.18,-0.06] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0572114 </td>
   <td style="text-align:right;"> -0.1184521 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cl_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.4927919 </td>
   <td style="text-align:right;"> 0.4686347 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.09,0.04] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0621800 </td>
   <td style="text-align:right;"> -0.0221735 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Notill </td>
   <td style="text-align:right;"> 0.5220304 </td>
   <td style="text-align:right;"> 0.4928910 </td>
   <td style="text-align:right;"> 117 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.09,0.01] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0883626 </td>
   <td style="text-align:right;"> -0.0349142 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6484843 </td>
   <td style="text-align:right;"> 0.7180182 </td>
   <td style="text-align:right;"> 102 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.01,0] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000029 </td>
   <td style="text-align:right;"> -0.0027437 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4831565 </td>
   <td style="text-align:right;"> 0.4425618 </td>
   <td style="text-align:right;"> 98 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.07,-0.01] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0940279 </td>
   <td style="text-align:right;"> -0.0397764 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.9220560 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> -0.0000296 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6044718 </td>
   <td style="text-align:right;"> 0.5166052 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.04,0] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0002070 </td>
   <td style="text-align:right;"> -0.0026666 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6127187 </td>
   <td style="text-align:right;"> 0.5461255 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.13,0] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0009269 </td>
   <td style="text-align:right;"> -0.0144895 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4044648 </td>
   <td style="text-align:right;"> 0.3127273 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.21,-0.07] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.0119066 </td>
   <td style="text-align:right;"> -0.1418430 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3659522 </td>
   <td style="text-align:right;"> 0.2741958 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.21,-0.09] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0202586 </td>
   <td style="text-align:right;"> -0.1486046 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3659522 </td>
   <td style="text-align:right;"> 0.2741958 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.21,-0.09] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0202586 </td>
   <td style="text-align:right;"> -0.1486046 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5859804 </td>
   <td style="text-align:right;"> 0.6346863 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.04,0] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0387294 </td>
   <td style="text-align:right;"> -0.0000463 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5113534 </td>
   <td style="text-align:right;"> 0.5731132 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0.14] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.0672597 </td>
   <td style="text-align:right;"> 0.0696838 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3953367 </td>
   <td style="text-align:right;"> 0.3690037 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.33,-0.2] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.0891407 </td>
   <td style="text-align:right;"> -0.2628108 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.8923987 </td>
   <td style="text-align:right;"> 0.8708487 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.11,0] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0010011 </td>
   <td style="text-align:right;"> -0.0000652 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6361116 </td>
   <td style="text-align:right;"> 0.5461255 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [-0.22,0] </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0424970 </td>
   <td style="text-align:right;"> -0.0000830 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>

![](produceDataAnalysis_files/figure-html/percentileShiftsTable vegs-1.png)<!-- -->

#### BQI Shifts

![](produceDataAnalysis_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

![](produceDataAnalysis_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](produceDataAnalysis_files/figure-html/bqi1bqi2vegetables-1.png)<!-- -->







