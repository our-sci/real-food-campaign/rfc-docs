library(tidyverse)

desertsTable <- read_csv("../dataset/desertsAndHydroTable.csv") %>%
  mutate(
    `Sample ID` = as.character(`Sample ID`)
  ) 

fullDataset <- read_rds("../dataset/fullTableParsed.Rds") %>%
  select(
    lab_id,
    sample_id,
    BQI,
    BQIMinerals,
    BQIBrix,
    BQINutrients
  ) %>%
  rename(
    `Lab ID` = lab_id,
    `Sample ID` = sample_id
  ) 

desertsTable <- left_join( x=desertsTable, y=fullDataset, by=c( "Lab ID", "Sample ID"  ) )

check <- c(
  "11-847",
  "11-1499",
  "11-783"
)

desertsTable %>% filter( `Lab ID` %in% check ) %>% select( "Crop, P", "Crop, Mg" )

write_csv( desertsTable, "./desertsTableWithBQI.csv", na=" " )
