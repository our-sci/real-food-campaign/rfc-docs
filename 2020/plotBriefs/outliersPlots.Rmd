```{r}
library(tidyverse)
dataframe <- read_rds( "../dataset/fullTableParsed.Rds" ) %>%
  filter( !is.na( sample_type ), sample_type != "Other" ) %>%
  mutate(
    sample_type = ifelse( sample_type == "bok_choy_bok_choi_pak_choy", "bok_choi", sample_type )
  )

elementSymbols <- dataframe %>%
  select( matches( "produce_xrf_" ) ) %>%
  select( - matches( "Percentile" ) ) %>%
  colnames %>% str_remove( "produce_xrf_" ) %>%
  keep( . != "Mo" )
```

# Calcium in Kale

```{r}
kaleData <- dataframe %>%
  filter( sample_type == "kale", !is.na( produce_xrf_Ca ) ) %>%
  select(
    produce_xrf_Ca,
    climateRegion,
    soilSuborder,
    variety,
    sample_type
  ) %>%
  rename(
    "Crop_Ca" = produce_xrf_Ca
  )

kaleData %>%
  ggplot() +
  aes( x = Crop_Ca, fill = "salmon" ) +
  geom_histogram() +
  guides(
    fill = "none"
  ) +
  labs(
    title = "Ca in Kale",
    x = "Ca, mg 100g-1 FW",
    y = "Amount of Samples"
  )
ggsave( "./outliersStory/kaleCaHistogram.png" )

kaleClimateNs <- kaleData %>%
  group_by( climateRegion ) %>%
  summarise( sampleSize = n() )

kaleData %>%
  ggplot() +
  aes( x = climateRegion, fill = climateRegion ) +
  geom_boxplot( aes( y = Crop_Ca ) ) +
  geom_label( data = kaleClimateNs, aes(label=sampleSize), y = 100, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "Ca in Kale",
    subtitle = "By Climate Region",
    y = "Ca, mg 100g-1 FW",
    x = "Climate Region"
  ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/kaleCaClimateRegion.png" )

kaleSoilNs <- kaleData %>%
  group_by( soilSuborder ) %>%
  summarise( sampleSize = n() )

kaleData %>%
  ggplot() +
  aes( x = soilSuborder, fill = soilSuborder ) +
  geom_boxplot( aes( y = Crop_Ca ) ) +
  geom_label( data = kaleSoilNs, aes(label=sampleSize), y = 100, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "Ca in Kale",
    subtitle = "By Soil Suborder",
    y = "Ca, mg 100g-1 FW",
    x = "Soil Suborder"
  ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/kaleCaSoilSuborder.png" )

kaleVarietyNs <- kaleData %>%
  group_by( variety ) %>%
  summarise( sampleSize = n() )

kaleData %>%
  ggplot() +
  aes( x = variety, fill = variety ) +
  geom_boxplot( aes( y = Crop_Ca ) ) +
  geom_label( data = kaleVarietyNs, aes(label=sampleSize), y = 100, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "Ca in Kale",
    subtitle = "By Variety",
    y = "Ca, mg 100g-1 FW",
    x = "Crop Variety"
  ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/kaleCaCropVariety.png", height = 13 )

```


# Potassium in Wheat

```{r}
wheatData <- dataframe %>%
  filter( sample_type == "wheat", !is.na( produce_xrf_K ) ) %>%
  select(
    produce_xrf_K,
    climateRegion,
    soilSuborder,
    variety,
    sample_type
  ) %>%
  rename(
    "Crop_K" = produce_xrf_K
  )

wheatData %>%
  ggplot() +
  aes( x = Crop_K, fill = "watermelon" ) +
  geom_histogram() +
  guides(
    fill = "none"
  ) +
  labs(
    title = "K in Wheat",
    x = "K, mg 100g-1 FW",
    y = "Amount of Samples"
      ) +
  theme_set(theme_gray(base_size = 25)) +
  ggsave( "./outliersStory/wheatKHistogram.png" )

wheatClimateNs <- wheatData %>%
  group_by( climateRegion ) %>%
  summarise( sampleSize = n() )

wheatData %>%
  ggplot() +
  aes( x = climateRegion, fill = climateRegion ) +
  geom_boxplot( aes( y = Crop_K ) ) +
  geom_label( data = wheatClimateNs, aes(label=sampleSize), y = 700, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "K in Wheat",
    subtitle = "By Climate Region",
    y = "K, mg 100g-1 FW",
    x = "Climate Region"
  ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/wheatKClimateRegion.png" )

wheatSoilNs <- wheatData %>%
  group_by( soilSuborder ) %>%
  summarise( sampleSize = n() )

wheatData %>%
  ggplot() +
  aes( x = soilSuborder, fill = soilSuborder ) +
  geom_boxplot( aes( y = Crop_K ) ) +
  geom_label( data = wheatSoilNs, aes(label=sampleSize), y = 700, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "K in Wheat",
    subtitle = "By Soil Suborder",
    y = "Ca, mg 100g-1 FW",
    x = "Soil Suborder"
      ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/wheatKSoilSuborder.png" )

wheatVarietyNs <- wheatData %>%
  group_by( variety ) %>%
  summarise( sampleSize = n() )

wheatData %>%
  ggplot() +
  aes( x = variety, fill = variety ) +
  geom_boxplot( aes( y = Crop_K ) ) +
  geom_label( data = wheatVarietyNs, aes(label=sampleSize), y = 820, size = 10 ) +
  guides(
    fill = "none"
  ) +
  labs(
    title = "K in Wheat",
    subtitle = "By Variety",
    y = "Ca, mg 100g-1 FW",
    x = "Crop Variety"
  ) +
  theme_set(theme_gray(base_size = 25)) +
  theme(
    axis.text.x = element_text(
      angle = 90,
      vjust = 0.5
      ## hjust = 0.5
      ## debug = TRUE
    ),
    )
ggsave( "./outliersStory/wheatKCropVariety.png", height = 16 )

```
