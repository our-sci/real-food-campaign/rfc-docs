## Hi! This script will check for ID sanity in a dataset by applying all the ID styles I could find via regexp and then it will break the IDs separating the coding of subsamples into a different column.
## It will work via commandline and it will print some relevant info while it works, especially the list of unmatched IDs.
## I suggest breaking ID into three fields: sampple_id, subsample_id and miscelaneous_id for border cases. When matching, this three will have a logical precedence.


## Interface Ideas: it should take fixed format tables, it should take the pointer to a separate file containing the regexs lists, making it easier to tune, it could even be JSON.

library("tidyverse")
library("jsonlite")
library("magrittr")

## Let's start by reading the JSON file where Greg merged all siware data, it required a previos SED sustitution in order to be understandable by fromJSON. I'll store it both as a rosetta stone CSV for sharing and as an RDS object for my own purposes.

siwareCombinedData <- fromJSON( "./siwareCombinedSanitized.json" )
siwareData <- as_tibble(siwareCombinedData) %>% arrange( id )
write_rds(x=siwareData, path="siwareCombined.Rmd")
## write_csv(siwareData,path="siwareCombined.csv")

## siwareData$siwareValues %<>% map( as.double )

siwareData$siwareValues %<>% map( ~ if (!is.na(.x)) { as.double(.x) } else {NaN} )
siwareData$siwareValues %<>% map( ~ tail(x=.x,n=144) )

##let's fix a typo
siwareData[ which( siwareData$id == "20022b" ), "id" ] <- "2002b"
siwareFrequencies <- siwareData %>% select( "siwareWavelength" ) %>% slice(3) %>% unlist() %>% tail( n=144 ) %>% map_dbl( as.double )

siwareNameFrequencies <- map_dbl( siwareFrequencies, round ) %>% map_chr( ~ paste0( "Siware",.x, "nm" ) ) %>% unname

siwareData2 <- siwareData
waveNumbers <- siwareNameFrequencies 
waveNumbers

siwareDataJuice <- siwareData2 %>% filter( siwareType == "ju"  )

listCell <- function( i, df ) {
  column <- map( df$siwareValues, ~ .x[i] )
  column2 <- map_chr( column, ~ifelse( is.null(.x), NaN, .x ) )
  return(column2)
}

## listCell(3,siwareDataJuice)

spreadWaves <- function( type, namesList, df ) {
  for ( i in 1:length(namesList) ) {
    df[, paste0( type, namesList[i]) ] <- listCell(i,df) %>% map_dbl( as.double )
  }
  return(df)
}

swDJuice <- spreadWaves( "Juice-", waveNumbers, siwareDataJuice )
swDJuice

siwareDataTissue <- siwareData2 %>% filter( siwareType == "ti"  )

swDTis <- spreadWaves( "Surface-", waveNumbers, siwareDataTissue )

siwareDataS4 <- siwareData2 %>% filter( siwareType == "s4"  )

swDS4 <- spreadWaves( "Soil_0_10cm", waveNumbers, siwareDataS4 )
siwareDataS8 <- siwareData2 %>% filter( siwareType == "s8"  )

swDS8 <- spreadWaves( "Soil_10_20cm", waveNumbers, siwareDataS8 )

joinJuTi <- full_join( swDJuice, swDTis, by = "id" ) %>% mutate() %>% select( -contains("siwareWavelength"), -contains("Values"), -contains("siwareType") )  %>% rename( siwareJuiceDate = siwareDate.x, siwareSurfaceDate =  siwareDate.y )

joinS10s20 <- full_join( swDS4, swDS8, by = "id" ) %>% select( -contains("siwareWavelength"), -contains("Values"), -contains("siwareType") ) %>% rename( siwareSoil10Date = siwareDate.x, siwareSoil20Date = siwareDate.y )

siwareData3 <- full_join( joinJuTi, joinS10s20 )

## siwareData3 <- bind_rows( list( swDJuice, swDTis, swDS4, swDS8 ) )


siwareData <- siwareData3

write_rds( x=siwareData,path="siwareData.Rds" )


## Both tables are now transformed into the exoteric non-compatible format of my liking: dplyr's tibbles. They are a refinement of R's traditional data frames and heavily inspired by panda's refinements over the last ones.

columnSpecification <- cols(
  Polyphenols = col_double(),
  Antioxidants = col_double(),
  Brix = col_double(),
  `ID` = col_character(),
  `Soil 0-10cm Organic Matter %` = col_double(),
  `Soil 0-10cm Total Organic Carbon %` = col_double(),
  `Soil 0-10cm pH` = col_double(),
  `Soil 0-10cm Soil Respiration` = col_double(),
  `Soil 0-10cm, Sodium (noisy)` = col_double(),
  `Soil 0-10cm, Magnesium (noisy)` = col_double(),
  `Soil 0-10cm, Aluminum` = col_double(),
  `Soil 0-10cm, Silicon %` = col_double(),
  `Soil 0-10cm, Phosphorous` = col_double(),
  `Soil 0-10cm, Sulfur` = col_double(),
  `Soil 0-10cm, Potassium` = col_double(),
  `Soil 0-10cm, Calcium` = col_double(),
  `Soil 0-10cm, Titanium` = col_double(),
  `Soil 0-10cm, Vanadium` = col_double(),
  `Soil 0-10cm, Chromium` = col_double(),
  `Soil 0-10cm, Manganese` = col_double(),
  `Soil 0-10cm, Iron` = col_double(),
  `Soil 0-10cm, Nickel` = col_double(),
  `Soil 0-10cm, Copper` = col_double(),
  `Soil 0-10cm, Zinc` = col_double(),
  `Soil 0-10cm, Arsenic` = col_double(),
  `Soil 0-10cm, Selenium` = col_double(),
  `Soil 0-10cm, Rubidium` = col_double(),
  `Soil 0-10cm, Strontium` = col_double(),
  `Soil 0-10cm, Molybdenum` = col_double(),
  `Soil 0-10cm, Lead` = col_double(),
  `Soil 10-20cm Organic Matter %` = col_double(),
  `Soil 10-20cm Total Organic Carbon %` = col_double(),
  `Soil 10-20cm pH` = col_double(),
  `Soil 10-20cm Soil Respiration` = col_double(),
  `Soil 10-20cm, Sodium (noisy)` = col_double(),
  `Soil 10-20cm, Magnesium (noisy)` = col_double(),
  `Soil 10-20cm, Aluminum` = col_double(),
  `Soil 10-20cm, Silicon %` = col_double(),
  `Soil 10-20cm, Phosphorous` = col_double(),
  `Soil 10-20cm, Sulfur` = col_double(),
  `Soil 10-20cm, Potassium` = col_double(),
  `Soil 10-20cm, Calcium` = col_double(),
  `Soil 10-20cm, Titanium` = col_double(),
  `Soil 10-20cm, Vanadium` = col_double(),
  `Soil 10-20cm, Chromium` = col_double(),
  `Soil 10-20cm, Manganese` = col_double(),
  `Soil 10-20cm, Iron` = col_double(),
  `Soil 10-20cm, Nickel` = col_double(),
  `Soil 10-20cm, Copper` = col_double(),
  `Soil 10-20cm, Zinc` = col_double(),
  `Soil 10-20cm, Arsenic` = col_double(),
  `Soil 10-20cm, Selenium` = col_double(),
  `Soil 10-20cm, Rubidium` = col_double(),
  `Soil 10-20cm, Strontium` = col_double(),
  `Soil 10-20cm, Molybdenum` = col_double(),
  `Soil 10-20cm, Lead` = col_double(),
  `Planting` = col_character()
  ## .default = "c"
)

rfcData <- read_csv("./All Data (human readable column names).csv", col_types = columnSpecification, guess_max = 1500 )
## rfcData2 <- read_csv("./All Data (human readable column names).csv", col_types = )

## We will require some work over the IDs in order to get a proper merging. Before making decisions I'll try to split both columns into IDs and sub IDs.

## Here, we can look at the sequences of IDs.

crops <- rfcData$Type %>% unique

map( crops, function(crop) {
  dataframe <- rfcData %>% filter( Type == crop ) %>% select( contains( "Crop" ) ) %>% na.omit
  filename = paste0("./mineralData", crop, ".csv" )
  write_csv( x=dataframe, path= filename )
} )

rfcData %>% filter( Type == "cherry_tomato" ) %>% select( contains( "Crop" ) ) %>% na.omit

rfcData <- rfcData %>% arrange(ID)
siwareData <- siwareData %>% arrange(id)

rfcData$ID
siwareData$id

rfcIDs <- unique( rfcData$ID ) %>% sort
siwareIDs <- unique( siwareData$id ) %>% sort

## Splitting of ID in id_main and id_subsample.
## by looking at those two ordered lists, I realize these are the cases I should consider:

## For rfc:
## 1) Four digits, a dot character and a number. This ones have subsamples. (first rule in rfcRules vector).
## 2) Four (or three?) digits and an alfanumeric character. (first rule)
##   1 and two cover everything up to 2000 index, sorted.
## 2b) Three digits, just an instance of A and three digits.
## 3) Variety name.  <- Won't make a rule for this one, I think its better handled manually. Perhaps it would be right to sanitize it to just [[:alnum:]_] and then all to lowercase.
## 4) FCH and digit or two digits, (rule 3), JTS2p (only exponent of rule 4) and KXX ( rule 5 )
## 5) LB and three digits. This ones have subsamples. (r6)
## 6) lettuce number (7)
## 7) the not and NOTs (8)
## 8) QC, one or two digits, instantiated with "-" (9)
## 9) UF and three digits, rule 10.
## 10) SCH and a digit

## Siware ones tolerat very well the first rule. Some of them begin with an uppercase A so I can still break at "points and lowercase" In 1954 (ordered) they start a kXXx pattern where x is a lowercase.

rfcRules <- c(
  "^[0-9]{1,4}\\.?[0-9]?|[a-z]?$\\B",
  "^A[0-9]{2,3}\\.?[0-9]?|[a-z]?$\\B",
  "^FCH[0-9]{1,2}\\.?[0-9]?|[a-z]?$\\B",
  "^JT",
  "^(K|k)[0-9]{1,2}\\.?[0-9]?|[a-z]?$\\B",
  "^LB[0-9]{0,3}\\.?[0-9]?|[a-z]?$\\B",
  "^lettuce\\.?[0-9]?|[a-z]?$\\B",
  "^(not)|(NOT)",
  "^QC[0-9]{1,2}\\.?[0-9]?|[a-z]?$\\B",
  "UF[0-9]{3}\\.?[0-9]?|[a-z]?$\\B",
  "^SCH[0-9]{1}\\.?[0-9]?|[a-z]?$\\B"
)
fractionIndicatorRule <- "\\.?\\-?([a-z]|[0-9])?$"

firstChunkRules <-  c(
  "^[0-9]{1,4}",
  "^A[0-9]{2,3}",
  "^FCH[0-9]{1,2}",
  "^JT",
  "^(K|k)[0-9]{1,2}",
  "^LB[0-9]{0,3}",
  "^lettuce",
  "^(not)|(NOT)",
  "^QC[0-9]{1,2}",
  "UF[0-9]{3}",
  "^SCH[0-9]{1}"
)

## as for now, the only troublesome IDs were the kindo of "857", they are the only ones with a special subsample selection rule.


namesSubSets <- c()

for ( i in 1:length(rfcRules) ) {
  namesSubSets[i] <- paste0( "rule" , i )
}

names(rfcRules) <- namesSubSets

## For each rule, we are selecting all the IDs described by it.

subSets <- c()

for ( i in 1:length(rfcRules) ) {
  subSets[i] <- list( grep(x=rfcIDs, pattern=rfcRules[i], value = T ) )
}

names(subSets) <- namesSubSets
rfcSubsets <- subSets

## Let's subtract everything we've cathegorized from the original IDs, to see what IDs don't follow any rule.

remainingIDs <- rfcIDs

for ( i in subSets ) {
  remainingIDs <- setdiff(remainingIDs,i)
}

## The set of remaining IDs should be printed. The most important sanity test is looking at this remaning border cases, there's no help to this unless we start doing some kind of rejection to badly named surveys (which I don't know if will look unpolite with collaborators given some of them are not scientists).

## We will test for disjoint tests for the rules. This isn't by any mean a fundamental result but it is an additional source of confidence, this function really understands its IDs.

comparations <- c()
for ( i in 1:length( subSets ) ) {
  for ( j in 1:length(subSets) ) {
    if ( j > i ) {
      comparations <- c( comparations, intersect( subSets[i], subSets[j] ) )
    }
  }
}
comparationResultRFC <- sum(map_dbl( comparations, length ))
print( comparationResultRFC )

print(remainingIDs)
miscelaneousRFC <- remainingIDs

## To test if everything does still exist, we can sort again and compare.
## as vectors tolerate repeated elements, first concatenating will guarantee nothing is repeated.

namesSubSets <- c()

for ( i in 1:length(rfcRules) ) {
  namesSubSets[i] <- paste0( "rule" , i )
}

names(rfcRules) <- namesSubSets

namesSubSets <- c()

for ( i in 1:length(rfcRules) ) {
  subSets[i] <- list(grep(x=siwareIDs, pattern=rfcRules[i], value = T ))
}

names(subSets) <- namesSubSets
siwareSubsets <- subSets

## Let's subtract everything we've cathegorized from the original IDs

remainingIDs <- siwareIDs

for ( i in subSets ) {
  remainingIDs <- setdiff(remainingIDs,i)
}

remainingIDs
miscelaneousSiware <- remainingIDs
print(miscelaneousSiware)

comparations <- c()
for ( i in 1:length( subSets ) ) {
  for ( j in 1:length(subSets) ) {
    if ( j > i ) {
      comparations <- c( comparations, intersect( subSets[i], subSets[j] ) )
    }
  }
}
comparationResultSiware <- sum(map_dbl( comparations, length ))

## Utterly convinced about our rules, we move to ID partitioning.

## We'll need to fix those IDs where there is no separator between the main an fraction ids, in the vein of '4544c', ohterwise, the splitting function will erase that part because we would need to feed it as the separator. For that reason, we will use a custom string breaker. Below is the function that showed me this problem(dplyr::separate).

## rfcBrokenIDs <- rfcData %>% separate( col=ID, into = c("sample_id","subsample_id"),sep="[a-z]//B|\\.[0-9]|[a-z]//B|-" )

## We will identify wich pattern does fit each ID and we will identify the chunks we want for the main and susbample ids.

grepAgainstList <- function(id) {
  tests <- map_chr( rfcRules, ~( grep(pattern = .x, x=id, value = T) )[1] )
  return( tests )
}

findRule <- function(idList) {
  map_dbl( idList, ~which( !is.na(grepAgainstList(.x)) )[1] )
}

rfcData$idSplittingRule <- findRule(rfcData$ID)

## Let's start tracking our dirt.

removeThisColumns <- c("idSplittingRule")

## The rule about "not" stuff needs a speacial behaviour. NA's do also, but they just work(TM).

## splitIDOld <- function( ruleSelected, string ) {
##   main <- sub(pattern="\\.|\\-([0-9]){1}$|\\.?\\-?([a-z]){0,}$",replacement = "",x=string)
##   subNotSane <- sub( pattern=ruleSelected, replacement = "",x=string )
##   sub <- sub( pattern = "^[\\.,\\-]",replacement = "",x=subNotSane )
##   if ( is.na(ruleSelected) || ruleSelected == "^(not)|(NOT)")  {
##     return( c(string,"") )
##   } else { return( c(main,sub) ) }
## }

## Here we finally split the IDs and get a list of pairs that immediately get splitten and added as cols.

mainChunkRegexp <- "(\\.|\\-)([0-9]){1}$|\\.?\\-?([a-z]){0,}$"

splitID <- function( ruleSelected, string ) {
  main <- sub(pattern= mainChunkRegexp ,replacement = "",x=string)
  subNotSane <- sub( pattern=ruleSelected, replacement = "",x=string )
  sub <- sub( pattern = "^[\\.,\\-]",replacement = "",x=subNotSane )
  if ( is.na(ruleSelected) || ruleSelected == "^(not)|(NOT)")  {
    return( c(string,"") )
  } else { return( c(main,sub) ) }
}

## splitID(firstChunkRules[1],"4001a")
## splitID(firstChunkRules[1],"4001-a")
## splitID(firstChunkRules[1],"4001.a")

rfcData$splittenIDs <- map2( rfcData$idSplittingRule , rfcData$ID, ~splitID( firstChunkRules[.x], .y )  )

rfcData$id_sample <- map_chr( rfcData$splittenIDs, first )
rfcData$id_subsample <- map_chr( rfcData$splittenIDs, last )

## more dirt tracked
removeThisColumns <- c( "removeThisColumns", "splittenIDs" )


## Let's look at our rows, this ones were initially problematic.
select( rfcData, one_of("ID", "id_sample","id_subsample" ) ) %>% slice(2001)
select( rfcData, one_of("ID", "id_sample","id_subsample" ) ) %>% slice(2168)
select( rfcData, one_of("ID", "id_sample","id_subsample" ) ) %>% slice(1895)

## Troublesome patterns: three numbers, K plus number

## repeat for the other dataset


siwareData$idSplittingRule <- findRule(siwareData$id)

siwareData$splittenIds <- map2( siwareData$idSplittingRule , siwareData$id, ~splitID( firstChunkRules[.x], .y )  )

siwareData$id_sample <- map_chr( siwareData$splittenIds, first )

siwareData$id_subsample <- map_chr( siwareData$splittenIds, last )

## The join is also non standard. One option is to do the full join and then work with the combinations in the duplicated subsample column for each id_sample.

joinedDataDirty <- full_join( x=rfcData, y=siwareData, by="id_sample" )

## Let's look at how we are doing.

joinedDataDirty %>% filter( id_sample == 5116 ) %>% select( one_of( "id_sample","id_subsample.x", "id_subsample.y" ) ) %>% glimpse()

## We've got more rows than rfcData... we should be good.

## Now, we remove rows where the id from the subsample differs from dataset to dataset, expurious matches.
## This could be wasting good data in the case where there are different schemes from one sampling procees to the other. To check for it, I should get the subsamples available for every set of posible copies and consider the case when the set of subsample_ids for a given fixed sample_id are different on every dataset.

## !! LAST COMMENT is important and should be implemented. By checking at row numbers, apparently 1.000 rows get lost if I don't fix this.


joinedData <- joinedDataDirty %>% filter( id_subsample.x == id_subsample.y )

## one interesting sanity check, is this redundant from the way this was made?

joinedData %>% filter( ID == id )

## This is the structure of the test:

test5116 <- filter( joinedDataDirty, id_sample == "5116" ) %>% select( one_of( "id", "ID", "id_sample", "id_subsample.x", "id_subsample.y") )
test5116x <- test5116$id_subsample.x %>% unique
test5116y <-   test5116$id_subsample.y %>% unique

print( c( test5116x, test5116y ) )

## Here, the subsets checking is done.

arePartitionsEqual <- function( id, df, verbose=FALSE ) {
  test <- df %>% filter( id_sample == !!id ) %>% select( one_of( "id", "ID", "id_sample", "id_subsample.x", "id_subsample.y") )
  subsetx <- test$id_subsample.x %>% unique
  subsety <- test$id_subsample.y %>% unique
  output <- c( setequal( subsetx, subsety ), id )
  if ( verbose ) {
    output <- list( subsetx, subsety, output )
  }
  return( output )
}

testGlobal <- map( joinedDataDirty$id_sample, ~arePartitionsEqual( .x, joinedDataDirty ) )

failedBools <- map_lgl( testGlobal, ~ first(.x) %>% as.logical )
failedIndexes <- which( failedBools == FALSE )
failedIDs <- joinedDataDirty[failedIndexes,] %>% select( "id_sample" ) %>% unique %>% unlist

arePartitionsEqual(failedIDs[2],joinedDataDirty, TRUE)

## The first two cases have showed us a pattern each. First one, the more obvious: one side is subsampled, the other isn't. The second one is of the obnoxious but joyfull type: it's just that you can't match the empty sets, also "" and NA.
## ex ID 2002

joinedDataCleaning <- joinedDataDirty
joinedDataCleaning$id_subsample.x <- map_chr( joinedDataDirty$id_subsample.x, ~ ifelse( is.na(.x), "", .x ) )
joinedDataCleaning$id_subsample.y <- map_chr( joinedDataDirty$id_subsample.y, ~ ifelse( is.na(.x), "", .x ) )

arePartitionsEqual(failedIDs[2],joinedDataDirty, TRUE)
arePartitionsEqual(failedIDs[3],joinedDataCleaning, TRUE)

## joinedPartitionsCleaning solved one question.

testGlobal2 <- map( joinedDataCleaning$id_sample, ~arePartitionsEqual( .x, joinedDataCleaning ) )

failedBools <- map_lgl( testGlobal2, ~ first(.x) %>% as.logical )
failedIndexes <- which( failedBools == FALSE )
failedIDs <- joinedDataCleaning[failedIndexes,] %>% select( "id_sample" ) %>% unique %>% unlist

arePartitionsEqual(failedIDs[1],joinedDataDirty, TRUE)

test2000 <- filter( joinedDataDirty, id_sample == "2000" ) %>% select( one_of( "id", "ID", "id_sample", "id_subsample.x", "id_subsample.y") )
test2000x <- test2000$id_subsample.x %>% unique
test2000y <-   test2000$id_subsample.y %>% unique
test2000

## This rule should capture the case when a subset is empty and the other isn't.
## We'll face the case of NAs also.

joinedDataCleaning$rowN <- 1:nrow(joinedDataCleaning)

subsampledAgainstOnlySample <- function( id, df, verbose=FALSE ) {
  test <- df %>% filter( id_sample == !!id ) %>% select( one_of( "id", "ID", "id_sample", "id_subsample.x", "id_subsample.y", "rowN") )
  ## print( test )
  subsetx <- test$id_subsample.x %>% unique %>% unlist()
  subsety <- test$id_subsample.y %>% unique %>% unlist()
  indexes <- test$rowN %>% unlist
  ## print( length(setdiff(subsetx, c("",NA)) ) )
  ## print( length(setdiff(subsety, c("",NA)) ) )
  if ( length(setdiff(subsetx, c("",NA)) ) == 0 )  {
    ## print("entra x")
    for ( i in 1:length(indexes) ) { df[ indexes[i] ,"id_subsample.x"] <- subsety[i] }
  }
  else if ( length(setdiff(subsety, c("",NA)) ) == 0 ) {
    ## print("entra y")
    for ( i in 1:length(indexes) ) { df[ indexes[i] ,"id_subsample.y"] <- subsetx[i] }
    ## df[ indexes, "id_subsample.x" ] <- sample( x= subsetx, replace = T, size = length(indexes) )
  }
  return(df)
}

## let's try it with the model case
cleaned2000 <- subsampledAgainstOnlySample( "2000", joinedDataCleaning, TRUE )
arePartitionsEqual("2000",cleaned2000, TRUE)

idVars <- c("id", "ID", "id_sample", "id_subsample.x", "id_subsample.y")

test2000 <- filter( cleaned2000, id_sample == "2000" ) %>% select( one_of(!!idVars) )
test2000x <- test2000$id_subsample.x %>% unique %>% unlist
test2000y <-   test2000$id_subsample.y %>% unique %>% unlist
test2000

joinedDataCleaning2 <- joinedDataCleaning
for ( i in failedIDs ) {
  joinedDataCleaning2 <- subsampledAgainstOnlySample( i, joinedDataCleaning2 )
}

testGlobal3 <- map( joinedDataCleaning2$id_sample, ~arePartitionsEqual( .x, joinedDataCleaning2 ) )

failedBools3 <- map_lgl( testGlobal3, ~ first(.x) %>% as.logical )
failedIndexes3 <- which( failedBools3 == FALSE )
failedIDs3 <- joinedDataCleaning2[failedIndexes3,] %>% select( "id_sample" ) %>% unique %>% unlist

lookIdSubsamples <- function(id, df = joinedDataCleaning2) {
  test <- filter( df, id_sample == !!id ) %>% select( one_of( "id", "ID", "id_sample", "id_subsample.x", "id_subsample.y") )
  testx <- test$id_subsample.x  %>% unlist
  testy <-   test$id_subsample.y %>% unlist
  ## return( list( "test" = test, testx, testy ) )
  return( test )
}

lookIdSubsamples("2000")

lookIdSubsamples("2015")
lookIdSubsamples("4007") %>% filter( id_subsample.x == id_subsample.y )

## Let's look again

## Decoupling of wavelength data
joinedData <- joinedDataCleaning2 %>% filter( id_subsample.x == id_subsample.y )



joinedData2 <- joinedData %>% mutate( id_subsample = id_subsample.x )
joinedData3 <- joinedData %>% select( -one_of( "siwareValues", "siwareWavelength", "splittenIds", "splittenIDs", "id", "ID", "id.SplittingRule", "id.SplittingRule.x", "rowN" ) )

classes <- sapply( joinedData3, class )
classes

write_csv( x=joinedData3, path="./completeDatasetHumanReadable.csv" )
write_csv( x=enframe(siwareFrequencies), path="./siwareFrequencies.csv" )

write( toJSON(joinedData3, pretty = T), "./completeDatasetHumanReadable.json" )
write( toJSON(siwareFrequencies, pretty = T), "./siwareFrequencies.json" )

write_rds( x=joinedData3, path="completeDatasetHumanReadable.Rds" )
write_rds( x=siwareFrequencies, path="siwareFrequencies.Rds" )
