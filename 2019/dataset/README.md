# Versions of the Dataset on Offer

SWIR data is available in columns with names of the format "siwareWavelength". The length value of such wavelengths is available in a separate object, called "siwareFrequencies", they are prefixed with indications alluding to wether the scan was done on the juice or the tissues of the respective produce. Surface scans over the tissues have been repeated up to three times for each sample. Down below we clarify how this is informed in every version of the dataset.

OurSci's device provides measurements between 365-940nm. Its meassures are organized as "Juice Scan [wavelength]nm". and "Surface Scan [wavelength]nm" respectively.

The data offered was obtained by merging two different datasets, one for the SWIR and one for the fixed frequencies device. The merging is obtained through the scripts "mergingScript.R" (Human Readable) and "mergingForCompleteDataset.R" (All variables). They are quite verbose as denesting the JSON ojects into prediction ready dataframes is a complex process. A good part of the script is dedicated to do an automated merging for IDs that are sometimes coded in different systems, using regexp.

## Versions Offered for Every File

Working with big, high-dimensional datasets is some times laborious. Intending to ensure minimum effort will have to be spent on dataframe refactoring by interested third parties, we offer these datasets in all the formats we believe could be of immediate use for an analyst:

* The versions with "RepCols" (as suffix) have separate columns for every tissue surface scan replica. This implies that for a given sample you will find variables as "rep1-Surface-siwareWavelengthX" where X is the wavelength value and 1 means you are considering the first iteration. Theses column families exist up to rep3, but not all the samples are replicated three times, so you will find `NA`s for some of them.

* The versions without additions on their names have one row per observation. This means you'll find several rows for each unique ID, one for every replica available. For this reason, `Surface-siwareWavelengthX` columns are now unique and lack the `repN` tag. This version was used for our first round of data analysis (spectral regressions) and you'll find a highly curated version on the respective folder ( repetitions have been tagged and outliers have been detected and flagged ).

* The most simplified version is `completeAvgDatasetHumanReadable`, where each samples replicas have been averaged in order to offer a unique dimension for every wavelength.

## Files Available for the Dataset

### siwareFrequencies

This file details each wavelenght used in the NIR spectroscopy tests. It is available in the same three formats than the dataset.

### completeDatasetHumanReadable

This version has a limited set of variables, with human readable names. It is available in JSON, CSV and Rds fomats. The three files are equal in their contents. Besides spectroscopic measures, soil and crop chemical compositions are available on a good amount of the samples.

### completeDatasetAllVariables

This file contains much more detailed information on each sample, the variable names retain their original parsing oriented format. 
Some important aditional variables are farm practices (ammendments, irrigation, agroecological practices, respiration) and calibration measures of the reflectometer.

* You can check over the merging process through the available `mergingCompleteDatasetRepColumns.R`.
