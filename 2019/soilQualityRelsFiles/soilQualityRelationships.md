---
title: "2019 Survey, Soil Quality Relationships"
author: "Real Food Campaign"
date: "12/10/2020"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: true
---





# Context and Purpose of this Brief

  This document concentrates our analysis around soil helath indicators. Our main intentios are **measuring impact of soil helath over our crop quality variables**, **assesment of the potential of this variables to add predictive power to our machine learning models** and **summarising and testing the effects of management practices over soil health indicators**.


#### Crop Nutritional Density Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Nutritional </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
  </tr>
</tbody>
</table>

### Soil Health Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Soil Health Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Soil </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Organic Carbon Percentage 10cm deep </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Organic Carbon Percentage 20cm deep </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Soil PH, 10cm deep </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Soil PH, 10cm deep </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Soil respiration, 10cm deep </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Soil respiration, 10cm deep </td>
  </tr>
</tbody>
</table>

## Methodologies

  We've realized extensive medians comparisons in the past for crop nutritional quality indicators ( *antioxidants*, *polyphenols* and *brix* ). Methods, code implementations and a detailed guide on how we are interpreting results are all detailed on a previous brief available on the same site where this one is offered, *2019 Survey, Quality Relationships Analysis*.

  The predictive potential and impact of soil health indicators over nutritional density indicators will be measured by a non parametrical measure of correlation, *Spearman's Rank Correlation Coefficient*, in line with the rank tests employed for median shifts on our *Quality Relationships* synthesis tables.








# Cuantification of Soil Quality Variables over Produce Quality Variables

  As stated, we are measuring correlation between soil and nutritional variables for each crop.
  Two linear regressions will be adjusted for each crop, explaining a nutritional variable with all six soil variables.
  
  On a first step, a **first linear model** is adjusted for all the explicative variables and all data points available. Then an **$\alpha$ trimming** is performed for the $15%$ biggest residuals and a **second model** is adjusted with this high residuals trimmmed dataset.
  
  We don't expect such a simple regression to yield a complete prediction of this variables, which have proven remarkably complex to predict in a regressive context. Good $R^2$ results will inform that this variables are good material to increase the predictive power of our current complex models, like Random Forests which already employ spectrometry, regions, varieties, etc.

## Fit Quality on Each Crop and Variable

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Samples </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> R2 </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> trimmed R2 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> grape </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8981291 </td>
   <td style="text-align:right;"> 1.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> grape </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.9457203 </td>
   <td style="text-align:right;"> 1.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> grape </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.9923725 </td>
   <td style="text-align:right;"> 1.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0.3486084 </td>
   <td style="text-align:right;"> 0.8021348 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 80 </td>
   <td style="text-align:right;"> 0.1889251 </td>
   <td style="text-align:right;"> 0.6368549 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 0.1775636 </td>
   <td style="text-align:right;"> 0.6129486 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> -0.0267500 </td>
   <td style="text-align:right;"> 0.5415194 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carrot </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 0.1995933 </td>
   <td style="text-align:right;"> 0.5201714 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.2907912 </td>
   <td style="text-align:right;"> 0.5095312 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spinach </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.0976526 </td>
   <td style="text-align:right;"> 0.3020299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carrot </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 0.2050700 </td>
   <td style="text-align:right;"> 0.2716114 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cherry_tomato </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 0.0265322 </td>
   <td style="text-align:right;"> 0.2238811 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 126 </td>
   <td style="text-align:right;"> 0.0819652 </td>
   <td style="text-align:right;"> 0.2150184 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cherry_tomato </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.0165349 </td>
   <td style="text-align:right;"> 0.1482648 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kale </td>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:right;"> 177 </td>
   <td style="text-align:right;"> 0.0665652 </td>
   <td style="text-align:right;"> 0.0987224 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lettuce </td>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 0.0478024 </td>
   <td style="text-align:right;"> 0.0839467 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cherry_tomato </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 71 </td>
   <td style="text-align:right;"> -0.0357190 </td>
   <td style="text-align:right;"> 0.0754874 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carrot </td>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> -0.0145288 </td>
   <td style="text-align:right;"> 0.0132876 </td>
  </tr>
</tbody>
</table>

### Remarks on Linear Model Fits

  It is evident that the `grape` regression is overfitted (6 variables, 14 observations), but the regressions over `lettuce` show an excellent fit for both `antioxidants` and `polyphenols` and the regressions for `spinach` show promising values for `antioxidants` and `brix`. These four models have healthy sample sizes.

  We are not cross validating this models because sample sizes are not big enough and because we won't be aplying immediately this method due to the fact that this variables are much less available than the other variables employed, therefore incorporating them would severely hinder the total amount of samples employed for model training. This explorations indicate that obtaining more soil information is worth the effort.

  This table is ordered according to the $\alpha$ trimeed $R^2$ and it can be observed that results until row 12 are encouraging. This means that, besides the two succesfull regressions mentioned, these soil variables contain a considerable amount of useful information and would probably be of value if added to complexer predictive models.
  
### Comparison Between the Succesive Models

  Outliers are a main concernt when working with these hightly volatile variables. $\alpha$ trimming, even while being a rather primitive solution, yielded dramatic increases in accuracy for some regressions, notably `polyphenols` on `spinach`, which cuadruples its $R^2$ and changes from a low performance to a high value.

### Attempt of a Non Overfitted Model for Grapes

  We'll fit `grapes` with two alternative datasets of three variables each, one for  the "10cm" depth, the other for "20cm".
  
  Due to sample scarcity, these results are just indicators about the potential interest of a further exploration of models explaining nutritional content by soil variables.


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Depth </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> R^2 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:left;"> 10cm </td>
   <td style="text-align:right;"> 0.3800516 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> antioxidants </td>
   <td style="text-align:left;"> 20cm </td>
   <td style="text-align:right;"> 0.9307938 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:left;"> 10cm </td>
   <td style="text-align:right;"> 0.1928860 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> polyphenols </td>
   <td style="text-align:left;"> 20cm </td>
   <td style="text-align:right;"> 0.3098691 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:left;"> 10cm </td>
   <td style="text-align:right;"> 0.5049285 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> brix </td>
   <td style="text-align:left;"> 20cm </td>
   <td style="text-align:right;"> -0.1053847 </td>
  </tr>
</tbody>
</table>

### Residuals Graphics for the Most Promissing Regressions

#### Antioxidants on Lettuce

![](soilQualityRelationships_files/figure-html/lettuceAnti-1.png)<!-- -->![](soilQualityRelationships_files/figure-html/lettuceAnti-2.png)<!-- -->![](soilQualityRelationships_files/figure-html/lettuceAnti-3.png)<!-- -->

#### Antioxidants on Lettuce

![](soilQualityRelationships_files/figure-html/lettucePoly-1.png)<!-- -->![](soilQualityRelationships_files/figure-html/lettucePoly-2.png)<!-- -->![](soilQualityRelationships_files/figure-html/lettucePoly-3.png)<!-- -->


#### Antioxidants on Spinach

![](soilQualityRelationships_files/figure-html/spinachAnti-1.png)<!-- -->![](soilQualityRelationships_files/figure-html/spinachAnti-2.png)<!-- -->![](soilQualityRelationships_files/figure-html/spinachAnti-3.png)<!-- -->

#### Antioxidants on Spinach

![](soilQualityRelationships_files/figure-html/spinachPoly-1.png)<!-- -->![](soilQualityRelationships_files/figure-html/spinachPoly-2.png)<!-- -->![](soilQualityRelationships_files/figure-html/spinachPoly-3.png)<!-- -->

# Quality Relationship Tables

  We offer separatedly the synthesis of this tables on the same site were this brief is available, showing the shifts as percentages of deviation over the median and the sample sizes. 
  
  This variables have shown interesting behaviours. As we are not accounting for the crop factor, populations are robust for most evaluated practices, and most of them have shown shifts that our test could detect as significant. 
  
  Tables offered below are much deeper so the recommended procedure is searching for interesting data on the synthesis tables, and then getting a deeper insight by looking at the complete summaries. 
  
  Besites the medians shift analysis, we've performed a much more informal summary for the frequency of factors on certaing percentiles. The intuitive idea behind this procedure is that if the frequency of *Practice X* over the whole dataset is $30%$ but while looking at the "10%" biggest values for the vaiable it doubles to $60%$, it is probably *concentrated* among the most succesfull crops. There are tables for the best and worst percentiles showing this behaviour. 
  
  It's main advantage is that it spares the reader looking at the enormuos amount of graphics that would have been involved in comparint all these factors for each of the three variables and that it shows patterns even while not detected as relevant by the test, could be interesting for certain investigators or worth an additional exploration.

## Percentual Shift Over the Median Value for Farm Practices

### p.values Code Key

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 5%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 5%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;">Between 5% and 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;">Between 5% and 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;">10% to 20%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;">10% to 20%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.4) !important;">20% to 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.4) !important;">20% to 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.3) !important;">50% to 90%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.3) !important;">50% to 90%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;">90% or more</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;">90% or more</span> </td>
  </tr>
</tbody>
</table>

### Tables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biodynamic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 64">-11.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 56">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 58">4.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 67">0.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 58">5.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 61">-1.87</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 228">-30.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 201">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 225">2.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 229">-22.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 207">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 242">-16.35</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Cover Crops </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 277">-16.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 261">-3.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 273">3.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 287">-5.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 267">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 290">-8.86</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Irrigation </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 204">-7.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 155">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 171">23.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 209">-11.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 188">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 200">2.34</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nonchlorine Water </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 326">-9.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 257">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 294">13.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 339">-10.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 280">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 322">-2.98</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nospray </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 186">-1.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 201">-3.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 201">14.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 191">6.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 200">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 203">8.05</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 437">2.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 374">-3.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 421">19.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 455">0.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 389">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 451">2</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 573">-6.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 511">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 549">17.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 582">-7.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 526">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 589">3.54</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Soil Amendments

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Alfalfa Meal </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 69">33.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 55">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 61">4.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 73">3.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 52">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 68">-7.17</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> All Purpose </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">-25.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">0.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">-29.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 21">-8.35</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Azomite </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">7.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">-8.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-22.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-23.12</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Blood Meal </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">5.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">5.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">14.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">3.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-8.75</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Boron </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 19">28.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 5">-15.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">0.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">15.38</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Calcium </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-41.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">60.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-3.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Compost </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">-8.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 22">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">-0.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">-19.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-5.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 26">-30.94</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Compost Tea </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-41.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">60.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-3.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Compost With Chicken Manure And Peatmoss </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-30.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">21.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-34.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-15.25</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Copper Sulfate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">43.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">5.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">33.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 23">33.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">23.43</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Donald Stoner Compost </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">-19.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">29.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-33.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">20.54</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Down To Earth </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-26.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">3.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">-1.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-27.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-6.47</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Epsom Salt </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">-3.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">-40</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">-36.25</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Feather Meal </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">8.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">3.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 35">3.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-7.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 50">-7.12</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Fish Emulsion </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-11.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">54.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 24">-24.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 20">47.68</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Fish Seaweed </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-41.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">60.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-3.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Fishbone Meal </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-28.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-13.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-37.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-21.24</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Green Sand </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">3.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">10.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">20</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">5.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">18.14</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Gypsum </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 39">-19.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 36">8.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 37">-9.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 40">-19.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 32">8.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 38">-19.23</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Holiday Brook Farm </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 30">74.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 31">6.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">14.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 31">-5.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 33">-4.37</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Kelp Meal </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">6.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">10.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">24.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">32.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">5.42</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Manganese Sulfate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">4.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">2.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">23.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nutrilive Vitality </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-41.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">60.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-59.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">-3.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nutrisprout </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 25">-7.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 29">-0.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 26">-21.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 27">3.45</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Potash </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-13.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">0.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-12.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">3.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-13.94</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Potassium Sulfate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 90">-11.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 73">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 74">-3.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 89">-10.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 77">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 88">2.53</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Pro Grow </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">21.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 9">12.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 5">-3.94</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Proboost </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-19.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 15">-4.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">-14.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-11.36</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Progro </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-28.78</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">-26.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">-22.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 6">-53.98</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Revita </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">3.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">10.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">20</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">5.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 7">18.14</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Seasalt </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-28.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">19.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-5.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">57.56</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sodium Molybdate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">7.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">5.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">27.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">17.26</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sodium Selenate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">7.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">5.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">27.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">17.26</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Solubor </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">8.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">11.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">39.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">12.98</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sulfur </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">10.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 17">17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 18">17.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 13">11.06</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Tennessee Brown Rock Phosphate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-2.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">-10.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.1) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">7.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">-7.95</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Worm Castings </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 12">15.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 10">4.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Zinc Sulfate </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">8.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 11">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">11.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 16">39.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.7) !important;" data-toggle="tooltip" data-placement="right" title="Sample size: 14">12.98</span> </td>
  </tr>
</tbody>
</table>

## Quality Replationships for PH at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.975862 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:left;"> [5.8,5.9] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 5.899933 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 5.940398 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0959527 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000192 </td>
   <td style="text-align:right;"> 51 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 5.755382 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 511 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000020 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000312 </td>
   <td style="text-align:right;"> 113 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 5.759786 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 374 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000001 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.1999633 </td>
   <td style="text-align:right;"> 93 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 5.815522 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000029 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.2000118 </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 6.135714 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.6928675 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.0000469 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 5.790115 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 261 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000008 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.1999766 </td>
   <td style="text-align:right;"> 71 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 5.829961 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 257 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0001546 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000092 </td>
   <td style="text-align:right;"> 56 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 5.802581 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0069580 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000687 </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> aea </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.5 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> -0.2999382 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 5.874545 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.6135483 </td>
   <td style="text-align:right;"> 0.0000665 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 5.933333 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.1790365 </td>
   <td style="text-align:right;"> 0.0000586 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 5.866667 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.3458404 </td>
   <td style="text-align:right;"> 0.0000401 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 5.988235 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.2442222 </td>
   <td style="text-align:right;"> 0.0000162 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 5.836364 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.4479175 </td>
   <td style="text-align:right;"> 0.0000339 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 5.814286 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.6648692 </td>
   <td style="text-align:right;"> 0.0000224 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 6.800000 </td>
   <td style="text-align:right;"> 6.8 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.8,1.3] </td>
   <td style="text-align:right;"> 0.8 </td>
   <td style="text-align:right;"> 1.3 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 1.0000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 5.752941 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5775055 </td>
   <td style="text-align:right;"> -0.0000143 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carbonotite </td>
   <td style="text-align:right;"> 5.800000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-1.2,0.3] </td>
   <td style="text-align:right;"> -1.2 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9678132 </td>
   <td style="text-align:right;"> 0.0000619 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 6.890476 </td>
   <td style="text-align:right;"> 6.7 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.8,1.1] </td>
   <td style="text-align:right;"> 0.8 </td>
   <td style="text-align:right;"> 1.1 </td>
   <td style="text-align:right;"> 0.5731729 </td>
   <td style="text-align:right;"> 0.9000746 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chilean_nitrate </td>
   <td style="text-align:right;"> 7.500000 </td>
   <td style="text-align:right;"> 7.5 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [1.5,2] </td>
   <td style="text-align:right;"> 1.5 </td>
   <td style="text-align:right;"> 2.0 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 1.7000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cobalt_carbonate </td>
   <td style="text-align:right;"> 7.500000 </td>
   <td style="text-align:right;"> 7.5 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [1.5,2] </td>
   <td style="text-align:right;"> 1.5 </td>
   <td style="text-align:right;"> 2.0 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 1.7000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 5.695455 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0547095 </td>
   <td style="text-align:right;"> -0.0000174 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 6.800000 </td>
   <td style="text-align:right;"> 6.8 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.8,1.3] </td>
   <td style="text-align:right;"> 0.8 </td>
   <td style="text-align:right;"> 1.3 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 1.0000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 5.752941 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5775055 </td>
   <td style="text-align:right;"> -0.0000143 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 5.787500 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9626657 </td>
   <td style="text-align:right;"> 0.0000516 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 6.314286 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.0732500 </td>
   <td style="text-align:right;"> 0.2999846 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.5 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9321166 </td>
   <td style="text-align:right;"> -0.2999382 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 5.933333 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.7139568 </td>
   <td style="text-align:right;"> 0.1999668 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 6.016667 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.5420832 </td>
   <td style="text-align:right;"> 0.1999675 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 5.675000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3996961 </td>
   <td style="text-align:right;"> -0.2999411 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earthfort_provide </td>
   <td style="text-align:right;"> 6.160000 </td>
   <td style="text-align:right;"> 6.2 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.2,0.5] </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.5 </td>
   <td style="text-align:right;"> 0.8280192 </td>
   <td style="text-align:right;"> 0.3999959 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 5.620000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.1930835 </td>
   <td style="text-align:right;"> -0.1999404 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 6.700000 </td>
   <td style="text-align:right;"> 6.7 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.6,1.1] </td>
   <td style="text-align:right;"> 0.6 </td>
   <td style="text-align:right;"> 1.1 </td>
   <td style="text-align:right;"> 0.9528180 </td>
   <td style="text-align:right;"> 0.9999485 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 5.800000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.8869067 </td>
   <td style="text-align:right;"> 0.0000615 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 6.050000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.0133729 </td>
   <td style="text-align:right;"> 0.1999495 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 5.752941 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9320141 </td>
   <td style="text-align:right;"> -0.0000184 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 5.752941 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5775055 </td>
   <td style="text-align:right;"> -0.0000143 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 5.866667 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.3458404 </td>
   <td style="text-align:right;"> 0.0000401 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 5.800000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9415092 </td>
   <td style="text-align:right;"> 0.0000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9322322 </td>
   <td style="text-align:right;"> 0.1999485 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 5.700000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5079411 </td>
   <td style="text-align:right;"> -0.0000396 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 6.428571 </td>
   <td style="text-align:right;"> 6.6 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [0.2,0.9] </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.9 </td>
   <td style="text-align:right;"> 0.3373054 </td>
   <td style="text-align:right;"> 0.5999991 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 6.477778 </td>
   <td style="text-align:right;"> 6.4 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:left;"> [0.5,0.7] </td>
   <td style="text-align:right;"> 0.5 </td>
   <td style="text-align:right;"> 0.7 </td>
   <td style="text-align:right;"> 0.3037173 </td>
   <td style="text-align:right;"> 0.5000257 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 5.663636 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000699 </td>
   <td style="text-align:right;"> -0.0000683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 5.585714 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2579862 </td>
   <td style="text-align:right;"> -0.2999562 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 6.200000 </td>
   <td style="text-align:right;"> 6.2 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.8,0.7] </td>
   <td style="text-align:right;"> -0.8 </td>
   <td style="text-align:right;"> 0.7 </td>
   <td style="text-align:right;"> 0.9678132 </td>
   <td style="text-align:right;"> 0.4000619 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 6.428571 </td>
   <td style="text-align:right;"> 6.6 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [0.2,0.9] </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.9 </td>
   <td style="text-align:right;"> 0.3373054 </td>
   <td style="text-align:right;"> 0.5999991 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 6.000000 </td>
   <td style="text-align:right;"> 6.0 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.2,0.5] </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.5 </td>
   <td style="text-align:right;"> 0.9239037 </td>
   <td style="text-align:right;"> 0.2000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.5 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9321166 </td>
   <td style="text-align:right;"> -0.2999382 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 5.844444 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.5487359 </td>
   <td style="text-align:right;"> 0.0000445 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 5.800000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9415092 </td>
   <td style="text-align:right;"> 0.0000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9322322 </td>
   <td style="text-align:right;"> 0.1999485 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> natural_mined_gypsum </td>
   <td style="text-align:right;"> 7.500000 </td>
   <td style="text-align:right;"> 7.5 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [1.5,2] </td>
   <td style="text-align:right;"> 1.5 </td>
   <td style="text-align:right;"> 2.0 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 1.7000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 5.612500 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0296213 </td>
   <td style="text-align:right;"> -0.2000626 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 5.752941 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5775055 </td>
   <td style="text-align:right;"> -0.0000143 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 5.677778 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0496099 </td>
   <td style="text-align:right;"> -0.0000459 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_compost </td>
   <td style="text-align:right;"> 5.700000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5079411 </td>
   <td style="text-align:right;"> -0.0000396 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_tea </td>
   <td style="text-align:right;"> 5.700000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5079411 </td>
   <td style="text-align:right;"> -0.0000396 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9322322 </td>
   <td style="text-align:right;"> 0.1999485 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 5.728571 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.6433027 </td>
   <td style="text-align:right;"> -0.0000004 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 6.105480 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.0016167 </td>
   <td style="text-align:right;"> 0.0000372 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2282170 </td>
   <td style="text-align:right;"> -0.2000380 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 5.560000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.4846931 </td>
   <td style="text-align:right;"> -0.2999427 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 5.836364 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.4479175 </td>
   <td style="text-align:right;"> 0.0000339 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 5.333333 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.8,-0.3] </td>
   <td style="text-align:right;"> -0.8 </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.3208109 </td>
   <td style="text-align:right;"> -0.4999827 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> provide </td>
   <td style="text-align:right;"> 5.850000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.5490622 </td>
   <td style="text-align:right;"> 0.0000159 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 6.200000 </td>
   <td style="text-align:right;"> 6.2 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.8,0.7] </td>
   <td style="text-align:right;"> -0.8 </td>
   <td style="text-align:right;"> 0.7 </td>
   <td style="text-align:right;"> 0.9678132 </td>
   <td style="text-align:right;"> 0.4000619 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 6.428571 </td>
   <td style="text-align:right;"> 6.6 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [0.2,0.9] </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.9 </td>
   <td style="text-align:right;"> 0.3373054 </td>
   <td style="text-align:right;"> 0.5999991 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 5.733333 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.4669773 </td>
   <td style="text-align:right;"> -0.0000371 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 6.145454 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.1109551 </td>
   <td style="text-align:right;"> 0.0000831 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 6.145454 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.1109551 </td>
   <td style="text-align:right;"> 0.0000831 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 6.145454 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.1109551 </td>
   <td style="text-align:right;"> 0.0000831 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 5.800000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.9527776 </td>
   <td style="text-align:right;"> 0.0000618 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 5.742857 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.4128315 </td>
   <td style="text-align:right;"> -0.0000192 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 5.680000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.2 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.1407214 </td>
   <td style="text-align:right;"> -0.0000694 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 5.835714 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2 </td>
   <td style="text-align:right;"> 0.4578585 </td>
   <td style="text-align:right;"> 0.0000516 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 6.140000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0,0.8] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.8 </td>
   <td style="text-align:right;"> 0.3701863 </td>
   <td style="text-align:right;"> 0.0000158 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_power </td>
   <td style="text-align:right;"> 5.700000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.3 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.5079411 </td>
   <td style="text-align:right;"> -0.0000396 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 6.145454 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.1109551 </td>
   <td style="text-align:right;"> 0.0000831 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.857713 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 586 </td>
   <td style="text-align:left;"> [5.8,5.8] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.799971 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.2258065 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0230011 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8172043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 7.8537634 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.2258065 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0394304 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 5.7267025 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 5.6098310 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 5.6098310 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 5.6098310 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9268817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.2258065 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0613363 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 3.6814516 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.2473118 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.0799562 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 3.0930918 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.8049155 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0860215 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0328587 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 2.6179211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0430108 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7849462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7849462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7849462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7849462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2150538 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.1270537 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.6926214 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.6362007 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3440860 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 1.5629380 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0860215 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0602410 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 1.4279570 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0215054 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0164294 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.3089606 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.3333333 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.2814896 </td>
   <td style="text-align:right;"> 257 </td>
   <td style="text-align:right;"> 1.1841764 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2150538 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 0.9768362 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6021505 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0.6418401 </td>
   <td style="text-align:right;"> 586 </td>
   <td style="text-align:right;"> 0.9381629 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2150538 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.2858708 </td>
   <td style="text-align:right;"> 261 </td>
   <td style="text-align:right;"> 0.7522762 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1075269 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.1697700 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 0.6333680 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.2365591 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.4096386 </td>
   <td style="text-align:right;"> 374 </td>
   <td style="text-align:right;"> 0.5774826 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.1935484 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.5596933 </td>
   <td style="text-align:right;"> 511 </td>
   <td style="text-align:right;"> 0.3458115 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0240964 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0361446 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0098576 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0197152 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109529 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0913043 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0230011 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0304348 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0304348 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0304348 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 3.9695652 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.1304348 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.0394304 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 3.3079710 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9847826 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.1086957 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.0613363 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 1.7721273 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.7012422 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0304348 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.6345269 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.5878261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0521739 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0328587 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 1.5878261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1826087 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.1270537 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.4372564 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0260870 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.4010230 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.1086957 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.0799562 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 1.3594401 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0173913 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.3231884 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3231884 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3231884 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0782609 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0602410 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 1.2991304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.2695652 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 1.2244430 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6956522 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:right;"> 0.6418401 </td>
   <td style="text-align:right;"> 586 </td>
   <td style="text-align:right;"> 1.0838403 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0826087 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0826087 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0826087 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0826087 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0173913 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0164294 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.0585507 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9923913 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9923913 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2000000 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 0.9084577 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8506211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2347826 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.2858708 </td>
   <td style="text-align:right;"> 261 </td>
   <td style="text-align:right;"> 0.8212894 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1304348 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.1697700 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 0.7683029 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.3000000 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 0.4096386 </td>
   <td style="text-align:right;"> 374 </td>
   <td style="text-align:right;"> 0.7323529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2043478 </td>
   <td style="text-align:right;"> 47 </td>
   <td style="text-align:right;"> 0.2814896 </td>
   <td style="text-align:right;"> 257 </td>
   <td style="text-align:right;"> 0.7259516 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7217391 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7217391 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.6615942 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.3608696 </td>
   <td style="text-align:right;"> 83 </td>
   <td style="text-align:right;"> 0.5596933 </td>
   <td style="text-align:right;"> 511 </td>
   <td style="text-align:right;"> 0.6447630 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.5670807 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4670077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4670077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4670077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0086957 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4670077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0098576 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.4410628 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0130435 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0361446 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.3608696 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.2835404 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.2835404 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0197152 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.2205314 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0043478 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0240964 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.1804348 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109529 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 6.6642336 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 6.6642336 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0218978 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 6.6642336 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0145985 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 6.6642336 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 5.3313869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0218978 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4.9981752 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0364964 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 4.7601668 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.4428224 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0729927 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 4.1651460 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0218978 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9985401 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0583942 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0197152 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.9618816 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0656934 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0240964 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 2.7262774 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0437956 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3520824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.2214112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2214112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2214112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2214112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2214112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.9040667 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.3065693 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.1697700 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 1.8057923 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.5680550 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.5680550 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.5680550 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0291971 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.5680550 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.4379562 </td>
   <td style="text-align:right;"> 60 </td>
   <td style="text-align:right;"> 0.2814896 </td>
   <td style="text-align:right;"> 257 </td>
   <td style="text-align:right;"> 1.5558522 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0218978 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4280501 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3138686 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 1.4256818 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3328467 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1386861 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.1270537 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.0915555 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.3065693 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.2858708 </td>
   <td style="text-align:right;"> 261 </td>
   <td style="text-align:right;"> 1.0724054 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.3868613 </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 0.4096386 </td>
   <td style="text-align:right;"> 374 </td>
   <td style="text-align:right;"> 0.9443967 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.5036496 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 0.5596933 </td>
   <td style="text-align:right;"> 511 </td>
   <td style="text-align:right;"> 0.8998672 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0145985 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0164294 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.8885645 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0145985 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0175246 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8330292 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.4817518 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 0.6418401 </td>
   <td style="text-align:right;"> 586 </td>
   <td style="text-align:right;"> 0.7505792 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0218978 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0328587 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.6664234 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0510949 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0799562 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 0.6390361 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1386861 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.2201533 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 0.6299524 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.4760167 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0394304 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.1851176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0072993 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0613363 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0.1190042 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0602410 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186199 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0153341 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0230011 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0131435 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0065717 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0361446 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0054765 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0098576 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0032859 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0043812 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0010953 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0076670 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0021906 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109529 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0120482 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>


## Quality Replationships for PH at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.744318 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:left;"> [5.65,5.75] </td>
   <td style="text-align:right;"> 5.65 </td>
   <td style="text-align:right;"> 5.75 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 5.749958 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 5.899227 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.1266536 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.0000685 </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 10.117521 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 526 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.5797791 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000881 </td>
   <td style="text-align:right;"> 112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 5.716339 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 389 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000028 </td>
   <td style="text-align:right;"> 90 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 17.413700 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.9404519 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.0000419 </td>
   <td style="text-align:right;"> 49 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 6.086207 </td>
   <td style="text-align:right;"> 6.0 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> [0.2,0.3] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.1237186 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.2999890 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 14.427491 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 267 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000472 </td>
   <td style="text-align:right;"> 68 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 5.756486 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 280 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.8635386 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000007 </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 5.714894 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 188 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6591763 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0000675 </td>
   <td style="text-align:right;"> 43 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 5.713462 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000001 </td>
   <td style="text-align:right;"> -0.0000411 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 5.660000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.5906708 </td>
   <td style="text-align:right;"> -0.0000271 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 5.766667 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.7108984 </td>
   <td style="text-align:right;"> 0.0000473 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 5.940000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0069576 </td>
   <td style="text-align:right;"> 0.2000106 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 5.872727 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0333818 </td>
   <td style="text-align:right;"> 0.1999946 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 5.766667 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.8501237 </td>
   <td style="text-align:right;"> -0.0000119 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 6.90 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.9,1.4] </td>
   <td style="text-align:right;"> 0.90 </td>
   <td style="text-align:right;"> 1.40 </td>
   <td style="text-align:right;"> 0.6856710 </td>
   <td style="text-align:right;"> 1.1000795 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 5.694118 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.4054974 </td>
   <td style="text-align:right;"> -0.0000397 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 6.705263 </td>
   <td style="text-align:right;"> 6.60 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [0.8,0.9] </td>
   <td style="text-align:right;"> 0.80 </td>
   <td style="text-align:right;"> 0.90 </td>
   <td style="text-align:right;"> 0.2197058 </td>
   <td style="text-align:right;"> 0.8999336 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 5.554167 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0293876 </td>
   <td style="text-align:right;"> -0.2999222 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 6.90 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [0.9,1.4] </td>
   <td style="text-align:right;"> 0.90 </td>
   <td style="text-align:right;"> 1.40 </td>
   <td style="text-align:right;"> 0.6856710 </td>
   <td style="text-align:right;"> 1.1000795 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 5.694118 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.4054974 </td>
   <td style="text-align:right;"> -0.0000397 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 5.812500 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.1544458 </td>
   <td style="text-align:right;"> 0.0000378 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 5.950000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.1006516 </td>
   <td style="text-align:right;"> 0.1999469 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6856710 </td>
   <td style="text-align:right;"> -0.2999205 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 5.787500 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.5054400 </td>
   <td style="text-align:right;"> 0.0000609 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 5.650000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6520609 </td>
   <td style="text-align:right;"> -0.0000424 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 5.850000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.2864256 </td>
   <td style="text-align:right;"> 0.0000846 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earthfort_provide </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.5217845 </td>
   <td style="text-align:right;"> -0.2999254 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 6.000000 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.4,0.5] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.7757850 </td>
   <td style="text-align:right;"> 0.2000806 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 5.575000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0868202 </td>
   <td style="text-align:right;"> -0.1999424 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 5.690909 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.4709269 </td>
   <td style="text-align:right;"> -0.0000247 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 5.848485 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6582526 </td>
   <td style="text-align:right;"> 0.0000336 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 5.644444 </td>
   <td style="text-align:right;"> 5.65 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.2516201 </td>
   <td style="text-align:right;"> -0.0000337 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 5.694118 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.4054974 </td>
   <td style="text-align:right;"> -0.0000397 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 5.766667 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.7108984 </td>
   <td style="text-align:right;"> 0.0000473 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6197282 </td>
   <td style="text-align:right;"> -0.2999218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 5.533333 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.3110893 </td>
   <td style="text-align:right;"> -0.0000335 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 5.740000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.9852022 </td>
   <td style="text-align:right;"> 0.0000559 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 6.160000 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.2,0.7] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.1276980 </td>
   <td style="text-align:right;"> 0.4999838 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 6.281250 </td>
   <td style="text-align:right;"> 6.40 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [0.3,0.7] </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.4127764 </td>
   <td style="text-align:right;"> 0.5000434 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 5.529032 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0249059 </td>
   <td style="text-align:right;"> -0.2999448 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 5.542857 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.2147021 </td>
   <td style="text-align:right;"> -0.2999618 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 5.933333 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [0,0.5] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.9221465 </td>
   <td style="text-align:right;"> 0.2000253 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 6.160000 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.2,0.7] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.1276980 </td>
   <td style="text-align:right;"> 0.4999838 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 5.680000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.2,0] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.5480708 </td>
   <td style="text-align:right;"> -0.0000496 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6856710 </td>
   <td style="text-align:right;"> -0.2999205 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.90 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.4199936 </td>
   <td style="text-align:right;"> 0.1999840 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6197282 </td>
   <td style="text-align:right;"> -0.2999218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 5.700000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0.3] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.7608348 </td>
   <td style="text-align:right;"> -0.0000068 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0012583 </td>
   <td style="text-align:right;"> -0.0000338 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 5.694118 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.4054974 </td>
   <td style="text-align:right;"> -0.0000397 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 5.706444 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.7231107 </td>
   <td style="text-align:right;"> 0.0000162 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_compost </td>
   <td style="text-align:right;"> 5.740000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.9852022 </td>
   <td style="text-align:right;"> 0.0000559 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_tea </td>
   <td style="text-align:right;"> 5.740000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.9852022 </td>
   <td style="text-align:right;"> 0.0000559 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 5.533333 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.3110893 </td>
   <td style="text-align:right;"> -0.0000335 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 5.871429 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0177724 </td>
   <td style="text-align:right;"> 0.1999356 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 5.993506 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.0032812 </td>
   <td style="text-align:right;"> 0.0000104 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 5.719333 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.14,0.06] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.8975695 </td>
   <td style="text-align:right;"> 0.0000446 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.2159753 </td>
   <td style="text-align:right;"> -0.0000327 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 5.872727 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0333818 </td>
   <td style="text-align:right;"> 0.1999946 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 5.866667 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.2864714 </td>
   <td style="text-align:right;"> 0.1999925 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> provide </td>
   <td style="text-align:right;"> 5.575000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.2246285 </td>
   <td style="text-align:right;"> -0.1999423 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> purple_cow_activated_compost </td>
   <td style="text-align:right;"> 6.000000 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-0.4,0.5] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.7757850 </td>
   <td style="text-align:right;"> 0.2000806 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 5.933333 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [0,0.5] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.9221465 </td>
   <td style="text-align:right;"> 0.2000253 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 6.160000 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.2,0.7] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.1276980 </td>
   <td style="text-align:right;"> 0.4999838 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 5.658333 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.3,0] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0043321 </td>
   <td style="text-align:right;"> -0.0000406 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.90 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.4199936 </td>
   <td style="text-align:right;"> 0.1999840 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.90 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.4199936 </td>
   <td style="text-align:right;"> 0.1999840 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.90 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.4199936 </td>
   <td style="text-align:right;"> 0.1999840 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.5,0] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.6856710 </td>
   <td style="text-align:right;"> -0.2999205 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 5.818182 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.1957767 </td>
   <td style="text-align:right;"> 0.0000569 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 5.830000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.1628760 </td>
   <td style="text-align:right;"> 0.0000229 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 5.818182 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0,0.2] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.1957767 </td>
   <td style="text-align:right;"> 0.0000569 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 6.071429 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.2018489 </td>
   <td style="text-align:right;"> 0.0000283 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_power </td>
   <td style="text-align:right;"> 5.740000 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.2,0.3] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.9852022 </td>
   <td style="text-align:right;"> 0.0000559 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 5.900000 </td>
   <td style="text-align:right;"> 5.90 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.4199936 </td>
   <td style="text-align:right;"> 0.1999840 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 9.833868 </td>
   <td style="text-align:right;"> 5.80 </td>
   <td style="text-align:right;"> 574 </td>
   <td style="text-align:left;"> [5.75,5.75] </td>
   <td style="text-align:right;"> 5.75 </td>
   <td style="text-align:right;"> 5.75 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.749969 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.2065217 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0209713 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 9.8478261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8478261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8478261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.2065217 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0353201 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 5.8471467 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9391304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9391304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9391304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.2173913 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0640177 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 3.3958021 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.2391304 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.0849890 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> 2.8136646 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.8136646 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.3478261 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0.2207506 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 1.5756522 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0543478 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0364238 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.4920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3369565 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.2284768 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 1.4747952 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0217391 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.3130435 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1195652 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0971302 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.2309783 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0108696 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.2309783 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0652174 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0573951 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 1.1362876 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6956522 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 0.6335541 </td>
   <td style="text-align:right;"> 574 </td>
   <td style="text-align:right;"> 1.0980155 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2282609 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.2947020 </td>
   <td style="text-align:right;"> 267 </td>
   <td style="text-align:right;"> 0.7745481 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2391304 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.3090508 </td>
   <td style="text-align:right;"> 280 </td>
   <td style="text-align:right;"> 0.7737578 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0108696 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.5471014 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.2282609 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.4293598 </td>
   <td style="text-align:right;"> 389 </td>
   <td style="text-align:right;"> 0.5316307 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.0869565 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.2075055 </td>
   <td style="text-align:right;"> 188 </td>
   <td style="text-align:right;"> 0.4190564 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.2391304 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.5805740 </td>
   <td style="text-align:right;"> 526 </td>
   <td style="text-align:right;"> 0.4118863 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0264901 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0176600 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0342163 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0231788 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0154525 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0110375 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0833333 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0209713 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 3.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0043860 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 3.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0043860 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 3.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.6491228 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.6491228 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0833333 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0353201 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 2.3593750 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.1315789 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.0640177 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 2.0553539 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9868421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9868421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9868421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9868421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9868421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0307018 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.8543860 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.1403509 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0.0849890 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> 1.6514012 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.4449761 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.4449761 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.4449761 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.4449761 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0219298 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0154525 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4191729 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1315789 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.0971302 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.3546651 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3245614 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3245614 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3245614 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0043860 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3245614 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0110375 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.1921053 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2543860 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.2207506 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 1.1523684 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0657895 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0573951 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 1.1462551 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.1353383 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.2500000 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0.2284768 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 1.0942029 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6929825 </td>
   <td style="text-align:right;"> 158 </td>
   <td style="text-align:right;"> 0.6335541 </td>
   <td style="text-align:right;"> 574 </td>
   <td style="text-align:right;"> 1.0938016 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0176600 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.9934211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.9934211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0043860 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9934211 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0350877 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0364238 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.9633174 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0175439 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.8830409 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2543860 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.2947020 </td>
   <td style="text-align:right;"> 267 </td>
   <td style="text-align:right;"> 0.8631973 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1710526 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.2075055 </td>
   <td style="text-align:right;"> 188 </td>
   <td style="text-align:right;"> 0.8243281 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2456140 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0.3090508 </td>
   <td style="text-align:right;"> 280 </td>
   <td style="text-align:right;"> 0.7947368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.3377193 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> 0.4293598 </td>
   <td style="text-align:right;"> 389 </td>
   <td style="text-align:right;"> 0.7865647 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.4122807 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.5805740 </td>
   <td style="text-align:right;"> 526 </td>
   <td style="text-align:right;"> 0.7101261 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.6622807 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.6622807 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.5298246 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0043860 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4967105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.4415205 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.4415205 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0264901 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.3311404 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0087719 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0342163 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.2563667 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0231788 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 6.6617647 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.1397059 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0264901 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 5.2738971 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0441176 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 4.9963235 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 4.4411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0514706 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 3.8860294 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0808824 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0231788 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 3.4894958 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0882353 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0364238 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 2.4224599 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.4926471 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 0.2075055 </td>
   <td style="text-align:right;"> 188 </td>
   <td style="text-align:right;"> 2.3741395 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0441176 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.2205882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2205882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2205882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2205882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.2205882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.1397059 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0849890 </td>
   <td style="text-align:right;"> 77 </td>
   <td style="text-align:right;"> 1.6438121 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3382353 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.2284768 </td>
   <td style="text-align:right;"> 207 </td>
   <td style="text-align:right;"> 1.4803922 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1323529 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0971302 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.3626337 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3323529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3323529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3323529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3323529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.3602941 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.2947020 </td>
   <td style="text-align:right;"> 267 </td>
   <td style="text-align:right;"> 1.2225711 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.2112299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.2112299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.2112299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.3602941 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.3090508 </td>
   <td style="text-align:right;"> 280 </td>
   <td style="text-align:right;"> 1.1658088 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0132450 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.1102941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.9516807 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.8882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.4852941 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 0.5805740 </td>
   <td style="text-align:right;"> 526 </td>
   <td style="text-align:right;"> 0.8358868 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0294118 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0353201 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0.8327206 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0176600 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8327206 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8327206 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 0.6335541 </td>
   <td style="text-align:right;"> 574 </td>
   <td style="text-align:right;"> 0.7891986 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0147059 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7401961 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.3161765 </td>
   <td style="text-align:right;"> 43 </td>
   <td style="text-align:right;"> 0.4293598 </td>
   <td style="text-align:right;"> 389 </td>
   <td style="text-align:right;"> 0.7363904 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1250000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.2207506 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 0.5662500 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0198675 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.3700980 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0573951 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0.1281109 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0073529 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0640177 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.1148580 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0165563 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0209713 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0342163 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0077263 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0022075 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0187638 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0154525 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0066225 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0121413 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0044150 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0011038 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0033113 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0055188 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0110375 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088300 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>


## Quality Replationships for respiration at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 28.66019 </td>
   <td style="text-align:right;"> 26.335 </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:left;"> [25.75,30.53] </td>
   <td style="text-align:right;"> 25.75 </td>
   <td style="text-align:right;"> 30.53 </td>
   <td style="text-align:right;"> 0.0000023 </td>
   <td style="text-align:right;"> 28.12008 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 27.76040 </td>
   <td style="text-align:right;"> 27.720 </td>
   <td style="text-align:right;"> 225 </td>
   <td style="text-align:left;"> [-1.92,3.12] </td>
   <td style="text-align:right;"> -1.92 </td>
   <td style="text-align:right;"> 3.12 </td>
   <td style="text-align:right;"> 0.6257743 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.6399989 </td>
   <td style="text-align:right;"> 56 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 23.04000 </td>
   <td style="text-align:right;"> 23.040 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-40.13,20.51] </td>
   <td style="text-align:right;"> -40.13 </td>
   <td style="text-align:right;"> 20.51 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -3.3327738 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 32.66064 </td>
   <td style="text-align:right;"> 31.630 </td>
   <td style="text-align:right;"> 549 </td>
   <td style="text-align:left;"> [2.59,7.18] </td>
   <td style="text-align:right;"> 2.59 </td>
   <td style="text-align:right;"> 7.18 </td>
   <td style="text-align:right;"> 0.7880410 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 4.8700755 </td>
   <td style="text-align:right;"> 126 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 33.08257 </td>
   <td style="text-align:right;"> 31.350 </td>
   <td style="text-align:right;"> 421 </td>
   <td style="text-align:left;"> [3.05,7.8] </td>
   <td style="text-align:right;"> 3.05 </td>
   <td style="text-align:right;"> 7.80 </td>
   <td style="text-align:right;"> 0.7667839 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.4599526 </td>
   <td style="text-align:right;"> 105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 31.63134 </td>
   <td style="text-align:right;"> 32.440 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:left;"> [1.26,6.7] </td>
   <td style="text-align:right;"> 1.26 </td>
   <td style="text-align:right;"> 6.70 </td>
   <td style="text-align:right;"> 0.1971081 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.9857821 </td>
   <td style="text-align:right;"> 51 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 28.28414 </td>
   <td style="text-align:right;"> 27.985 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:left;"> [-2.69,4.8] </td>
   <td style="text-align:right;"> -2.69 </td>
   <td style="text-align:right;"> 4.80 </td>
   <td style="text-align:right;"> 0.8454125 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 1.1699640 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 28.37810 </td>
   <td style="text-align:right;"> 28.900 </td>
   <td style="text-align:right;"> 273 </td>
   <td style="text-align:left;"> [-1.64,3.56] </td>
   <td style="text-align:right;"> -1.64 </td>
   <td style="text-align:right;"> 3.56 </td>
   <td style="text-align:right;"> 0.3335710 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 1.0500512 </td>
   <td style="text-align:right;"> 76 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 31.22806 </td>
   <td style="text-align:right;"> 30.840 </td>
   <td style="text-align:right;"> 294 </td>
   <td style="text-align:left;"> [1.09,6.17] </td>
   <td style="text-align:right;"> 1.09 </td>
   <td style="text-align:right;"> 6.17 </td>
   <td style="text-align:right;"> 0.5931234 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.7199848 </td>
   <td style="text-align:right;"> 65 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 34.18363 </td>
   <td style="text-align:right;"> 32.420 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> [3.74,9.63] </td>
   <td style="text-align:right;"> 3.74 </td>
   <td style="text-align:right;"> 9.63 </td>
   <td style="text-align:right;"> 0.7196349 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 6.6629333 </td>
   <td style="text-align:right;"> 51 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> aea </td>
   <td style="text-align:right;"> 15.21000 </td>
   <td style="text-align:right;"> 15.210 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-28.28,-0.26] </td>
   <td style="text-align:right;"> -28.28 </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -12.6079843 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> aea_rejuvenate </td>
   <td style="text-align:right;"> 28.08000 </td>
   <td style="text-align:right;"> 28.080 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-9.87,8.63] </td>
   <td style="text-align:right;"> -9.87 </td>
   <td style="text-align:right;"> 8.63 </td>
   <td style="text-align:right;"> 0.9458667 </td>
   <td style="text-align:right;"> -0.3241589 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 30.53639 </td>
   <td style="text-align:right;"> 29.710 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-1.26,4.04] </td>
   <td style="text-align:right;"> -1.26 </td>
   <td style="text-align:right;"> 4.04 </td>
   <td style="text-align:right;"> 0.7370390 </td>
   <td style="text-align:right;"> 1.3500201 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 28.47842 </td>
   <td style="text-align:right;"> 27.690 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [-4.19,4.6] </td>
   <td style="text-align:right;"> -4.19 </td>
   <td style="text-align:right;"> 4.60 </td>
   <td style="text-align:right;"> 0.9148673 </td>
   <td style="text-align:right;"> 0.0999835 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alpha </td>
   <td style="text-align:right;"> 31.99667 </td>
   <td style="text-align:right;"> 32.420 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-7.51,13.88] </td>
   <td style="text-align:right;"> -7.51 </td>
   <td style="text-align:right;"> 13.88 </td>
   <td style="text-align:right;"> 0.8983916 </td>
   <td style="text-align:right;"> 3.8055749 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 25.81857 </td>
   <td style="text-align:right;"> 27.430 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-9.46,4.31] </td>
   <td style="text-align:right;"> -9.46 </td>
   <td style="text-align:right;"> 4.31 </td>
   <td style="text-align:right;"> 0.6521065 </td>
   <td style="text-align:right;"> -2.3300443 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> basalt </td>
   <td style="text-align:right;"> 37.55000 </td>
   <td style="text-align:right;"> 36.410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.08,18.69] </td>
   <td style="text-align:right;"> -1.08 </td>
   <td style="text-align:right;"> 18.69 </td>
   <td style="text-align:right;"> 0.9819432 </td>
   <td style="text-align:right;"> 8.8400725 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 29.92353 </td>
   <td style="text-align:right;"> 30.950 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-2.91,5.86] </td>
   <td style="text-align:right;"> -2.91 </td>
   <td style="text-align:right;"> 5.86 </td>
   <td style="text-align:right;"> 0.6347025 </td>
   <td style="text-align:right;"> 1.5699423 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 29.72909 </td>
   <td style="text-align:right;"> 30.680 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-3.8,6.71] </td>
   <td style="text-align:right;"> -3.80 </td>
   <td style="text-align:right;"> 6.71 </td>
   <td style="text-align:right;"> 0.6973518 </td>
   <td style="text-align:right;"> 1.4500360 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 23.66600 </td>
   <td style="text-align:right;"> 24.940 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-14.45,4.71] </td>
   <td style="text-align:right;"> -14.45 </td>
   <td style="text-align:right;"> 4.71 </td>
   <td style="text-align:right;"> 0.7962505 </td>
   <td style="text-align:right;"> -4.3823017 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_10_percent </td>
   <td style="text-align:right;"> 46.09400 </td>
   <td style="text-align:right;"> 46.920 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [8.46,27.41] </td>
   <td style="text-align:right;"> 8.46 </td>
   <td style="text-align:right;"> 27.41 </td>
   <td style="text-align:right;"> 0.8986899 </td>
   <td style="text-align:right;"> 18.4000241 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 23.32875 </td>
   <td style="text-align:right;"> 20.740 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-11.87,1.3] </td>
   <td style="text-align:right;"> -11.87 </td>
   <td style="text-align:right;"> 1.30 </td>
   <td style="text-align:right;"> 0.6489755 </td>
   <td style="text-align:right;"> -5.5200317 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 45.62294 </td>
   <td style="text-align:right;"> 46.250 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [12.47,21.54] </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> 21.54 </td>
   <td style="text-align:right;"> 0.7159681 </td>
   <td style="text-align:right;"> 17.3535765 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carbonotite </td>
   <td style="text-align:right;"> 35.67000 </td>
   <td style="text-align:right;"> 34.930 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-2.1,15.95] </td>
   <td style="text-align:right;"> -2.10 </td>
   <td style="text-align:right;"> 15.95 </td>
   <td style="text-align:right;"> 0.9819633 </td>
   <td style="text-align:right;"> 7.0147830 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> casella_organics </td>
   <td style="text-align:right;"> 19.37333 </td>
   <td style="text-align:right;"> 17.320 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-17.03,-1.45] </td>
   <td style="text-align:right;"> -17.03 </td>
   <td style="text-align:right;"> -1.45 </td>
   <td style="text-align:right;"> 0.8003520 </td>
   <td style="text-align:right;"> -9.2020377 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 23.49381 </td>
   <td style="text-align:right;"> 23.300 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-8.77,-0.9] </td>
   <td style="text-align:right;"> -8.77 </td>
   <td style="text-align:right;"> -0.90 </td>
   <td style="text-align:right;"> 0.9036635 </td>
   <td style="text-align:right;"> -4.7400368 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chilean_nitrate </td>
   <td style="text-align:right;"> 32.99667 </td>
   <td style="text-align:right;"> 33.110 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.67,14.87] </td>
   <td style="text-align:right;"> -6.67 </td>
   <td style="text-align:right;"> 14.87 </td>
   <td style="text-align:right;"> 0.9742265 </td>
   <td style="text-align:right;"> 5.1627857 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cobalt_carbonate </td>
   <td style="text-align:right;"> 32.99667 </td>
   <td style="text-align:right;"> 33.110 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.67,14.87] </td>
   <td style="text-align:right;"> -6.67 </td>
   <td style="text-align:right;"> 14.87 </td>
   <td style="text-align:right;"> 0.9742265 </td>
   <td style="text-align:right;"> 5.1627857 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 28.27348 </td>
   <td style="text-align:right;"> 26.290 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-4.02,3.82] </td>
   <td style="text-align:right;"> -4.02 </td>
   <td style="text-align:right;"> 3.82 </td>
   <td style="text-align:right;"> 0.5431534 </td>
   <td style="text-align:right;"> -0.1199532 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 23.32875 </td>
   <td style="text-align:right;"> 20.740 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-11.87,1.3] </td>
   <td style="text-align:right;"> -11.87 </td>
   <td style="text-align:right;"> 1.30 </td>
   <td style="text-align:right;"> 0.6489755 </td>
   <td style="text-align:right;"> -5.5200317 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 45.62294 </td>
   <td style="text-align:right;"> 46.250 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [12.47,21.54] </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> 21.54 </td>
   <td style="text-align:right;"> 0.7159681 </td>
   <td style="text-align:right;"> 17.3535765 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 35.12500 </td>
   <td style="text-align:right;"> 38.240 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-0.25,13.71] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 13.71 </td>
   <td style="text-align:right;"> 0.3345214 </td>
   <td style="text-align:right;"> 6.2012130 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> consortium </td>
   <td style="text-align:right;"> 25.67500 </td>
   <td style="text-align:right;"> 25.675 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-17.35,10.24] </td>
   <td style="text-align:right;"> -17.35 </td>
   <td style="text-align:right;"> 10.24 </td>
   <td style="text-align:right;"> 0.9173973 </td>
   <td style="text-align:right;"> -2.6176332 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 38.46214 </td>
   <td style="text-align:right;"> 34.400 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [4.06,15.14] </td>
   <td style="text-align:right;"> 4.06 </td>
   <td style="text-align:right;"> 15.14 </td>
   <td style="text-align:right;"> 0.3761987 </td>
   <td style="text-align:right;"> 9.6100108 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> crab_shell </td>
   <td style="text-align:right;"> 27.80667 </td>
   <td style="text-align:right;"> 30.190 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-12.16,10.24] </td>
   <td style="text-align:right;"> -12.16 </td>
   <td style="text-align:right;"> 10.24 </td>
   <td style="text-align:right;"> 0.6834970 </td>
   <td style="text-align:right;"> -0.5599561 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 37.99333 </td>
   <td style="text-align:right;"> 40.580 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.28,17.63] </td>
   <td style="text-align:right;"> 1.28 </td>
   <td style="text-align:right;"> 17.63 </td>
   <td style="text-align:right;"> 0.5325693 </td>
   <td style="text-align:right;"> 9.8199623 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 37.69200 </td>
   <td style="text-align:right;"> 34.885 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [2.37,14.93] </td>
   <td style="text-align:right;"> 2.37 </td>
   <td style="text-align:right;"> 14.93 </td>
   <td style="text-align:right;"> 0.6274633 </td>
   <td style="text-align:right;"> 8.5699565 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 27.99312 </td>
   <td style="text-align:right;"> 27.560 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-5.38,4.51] </td>
   <td style="text-align:right;"> -5.38 </td>
   <td style="text-align:right;"> 4.51 </td>
   <td style="text-align:right;"> 0.9605426 </td>
   <td style="text-align:right;"> -0.4100505 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 31.91200 </td>
   <td style="text-align:right;"> 28.560 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-5.03,11.35] </td>
   <td style="text-align:right;"> -5.03 </td>
   <td style="text-align:right;"> 11.35 </td>
   <td style="text-align:right;"> 0.5120683 </td>
   <td style="text-align:right;"> 3.3875239 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earthfort_provide </td>
   <td style="text-align:right;"> 43.04400 </td>
   <td style="text-align:right;"> 44.480 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [6.98,22.56] </td>
   <td style="text-align:right;"> 6.98 </td>
   <td style="text-align:right;"> 22.56 </td>
   <td style="text-align:right;"> 0.7255787 </td>
   <td style="text-align:right;"> 14.7099651 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 48.63000 </td>
   <td style="text-align:right;"> 50.010 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [12.71,27.84] </td>
   <td style="text-align:right;"> 12.71 </td>
   <td style="text-align:right;"> 27.84 </td>
   <td style="text-align:right;"> 0.6951918 </td>
   <td style="text-align:right;"> 20.4049902 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 26.55857 </td>
   <td style="text-align:right;"> 31.070 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-9.59,6.16] </td>
   <td style="text-align:right;"> -9.59 </td>
   <td style="text-align:right;"> 6.16 </td>
   <td style="text-align:right;"> 0.2968314 </td>
   <td style="text-align:right;"> -1.1200440 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 24.63500 </td>
   <td style="text-align:right;"> 25.285 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-11.64,4.5] </td>
   <td style="text-align:right;"> -11.64 </td>
   <td style="text-align:right;"> 4.50 </td>
   <td style="text-align:right;"> 0.8391628 </td>
   <td style="text-align:right;"> -3.2972126 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 41.82455 </td>
   <td style="text-align:right;"> 35.500 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [5.37,18.48] </td>
   <td style="text-align:right;"> 5.37 </td>
   <td style="text-align:right;"> 18.48 </td>
   <td style="text-align:right;"> 0.2721970 </td>
   <td style="text-align:right;"> 11.9500096 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 29.98457 </td>
   <td style="text-align:right;"> 31.090 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:left;"> [-2.27,4.74] </td>
   <td style="text-align:right;"> -2.27 </td>
   <td style="text-align:right;"> 4.74 </td>
   <td style="text-align:right;"> 0.3578177 </td>
   <td style="text-align:right;"> 1.1300226 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 44.89222 </td>
   <td style="text-align:right;"> 44.480 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [10.7,20.62] </td>
   <td style="text-align:right;"> 10.70 </td>
   <td style="text-align:right;"> 20.62 </td>
   <td style="text-align:right;"> 0.7765740 </td>
   <td style="text-align:right;"> 15.7399926 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 45.62294 </td>
   <td style="text-align:right;"> 46.250 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [12.47,21.54] </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> 21.54 </td>
   <td style="text-align:right;"> 0.7159681 </td>
   <td style="text-align:right;"> 17.3535765 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 24.31667 </td>
   <td style="text-align:right;"> 27.430 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-11.68,3.37] </td>
   <td style="text-align:right;"> -11.68 </td>
   <td style="text-align:right;"> 3.37 </td>
   <td style="text-align:right;"> 0.4558786 </td>
   <td style="text-align:right;"> -3.8750570 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 31.03000 </td>
   <td style="text-align:right;"> 31.030 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-8.75,13.01] </td>
   <td style="text-align:right;"> -8.75 </td>
   <td style="text-align:right;"> 13.01 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 3.2246109 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 38.54500 </td>
   <td style="text-align:right;"> 38.545 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0.14,19.34] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 19.34 </td>
   <td style="text-align:right;"> 0.9373914 </td>
   <td style="text-align:right;"> 10.2600343 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 34.26143 </td>
   <td style="text-align:right;"> 31.210 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.38,12.7] </td>
   <td style="text-align:right;"> -1.38 </td>
   <td style="text-align:right;"> 12.70 </td>
   <td style="text-align:right;"> 0.4802645 </td>
   <td style="text-align:right;"> 5.7400170 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 25.92270 </td>
   <td style="text-align:right;"> 23.500 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:left;"> [-5.99,0.54] </td>
   <td style="text-align:right;"> -5.99 </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.4202979 </td>
   <td style="text-align:right;"> -2.8399687 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 31.72387 </td>
   <td style="text-align:right;"> 26.880 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:left;"> [-1.92,5.98] </td>
   <td style="text-align:right;"> -1.92 </td>
   <td style="text-align:right;"> 5.98 </td>
   <td style="text-align:right;"> 0.2476259 </td>
   <td style="text-align:right;"> 1.9415550 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 46.39714 </td>
   <td style="text-align:right;"> 47.700 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [10.14,30.64] </td>
   <td style="text-align:right;"> 10.14 </td>
   <td style="text-align:right;"> 30.64 </td>
   <td style="text-align:right;"> 0.8306471 </td>
   <td style="text-align:right;"> 21.0925712 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> humates </td>
   <td style="text-align:right;"> 37.55000 </td>
   <td style="text-align:right;"> 36.410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.08,18.69] </td>
   <td style="text-align:right;"> -1.08 </td>
   <td style="text-align:right;"> 18.69 </td>
   <td style="text-align:right;"> 0.9819432 </td>
   <td style="text-align:right;"> 8.8400725 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 35.52800 </td>
   <td style="text-align:right;"> 36.990 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.97,15.14] </td>
   <td style="text-align:right;"> -0.97 </td>
   <td style="text-align:right;"> 15.14 </td>
   <td style="text-align:right;"> 0.7131185 </td>
   <td style="text-align:right;"> 7.2800026 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 35.45727 </td>
   <td style="text-align:right;"> 31.450 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [1.22,12.51] </td>
   <td style="text-align:right;"> 1.22 </td>
   <td style="text-align:right;"> 12.51 </td>
   <td style="text-align:right;"> 0.2995836 </td>
   <td style="text-align:right;"> 6.9000441 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 28.26200 </td>
   <td style="text-align:right;"> 31.140 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-8.82,7.85] </td>
   <td style="text-align:right;"> -8.82 </td>
   <td style="text-align:right;"> 7.85 </td>
   <td style="text-align:right;"> 0.5105380 </td>
   <td style="text-align:right;"> -0.0868278 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 37.99333 </td>
   <td style="text-align:right;"> 40.580 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.28,17.63] </td>
   <td style="text-align:right;"> 1.28 </td>
   <td style="text-align:right;"> 17.63 </td>
   <td style="text-align:right;"> 0.5325693 </td>
   <td style="text-align:right;"> 9.8199623 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 28.98000 </td>
   <td style="text-align:right;"> 27.410 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [-5.76,6.72] </td>
   <td style="text-align:right;"> -5.76 </td>
   <td style="text-align:right;"> 6.72 </td>
   <td style="text-align:right;"> 0.7522665 </td>
   <td style="text-align:right;"> 0.5900224 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 31.03000 </td>
   <td style="text-align:right;"> 31.030 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-8.75,13.01] </td>
   <td style="text-align:right;"> -8.75 </td>
   <td style="text-align:right;"> 13.01 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 3.2246109 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 36.51000 </td>
   <td style="text-align:right;"> 36.510 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-6.98,21.04] </td>
   <td style="text-align:right;"> -6.98 </td>
   <td style="text-align:right;"> 21.04 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 8.6920157 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> natural_mined_gypsum </td>
   <td style="text-align:right;"> 32.99667 </td>
   <td style="text-align:right;"> 33.110 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.67,14.87] </td>
   <td style="text-align:right;"> -6.67 </td>
   <td style="text-align:right;"> 14.87 </td>
   <td style="text-align:right;"> 0.9742265 </td>
   <td style="text-align:right;"> 5.1627857 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 35.05000 </td>
   <td style="text-align:right;"> 36.000 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [1.88,11.85] </td>
   <td style="text-align:right;"> 1.88 </td>
   <td style="text-align:right;"> 11.85 </td>
   <td style="text-align:right;"> 0.6417414 </td>
   <td style="text-align:right;"> 6.7999668 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 45.62294 </td>
   <td style="text-align:right;"> 46.250 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [12.47,21.54] </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> 21.54 </td>
   <td style="text-align:right;"> 0.7159681 </td>
   <td style="text-align:right;"> 17.3535765 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 29.93793 </td>
   <td style="text-align:right;"> 30.920 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:left;"> [-5.3,5.3] </td>
   <td style="text-align:right;"> -5.30 </td>
   <td style="text-align:right;"> 5.30 </td>
   <td style="text-align:right;"> 0.3130592 </td>
   <td style="text-align:right;"> -0.0755552 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 38.54500 </td>
   <td style="text-align:right;"> 38.545 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [0.14,19.34] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 19.34 </td>
   <td style="text-align:right;"> 0.9373914 </td>
   <td style="text-align:right;"> 10.2600343 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 28.36143 </td>
   <td style="text-align:right;"> 29.200 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-4.43,4.77] </td>
   <td style="text-align:right;"> -4.43 </td>
   <td style="text-align:right;"> 4.77 </td>
   <td style="text-align:right;"> 0.6335659 </td>
   <td style="text-align:right;"> 0.0500024 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 27.75703 </td>
   <td style="text-align:right;"> 26.360 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:left;"> [-3.35,1.44] </td>
   <td style="text-align:right;"> -3.35 </td>
   <td style="text-align:right;"> 1.44 </td>
   <td style="text-align:right;"> 0.7207855 </td>
   <td style="text-align:right;"> -0.8830074 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 20.09333 </td>
   <td style="text-align:right;"> 16.640 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-18.31,-0.13] </td>
   <td style="text-align:right;"> -18.31 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.6681257 </td>
   <td style="text-align:right;"> -9.3056511 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 41.37750 </td>
   <td style="text-align:right;"> 39.280 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [2.58,20.87] </td>
   <td style="text-align:right;"> 2.58 </td>
   <td style="text-align:right;"> 20.87 </td>
   <td style="text-align:right;"> 0.9992512 </td>
   <td style="text-align:right;"> 11.5099223 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 26.93733 </td>
   <td style="text-align:right;"> 26.920 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-6,3.36] </td>
   <td style="text-align:right;"> -6.00 </td>
   <td style="text-align:right;"> 3.36 </td>
   <td style="text-align:right;"> 0.8834612 </td>
   <td style="text-align:right;"> -1.2700008 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 21.01143 </td>
   <td style="text-align:right;"> 19.290 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-14.02,-0.56] </td>
   <td style="text-align:right;"> -14.02 </td>
   <td style="text-align:right;"> -0.56 </td>
   <td style="text-align:right;"> 0.7290331 </td>
   <td style="text-align:right;"> -7.4800419 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> provide </td>
   <td style="text-align:right;"> 55.71750 </td>
   <td style="text-align:right;"> 55.620 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [17.38,36.49] </td>
   <td style="text-align:right;"> 17.38 </td>
   <td style="text-align:right;"> 36.49 </td>
   <td style="text-align:right;"> 0.9310396 </td>
   <td style="text-align:right;"> 27.3802233 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> purple_cow_activated_compost </td>
   <td style="text-align:right;"> 48.63000 </td>
   <td style="text-align:right;"> 50.010 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [12.71,27.84] </td>
   <td style="text-align:right;"> 12.71 </td>
   <td style="text-align:right;"> 27.84 </td>
   <td style="text-align:right;"> 0.6951918 </td>
   <td style="text-align:right;"> 20.4049902 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 35.41167 </td>
   <td style="text-align:right;"> 35.910 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.32,14.21] </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> 14.21 </td>
   <td style="text-align:right;"> 0.8425576 </td>
   <td style="text-align:right;"> 7.2200225 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 34.26143 </td>
   <td style="text-align:right;"> 31.210 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.38,12.7] </td>
   <td style="text-align:right;"> -1.38 </td>
   <td style="text-align:right;"> 12.70 </td>
   <td style="text-align:right;"> 0.4802645 </td>
   <td style="text-align:right;"> 5.7400170 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 33.76083 </td>
   <td style="text-align:right;"> 34.970 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [0.58,10.56] </td>
   <td style="text-align:right;"> 0.58 </td>
   <td style="text-align:right;"> 10.56 </td>
   <td style="text-align:right;"> 0.5807022 </td>
   <td style="text-align:right;"> 5.5499754 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seashield </td>
   <td style="text-align:right;"> 28.08000 </td>
   <td style="text-align:right;"> 28.080 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-9.87,8.63] </td>
   <td style="text-align:right;"> -9.87 </td>
   <td style="text-align:right;"> 8.63 </td>
   <td style="text-align:right;"> 0.9458667 </td>
   <td style="text-align:right;"> -0.3241589 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 29.98417 </td>
   <td style="text-align:right;"> 30.090 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-3.42,6.79] </td>
   <td style="text-align:right;"> -3.42 </td>
   <td style="text-align:right;"> 6.79 </td>
   <td style="text-align:right;"> 0.8419503 </td>
   <td style="text-align:right;"> 1.7059198 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 29.98417 </td>
   <td style="text-align:right;"> 30.090 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-3.42,6.79] </td>
   <td style="text-align:right;"> -3.42 </td>
   <td style="text-align:right;"> 6.79 </td>
   <td style="text-align:right;"> 0.8419503 </td>
   <td style="text-align:right;"> 1.7059198 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 31.87562 </td>
   <td style="text-align:right;"> 32.940 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-1.37,7.94] </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 7.94 </td>
   <td style="text-align:right;"> 0.5478881 </td>
   <td style="text-align:right;"> 3.4199579 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 27.28333 </td>
   <td style="text-align:right;"> 25.690 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-8.6,6.07] </td>
   <td style="text-align:right;"> -8.60 </td>
   <td style="text-align:right;"> 6.07 </td>
   <td style="text-align:right;"> 0.7786250 </td>
   <td style="text-align:right;"> -1.1500091 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 34.81412 </td>
   <td style="text-align:right;"> 34.090 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-0.13,10.02] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 10.02 </td>
   <td style="text-align:right;"> 0.7035902 </td>
   <td style="text-align:right;"> 4.8799157 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 26.88700 </td>
   <td style="text-align:right;"> 28.510 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-6.92,4.32] </td>
   <td style="text-align:right;"> -6.92 </td>
   <td style="text-align:right;"> 4.32 </td>
   <td style="text-align:right;"> 0.5624708 </td>
   <td style="text-align:right;"> -1.3668086 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 25.41000 </td>
   <td style="text-align:right;"> 24.650 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-8.07,2] </td>
   <td style="text-align:right;"> -8.07 </td>
   <td style="text-align:right;"> 2.00 </td>
   <td style="text-align:right;"> 0.9487568 </td>
   <td style="text-align:right;"> -2.9200138 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> whit_acres_mix </td>
   <td style="text-align:right;"> 25.67500 </td>
   <td style="text-align:right;"> 25.675 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-17.35,10.24] </td>
   <td style="text-align:right;"> -17.35 </td>
   <td style="text-align:right;"> 10.24 </td>
   <td style="text-align:right;"> 0.9173973 </td>
   <td style="text-align:right;"> -2.6176332 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 23.32875 </td>
   <td style="text-align:right;"> 20.740 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-11.87,1.3] </td>
   <td style="text-align:right;"> -11.87 </td>
   <td style="text-align:right;"> 1.30 </td>
   <td style="text-align:right;"> 0.6489755 </td>
   <td style="text-align:right;"> -5.5200317 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zeolite </td>
   <td style="text-align:right;"> 37.55000 </td>
   <td style="text-align:right;"> 36.410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.08,18.69] </td>
   <td style="text-align:right;"> -1.08 </td>
   <td style="text-align:right;"> 18.69 </td>
   <td style="text-align:right;"> 0.9819432 </td>
   <td style="text-align:right;"> 8.8400725 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 31.87562 </td>
   <td style="text-align:right;"> 32.940 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-1.37,7.94] </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 7.94 </td>
   <td style="text-align:right;"> 0.5478881 </td>
   <td style="text-align:right;"> 3.4199579 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 29.26395 </td>
   <td style="text-align:right;"> 27.795 </td>
   <td style="text-align:right;"> 648 </td>
   <td style="text-align:left;"> [27.76,29.62] </td>
   <td style="text-align:right;"> 27.76 </td>
   <td style="text-align:right;"> 29.62 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 28.69995 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 9.8173077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 6.5448718 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 6.5448718 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0673077 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 4.2950721 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0288462 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 4.2074176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9269231 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0480769 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0176298 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.7270299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0288462 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 2.6774476 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2.4543269 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4543269 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4543269 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4543269 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099548 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099548 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099548 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.3099548 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0288462 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 2.1037088 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.9634615 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0480769 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0303624 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 1.5834367 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0284035 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 1.3541114 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2019231 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.1527914 </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 1.3215607 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.7019231 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 0.5377081 </td>
   <td style="text-align:right;"> 549 </td>
   <td style="text-align:right;"> 1.3053979 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.2115385 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.1674829 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:right;"> 1.2630454 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.5192308 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.4123408 </td>
   <td style="text-align:right;"> 421 </td>
   <td style="text-align:right;"> 1.2592271 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.1549774 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2115385 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0.1968658 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 1.0745312 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.9817308 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0576923 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0597453 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.9656368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5961538 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6346719 </td>
   <td style="text-align:right;"> 648 </td>
   <td style="text-align:right;"> 0.9393103 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8924825 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.2879530 </td>
   <td style="text-align:right;"> 294 </td>
   <td style="text-align:right;"> 0.8014129 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.6135817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.6135817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0342801 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 0.5609890 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0288462 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0568071 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.5077918 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.0961538 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.2673849 </td>
   <td style="text-align:right;"> 273 </td>
   <td style="text-align:right;"> 0.3596083 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0724780 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.2653326 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0096154 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0362390 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0.2653326 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2203722 </td>
   <td style="text-align:right;"> 225 </td>
   <td style="text-align:right;"> 0.1745299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009794 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0186092 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0205681 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0225269 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088149 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0146915 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9727626 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9727626 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3106355 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3106355 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0544747 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.2716869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0544747 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.2716869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0544747 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.2716869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0544747 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.2716869 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1782101 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0505837 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0176298 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.8692175 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.8376876 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.6485084 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.6485084 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0350195 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 2.2346790 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 1.9863813 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9863813 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9863813 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.8058012 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0194553 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4188438 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0389105 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0284035 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 1.3699182 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.3242542 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6614786 </td>
   <td style="text-align:right;"> 170 </td>
   <td style="text-align:right;"> 0.5377081 </td>
   <td style="text-align:right;"> 549 </td>
   <td style="text-align:right;"> 1.2301815 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0116732 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.1918288 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1789883 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.1527914 </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 1.1714557 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.4785992 </td>
   <td style="text-align:right;"> 123 </td>
   <td style="text-align:right;"> 0.4123408 </td>
   <td style="text-align:right;"> 421 </td>
   <td style="text-align:right;"> 1.1606884 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0350195 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0303624 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 1.1533827 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.1350750 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.1350750 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2217899 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0.1968658 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 1.1266043 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0116732 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0834807 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1789883 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.1674829 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:right;"> 1.0686964 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9931907 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9931907 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9931907 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6031128 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 0.6346719 </td>
   <td style="text-align:right;"> 648 </td>
   <td style="text-align:right;"> 0.9502750 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0155642 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.9347677 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2568093 </td>
   <td style="text-align:right;"> 66 </td>
   <td style="text-align:right;"> 0.2879530 </td>
   <td style="text-align:right;"> 294 </td>
   <td style="text-align:right;"> 0.8918447 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0272374 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0342801 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 0.7945525 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7945525 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7945525 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7945525 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0466926 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0597453 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.7815271 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.1789883 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.2673849 </td>
   <td style="text-align:right;"> 273 </td>
   <td style="text-align:right;"> 0.6694032 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.6621271 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.6621271 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.1167315 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.2203722 </td>
   <td style="text-align:right;"> 225 </td>
   <td style="text-align:right;"> 0.5297017 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.4965953 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4965953 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4965953 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4965953 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0272374 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0568071 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.4794714 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4673838 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0077821 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0186092 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.4181855 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0272374 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0724780 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.3758019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0116732 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0362390 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0.3221159 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.2482977 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.2482977 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0038911 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0225269 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.1727288 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009794 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0205681 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088149 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0146915 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0129870 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 6.6298701 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0259740 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.4199134 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0194805 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3149351 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0844156 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0284035 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 2.9720107 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2987013 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.1527914 </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 1.9549617 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.3259740 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.8051948 </td>
   <td style="text-align:right;"> 124 </td>
   <td style="text-align:right;"> 0.6346719 </td>
   <td style="text-align:right;"> 648 </td>
   <td style="text-align:right;"> 1.2686789 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.1049784 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2727273 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.2673849 </td>
   <td style="text-align:right;"> 273 </td>
   <td style="text-align:right;"> 1.0199800 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.2207792 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 0.2203722 </td>
   <td style="text-align:right;"> 225 </td>
   <td style="text-align:right;"> 1.0018470 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0129870 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.9471243 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.9471243 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.9471243 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1818182 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.1968658 </td>
   <td style="text-align:right;"> 201 </td>
   <td style="text-align:right;"> 0.9235640 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0129870 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8287338 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8287338 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8287338 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8287338 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.3896104 </td>
   <td style="text-align:right;"> 60 </td>
   <td style="text-align:right;"> 0.5377081 </td>
   <td style="text-align:right;"> 549 </td>
   <td style="text-align:right;"> 0.7245760 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0259740 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0362390 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0.7167427 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.6629870 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.1883117 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 0.2879530 </td>
   <td style="text-align:right;"> 294 </td>
   <td style="text-align:right;"> 0.6539668 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0129870 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0205681 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.6314162 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0194805 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0342801 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 0.5682746 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.2077922 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0.4123408 </td>
   <td style="text-align:right;"> 421 </td>
   <td style="text-align:right;"> 0.5039331 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0259740 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0568071 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.4572324 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0324675 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0724780 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.4479642 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.4143669 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.3899924 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.0584416 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.1674829 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:right;"> 0.3489405 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0186092 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.3489405 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0064935 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0597453 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.1086864 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009794 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0225269 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0097943 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0176298 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0303624 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0107738 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048972 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0088149 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029383 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0137120 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0078355 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0146915 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0068560 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0117532 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0058766 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0166503 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019589 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039177 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0156709 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Soil Respiration at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 24.49621 </td>
   <td style="text-align:right;"> 22.180 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:left;"> [21.31,25.83] </td>
   <td style="text-align:right;"> 21.31 </td>
   <td style="text-align:right;"> 25.83 </td>
   <td style="text-align:right;"> 0.0000242 </td>
   <td style="text-align:right;"> 23.47038 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 19.86996 </td>
   <td style="text-align:right;"> 17.955 </td>
   <td style="text-align:right;"> 242 </td>
   <td style="text-align:left;"> [-6.14,-1.5] </td>
   <td style="text-align:right;"> -6.14 </td>
   <td style="text-align:right;"> -1.50 </td>
   <td style="text-align:right;"> 0.7570479 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -3.8363491 </td>
   <td style="text-align:right;"> 59 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 24.28000 </td>
   <td style="text-align:right;"> 24.280 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-12.47,15.07] </td>
   <td style="text-align:right;"> -12.47 </td>
   <td style="text-align:right;"> 15.07 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 2.0975850 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 24.59467 </td>
   <td style="text-align:right;"> 22.400 </td>
   <td style="text-align:right;"> 589 </td>
   <td style="text-align:left;"> [-1.29,2.98] </td>
   <td style="text-align:right;"> -1.29 </td>
   <td style="text-align:right;"> 2.98 </td>
   <td style="text-align:right;"> 0.6179390 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.8300523 </td>
   <td style="text-align:right;"> 127 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 24.27678 </td>
   <td style="text-align:right;"> 21.950 </td>
   <td style="text-align:right;"> 451 </td>
   <td style="text-align:left;"> [-1.82,2.66] </td>
   <td style="text-align:right;"> -1.82 </td>
   <td style="text-align:right;"> 2.66 </td>
   <td style="text-align:right;"> 0.5888693 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.4699676 </td>
   <td style="text-align:right;"> 106 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 25.77429 </td>
   <td style="text-align:right;"> 23.770 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:left;"> [-0.66,4.37] </td>
   <td style="text-align:right;"> -0.66 </td>
   <td style="text-align:right;"> 4.37 </td>
   <td style="text-align:right;"> 0.8652200 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 1.8900180 </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 23.03852 </td>
   <td style="text-align:right;"> 21.930 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:left;"> [-4.22,2.8] </td>
   <td style="text-align:right;"> -4.22 </td>
   <td style="text-align:right;"> 2.80 </td>
   <td style="text-align:right;"> 0.8919646 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.4400333 </td>
   <td style="text-align:right;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 21.61352 </td>
   <td style="text-align:right;"> 19.760 </td>
   <td style="text-align:right;"> 290 </td>
   <td style="text-align:left;"> [-4.54,0.18] </td>
   <td style="text-align:right;"> -4.54 </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 0.8102251 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -2.0800088 </td>
   <td style="text-align:right;"> 73 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 23.02727 </td>
   <td style="text-align:right;"> 21.610 </td>
   <td style="text-align:right;"> 322 </td>
   <td style="text-align:left;"> [-2.95,1.6] </td>
   <td style="text-align:right;"> -2.95 </td>
   <td style="text-align:right;"> 1.60 </td>
   <td style="text-align:right;"> 0.9403311 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -0.6999473 </td>
   <td style="text-align:right;"> 65 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 24.49880 </td>
   <td style="text-align:right;"> 22.375 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:left;"> [-1.99,2.99] </td>
   <td style="text-align:right;"> -1.99 </td>
   <td style="text-align:right;"> 2.99 </td>
   <td style="text-align:right;"> 0.8156657 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.5499519 </td>
   <td style="text-align:right;"> 50 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> aea_rejuvenate </td>
   <td style="text-align:right;"> 8.670000 </td>
   <td style="text-align:right;"> 8.670 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-21.72,-6.61] </td>
   <td style="text-align:right;"> -21.72 </td>
   <td style="text-align:right;"> -6.61 </td>
   <td style="text-align:right;"> 0.9192255 </td>
   <td style="text-align:right;"> -13.0199834 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 20.843088 </td>
   <td style="text-align:right;"> 21.610 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:left;"> [-4.07,0.66] </td>
   <td style="text-align:right;"> -4.07 </td>
   <td style="text-align:right;"> 0.66 </td>
   <td style="text-align:right;"> 0.1886917 </td>
   <td style="text-align:right;"> -1.6399554 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 20.770000 </td>
   <td style="text-align:right;"> 21.020 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-5.92,1.65] </td>
   <td style="text-align:right;"> -5.92 </td>
   <td style="text-align:right;"> 1.65 </td>
   <td style="text-align:right;"> 0.5044492 </td>
   <td style="text-align:right;"> -1.9099954 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alpha </td>
   <td style="text-align:right;"> 31.470000 </td>
   <td style="text-align:right;"> 31.470 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> [-24.24,26.11] </td>
   <td style="text-align:right;"> -24.24 </td>
   <td style="text-align:right;"> 26.11 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 10.0332133 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 16.397000 </td>
   <td style="text-align:right;"> 16.030 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-10.25,-1.38] </td>
   <td style="text-align:right;"> -10.25 </td>
   <td style="text-align:right;"> -1.38 </td>
   <td style="text-align:right;"> 0.9585901 </td>
   <td style="text-align:right;"> -5.2899413 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> basalt </td>
   <td style="text-align:right;"> 21.806667 </td>
   <td style="text-align:right;"> 16.480 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-12.71,9.65] </td>
   <td style="text-align:right;"> -12.71 </td>
   <td style="text-align:right;"> 9.65 </td>
   <td style="text-align:right;"> 0.5642315 </td>
   <td style="text-align:right;"> -1.9317218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 21.197647 </td>
   <td style="text-align:right;"> 19.810 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-7.11,2.41] </td>
   <td style="text-align:right;"> -7.11 </td>
   <td style="text-align:right;"> 2.41 </td>
   <td style="text-align:right;"> 0.8946154 </td>
   <td style="text-align:right;"> -2.0030313 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 22.149091 </td>
   <td style="text-align:right;"> 22.440 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-4.62,4.33] </td>
   <td style="text-align:right;"> -4.62 </td>
   <td style="text-align:right;"> 4.33 </td>
   <td style="text-align:right;"> 0.7425801 </td>
   <td style="text-align:right;"> 0.1099606 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 26.032222 </td>
   <td style="text-align:right;"> 30.820 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-1.43,8.39] </td>
   <td style="text-align:right;"> -1.43 </td>
   <td style="text-align:right;"> 8.39 </td>
   <td style="text-align:right;"> 0.0538985 </td>
   <td style="text-align:right;"> 3.5199594 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_10_percent </td>
   <td style="text-align:right;"> 31.821667 </td>
   <td style="text-align:right;"> 32.160 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.4,17.81] </td>
   <td style="text-align:right;"> -0.40 </td>
   <td style="text-align:right;"> 17.81 </td>
   <td style="text-align:right;"> 0.6640684 </td>
   <td style="text-align:right;"> 8.5599641 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 10.094000 </td>
   <td style="text-align:right;"> 8.630 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-19.22,-6.16] </td>
   <td style="text-align:right;"> -19.22 </td>
   <td style="text-align:right;"> -6.16 </td>
   <td style="text-align:right;"> 0.7360008 </td>
   <td style="text-align:right;"> -11.5100050 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 21.270000 </td>
   <td style="text-align:right;"> 22.460 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-4.49,2.62] </td>
   <td style="text-align:right;"> -4.49 </td>
   <td style="text-align:right;"> 2.62 </td>
   <td style="text-align:right;"> 0.3867221 </td>
   <td style="text-align:right;"> -0.7608750 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carbonotite </td>
   <td style="text-align:right;"> 21.806667 </td>
   <td style="text-align:right;"> 16.480 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-12.71,9.65] </td>
   <td style="text-align:right;"> -12.71 </td>
   <td style="text-align:right;"> 9.65 </td>
   <td style="text-align:right;"> 0.5642315 </td>
   <td style="text-align:right;"> -1.9317218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> casella_organics </td>
   <td style="text-align:right;"> 9.861667 </td>
   <td style="text-align:right;"> 10.780 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-18.93,-6.43] </td>
   <td style="text-align:right;"> -18.93 </td>
   <td style="text-align:right;"> -6.43 </td>
   <td style="text-align:right;"> 0.7651529 </td>
   <td style="text-align:right;"> -11.8900566 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 20.629524 </td>
   <td style="text-align:right;"> 21.930 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-5.15,2.14] </td>
   <td style="text-align:right;"> -5.15 </td>
   <td style="text-align:right;"> 2.14 </td>
   <td style="text-align:right;"> 0.4231951 </td>
   <td style="text-align:right;"> -1.2000046 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chilean_nitrate </td>
   <td style="text-align:right;"> 26.440000 </td>
   <td style="text-align:right;"> 23.730 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.28,12.35] </td>
   <td style="text-align:right;"> -6.28 </td>
   <td style="text-align:right;"> 12.35 </td>
   <td style="text-align:right;"> 0.6628792 </td>
   <td style="text-align:right;"> 4.3399728 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cobalt_carbonate </td>
   <td style="text-align:right;"> 26.440000 </td>
   <td style="text-align:right;"> 23.730 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.28,12.35] </td>
   <td style="text-align:right;"> -6.28 </td>
   <td style="text-align:right;"> 12.35 </td>
   <td style="text-align:right;"> 0.6628792 </td>
   <td style="text-align:right;"> 4.3399728 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 15.741923 </td>
   <td style="text-align:right;"> 13.730 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:left;"> [-10.27,-4] </td>
   <td style="text-align:right;"> -10.27 </td>
   <td style="text-align:right;"> -4.00 </td>
   <td style="text-align:right;"> 0.7295271 </td>
   <td style="text-align:right;"> -7.0800154 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 10.094000 </td>
   <td style="text-align:right;"> 8.630 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-19.22,-6.16] </td>
   <td style="text-align:right;"> -19.22 </td>
   <td style="text-align:right;"> -6.16 </td>
   <td style="text-align:right;"> 0.7360008 </td>
   <td style="text-align:right;"> -11.5100050 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 21.270000 </td>
   <td style="text-align:right;"> 22.460 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-4.49,2.62] </td>
   <td style="text-align:right;"> -4.49 </td>
   <td style="text-align:right;"> 2.62 </td>
   <td style="text-align:right;"> 0.3867221 </td>
   <td style="text-align:right;"> -0.7608750 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 20.251250 </td>
   <td style="text-align:right;"> 16.760 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-7.43,0.89] </td>
   <td style="text-align:right;"> -7.43 </td>
   <td style="text-align:right;"> 0.89 </td>
   <td style="text-align:right;"> 0.6253276 </td>
   <td style="text-align:right;"> -3.4899632 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 28.843333 </td>
   <td style="text-align:right;"> 27.740 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [0.48,10.65] </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 10.65 </td>
   <td style="text-align:right;"> 0.7706889 </td>
   <td style="text-align:right;"> 5.3600074 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> crab_shell </td>
   <td style="text-align:right;"> 24.113333 </td>
   <td style="text-align:right;"> 24.280 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-4.36,7.26] </td>
   <td style="text-align:right;"> -4.36 </td>
   <td style="text-align:right;"> 7.26 </td>
   <td style="text-align:right;"> 0.9973615 </td>
   <td style="text-align:right;"> 2.8300035 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 15.716667 </td>
   <td style="text-align:right;"> 12.130 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-14.41,-0.42] </td>
   <td style="text-align:right;"> -14.41 </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> 0.6347244 </td>
   <td style="text-align:right;"> -7.5899916 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 26.845000 </td>
   <td style="text-align:right;"> 26.845 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-0.45,9.37] </td>
   <td style="text-align:right;"> -0.45 </td>
   <td style="text-align:right;"> 9.37 </td>
   <td style="text-align:right;"> 0.7886017 </td>
   <td style="text-align:right;"> 4.7000228 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 21.236111 </td>
   <td style="text-align:right;"> 21.070 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-5.84,2.29] </td>
   <td style="text-align:right;"> -5.84 </td>
   <td style="text-align:right;"> 2.29 </td>
   <td style="text-align:right;"> 0.6143842 </td>
   <td style="text-align:right;"> -1.4800082 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 33.997500 </td>
   <td style="text-align:right;"> 42.830 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-3.45,24.77] </td>
   <td style="text-align:right;"> -3.45 </td>
   <td style="text-align:right;"> 24.77 </td>
   <td style="text-align:right;"> 0.3890711 </td>
   <td style="text-align:right;"> 15.6200497 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earthfort_provide </td>
   <td style="text-align:right;"> 39.432000 </td>
   <td style="text-align:right;"> 42.040 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [9.1,24.64] </td>
   <td style="text-align:right;"> 9.10 </td>
   <td style="text-align:right;"> 24.64 </td>
   <td style="text-align:right;"> 0.5384922 </td>
   <td style="text-align:right;"> 17.8399936 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 29.625000 </td>
   <td style="text-align:right;"> 28.700 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0.74,13.25] </td>
   <td style="text-align:right;"> 0.74 </td>
   <td style="text-align:right;"> 13.25 </td>
   <td style="text-align:right;"> 0.9542909 </td>
   <td style="text-align:right;"> 7.4370217 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 13.803846 </td>
   <td style="text-align:right;"> 12.620 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> [-12.52,-4.33] </td>
   <td style="text-align:right;"> -12.52 </td>
   <td style="text-align:right;"> -4.33 </td>
   <td style="text-align:right;"> 0.8446963 </td>
   <td style="text-align:right;"> -8.2938158 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 15.918333 </td>
   <td style="text-align:right;"> 21.100 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-14.68,1.48] </td>
   <td style="text-align:right;"> -14.68 </td>
   <td style="text-align:right;"> 1.48 </td>
   <td style="text-align:right;"> 0.2174025 </td>
   <td style="text-align:right;"> -6.7225741 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 24.700909 </td>
   <td style="text-align:right;"> 23.360 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-2.11,6.5] </td>
   <td style="text-align:right;"> -2.11 </td>
   <td style="text-align:right;"> 6.50 </td>
   <td style="text-align:right;"> 0.7684689 </td>
   <td style="text-align:right;"> 2.6099374 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 22.081200 </td>
   <td style="text-align:right;"> 21.060 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-4.6,1.5] </td>
   <td style="text-align:right;"> -4.60 </td>
   <td style="text-align:right;"> 1.50 </td>
   <td style="text-align:right;"> 0.5321402 </td>
   <td style="text-align:right;"> -1.6300053 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 33.869500 </td>
   <td style="text-align:right;"> 33.900 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> [6.71,15.01] </td>
   <td style="text-align:right;"> 6.71 </td>
   <td style="text-align:right;"> 15.01 </td>
   <td style="text-align:right;"> 0.5177106 </td>
   <td style="text-align:right;"> 10.9100451 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 21.270000 </td>
   <td style="text-align:right;"> 22.460 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-4.49,2.62] </td>
   <td style="text-align:right;"> -4.49 </td>
   <td style="text-align:right;"> 2.62 </td>
   <td style="text-align:right;"> 0.3867221 </td>
   <td style="text-align:right;"> -0.7608750 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 16.576667 </td>
   <td style="text-align:right;"> 16.030 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-11.98,-0.29] </td>
   <td style="text-align:right;"> -11.98 </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.8938950 </td>
   <td style="text-align:right;"> -4.8600320 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 6.880000 </td>
   <td style="text-align:right;"> 6.880 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-25.41,-7.85] </td>
   <td style="text-align:right;"> -25.41 </td>
   <td style="text-align:right;"> -7.85 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -14.5596280 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 31.213333 </td>
   <td style="text-align:right;"> 34.920 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.45,16.06] </td>
   <td style="text-align:right;"> 1.45 </td>
   <td style="text-align:right;"> 16.06 </td>
   <td style="text-align:right;"> 0.3238627 </td>
   <td style="text-align:right;"> 8.9300463 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 25.640000 </td>
   <td style="text-align:right;"> 36.420 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-8.7,15.46] </td>
   <td style="text-align:right;"> -8.70 </td>
   <td style="text-align:right;"> 15.46 </td>
   <td style="text-align:right;"> 0.1096070 </td>
   <td style="text-align:right;"> 4.1499593 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 18.111842 </td>
   <td style="text-align:right;"> 18.700 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:left;"> [-7.09,-1.75] </td>
   <td style="text-align:right;"> -7.09 </td>
   <td style="text-align:right;"> -1.75 </td>
   <td style="text-align:right;"> 0.3095166 </td>
   <td style="text-align:right;"> -4.4000373 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 21.499394 </td>
   <td style="text-align:right;"> 21.610 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [-4.28,2.3] </td>
   <td style="text-align:right;"> -4.28 </td>
   <td style="text-align:right;"> 2.30 </td>
   <td style="text-align:right;"> 0.5448264 </td>
   <td style="text-align:right;"> -1.0000582 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 21.073333 </td>
   <td style="text-align:right;"> 19.100 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-7.89,4.33] </td>
   <td style="text-align:right;"> -7.89 </td>
   <td style="text-align:right;"> 4.33 </td>
   <td style="text-align:right;"> 0.6841948 </td>
   <td style="text-align:right;"> -0.9700139 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> humates </td>
   <td style="text-align:right;"> 21.806667 </td>
   <td style="text-align:right;"> 16.480 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-12.71,9.65] </td>
   <td style="text-align:right;"> -12.71 </td>
   <td style="text-align:right;"> 9.65 </td>
   <td style="text-align:right;"> 0.5642315 </td>
   <td style="text-align:right;"> -1.9317218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 27.642000 </td>
   <td style="text-align:right;"> 27.950 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-2.19,12.2] </td>
   <td style="text-align:right;"> -2.19 </td>
   <td style="text-align:right;"> 12.20 </td>
   <td style="text-align:right;"> 0.7287254 </td>
   <td style="text-align:right;"> 5.3366693 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 24.490000 </td>
   <td style="text-align:right;"> 26.935 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-6.61,10.21] </td>
   <td style="text-align:right;"> -6.61 </td>
   <td style="text-align:right;"> 10.21 </td>
   <td style="text-align:right;"> 0.4300776 </td>
   <td style="text-align:right;"> 1.2397029 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 15.332000 </td>
   <td style="text-align:right;"> 16.170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-14.14,-1] </td>
   <td style="text-align:right;"> -14.14 </td>
   <td style="text-align:right;"> -1.00 </td>
   <td style="text-align:right;"> 0.7451284 </td>
   <td style="text-align:right;"> -6.3499821 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 15.716667 </td>
   <td style="text-align:right;"> 12.130 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-14.41,-0.42] </td>
   <td style="text-align:right;"> -14.41 </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> 0.6347244 </td>
   <td style="text-align:right;"> -7.5899916 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 27.242500 </td>
   <td style="text-align:right;"> 22.795 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-1.53,9.39] </td>
   <td style="text-align:right;"> -1.53 </td>
   <td style="text-align:right;"> 9.39 </td>
   <td style="text-align:right;"> 0.3852263 </td>
   <td style="text-align:right;"> 3.6939713 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 6.880000 </td>
   <td style="text-align:right;"> 6.880 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-25.41,-7.85] </td>
   <td style="text-align:right;"> -25.41 </td>
   <td style="text-align:right;"> -7.85 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> -14.5596280 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 15.297500 </td>
   <td style="text-align:right;"> 17.100 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-15.44,0.27] </td>
   <td style="text-align:right;"> -15.44 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.6408699 </td>
   <td style="text-align:right;"> -6.5953589 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> natural_mined_gypsum </td>
   <td style="text-align:right;"> 26.440000 </td>
   <td style="text-align:right;"> 23.730 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-6.28,12.35] </td>
   <td style="text-align:right;"> -6.28 </td>
   <td style="text-align:right;"> 12.35 </td>
   <td style="text-align:right;"> 0.6628792 </td>
   <td style="text-align:right;"> 4.3399728 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 25.475556 </td>
   <td style="text-align:right;"> 25.390 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-3.63,6.85] </td>
   <td style="text-align:right;"> -3.63 </td>
   <td style="text-align:right;"> 6.85 </td>
   <td style="text-align:right;"> 0.4618069 </td>
   <td style="text-align:right;"> 1.3899873 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 21.270000 </td>
   <td style="text-align:right;"> 22.460 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-4.49,2.62] </td>
   <td style="text-align:right;"> -4.49 </td>
   <td style="text-align:right;"> 2.62 </td>
   <td style="text-align:right;"> 0.3867221 </td>
   <td style="text-align:right;"> -0.7608750 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 25.022963 </td>
   <td style="text-align:right;"> 20.200 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:left;"> [-3.17,5.06] </td>
   <td style="text-align:right;"> -3.17 </td>
   <td style="text-align:right;"> 5.06 </td>
   <td style="text-align:right;"> 0.3549328 </td>
   <td style="text-align:right;"> 0.7900228 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 31.213333 </td>
   <td style="text-align:right;"> 34.920 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.45,16.06] </td>
   <td style="text-align:right;"> 1.45 </td>
   <td style="text-align:right;"> 16.06 </td>
   <td style="text-align:right;"> 0.3238627 </td>
   <td style="text-align:right;"> 8.9300463 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 19.110000 </td>
   <td style="text-align:right;"> 21.000 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-8.07,1.36] </td>
   <td style="text-align:right;"> -8.07 </td>
   <td style="text-align:right;"> 1.36 </td>
   <td style="text-align:right;"> 0.3095096 </td>
   <td style="text-align:right;"> -3.1900442 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 23.597159 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:left;"> [-1.7,2.52] </td>
   <td style="text-align:right;"> -1.70 </td>
   <td style="text-align:right;"> 2.52 </td>
   <td style="text-align:right;"> 0.4861297 </td>
   <td style="text-align:right;"> 0.5799964 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 19.970000 </td>
   <td style="text-align:right;"> 20.010 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-8.49,3.12] </td>
   <td style="text-align:right;"> -8.49 </td>
   <td style="text-align:right;"> 3.12 </td>
   <td style="text-align:right;"> 0.9692251 </td>
   <td style="text-align:right;"> -1.3200565 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 22.128000 </td>
   <td style="text-align:right;"> 27.750 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-9.87,9.07] </td>
   <td style="text-align:right;"> -9.87 </td>
   <td style="text-align:right;"> 9.07 </td>
   <td style="text-align:right;"> 0.2273440 </td>
   <td style="text-align:right;"> -0.9003791 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 19.646429 </td>
   <td style="text-align:right;"> 21.000 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-7.17,1.74] </td>
   <td style="text-align:right;"> -7.17 </td>
   <td style="text-align:right;"> 1.74 </td>
   <td style="text-align:right;"> 0.3827642 </td>
   <td style="text-align:right;"> -2.6000602 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 9.218333 </td>
   <td style="text-align:right;"> 8.870 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-19.24,-7.59] </td>
   <td style="text-align:right;"> -19.24 </td>
   <td style="text-align:right;"> -7.59 </td>
   <td style="text-align:right;"> 0.9569254 </td>
   <td style="text-align:right;"> -12.3515639 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> provide </td>
   <td style="text-align:right;"> 30.035000 </td>
   <td style="text-align:right;"> 23.565 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-5.23,16.36] </td>
   <td style="text-align:right;"> -5.23 </td>
   <td style="text-align:right;"> 16.36 </td>
   <td style="text-align:right;"> 0.7465758 </td>
   <td style="text-align:right;"> 3.9459254 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> purple_cow_activated_compost </td>
   <td style="text-align:right;"> 29.625000 </td>
   <td style="text-align:right;"> 28.700 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [0.74,13.25] </td>
   <td style="text-align:right;"> 0.74 </td>
   <td style="text-align:right;"> 13.25 </td>
   <td style="text-align:right;"> 0.9542909 </td>
   <td style="text-align:right;"> 7.4370217 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 22.524444 </td>
   <td style="text-align:right;"> 21.750 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [-5.88,5.08] </td>
   <td style="text-align:right;"> -5.88 </td>
   <td style="text-align:right;"> 5.08 </td>
   <td style="text-align:right;"> 0.8850528 </td>
   <td style="text-align:right;"> -0.1086896 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 25.640000 </td>
   <td style="text-align:right;"> 36.420 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-8.7,15.46] </td>
   <td style="text-align:right;"> -8.70 </td>
   <td style="text-align:right;"> 15.46 </td>
   <td style="text-align:right;"> 0.1096070 </td>
   <td style="text-align:right;"> 4.1499593 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 35.165833 </td>
   <td style="text-align:right;"> 37.095 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [7.11,18.22] </td>
   <td style="text-align:right;"> 7.11 </td>
   <td style="text-align:right;"> 18.22 </td>
   <td style="text-align:right;"> 0.4210830 </td>
   <td style="text-align:right;"> 13.1700389 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seashield </td>
   <td style="text-align:right;"> 8.670000 </td>
   <td style="text-align:right;"> 8.670 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-21.72,-6.61] </td>
   <td style="text-align:right;"> -21.72 </td>
   <td style="text-align:right;"> -6.61 </td>
   <td style="text-align:right;"> 0.9192255 </td>
   <td style="text-align:right;"> -13.0199834 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 27.023636 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-0.43,8.33] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> 8.33 </td>
   <td style="text-align:right;"> 0.3118891 </td>
   <td style="text-align:right;"> 3.9500137 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 27.023636 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-0.43,8.33] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> 8.33 </td>
   <td style="text-align:right;"> 0.3118891 </td>
   <td style="text-align:right;"> 3.9500137 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 25.905714 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.37,6.86] </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 6.86 </td>
   <td style="text-align:right;"> 0.5274912 </td>
   <td style="text-align:right;"> 2.9693484 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 11.036667 </td>
   <td style="text-align:right;"> 10.950 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-17.63,-5.24] </td>
   <td style="text-align:right;"> -17.63 </td>
   <td style="text-align:right;"> -5.24 </td>
   <td style="text-align:right;"> 0.8817040 </td>
   <td style="text-align:right;"> -10.9381826 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 25.275385 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> [-2.51,7.01] </td>
   <td style="text-align:right;"> -2.51 </td>
   <td style="text-align:right;"> 7.01 </td>
   <td style="text-align:right;"> 0.6880794 </td>
   <td style="text-align:right;"> 2.5299806 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 18.088000 </td>
   <td style="text-align:right;"> 19.455 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-9.32,0.4] </td>
   <td style="text-align:right;"> -9.32 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.5029270 </td>
   <td style="text-align:right;"> -3.9936064 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 21.613333 </td>
   <td style="text-align:right;"> 22.230 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-7.49,3.38] </td>
   <td style="text-align:right;"> -7.49 </td>
   <td style="text-align:right;"> 3.38 </td>
   <td style="text-align:right;"> 0.4475265 </td>
   <td style="text-align:right;"> -1.8199869 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 10.094000 </td>
   <td style="text-align:right;"> 8.630 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-19.22,-6.16] </td>
   <td style="text-align:right;"> -19.22 </td>
   <td style="text-align:right;"> -6.16 </td>
   <td style="text-align:right;"> 0.7360008 </td>
   <td style="text-align:right;"> -11.5100050 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zeolite </td>
   <td style="text-align:right;"> 21.806667 </td>
   <td style="text-align:right;"> 16.480 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-12.71,9.65] </td>
   <td style="text-align:right;"> -12.71 </td>
   <td style="text-align:right;"> 9.65 </td>
   <td style="text-align:right;"> 0.5642315 </td>
   <td style="text-align:right;"> -1.9317218 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 25.905714 </td>
   <td style="text-align:right;"> 22.890 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.37,6.86] </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 6.86 </td>
   <td style="text-align:right;"> 0.5274912 </td>
   <td style="text-align:right;"> 2.9693484 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 23.934655 </td>
   <td style="text-align:right;"> 21.440 </td>
   <td style="text-align:right;"> 638 </td>
   <td style="text-align:left;"> [22.07,23.7] </td>
   <td style="text-align:right;"> 22.07 </td>
   <td style="text-align:right;"> 23.70 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 22.88 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0380952 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 7.9085714 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 7.4142857 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0380952 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 3.2952381 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.2952381 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0192678 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 2.9657143 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.8244898 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.8244898 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0077071 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2.4714286 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0095238 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4714286 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0380952 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.1968254 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.9771429 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7974026 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7974026 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.6476190 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 1.5208791 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2952381 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.1955684 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:right;"> 1.5096411 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0380952 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0260116 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 1.4645503 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1619048 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.1117534 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.4487685 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4122449 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4122449 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0144509 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.3180952 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.7428571 </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 0.6146435 </td>
   <td style="text-align:right;"> 638 </td>
   <td style="text-align:right;"> 1.2085983 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.5142857 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.4344894 </td>
   <td style="text-align:right;"> 451 </td>
   <td style="text-align:right;"> 1.1836554 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.1630252 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6380952 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 0.5674374 </td>
   <td style="text-align:right;"> 589 </td>
   <td style="text-align:right;"> 1.1245210 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1904762 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.1926782 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 0.9885714 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0587669 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.9723653 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2571429 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0.2793834 </td>
   <td style="text-align:right;"> 290 </td>
   <td style="text-align:right;"> 0.9203941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0761905 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0847784 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 0.8987013 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2761905 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 0.3102119 </td>
   <td style="text-align:right;"> 322 </td>
   <td style="text-align:right;"> 0.8903283 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0380952 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0481696 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 0.7908571 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.1428571 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.2331407 </td>
   <td style="text-align:right;"> 242 </td>
   <td style="text-align:right;"> 0.6127509 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0190476 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0655106 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 0.2907563 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019268 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009634 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0250482 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0154143 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0366089 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0317919 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0086705 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0009634 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 3.9770115 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1816092 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0344828 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.9827586 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.9827586 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.6513410 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.6513410 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0459770 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0192678 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 2.3862069 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.2725780 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.2725780 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0344828 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 1.9885057 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.9885057 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0191571 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.9885057 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0268199 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0144509 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.8559387 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.5908046 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.5908046 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0268199 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 1.5466156 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1685824 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 0.1117534 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.5085216 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0344828 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0260116 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 1.3256705 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0613027 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0481696 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.2726437 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2452107 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 0.1955684 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:right;"> 1.2538361 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 1.2236958 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.1931034 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.1362890 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.1362890 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6245211 </td>
   <td style="text-align:right;"> 163 </td>
   <td style="text-align:right;"> 0.5674374 </td>
   <td style="text-align:right;"> 589 </td>
   <td style="text-align:right;"> 1.1005991 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0919540 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.0847784 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.0846395 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0846395 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0846395 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6628352 </td>
   <td style="text-align:right;"> 173 </td>
   <td style="text-align:right;"> 0.6146435 </td>
   <td style="text-align:right;"> 638 </td>
   <td style="text-align:right;"> 1.0784059 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1992337 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0.1926782 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 1.0340230 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.4367816 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 0.4344894 </td>
   <td style="text-align:right;"> 451 </td>
   <td style="text-align:right;"> 1.0052756 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0077071 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.9942529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9942529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0306513 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0317919 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.9641240 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2988506 </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 0.3102119 </td>
   <td style="text-align:right;"> 322 </td>
   <td style="text-align:right;"> 0.9633755 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.9357674 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.8837803 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0086705 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.8837803 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2260536 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.2793834 </td>
   <td style="text-align:right;"> 290 </td>
   <td style="text-align:right;"> 0.8091161 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0153257 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.7575260 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0114943 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0154143 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.7456897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7230930 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0459770 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0655106 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 0.7018256 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.6628352 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0383142 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0587669 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.6519691 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.1494253 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.2331407 </td>
   <td style="text-align:right;"> 242 </td>
   <td style="text-align:right;"> 0.6409233 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0250482 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.3059240 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0076628 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0366089 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0.2093164 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0038314 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.1893815 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019268 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 6.6538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 6.6538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 6.6538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 6.6538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0320513 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 5.5448718 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.4358974 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9923077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9923077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9923077 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3269231 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 2.6615385 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0576923 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0250482 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 2.3032544 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.2179487 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2179487 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2179487 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2179487 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 2.0473373 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.9010989 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0067437 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.9010989 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.4038462 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 0.2331407 </td>
   <td style="text-align:right;"> 242 </td>
   <td style="text-align:right;"> 1.7321996 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0064103 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.6634615 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0064103 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.6634615 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0256410 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.5656109 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0705128 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0481696 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.4638462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0192308 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4258242 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0512821 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0366089 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 1.4008097 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.3307692 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.3307692 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1474359 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.1117534 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 1.3192971 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0833333 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0655106 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 1.2720588 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.3461538 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.2793834 </td>
   <td style="text-align:right;"> 290 </td>
   <td style="text-align:right;"> 1.2389920 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0320513 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0260116 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 1.2321937 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0384615 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0317919 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.2097902 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.3525641 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 0.3102119 </td>
   <td style="text-align:right;"> 322 </td>
   <td style="text-align:right;"> 1.1365265 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0125241 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 1.0236686 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.9505495 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5769231 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 0.6146435 </td>
   <td style="text-align:right;"> 638 </td>
   <td style="text-align:right;"> 0.9386303 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0847784 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 0.9073427 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0154143 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8317308 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1538462 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.1926782 </td>
   <td style="text-align:right;"> 200 </td>
   <td style="text-align:right;"> 0.7984615 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.3461538 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.4344894 </td>
   <td style="text-align:right;"> 451 </td>
   <td style="text-align:right;"> 0.7966911 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0448718 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0587669 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 0.7635561 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7393162 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7393162 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0173410 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7393162 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.3782051 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.5674374 </td>
   <td style="text-align:right;"> 589 </td>
   <td style="text-align:right;"> 0.6665143 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.6336996 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0128205 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0202312 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.6336996 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1217949 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.1955684 </td>
   <td style="text-align:right;"> 203 </td>
   <td style="text-align:right;"> 0.6227738 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019268 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009634 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0144509 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0096339 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0192678 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0048170 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0077071 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0163776 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0038536 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0057803 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0086705 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0115607 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0105973 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0028902 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0134875 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Carbon Mass Percentage at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 6.500381 </td>
   <td style="text-align:right;"> 5.720 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:left;"> [5.66,6.61] </td>
   <td style="text-align:right;"> 5.66 </td>
   <td style="text-align:right;"> 6.61 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 6.08995 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 4.309605 </td>
   <td style="text-align:right;"> 4.170 </td>
   <td style="text-align:right;"> 228 </td>
   <td style="text-align:left;"> [-2.34,-1.39] </td>
   <td style="text-align:right;"> -2.34 </td>
   <td style="text-align:right;"> -1.39 </td>
   <td style="text-align:right;"> 0.3180609 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -1.8499861 </td>
   <td style="text-align:right;"> 52 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 6.704000 </td>
   <td style="text-align:right;"> 6.190 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.7,1.9] </td>
   <td style="text-align:right;"> -0.70 </td>
   <td style="text-align:right;"> 1.90 </td>
   <td style="text-align:right;"> 0.7631018 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.8499707 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 6.045707 </td>
   <td style="text-align:right;"> 5.520 </td>
   <td style="text-align:right;"> 573 </td>
   <td style="text-align:left;"> [-0.84,0.04] </td>
   <td style="text-align:right;"> -0.84 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.3952675 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.4200144 </td>
   <td style="text-align:right;"> 128 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 6.534348 </td>
   <td style="text-align:right;"> 6.010 </td>
   <td style="text-align:right;"> 437 </td>
   <td style="text-align:left;"> [-0.29,0.58] </td>
   <td style="text-align:right;"> -0.29 </td>
   <td style="text-align:right;"> 0.58 </td>
   <td style="text-align:right;"> 0.5354258 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.1499857 </td>
   <td style="text-align:right;"> 102 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 6.094247 </td>
   <td style="text-align:right;"> 5.790 </td>
   <td style="text-align:right;"> 186 </td>
   <td style="text-align:left;"> [-0.61,0.3] </td>
   <td style="text-align:right;"> -0.61 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.5452298 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.0900059 </td>
   <td style="text-align:right;"> 44 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 6.151562 </td>
   <td style="text-align:right;"> 4.640 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:left;"> [-1.23,-0.17] </td>
   <td style="text-align:right;"> -1.23 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.2505384 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.7099361 </td>
   <td style="text-align:right;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 5.297184 </td>
   <td style="text-align:right;"> 4.650 </td>
   <td style="text-align:right;"> 277 </td>
   <td style="text-align:left;"> [-1.41,-0.6] </td>
   <td style="text-align:right;"> -1.41 </td>
   <td style="text-align:right;"> -0.60 </td>
   <td style="text-align:right;"> 0.8024790 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -1.0000000 </td>
   <td style="text-align:right;"> 66 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 6.009018 </td>
   <td style="text-align:right;"> 4.835 </td>
   <td style="text-align:right;"> 326 </td>
   <td style="text-align:left;"> [-1.01,-0.19] </td>
   <td style="text-align:right;"> -1.01 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.2610952 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.5900182 </td>
   <td style="text-align:right;"> 66 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 5.931814 </td>
   <td style="text-align:right;"> 5.320 </td>
   <td style="text-align:right;"> 204 </td>
   <td style="text-align:left;"> [-0.92,0.02] </td>
   <td style="text-align:right;"> -0.92 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.8654920 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.4500438 </td>
   <td style="text-align:right;"> 52 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> aea </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 6.900 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-1.37,3.91] </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 3.91 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 1.6899477 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> aea_rejuvenate </td>
   <td style="text-align:right;"> 6.385000 </td>
   <td style="text-align:right;"> 6.385 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.87,2.89] </td>
   <td style="text-align:right;"> -0.87 </td>
   <td style="text-align:right;"> 2.89 </td>
   <td style="text-align:right;"> 0.8360695 </td>
   <td style="text-align:right;"> 0.9599926 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 7.363913 </td>
   <td style="text-align:right;"> 7.070 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> [1.24,2.34] </td>
   <td style="text-align:right;"> 1.24 </td>
   <td style="text-align:right;"> 2.34 </td>
   <td style="text-align:right;"> 0.9298480 </td>
   <td style="text-align:right;"> 1.8300429 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 3.822000 </td>
   <td style="text-align:right;"> 3.940 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> [-2.22,-0.76] </td>
   <td style="text-align:right;"> -2.22 </td>
   <td style="text-align:right;"> -0.76 </td>
   <td style="text-align:right;"> 0.7518910 </td>
   <td style="text-align:right;"> -1.4000311 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alpha </td>
   <td style="text-align:right;"> 9.415000 </td>
   <td style="text-align:right;"> 9.415 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-1.8,9.76] </td>
   <td style="text-align:right;"> -1.80 </td>
   <td style="text-align:right;"> 9.76 </td>
   <td style="text-align:right;"> 0.8391891 </td>
   <td style="text-align:right;"> 3.2500357 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 7.185000 </td>
   <td style="text-align:right;"> 4.230 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-1.12,3.52] </td>
   <td style="text-align:right;"> -1.12 </td>
   <td style="text-align:right;"> 3.52 </td>
   <td style="text-align:right;"> 0.1499459 </td>
   <td style="text-align:right;"> 0.3918203 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> basalt </td>
   <td style="text-align:right;"> 6.482500 </td>
   <td style="text-align:right;"> 5.260 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.36,3.18] </td>
   <td style="text-align:right;"> -1.36 </td>
   <td style="text-align:right;"> 3.18 </td>
   <td style="text-align:right;"> 0.5337986 </td>
   <td style="text-align:right;"> 0.7340474 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 5.793529 </td>
   <td style="text-align:right;"> 5.130 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-0.52,1.16] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> 1.16 </td>
   <td style="text-align:right;"> 0.3763118 </td>
   <td style="text-align:right;"> 0.3200000 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 4.537273 </td>
   <td style="text-align:right;"> 4.640 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-1.81,0.16] </td>
   <td style="text-align:right;"> -1.81 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.8459170 </td>
   <td style="text-align:right;"> -0.6600138 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 6.958947 </td>
   <td style="text-align:right;"> 7.400 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [0.18,2.88] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 2.88 </td>
   <td style="text-align:right;"> 0.3942753 </td>
   <td style="text-align:right;"> 1.5500381 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_10_percent </td>
   <td style="text-align:right;"> 9.136250 </td>
   <td style="text-align:right;"> 9.110 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [2.48,5.1] </td>
   <td style="text-align:right;"> 2.48 </td>
   <td style="text-align:right;"> 5.10 </td>
   <td style="text-align:right;"> 0.8800296 </td>
   <td style="text-align:right;"> 3.7600458 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 4.476250 </td>
   <td style="text-align:right;"> 3.865 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-2.57,0.53] </td>
   <td style="text-align:right;"> -2.57 </td>
   <td style="text-align:right;"> 0.53 </td>
   <td style="text-align:right;"> 0.8200993 </td>
   <td style="text-align:right;"> -1.1499860 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 2.995882 </td>
   <td style="text-align:right;"> 2.940 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.13,-1.55] </td>
   <td style="text-align:right;"> -3.13 </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> 0.9734091 </td>
   <td style="text-align:right;"> -2.2599488 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carbonotite </td>
   <td style="text-align:right;"> 6.354000 </td>
   <td style="text-align:right;"> 5.300 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-1.06,2.46] </td>
   <td style="text-align:right;"> -1.06 </td>
   <td style="text-align:right;"> 2.46 </td>
   <td style="text-align:right;"> 0.5195740 </td>
   <td style="text-align:right;"> 0.6899971 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> casella_organics </td>
   <td style="text-align:right;"> 4.656667 </td>
   <td style="text-align:right;"> 4.445 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.21,0.68] </td>
   <td style="text-align:right;"> -2.21 </td>
   <td style="text-align:right;"> 0.68 </td>
   <td style="text-align:right;"> 0.9107799 </td>
   <td style="text-align:right;"> -0.6630552 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 3.649524 </td>
   <td style="text-align:right;"> 3.700 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-2.21,-0.93] </td>
   <td style="text-align:right;"> -2.21 </td>
   <td style="text-align:right;"> -0.93 </td>
   <td style="text-align:right;"> 0.8713659 </td>
   <td style="text-align:right;"> -1.5599475 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chilean_nitrate </td>
   <td style="text-align:right;"> 6.893333 </td>
   <td style="text-align:right;"> 4.990 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-1.66,4.05] </td>
   <td style="text-align:right;"> -1.66 </td>
   <td style="text-align:right;"> 4.05 </td>
   <td style="text-align:right;"> 0.3836176 </td>
   <td style="text-align:right;"> 0.9699608 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cobalt_carbonate </td>
   <td style="text-align:right;"> 6.893333 </td>
   <td style="text-align:right;"> 4.990 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-1.66,4.05] </td>
   <td style="text-align:right;"> -1.66 </td>
   <td style="text-align:right;"> 4.05 </td>
   <td style="text-align:right;"> 0.3836176 </td>
   <td style="text-align:right;"> 0.9699608 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 5.062174 </td>
   <td style="text-align:right;"> 4.150 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [-1.22,0.27] </td>
   <td style="text-align:right;"> -1.22 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.2000045 </td>
   <td style="text-align:right;"> -0.4799321 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 4.476250 </td>
   <td style="text-align:right;"> 3.865 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-2.57,0.53] </td>
   <td style="text-align:right;"> -2.57 </td>
   <td style="text-align:right;"> 0.53 </td>
   <td style="text-align:right;"> 0.8200993 </td>
   <td style="text-align:right;"> -1.1499860 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 2.995882 </td>
   <td style="text-align:right;"> 2.940 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.13,-1.55] </td>
   <td style="text-align:right;"> -3.13 </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> 0.9734091 </td>
   <td style="text-align:right;"> -2.2599488 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 3.549286 </td>
   <td style="text-align:right;"> 3.555 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-2.49,-0.9] </td>
   <td style="text-align:right;"> -2.49 </td>
   <td style="text-align:right;"> -0.90 </td>
   <td style="text-align:right;"> 0.9834755 </td>
   <td style="text-align:right;"> -1.6700424 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> consortium </td>
   <td style="text-align:right;"> 6.755000 </td>
   <td style="text-align:right;"> 7.290 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.71,3.3] </td>
   <td style="text-align:right;"> -0.71 </td>
   <td style="text-align:right;"> 3.30 </td>
   <td style="text-align:right;"> 0.6485260 </td>
   <td style="text-align:right;"> 1.4199949 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 7.786956 </td>
   <td style="text-align:right;"> 8.200 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [1.37,3.39] </td>
   <td style="text-align:right;"> 1.37 </td>
   <td style="text-align:right;"> 3.39 </td>
   <td style="text-align:right;"> 0.3090820 </td>
   <td style="text-align:right;"> 2.3599890 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> crab_shell </td>
   <td style="text-align:right;"> 7.112727 </td>
   <td style="text-align:right;"> 6.190 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0.68,2.81] </td>
   <td style="text-align:right;"> 0.68 </td>
   <td style="text-align:right;"> 2.81 </td>
   <td style="text-align:right;"> 0.2575718 </td>
   <td style="text-align:right;"> 1.7399870 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 3.583333 </td>
   <td style="text-align:right;"> 4.120 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-3.33,-0.24] </td>
   <td style="text-align:right;"> -3.33 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.4769272 </td>
   <td style="text-align:right;"> -1.7400131 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 4.285385 </td>
   <td style="text-align:right;"> 3.550 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:left;"> [-2.03,-0.23] </td>
   <td style="text-align:right;"> -2.03 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.3208509 </td>
   <td style="text-align:right;"> -1.0699740 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 3.800000 </td>
   <td style="text-align:right;"> 3.830 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-2.32,-0.74] </td>
   <td style="text-align:right;"> -2.32 </td>
   <td style="text-align:right;"> -0.74 </td>
   <td style="text-align:right;"> 0.9192426 </td>
   <td style="text-align:right;"> -1.4399702 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 10.548000 </td>
   <td style="text-align:right;"> 9.780 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [3.49,6.74] </td>
   <td style="text-align:right;"> 3.49 </td>
   <td style="text-align:right;"> 6.74 </td>
   <td style="text-align:right;"> 0.4793378 </td>
   <td style="text-align:right;"> 5.2300781 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 8.016667 </td>
   <td style="text-align:right;"> 8.075 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.08,4.07] </td>
   <td style="text-align:right;"> 1.08 </td>
   <td style="text-align:right;"> 4.07 </td>
   <td style="text-align:right;"> 0.8772015 </td>
   <td style="text-align:right;"> 2.6799536 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 2.888750 </td>
   <td style="text-align:right;"> 2.670 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-3.65,-1.37] </td>
   <td style="text-align:right;"> -3.65 </td>
   <td style="text-align:right;"> -1.37 </td>
   <td style="text-align:right;"> 0.8050679 </td>
   <td style="text-align:right;"> -2.3499810 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 3.284000 </td>
   <td style="text-align:right;"> 3.240 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-3.74,-0.65] </td>
   <td style="text-align:right;"> -3.74 </td>
   <td style="text-align:right;"> -0.65 </td>
   <td style="text-align:right;"> 0.9711157 </td>
   <td style="text-align:right;"> -1.9400010 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 4.231818 </td>
   <td style="text-align:right;"> 4.490 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-2.08,-0.15] </td>
   <td style="text-align:right;"> -2.08 </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.5828974 </td>
   <td style="text-align:right;"> -1.0000422 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 6.112000 </td>
   <td style="text-align:right;"> 5.880 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.37,1.26] </td>
   <td style="text-align:right;"> -0.37 </td>
   <td style="text-align:right;"> 1.26 </td>
   <td style="text-align:right;"> 0.6964467 </td>
   <td style="text-align:right;"> 0.4600203 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 4.937143 </td>
   <td style="text-align:right;"> 3.955 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.55,0.39] </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.2482963 </td>
   <td style="text-align:right;"> -0.6199744 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 2.995882 </td>
   <td style="text-align:right;"> 2.940 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.13,-1.55] </td>
   <td style="text-align:right;"> -3.13 </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> 0.9734091 </td>
   <td style="text-align:right;"> -2.2599488 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 3.653333 </td>
   <td style="text-align:right;"> 3.550 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-3.2,-0.42] </td>
   <td style="text-align:right;"> -3.20 </td>
   <td style="text-align:right;"> -0.42 </td>
   <td style="text-align:right;"> 0.8660546 </td>
   <td style="text-align:right;"> -1.5800579 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 7.070000 </td>
   <td style="text-align:right;"> 7.070 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.52,3.47] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> 3.47 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 1.8599475 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 4.376667 </td>
   <td style="text-align:right;"> 4.310 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.44,0.29] </td>
   <td style="text-align:right;"> -2.44 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.9370102 </td>
   <td style="text-align:right;"> -0.8399710 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 10.340000 </td>
   <td style="text-align:right;"> 10.340 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [3.08,6.48] </td>
   <td style="text-align:right;"> 3.08 </td>
   <td style="text-align:right;"> 6.48 </td>
   <td style="text-align:right;"> 0.9896093 </td>
   <td style="text-align:right;"> 5.1200014 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.630 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.21,1.63] </td>
   <td style="text-align:right;"> -1.21 </td>
   <td style="text-align:right;"> 1.63 </td>
   <td style="text-align:right;"> 0.7327109 </td>
   <td style="text-align:right;"> 0.1799457 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 4.348718 </td>
   <td style="text-align:right;"> 3.840 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:left;"> [-1.6,-0.56] </td>
   <td style="text-align:right;"> -1.60 </td>
   <td style="text-align:right;"> -0.56 </td>
   <td style="text-align:right;"> 0.3388803 </td>
   <td style="text-align:right;"> -1.0499568 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 9.466667 </td>
   <td style="text-align:right;"> 9.115 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:left;"> [3.22,4.78] </td>
   <td style="text-align:right;"> 3.22 </td>
   <td style="text-align:right;"> 4.78 </td>
   <td style="text-align:right;"> 0.7606003 </td>
   <td style="text-align:right;"> 4.0499888 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 5.848571 </td>
   <td style="text-align:right;"> 5.900 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-0.92,1.82] </td>
   <td style="text-align:right;"> -0.92 </td>
   <td style="text-align:right;"> 1.82 </td>
   <td style="text-align:right;"> 0.8915127 </td>
   <td style="text-align:right;"> 0.5899987 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> humates </td>
   <td style="text-align:right;"> 6.482500 </td>
   <td style="text-align:right;"> 5.260 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.36,3.18] </td>
   <td style="text-align:right;"> -1.36 </td>
   <td style="text-align:right;"> 3.18 </td>
   <td style="text-align:right;"> 0.5337986 </td>
   <td style="text-align:right;"> 0.7340474 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 7.380000 </td>
   <td style="text-align:right;"> 8.420 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.15,3.87] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 3.87 </td>
   <td style="text-align:right;"> 0.3250624 </td>
   <td style="text-align:right;"> 1.8599788 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 5.920909 </td>
   <td style="text-align:right;"> 5.300 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-0.7,1.46] </td>
   <td style="text-align:right;"> -0.70 </td>
   <td style="text-align:right;"> 1.46 </td>
   <td style="text-align:right;"> 0.6644529 </td>
   <td style="text-align:right;"> 0.3399844 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 3.914000 </td>
   <td style="text-align:right;"> 3.920 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-3.04,-0.08] </td>
   <td style="text-align:right;"> -3.04 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.9917457 </td>
   <td style="text-align:right;"> -1.2906836 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 3.583333 </td>
   <td style="text-align:right;"> 4.120 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-3.33,-0.24] </td>
   <td style="text-align:right;"> -3.33 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.4769272 </td>
   <td style="text-align:right;"> -1.7400131 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 5.483333 </td>
   <td style="text-align:right;"> 5.150 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [-0.95,1.16] </td>
   <td style="text-align:right;"> -0.95 </td>
   <td style="text-align:right;"> 1.16 </td>
   <td style="text-align:right;"> 0.7143477 </td>
   <td style="text-align:right;"> 0.2200095 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 7.070000 </td>
   <td style="text-align:right;"> 7.070 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.52,3.47] </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> 3.47 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 1.8599475 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 6.093333 </td>
   <td style="text-align:right;"> 6.480 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-1.5,2.56] </td>
   <td style="text-align:right;"> -1.50 </td>
   <td style="text-align:right;"> 2.56 </td>
   <td style="text-align:right;"> 0.6918640 </td>
   <td style="text-align:right;"> 0.8699674 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> natural_mined_gypsum </td>
   <td style="text-align:right;"> 6.893333 </td>
   <td style="text-align:right;"> 4.990 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-1.66,4.05] </td>
   <td style="text-align:right;"> -1.66 </td>
   <td style="text-align:right;"> 4.05 </td>
   <td style="text-align:right;"> 0.3836176 </td>
   <td style="text-align:right;"> 0.9699608 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 6.793333 </td>
   <td style="text-align:right;"> 7.950 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [0.26,2.44] </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 2.44 </td>
   <td style="text-align:right;"> 0.0327181 </td>
   <td style="text-align:right;"> 1.2099676 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 2.995882 </td>
   <td style="text-align:right;"> 2.940 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.13,-1.55] </td>
   <td style="text-align:right;"> -3.13 </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> 0.9734091 </td>
   <td style="text-align:right;"> -2.2599488 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 5.013200 </td>
   <td style="text-align:right;"> 4.730 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:left;"> [-1.19,0.4] </td>
   <td style="text-align:right;"> -1.19 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 0.9048951 </td>
   <td style="text-align:right;"> -0.4199311 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_compost </td>
   <td style="text-align:right;"> 10.340000 </td>
   <td style="text-align:right;"> 10.340 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [3.08,6.48] </td>
   <td style="text-align:right;"> 3.08 </td>
   <td style="text-align:right;"> 6.48 </td>
   <td style="text-align:right;"> 0.9896093 </td>
   <td style="text-align:right;"> 5.1200014 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_tea </td>
   <td style="text-align:right;"> 10.340000 </td>
   <td style="text-align:right;"> 10.340 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [3.08,6.48] </td>
   <td style="text-align:right;"> 3.08 </td>
   <td style="text-align:right;"> 6.48 </td>
   <td style="text-align:right;"> 0.9896093 </td>
   <td style="text-align:right;"> 5.1200014 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 4.376667 </td>
   <td style="text-align:right;"> 4.310 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.44,0.29] </td>
   <td style="text-align:right;"> -2.44 </td>
   <td style="text-align:right;"> 0.29 </td>
   <td style="text-align:right;"> 0.9370102 </td>
   <td style="text-align:right;"> -0.8399710 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 4.478571 </td>
   <td style="text-align:right;"> 4.640 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.75,0.03] </td>
   <td style="text-align:right;"> -1.75 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.6877100 </td>
   <td style="text-align:right;"> -0.7499651 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 5.110111 </td>
   <td style="text-align:right;"> 3.840 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:left;"> [-1.05,-0.25] </td>
   <td style="text-align:right;"> -1.05 </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.0052166 </td>
   <td style="text-align:right;"> -0.6499690 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 3.335000 </td>
   <td style="text-align:right;"> 3.335 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-3.89,-0.53] </td>
   <td style="text-align:right;"> -3.89 </td>
   <td style="text-align:right;"> -0.53 </td>
   <td style="text-align:right;"> 0.9734502 </td>
   <td style="text-align:right;"> -1.8947918 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 6.367143 </td>
   <td style="text-align:right;"> 6.400 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-0.3,2.21] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> 2.21 </td>
   <td style="text-align:right;"> 0.9643177 </td>
   <td style="text-align:right;"> 1.1600516 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 4.126667 </td>
   <td style="text-align:right;"> 4.220 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-1.95,-0.41] </td>
   <td style="text-align:right;"> -1.95 </td>
   <td style="text-align:right;"> -0.41 </td>
   <td style="text-align:right;"> 0.7682394 </td>
   <td style="text-align:right;"> -1.0900471 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 3.716000 </td>
   <td style="text-align:right;"> 3.605 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-2.75,-0.64] </td>
   <td style="text-align:right;"> -2.75 </td>
   <td style="text-align:right;"> -0.64 </td>
   <td style="text-align:right;"> 0.9426492 </td>
   <td style="text-align:right;"> -1.5699591 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> purple_cow_activated_compost </td>
   <td style="text-align:right;"> 8.016667 </td>
   <td style="text-align:right;"> 8.075 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [1.08,4.07] </td>
   <td style="text-align:right;"> 1.08 </td>
   <td style="text-align:right;"> 4.07 </td>
   <td style="text-align:right;"> 0.8772015 </td>
   <td style="text-align:right;"> 2.6799536 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 9.647778 </td>
   <td style="text-align:right;"> 9.240 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [2.42,6] </td>
   <td style="text-align:right;"> 2.42 </td>
   <td style="text-align:right;"> 6.00 </td>
   <td style="text-align:right;"> 0.8763232 </td>
   <td style="text-align:right;"> 4.1952608 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.630 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.21,1.63] </td>
   <td style="text-align:right;"> -1.21 </td>
   <td style="text-align:right;"> 1.63 </td>
   <td style="text-align:right;"> 0.7327109 </td>
   <td style="text-align:right;"> 0.1799457 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 3.653333 </td>
   <td style="text-align:right;"> 3.635 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-2.61,-0.73] </td>
   <td style="text-align:right;"> -2.61 </td>
   <td style="text-align:right;"> -0.73 </td>
   <td style="text-align:right;"> 0.9635923 </td>
   <td style="text-align:right;"> -1.5500445 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seashield </td>
   <td style="text-align:right;"> 6.385000 </td>
   <td style="text-align:right;"> 6.385 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.87,2.89] </td>
   <td style="text-align:right;"> -0.87 </td>
   <td style="text-align:right;"> 2.89 </td>
   <td style="text-align:right;"> 0.8360695 </td>
   <td style="text-align:right;"> 0.9599926 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 5.835833 </td>
   <td style="text-align:right;"> 5.070 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.64,1.23] </td>
   <td style="text-align:right;"> -0.64 </td>
   <td style="text-align:right;"> 1.23 </td>
   <td style="text-align:right;"> 0.4289882 </td>
   <td style="text-align:right;"> 0.3899808 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 5.835833 </td>
   <td style="text-align:right;"> 5.070 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.64,1.23] </td>
   <td style="text-align:right;"> -0.64 </td>
   <td style="text-align:right;"> 1.23 </td>
   <td style="text-align:right;"> 0.4289882 </td>
   <td style="text-align:right;"> 0.3899808 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 5.997500 </td>
   <td style="text-align:right;"> 5.150 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-0.43,1.23] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> 1.23 </td>
   <td style="text-align:right;"> 0.3417807 </td>
   <td style="text-align:right;"> 0.4500461 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 5.580000 </td>
   <td style="text-align:right;"> 5.250 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-1.41,1.76] </td>
   <td style="text-align:right;"> -1.41 </td>
   <td style="text-align:right;"> 1.76 </td>
   <td style="text-align:right;"> 0.9445184 </td>
   <td style="text-align:right;"> 0.0948657 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 5.870000 </td>
   <td style="text-align:right;"> 5.965 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-0.31,1.36] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> 1.36 </td>
   <td style="text-align:right;"> 0.7304066 </td>
   <td style="text-align:right;"> 0.5900144 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 5.929000 </td>
   <td style="text-align:right;"> 5.130 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-0.65,1.53] </td>
   <td style="text-align:right;"> -0.65 </td>
   <td style="text-align:right;"> 1.53 </td>
   <td style="text-align:right;"> 0.3815056 </td>
   <td style="text-align:right;"> 0.4899435 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 5.159286 </td>
   <td style="text-align:right;"> 4.940 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.04,0.69] </td>
   <td style="text-align:right;"> -1.04 </td>
   <td style="text-align:right;"> 0.69 </td>
   <td style="text-align:right;"> 0.8467152 </td>
   <td style="text-align:right;"> -0.1600390 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> whit_acres_mix </td>
   <td style="text-align:right;"> 6.755000 </td>
   <td style="text-align:right;"> 7.290 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.71,3.3] </td>
   <td style="text-align:right;"> -0.71 </td>
   <td style="text-align:right;"> 3.30 </td>
   <td style="text-align:right;"> 0.6485260 </td>
   <td style="text-align:right;"> 1.4199949 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 6.430833 </td>
   <td style="text-align:right;"> 6.280 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-0.97,2.54] </td>
   <td style="text-align:right;"> -0.97 </td>
   <td style="text-align:right;"> 2.54 </td>
   <td style="text-align:right;"> 0.8188215 </td>
   <td style="text-align:right;"> 0.8500276 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_power </td>
   <td style="text-align:right;"> 10.340000 </td>
   <td style="text-align:right;"> 10.340 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [3.08,6.48] </td>
   <td style="text-align:right;"> 3.08 </td>
   <td style="text-align:right;"> 6.48 </td>
   <td style="text-align:right;"> 0.9896093 </td>
   <td style="text-align:right;"> 5.1200014 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zeolite </td>
   <td style="text-align:right;"> 6.482500 </td>
   <td style="text-align:right;"> 5.260 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-1.36,3.18] </td>
   <td style="text-align:right;"> -1.36 </td>
   <td style="text-align:right;"> 3.18 </td>
   <td style="text-align:right;"> 0.5337986 </td>
   <td style="text-align:right;"> 0.7340474 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 5.997500 </td>
   <td style="text-align:right;"> 5.150 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-0.43,1.23] </td>
   <td style="text-align:right;"> -0.43 </td>
   <td style="text-align:right;"> 1.23 </td>
   <td style="text-align:right;"> 0.3417807 </td>
   <td style="text-align:right;"> 0.4500461 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.647013 </td>
   <td style="text-align:right;"> 5.210 </td>
   <td style="text-align:right;"> 596 </td>
   <td style="text-align:left;"> [5.26,5.65] </td>
   <td style="text-align:right;"> 5.26 </td>
   <td style="text-align:right;"> 5.65 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.454989 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0490196 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 4.9411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 4.9411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 4.3921569 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.1274510 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0297619 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 4.2823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 3.9529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0686275 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 3.6408669 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0784314 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 3.4373402 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.1078431 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 2.1741176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.9764706 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.1274510 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0684524 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 1.8618926 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.1176471 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0634921 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 1.8529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.4901961 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 0.3234127 </td>
   <td style="text-align:right;"> 326 </td>
   <td style="text-align:right;"> 1.5156983 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1568627 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.1041667 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 1.5058824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.6078431 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.4335317 </td>
   <td style="text-align:right;"> 437 </td>
   <td style="text-align:right;"> 1.4020730 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 1.2352941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 1.2352941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.1078431 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0892857 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 1.2078431 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6568627 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 0.5684524 </td>
   <td style="text-align:right;"> 573 </td>
   <td style="text-align:right;"> 1.1555282 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.2058824 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.2023810 </td>
   <td style="text-align:right;"> 204 </td>
   <td style="text-align:right;"> 1.0173010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.9882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5588235 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0.5912698 </td>
   <td style="text-align:right;"> 596 </td>
   <td style="text-align:right;"> 0.9451244 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8983957 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8235294 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8235294 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.1666667 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.2748016 </td>
   <td style="text-align:right;"> 277 </td>
   <td style="text-align:right;"> 0.6064982 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1078431 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.1845238 </td>
   <td style="text-align:right;"> 186 </td>
   <td style="text-align:right;"> 0.5844402 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.5813149 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0784314 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.2261905 </td>
   <td style="text-align:right;"> 228 </td>
   <td style="text-align:right;"> 0.3467492 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0386905 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.2533937 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0128968 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0248016 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0276680 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 3.4861660 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3201581 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.3201581 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0948617 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.0297619 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 3.1873518 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0276680 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 3.0988142 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 2.3905138 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0513834 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 2.2519333 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0395257 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 2.0969420 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0434783 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 2.0869565 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.1304348 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0684524 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 1.9054820 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.8109953 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.6600791 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0790514 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.5936759 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.5936759 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.5928854 </td>
   <td style="text-align:right;"> 150 </td>
   <td style="text-align:right;"> 0.4335317 </td>
   <td style="text-align:right;"> 437 </td>
   <td style="text-align:right;"> 1.3675708 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6916996 </td>
   <td style="text-align:right;"> 175 </td>
   <td style="text-align:right;"> 0.5684524 </td>
   <td style="text-align:right;"> 573 </td>
   <td style="text-align:right;"> 1.2168119 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0750988 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0634921 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 1.1828063 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.3794466 </td>
   <td style="text-align:right;"> 96 </td>
   <td style="text-align:right;"> 0.3234127 </td>
   <td style="text-align:right;"> 326 </td>
   <td style="text-align:right;"> 1.1732583 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.1383399 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 1.1383399 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1185771 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.1041667 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 1.1383399 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.2292490 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.2023810 </td>
   <td style="text-align:right;"> 204 </td>
   <td style="text-align:right;"> 1.1327598 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.0865972 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1936759 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.1845238 </td>
   <td style="text-align:right;"> 186 </td>
   <td style="text-align:right;"> 1.0495984 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6126482 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 0.5912698 </td>
   <td style="text-align:right;"> 596 </td>
   <td style="text-align:right;"> 1.0361567 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0711462 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0892857 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 0.7968379 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7968379 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7968379 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2173913 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 0.2748016 </td>
   <td style="text-align:right;"> 277 </td>
   <td style="text-align:right;"> 0.7910846 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.7030923 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.5691700 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.5196769 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.4980237 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.4980237 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4980237 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4980237 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0248016 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.4781028 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0909091 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.2261905 </td>
   <td style="text-align:right;"> 228 </td>
   <td style="text-align:right;"> 0.4019139 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.3984190 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.3320158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.3320158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0386905 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.3064761 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0128968 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 4.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0592105 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.5108359 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0592105 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.5108359 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0592105 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.5108359 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0592105 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 3.5108359 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0263158 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 3.3157895 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0263158 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 3.3157895 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0263158 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0328947 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 1.9504644 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0921053 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.8568421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0328947 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 1.7451524 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3815789 </td>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> 0.2261905 </td>
   <td style="text-align:right;"> 228 </td>
   <td style="text-align:right;"> 1.6869806 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0328947 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 1.6578947 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.3263158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.3263158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.7500000 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:right;"> 0.5912698 </td>
   <td style="text-align:right;"> 596 </td>
   <td style="text-align:right;"> 1.2684564 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2763158 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.2748016 </td>
   <td style="text-align:right;"> 277 </td>
   <td style="text-align:right;"> 1.0055102 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0855263 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0892857 </td>
   <td style="text-align:right;"> 90 </td>
   <td style="text-align:right;"> 0.9578947 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.8649886 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1513158 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.1845238 </td>
   <td style="text-align:right;"> 186 </td>
   <td style="text-align:right;"> 0.8200340 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.4539474 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 0.5684524 </td>
   <td style="text-align:right;"> 573 </td>
   <td style="text-align:right;"> 0.7985671 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0248016 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.7957895 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7368421 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.6315789 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1184211 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.2023810 </td>
   <td style="text-align:right;"> 204 </td>
   <td style="text-align:right;"> 0.5851393 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.1842105 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.3234127 </td>
   <td style="text-align:right;"> 326 </td>
   <td style="text-align:right;"> 0.5695835 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0128968 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.5101215 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0526316 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.1041667 </td>
   <td style="text-align:right;"> 105 </td>
   <td style="text-align:right;"> 0.5052632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0328947 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0684524 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:right;"> 0.4805492 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.4736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0386905 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.3400810 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.1315789 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.4335317 </td>
   <td style="text-align:right;"> 437 </td>
   <td style="text-align:right;"> 0.3035048 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.2883295 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0634921 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 0.2072368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alpha </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0297619 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Soil Carbon Mass Percentage at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 4.990210 </td>
   <td style="text-align:right;"> 4.490 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:left;"> [4.41,5.05] </td>
   <td style="text-align:right;"> 4.41 </td>
   <td style="text-align:right;"> 5.05 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 4.759937 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 3.789127 </td>
   <td style="text-align:right;"> 3.720 </td>
   <td style="text-align:right;"> 229 </td>
   <td style="text-align:left;"> [-1.44,-0.68] </td>
   <td style="text-align:right;"> -1.44 </td>
   <td style="text-align:right;"> -0.68 </td>
   <td style="text-align:right;"> 0.2044645 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -1.0700814 </td>
   <td style="text-align:right;"> 54 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 4.298000 </td>
   <td style="text-align:right;"> 4.610 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-1.54,0.57] </td>
   <td style="text-align:right;"> -1.54 </td>
   <td style="text-align:right;"> 0.57 </td>
   <td style="text-align:right;"> 0.5961665 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -0.4100090 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 4.705223 </td>
   <td style="text-align:right;"> 4.365 </td>
   <td style="text-align:right;"> 582 </td>
   <td style="text-align:left;"> [-0.71,0.01] </td>
   <td style="text-align:right;"> -0.71 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.3214175 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.3400133 </td>
   <td style="text-align:right;"> 127 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 5.078747 </td>
   <td style="text-align:right;"> 4.740 </td>
   <td style="text-align:right;"> 455 </td>
   <td style="text-align:left;"> [-0.31,0.39] </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> 0.39 </td>
   <td style="text-align:right;"> 0.3031946 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.0300182 </td>
   <td style="text-align:right;"> 105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 5.317173 </td>
   <td style="text-align:right;"> 4.920 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:left;"> [-0.08,0.72] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.72 </td>
   <td style="text-align:right;"> 0.6368467 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.3200055 </td>
   <td style="text-align:right;"> 47 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 5.517313 </td>
   <td style="text-align:right;"> 4.140 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:left;"> [-0.49,0.56] </td>
   <td style="text-align:right;"> -0.49 </td>
   <td style="text-align:right;"> 0.56 </td>
   <td style="text-align:right;"> 0.2150734 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.0100267 </td>
   <td style="text-align:right;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 4.646516 </td>
   <td style="text-align:right;"> 4.210 </td>
   <td style="text-align:right;"> 287 </td>
   <td style="text-align:left;"> [-0.63,0.06] </td>
   <td style="text-align:right;"> -0.63 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.9923021 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -0.2799360 </td>
   <td style="text-align:right;"> 70 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 4.566755 </td>
   <td style="text-align:right;"> 4.080 </td>
   <td style="text-align:right;"> 339 </td>
   <td style="text-align:left;"> [-0.79,-0.12] </td>
   <td style="text-align:right;"> -0.79 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.7656863 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.4899983 </td>
   <td style="text-align:right;"> 67 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 4.438421 </td>
   <td style="text-align:right;"> 3.990 </td>
   <td style="text-align:right;"> 209 </td>
   <td style="text-align:left;"> [-0.87,-0.15] </td>
   <td style="text-align:right;"> -0.87 </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.8980670 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -0.5299885 </td>
   <td style="text-align:right;"> 53 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> aea </td>
   <td style="text-align:right;"> 6.220000 </td>
   <td style="text-align:right;"> 6.220 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [-0.64,3.57] </td>
   <td style="text-align:right;"> -0.64 </td>
   <td style="text-align:right;"> 3.57 </td>
   <td style="text-align:right;"> 0.9850052 </td>
   <td style="text-align:right;"> 1.8299267 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> aea_rejuvenate </td>
   <td style="text-align:right;"> 3.660000 </td>
   <td style="text-align:right;"> 3.660 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-2.24,0.42] </td>
   <td style="text-align:right;"> -2.24 </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> 0.9588522 </td>
   <td style="text-align:right;"> -0.7500335 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alfalfa_meal </td>
   <td style="text-align:right;"> 4.630685 </td>
   <td style="text-align:right;"> 4.670 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:left;"> [-0.21,0.5] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.5853538 </td>
   <td style="text-align:right;"> 0.1700106 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> all_purpose </td>
   <td style="text-align:right;"> 3.301429 </td>
   <td style="text-align:right;"> 2.880 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-1.91,-0.75] </td>
   <td style="text-align:right;"> -1.91 </td>
   <td style="text-align:right;"> -0.75 </td>
   <td style="text-align:right;"> 0.6149734 </td>
   <td style="text-align:right;"> -1.3200213 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> azomite </td>
   <td style="text-align:right;"> 3.615000 </td>
   <td style="text-align:right;"> 3.070 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-1.89,-0.06] </td>
   <td style="text-align:right;"> -1.89 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.5465450 </td>
   <td style="text-align:right;"> -1.0300800 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> basalt </td>
   <td style="text-align:right;"> 8.012500 </td>
   <td style="text-align:right;"> 8.015 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [1.67,5.22] </td>
   <td style="text-align:right;"> 1.67 </td>
   <td style="text-align:right;"> 5.22 </td>
   <td style="text-align:right;"> 0.8399528 </td>
   <td style="text-align:right;"> 3.4000470 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> blood_meal </td>
   <td style="text-align:right;"> 5.098235 </td>
   <td style="text-align:right;"> 5.520 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-0.18,1.36] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 1.36 </td>
   <td style="text-align:right;"> 0.3138400 </td>
   <td style="text-align:right;"> 0.6500264 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> bone_char </td>
   <td style="text-align:right;"> 3.955455 </td>
   <td style="text-align:right;"> 3.830 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-1.2,0.19] </td>
   <td style="text-align:right;"> -1.20 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.8251511 </td>
   <td style="text-align:right;"> -0.4600513 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron </td>
   <td style="text-align:right;"> 4.584000 </td>
   <td style="text-align:right;"> 4.640 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> [-0.98,0.85] </td>
   <td style="text-align:right;"> -0.98 </td>
   <td style="text-align:right;"> 0.85 </td>
   <td style="text-align:right;"> 0.5964790 </td>
   <td style="text-align:right;"> 0.0099714 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_10_percent </td>
   <td style="text-align:right;"> 6.032500 </td>
   <td style="text-align:right;"> 5.765 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [0.13,2.7] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 2.70 </td>
   <td style="text-align:right;"> 0.9065517 </td>
   <td style="text-align:right;"> 1.4699901 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> boron_epsom_salt </td>
   <td style="text-align:right;"> 4.715000 </td>
   <td style="text-align:right;"> 3.180 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-1.89,0.71] </td>
   <td style="text-align:right;"> -1.89 </td>
   <td style="text-align:right;"> 0.71 </td>
   <td style="text-align:right;"> 0.5406185 </td>
   <td style="text-align:right;"> -0.8599934 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> calcium </td>
   <td style="text-align:right;"> 1.741765 </td>
   <td style="text-align:right;"> 1.590 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.27,-2.15] </td>
   <td style="text-align:right;"> -3.27 </td>
   <td style="text-align:right;"> -2.15 </td>
   <td style="text-align:right;"> 0.7864706 </td>
   <td style="text-align:right;"> -2.7100122 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> carbonotite </td>
   <td style="text-align:right;"> 7.168000 </td>
   <td style="text-align:right;"> 6.830 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [0.7,4.6] </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 4.60 </td>
   <td style="text-align:right;"> 0.8785439 </td>
   <td style="text-align:right;"> 2.5899670 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> casella_organics </td>
   <td style="text-align:right;"> 3.218333 </td>
   <td style="text-align:right;"> 2.830 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.52,-0.21] </td>
   <td style="text-align:right;"> -2.52 </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.7437976 </td>
   <td style="text-align:right;"> -1.3700131 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chicken_manure </td>
   <td style="text-align:right;"> 3.421905 </td>
   <td style="text-align:right;"> 3.480 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:left;"> [-1.52,-0.57] </td>
   <td style="text-align:right;"> -1.52 </td>
   <td style="text-align:right;"> -0.57 </td>
   <td style="text-align:right;"> 0.7690075 </td>
   <td style="text-align:right;"> -0.9999995 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> chilean_nitrate </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 5.270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.14,4.82] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 4.82 </td>
   <td style="text-align:right;"> 0.3775233 </td>
   <td style="text-align:right;"> 1.8699145 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cobalt_carbonate </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 5.270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.14,4.82] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 4.82 </td>
   <td style="text-align:right;"> 0.3775233 </td>
   <td style="text-align:right;"> 1.8699145 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost </td>
   <td style="text-align:right;"> 3.786500 </td>
   <td style="text-align:right;"> 3.030 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> [-1.57,-0.19] </td>
   <td style="text-align:right;"> -1.57 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.2747612 </td>
   <td style="text-align:right;"> -0.8700690 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_purchased </td>
   <td style="text-align:right;"> 4.715000 </td>
   <td style="text-align:right;"> 3.180 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [-1.89,0.71] </td>
   <td style="text-align:right;"> -1.89 </td>
   <td style="text-align:right;"> 0.71 </td>
   <td style="text-align:right;"> 0.5406185 </td>
   <td style="text-align:right;"> -0.8599934 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_tea </td>
   <td style="text-align:right;"> 1.741765 </td>
   <td style="text-align:right;"> 1.590 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.27,-2.15] </td>
   <td style="text-align:right;"> -3.27 </td>
   <td style="text-align:right;"> -2.15 </td>
   <td style="text-align:right;"> 0.7864706 </td>
   <td style="text-align:right;"> -2.7100122 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 2.850625 </td>
   <td style="text-align:right;"> 2.840 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [-2.14,-1.09] </td>
   <td style="text-align:right;"> -2.14 </td>
   <td style="text-align:right;"> -1.09 </td>
   <td style="text-align:right;"> 0.9604269 </td>
   <td style="text-align:right;"> -1.5599654 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> consortium </td>
   <td style="text-align:right;"> 5.020000 </td>
   <td style="text-align:right;"> 4.980 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.89,1.8] </td>
   <td style="text-align:right;"> -0.89 </td>
   <td style="text-align:right;"> 1.80 </td>
   <td style="text-align:right;"> 0.9917669 </td>
   <td style="text-align:right;"> 0.5900657 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> copper_sulfate </td>
   <td style="text-align:right;"> 6.160000 </td>
   <td style="text-align:right;"> 6.000 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:left;"> [0.78,2.2] </td>
   <td style="text-align:right;"> 0.78 </td>
   <td style="text-align:right;"> 2.20 </td>
   <td style="text-align:right;"> 0.8181450 </td>
   <td style="text-align:right;"> 1.5100142 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> crab_shell </td>
   <td style="text-align:right;"> 4.491818 </td>
   <td style="text-align:right;"> 4.630 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-0.69,0.7] </td>
   <td style="text-align:right;"> -0.69 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.7046085 </td>
   <td style="text-align:right;"> 0.1099493 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 2.833333 </td>
   <td style="text-align:right;"> 2.800 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.85,-0.46] </td>
   <td style="text-align:right;"> -2.85 </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 0.9186503 </td>
   <td style="text-align:right;"> -1.6600377 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> donald_stoner_compost </td>
   <td style="text-align:right;"> 2.913571 </td>
   <td style="text-align:right;"> 2.825 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-2.14,-0.94] </td>
   <td style="text-align:right;"> -2.14 </td>
   <td style="text-align:right;"> -0.94 </td>
   <td style="text-align:right;"> 0.8641322 </td>
   <td style="text-align:right;"> -1.5299470 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> down_to_earth </td>
   <td style="text-align:right;"> 3.380000 </td>
   <td style="text-align:right;"> 3.070 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-1.92,-0.64] </td>
   <td style="text-align:right;"> -1.92 </td>
   <td style="text-align:right;"> -0.64 </td>
   <td style="text-align:right;"> 0.8685348 </td>
   <td style="text-align:right;"> -1.2699832 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earth_care_farm </td>
   <td style="text-align:right;"> 4.642000 </td>
   <td style="text-align:right;"> 2.390 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-2.18,2.23] </td>
   <td style="text-align:right;"> -2.18 </td>
   <td style="text-align:right;"> 2.23 </td>
   <td style="text-align:right;"> 0.1649809 </td>
   <td style="text-align:right;"> -0.5400541 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> earthfort_provide </td>
   <td style="text-align:right;"> 2.974000 </td>
   <td style="text-align:right;"> 2.500 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-2.79,-0.39] </td>
   <td style="text-align:right;"> -2.79 </td>
   <td style="text-align:right;"> -0.39 </td>
   <td style="text-align:right;"> 0.5559508 </td>
   <td style="text-align:right;"> -1.5699399 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 4.866667 </td>
   <td style="text-align:right;"> 4.990 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.72,1.4] </td>
   <td style="text-align:right;"> -0.72 </td>
   <td style="text-align:right;"> 1.40 </td>
   <td style="text-align:right;"> 0.8147465 </td>
   <td style="text-align:right;"> 0.4964597 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> epsom_salt </td>
   <td style="text-align:right;"> 3.140000 </td>
   <td style="text-align:right;"> 2.300 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-2.62,-0.81] </td>
   <td style="text-align:right;"> -2.62 </td>
   <td style="text-align:right;"> -0.81 </td>
   <td style="text-align:right;"> 0.5868030 </td>
   <td style="text-align:right;"> -1.8200735 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 3.943333 </td>
   <td style="text-align:right;"> 2.405 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.36,0.85] </td>
   <td style="text-align:right;"> -2.36 </td>
   <td style="text-align:right;"> 0.85 </td>
   <td style="text-align:right;"> 0.2800459 </td>
   <td style="text-align:right;"> -1.0699646 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_made_at_thimble </td>
   <td style="text-align:right;"> 3.161818 </td>
   <td style="text-align:right;"> 2.990 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [-2.03,-0.58] </td>
   <td style="text-align:right;"> -2.03 </td>
   <td style="text-align:right;"> -0.58 </td>
   <td style="text-align:right;"> 0.8196507 </td>
   <td style="text-align:right;"> -1.2900013 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> feather_meal </td>
   <td style="text-align:right;"> 4.501600 </td>
   <td style="text-align:right;"> 3.530 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:left;"> [-0.94,0.25] </td>
   <td style="text-align:right;"> -0.94 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.1581984 </td>
   <td style="text-align:right;"> -0.3599555 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_emulsion </td>
   <td style="text-align:right;"> 3.450000 </td>
   <td style="text-align:right;"> 2.945 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:left;"> [-1.69,-0.49] </td>
   <td style="text-align:right;"> -1.69 </td>
   <td style="text-align:right;"> -0.49 </td>
   <td style="text-align:right;"> 0.3673768 </td>
   <td style="text-align:right;"> -1.1100119 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fish_seaweed </td>
   <td style="text-align:right;"> 1.741765 </td>
   <td style="text-align:right;"> 1.590 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.27,-2.15] </td>
   <td style="text-align:right;"> -3.27 </td>
   <td style="text-align:right;"> -2.15 </td>
   <td style="text-align:right;"> 0.7864706 </td>
   <td style="text-align:right;"> -2.7100122 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> fishbone_meal </td>
   <td style="text-align:right;"> 2.726667 </td>
   <td style="text-align:right;"> 2.800 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.85,-0.74] </td>
   <td style="text-align:right;"> -2.85 </td>
   <td style="text-align:right;"> -0.74 </td>
   <td style="text-align:right;"> 0.8372053 </td>
   <td style="text-align:right;"> -1.7000808 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> granite_meal </td>
   <td style="text-align:right;"> 6.110000 </td>
   <td style="text-align:right;"> 6.110 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.08,3.14] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 3.14 </td>
   <td style="text-align:right;"> 0.9809718 </td>
   <td style="text-align:right;"> 1.7199267 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 3.470000 </td>
   <td style="text-align:right;"> 3.490 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.09,0.01] </td>
   <td style="text-align:right;"> -2.09 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.9129301 </td>
   <td style="text-align:right;"> -0.9599722 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 13.180000 </td>
   <td style="text-align:right;"> 13.180 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [6.32,10.53] </td>
   <td style="text-align:right;"> 6.32 </td>
   <td style="text-align:right;"> 10.53 </td>
   <td style="text-align:right;"> 0.9983338 </td>
   <td style="text-align:right;"> 8.7899522 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> green_sand </td>
   <td style="text-align:right;"> 4.855714 </td>
   <td style="text-align:right;"> 5.790 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.3,1.67] </td>
   <td style="text-align:right;"> -1.30 </td>
   <td style="text-align:right;"> 1.67 </td>
   <td style="text-align:right;"> 0.2181180 </td>
   <td style="text-align:right;"> 0.2699933 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> gypsum </td>
   <td style="text-align:right;"> 3.803750 </td>
   <td style="text-align:right;"> 3.480 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:left;"> [-1.26,-0.52] </td>
   <td style="text-align:right;"> -1.26 </td>
   <td style="text-align:right;"> -0.52 </td>
   <td style="text-align:right;"> 0.8741464 </td>
   <td style="text-align:right;"> -0.8800009 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> holiday_brook_farm </td>
   <td style="text-align:right;"> 5.155151 </td>
   <td style="text-align:right;"> 5.100 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [0.27,1.1] </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 1.10 </td>
   <td style="text-align:right;"> 0.8967458 </td>
   <td style="text-align:right;"> 0.6799362 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 1.910000 </td>
   <td style="text-align:right;"> 1.730 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-3.56,-1.62] </td>
   <td style="text-align:right;"> -3.56 </td>
   <td style="text-align:right;"> -1.62 </td>
   <td style="text-align:right;"> 0.8562300 </td>
   <td style="text-align:right;"> -2.5899937 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> humates </td>
   <td style="text-align:right;"> 8.012500 </td>
   <td style="text-align:right;"> 8.015 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [1.67,5.22] </td>
   <td style="text-align:right;"> 1.67 </td>
   <td style="text-align:right;"> 5.22 </td>
   <td style="text-align:right;"> 0.8399528 </td>
   <td style="text-align:right;"> 3.4000470 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> imo_1_and_2 </td>
   <td style="text-align:right;"> 5.030000 </td>
   <td style="text-align:right;"> 4.870 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-0.81,1.86] </td>
   <td style="text-align:right;"> -0.81 </td>
   <td style="text-align:right;"> 1.86 </td>
   <td style="text-align:right;"> 0.9474824 </td>
   <td style="text-align:right;"> 0.5199970 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelp_meal </td>
   <td style="text-align:right;"> 6.003636 </td>
   <td style="text-align:right;"> 6.070 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:left;"> [0.11,2.66] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 2.66 </td>
   <td style="text-align:right;"> 0.7303633 </td>
   <td style="text-align:right;"> 1.5000113 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> kelplex </td>
   <td style="text-align:right;"> 3.280000 </td>
   <td style="text-align:right;"> 3.320 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> [-2.43,-0.09] </td>
   <td style="text-align:right;"> -2.43 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.9014473 </td>
   <td style="text-align:right;"> -1.1399944 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> krehers_5_4_3 </td>
   <td style="text-align:right;"> 2.833333 </td>
   <td style="text-align:right;"> 2.800 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.85,-0.46] </td>
   <td style="text-align:right;"> -2.85 </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 0.9186503 </td>
   <td style="text-align:right;"> -1.6600377 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> manganese_sulfate </td>
   <td style="text-align:right;"> 5.937778 </td>
   <td style="text-align:right;"> 4.900 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [0.18,2.16] </td>
   <td style="text-align:right;"> 0.18 </td>
   <td style="text-align:right;"> 2.16 </td>
   <td style="text-align:right;"> 0.3405077 </td>
   <td style="text-align:right;"> 1.0600118 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> menefee_humates </td>
   <td style="text-align:right;"> 6.110000 </td>
   <td style="text-align:right;"> 6.110 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.08,3.14] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 3.14 </td>
   <td style="text-align:right;"> 0.9809718 </td>
   <td style="text-align:right;"> 1.7199267 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> mushroom_soil </td>
   <td style="text-align:right;"> 2.782500 </td>
   <td style="text-align:right;"> 2.820 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-3.15,-0.46] </td>
   <td style="text-align:right;"> -3.15 </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 0.9283499 </td>
   <td style="text-align:right;"> -1.6399585 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> natural_mined_gypsum </td>
   <td style="text-align:right;"> 6.900000 </td>
   <td style="text-align:right;"> 5.270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> [-0.14,4.82] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 4.82 </td>
   <td style="text-align:right;"> 0.3775233 </td>
   <td style="text-align:right;"> 1.8699145 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 5.778947 </td>
   <td style="text-align:right;"> 4.480 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:left;"> [0.24,2.06] </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 2.06 </td>
   <td style="text-align:right;"> 0.0498730 </td>
   <td style="text-align:right;"> 1.0899752 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrilive_vitality </td>
   <td style="text-align:right;"> 1.741765 </td>
   <td style="text-align:right;"> 1.590 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:left;"> [-3.27,-2.15] </td>
   <td style="text-align:right;"> -3.27 </td>
   <td style="text-align:right;"> -2.15 </td>
   <td style="text-align:right;"> 0.7864706 </td>
   <td style="text-align:right;"> -2.7100122 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nutrisprout </td>
   <td style="text-align:right;"> 3.477692 </td>
   <td style="text-align:right;"> 3.555 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:left;"> [-1.55,-0.48] </td>
   <td style="text-align:right;"> -1.55 </td>
   <td style="text-align:right;"> -0.48 </td>
   <td style="text-align:right;"> 0.6306243 </td>
   <td style="text-align:right;"> -0.9899822 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_compost </td>
   <td style="text-align:right;"> 13.180000 </td>
   <td style="text-align:right;"> 13.180 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [6.32,10.53] </td>
   <td style="text-align:right;"> 6.32 </td>
   <td style="text-align:right;"> 10.53 </td>
   <td style="text-align:right;"> 0.9983338 </td>
   <td style="text-align:right;"> 8.7899522 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> own_tea </td>
   <td style="text-align:right;"> 13.180000 </td>
   <td style="text-align:right;"> 13.180 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [6.32,10.53] </td>
   <td style="text-align:right;"> 6.32 </td>
   <td style="text-align:right;"> 10.53 </td>
   <td style="text-align:right;"> 0.9983338 </td>
   <td style="text-align:right;"> 8.7899522 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oyster_shell_flour </td>
   <td style="text-align:right;"> 3.470000 </td>
   <td style="text-align:right;"> 3.490 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-2.09,0.01] </td>
   <td style="text-align:right;"> -2.09 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.9129301 </td>
   <td style="text-align:right;"> -0.9599722 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potash </td>
   <td style="text-align:right;"> 3.825000 </td>
   <td style="text-align:right;"> 3.815 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-1.25,-0.05] </td>
   <td style="text-align:right;"> -1.25 </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.9395851 </td>
   <td style="text-align:right;"> -0.5900459 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> potassium_sulfate </td>
   <td style="text-align:right;"> 4.300112 </td>
   <td style="text-align:right;"> 3.710 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:left;"> [-0.78,-0.08] </td>
   <td style="text-align:right;"> -0.78 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.2661340 </td>
   <td style="text-align:right;"> -0.4599926 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> powrflower </td>
   <td style="text-align:right;"> 2.480000 </td>
   <td style="text-align:right;"> 2.480 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-3.41,-0.76] </td>
   <td style="text-align:right;"> -3.41 </td>
   <td style="text-align:right;"> -0.76 </td>
   <td style="text-align:right;"> 0.9870626 </td>
   <td style="text-align:right;"> -1.9199806 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> pro_grow </td>
   <td style="text-align:right;"> 5.100000 </td>
   <td style="text-align:right;"> 4.960 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [-0.34,1.52] </td>
   <td style="text-align:right;"> -0.34 </td>
   <td style="text-align:right;"> 1.52 </td>
   <td style="text-align:right;"> 0.9594151 </td>
   <td style="text-align:right;"> 0.5900538 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> proboost </td>
   <td style="text-align:right;"> 3.747222 </td>
   <td style="text-align:right;"> 3.800 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [-1.26,-0.19] </td>
   <td style="text-align:right;"> -1.26 </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> 0.7787848 </td>
   <td style="text-align:right;"> -0.6600196 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> progro </td>
   <td style="text-align:right;"> 3.398000 </td>
   <td style="text-align:right;"> 3.540 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-1.81,-0.35] </td>
   <td style="text-align:right;"> -1.81 </td>
   <td style="text-align:right;"> -0.35 </td>
   <td style="text-align:right;"> 0.6561379 </td>
   <td style="text-align:right;"> -1.0099696 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> provide </td>
   <td style="text-align:right;"> 2.457500 </td>
   <td style="text-align:right;"> 2.240 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-3.55,-0.61] </td>
   <td style="text-align:right;"> -3.55 </td>
   <td style="text-align:right;"> -0.61 </td>
   <td style="text-align:right;"> 0.9002861 </td>
   <td style="text-align:right;"> -2.0600471 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> purple_cow_activated_compost </td>
   <td style="text-align:right;"> 4.866667 </td>
   <td style="text-align:right;"> 4.990 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-0.72,1.4] </td>
   <td style="text-align:right;"> -0.72 </td>
   <td style="text-align:right;"> 1.40 </td>
   <td style="text-align:right;"> 0.8147465 </td>
   <td style="text-align:right;"> 0.4964597 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> quantum </td>
   <td style="text-align:right;"> 4.993333 </td>
   <td style="text-align:right;"> 4.870 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [-0.46,1.44] </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 1.44 </td>
   <td style="text-align:right;"> 0.9804816 </td>
   <td style="text-align:right;"> 0.4900438 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> revita </td>
   <td style="text-align:right;"> 4.855714 </td>
   <td style="text-align:right;"> 5.790 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:left;"> [-1.3,1.67] </td>
   <td style="text-align:right;"> -1.30 </td>
   <td style="text-align:right;"> 1.67 </td>
   <td style="text-align:right;"> 0.2181180 </td>
   <td style="text-align:right;"> 0.2699933 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seasalt </td>
   <td style="text-align:right;"> 4.280000 </td>
   <td style="text-align:right;"> 4.145 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [-1.03,0.54] </td>
   <td style="text-align:right;"> -1.03 </td>
   <td style="text-align:right;"> 0.54 </td>
   <td style="text-align:right;"> 0.9750473 </td>
   <td style="text-align:right;"> -0.2299821 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seashield </td>
   <td style="text-align:right;"> 3.660000 </td>
   <td style="text-align:right;"> 3.660 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-2.24,0.42] </td>
   <td style="text-align:right;"> -2.24 </td>
   <td style="text-align:right;"> 0.42 </td>
   <td style="text-align:right;"> 0.9588522 </td>
   <td style="text-align:right;"> -0.7500335 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_molybdate </td>
   <td style="text-align:right;"> 6.178333 </td>
   <td style="text-align:right;"> 5.085 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [0.4,2.26] </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 2.26 </td>
   <td style="text-align:right;"> 0.3059574 </td>
   <td style="text-align:right;"> 1.2500179 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sodium_selenate </td>
   <td style="text-align:right;"> 6.178333 </td>
   <td style="text-align:right;"> 5.085 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> [0.4,2.26] </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 2.26 </td>
   <td style="text-align:right;"> 0.3059574 </td>
   <td style="text-align:right;"> 1.2500179 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> solubor </td>
   <td style="text-align:right;"> 6.636875 </td>
   <td style="text-align:right;"> 5.505 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [0.87,2.8] </td>
   <td style="text-align:right;"> 0.87 </td>
   <td style="text-align:right;"> 2.80 </td>
   <td style="text-align:right;"> 0.2172479 </td>
   <td style="text-align:right;"> 1.7897459 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> spectrum </td>
   <td style="text-align:right;"> 3.680000 </td>
   <td style="text-align:right;"> 3.720 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> [-1.87,0.19] </td>
   <td style="text-align:right;"> -1.87 </td>
   <td style="text-align:right;"> 0.19 </td>
   <td style="text-align:right;"> 0.8938968 </td>
   <td style="text-align:right;"> -0.7399999 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulfur </td>
   <td style="text-align:right;"> 5.518889 </td>
   <td style="text-align:right;"> 4.900 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:left;"> [0.2,1.52] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 1.52 </td>
   <td style="text-align:right;"> 0.4446212 </td>
   <td style="text-align:right;"> 0.7800528 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sulphur </td>
   <td style="text-align:right;"> 5.268000 </td>
   <td style="text-align:right;"> 5.390 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-0.14,1.58] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 1.58 </td>
   <td style="text-align:right;"> 0.7182195 </td>
   <td style="text-align:right;"> 0.7800188 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 5.107857 </td>
   <td style="text-align:right;"> 4.750 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:left;"> [-0.53,1.13] </td>
   <td style="text-align:right;"> -0.53 </td>
   <td style="text-align:right;"> 1.13 </td>
   <td style="text-align:right;"> 0.9881553 </td>
   <td style="text-align:right;"> 0.3599819 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> whit_acres_mix </td>
   <td style="text-align:right;"> 5.020000 </td>
   <td style="text-align:right;"> 4.980 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [-0.89,1.8] </td>
   <td style="text-align:right;"> -0.89 </td>
   <td style="text-align:right;"> 1.80 </td>
   <td style="text-align:right;"> 0.9917669 </td>
   <td style="text-align:right;"> 0.5900657 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_castings </td>
   <td style="text-align:right;"> 6.408000 </td>
   <td style="text-align:right;"> 3.225 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:left;"> [-1.22,3.86] </td>
   <td style="text-align:right;"> -1.22 </td>
   <td style="text-align:right;"> 3.86 </td>
   <td style="text-align:right;"> 0.1279580 </td>
   <td style="text-align:right;"> 0.2100153 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> worm_power </td>
   <td style="text-align:right;"> 13.180000 </td>
   <td style="text-align:right;"> 13.180 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:left;"> [6.32,10.53] </td>
   <td style="text-align:right;"> 6.32 </td>
   <td style="text-align:right;"> 10.53 </td>
   <td style="text-align:right;"> 0.9983338 </td>
   <td style="text-align:right;"> 8.7899522 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zeolite </td>
   <td style="text-align:right;"> 8.012500 </td>
   <td style="text-align:right;"> 8.015 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:left;"> [1.67,5.22] </td>
   <td style="text-align:right;"> 1.67 </td>
   <td style="text-align:right;"> 5.22 </td>
   <td style="text-align:right;"> 0.8399528 </td>
   <td style="text-align:right;"> 3.4000470 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> zinc_sulfate </td>
   <td style="text-align:right;"> 6.636875 </td>
   <td style="text-align:right;"> 5.505 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:left;"> [0.87,2.8] </td>
   <td style="text-align:right;"> 0.87 </td>
   <td style="text-align:right;"> 2.80 </td>
   <td style="text-align:right;"> 0.2172479 </td>
   <td style="text-align:right;"> 1.7897459 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 4.797248 </td>
   <td style="text-align:right;"> 4.390 </td>
   <td style="text-align:right;"> 585 </td>
   <td style="text-align:left;"> [4.4,4.71] </td>
   <td style="text-align:right;"> 4.40 </td>
   <td style="text-align:right;"> 4.71 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 4.550074 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable.

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 9.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4.9411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4.9411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4.9411765 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0392157 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 3.9529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9529412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0294118 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 3.7058824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.2941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0490196 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 3.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0490196 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 3.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0490196 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 2.6006192 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0588235 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 2.5780051 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0294118 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0294118 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 2.1960784 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 1.7967914 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.1176471 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0664683 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 1.7699737 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.6470588 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 1.4117647 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0686275 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.3835294 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.6176471 </td>
   <td style="text-align:right;"> 63 </td>
   <td style="text-align:right;"> 0.4513889 </td>
   <td style="text-align:right;"> 455 </td>
   <td style="text-align:right;"> 1.3683258 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.7941176 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 0.5803571 </td>
   <td style="text-align:right;"> 585 </td>
   <td style="text-align:right;"> 1.3683258 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.7254902 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.5773810 </td>
   <td style="text-align:right;"> 582 </td>
   <td style="text-align:right;"> 1.2565191 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2352941 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.1894841 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 1.2417616 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1078431 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0942460 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 1.1442724 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 1.0980392 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.3039216 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:right;"> 0.3363095 </td>
   <td style="text-align:right;"> 339 </td>
   <td style="text-align:right;"> 0.9036960 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8983957 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2549020 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.2847222 </td>
   <td style="text-align:right;"> 287 </td>
   <td style="text-align:right;"> 0.8952654 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0784314 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0882937 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 0.8883014 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1568627 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.2073413 </td>
   <td style="text-align:right;"> 209 </td>
   <td style="text-align:right;"> 0.7565438 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0148810 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.6588235 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.5490196 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0196078 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0396825 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0.4941176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.4705882 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0882353 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2271825 </td>
   <td style="text-align:right;"> 229 </td>
   <td style="text-align:right;"> 0.3883894 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0098039 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0724206 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 0.1353747 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0238095 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0327381 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0257937 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 3.9841897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.1873518 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0316206 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 2.8975925 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 2.4901186 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0553360 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 2.4251590 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.2766798 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 2.2766798 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0355731 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 2.1092769 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0316206 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0316206 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.9920949 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0355731 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 1.8872478 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.5936759 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.5936759 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 1.5936759 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.2924901 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.1894841 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 1.5436128 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0711462 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.4343083 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1343874 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 0.0942460 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 1.4259205 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0148810 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0237154 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 1.3280632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.7430830 </td>
   <td style="text-align:right;"> 188 </td>
   <td style="text-align:right;"> 0.5803571 </td>
   <td style="text-align:right;"> 585 </td>
   <td style="text-align:right;"> 1.2803892 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.5691700 </td>
   <td style="text-align:right;"> 144 </td>
   <td style="text-align:right;"> 0.4513889 </td>
   <td style="text-align:right;"> 455 </td>
   <td style="text-align:right;"> 1.2609304 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0790514 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.0664683 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 1.1893104 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6442688 </td>
   <td style="text-align:right;"> 163 </td>
   <td style="text-align:right;"> 0.5773810 </td>
   <td style="text-align:right;"> 582 </td>
   <td style="text-align:right;"> 1.1158470 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.2924901 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.2847222 </td>
   <td style="text-align:right;"> 287 </td>
   <td style="text-align:right;"> 1.0272824 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0197628 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.9960474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.2015810 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0.2073413 </td>
   <td style="text-align:right;"> 209 </td>
   <td style="text-align:right;"> 0.9722185 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0632411 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0724206 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 0.8732471 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2924901 </td>
   <td style="text-align:right;"> 74 </td>
   <td style="text-align:right;"> 0.3363095 </td>
   <td style="text-align:right;"> 339 </td>
   <td style="text-align:right;"> 0.8697051 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0118577 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8537549 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0276680 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0327381 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.8451312 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0671937 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0882937 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 0.7610250 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.6640316 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.6640316 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.1383399 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 0.2271825 </td>
   <td style="text-align:right;"> 229 </td>
   <td style="text-align:right;"> 0.6089373 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.4426877 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0158103 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0396825 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0.3984190 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.3984190 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0079051 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.3794466 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0039526 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.3621991 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0238095 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0257937 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.powrflower </td>
   <td style="text-align:right;"> 0.0263158 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 6.6315789 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.calcium </td>
   <td style="text-align:right;"> 0.0986842 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 5.8513932 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_tea </td>
   <td style="text-align:right;"> 0.0986842 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 5.8513932 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_seaweed </td>
   <td style="text-align:right;"> 0.0986842 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 5.8513932 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrilive_vitality </td>
   <td style="text-align:right;"> 0.0986842 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 5.8513932 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.homemade_johnson-su_compost </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 5.6842105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.provide </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 4.9736842 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.epsom_salt </td>
   <td style="text-align:right;"> 0.0526316 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 4.8229665 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_brewed_with_worm_castings </td>
   <td style="text-align:right;"> 0.0263158 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.4210526 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earth_care_farm </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.earthfort_provide </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 3.9789474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.down_to_earth </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.dead_green_crabs_and_seaweed </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fishbone_meal </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.krehers_5_4_3 </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 2.2105263 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.all_purpose </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.8947368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nutrisprout </td>
   <td style="text-align:right;"> 0.0460526 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0257937 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 1.7854251 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.feather_meal </td>
   <td style="text-align:right;"> 0.0855263 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.0496032 </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 1.7242105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.fish_emulsion </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0238095 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 1.6578947 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.mushroom_soil </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 1.6578947 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.3223684 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.2271825 </td>
   <td style="text-align:right;"> 229 </td>
   <td style="text-align:right;"> 1.4189841 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.azomite </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 1.3263158 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.casella_organics </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.1052632 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6118421 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.5803571 </td>
   <td style="text-align:right;"> 585 </td>
   <td style="text-align:right;"> 1.0542510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost </td>
   <td style="text-align:right;"> 0.0197368 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0198413 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.9947368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.5526316 </td>
   <td style="text-align:right;"> 84 </td>
   <td style="text-align:right;"> 0.5773810 </td>
   <td style="text-align:right;"> 582 </td>
   <td style="text-align:right;"> 0.9571351 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron </td>
   <td style="text-align:right;"> 0.0131579 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0148810 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.8842105 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> irrigation </td>
   <td style="text-align:right;"> 0.1776316 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0.2073413 </td>
   <td style="text-align:right;"> 209 </td>
   <td style="text-align:right;"> 0.8567112 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_epsom_salt </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8289474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_purchased </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8289474 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nonchlorine_water </td>
   <td style="text-align:right;"> 0.2565789 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.3363095 </td>
   <td style="text-align:right;"> 339 </td>
   <td style="text-align:right;"> 0.7629250 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> cover_crops </td>
   <td style="text-align:right;"> 0.1973684 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.2847222 </td>
   <td style="text-align:right;"> 287 </td>
   <td style="text-align:right;"> 0.6931964 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potassium_sulfate </td>
   <td style="text-align:right;"> 0.0592105 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0882937 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 0.6706091 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_castings </td>
   <td style="text-align:right;"> 0.0065789 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.6631579 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> nospray </td>
   <td style="text-align:right;"> 0.1250000 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.1894841 </td>
   <td style="text-align:right;"> 191 </td>
   <td style="text-align:right;"> 0.6596859 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 0.2763158 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.4513889 </td>
   <td style="text-align:right;"> 455 </td>
   <td style="text-align:right;"> 0.6121457 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0526316 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0942460 </td>
   <td style="text-align:right;"> 95 </td>
   <td style="text-align:right;"> 0.5584488 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.alfalfa_meal </td>
   <td style="text-align:right;"> 0.0394737 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0724206 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 0.5450613 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> other </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> biodynamic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0664683 </td>
   <td style="text-align:right;"> 67 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.aea_rejuvenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.basalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.blood_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0168651 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.bone_char </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.boron_10_percent </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0079365 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.carbonotite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chicken_manure </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0208333 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.chilean_nitrate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.cobalt_carbonate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.compost_with_chicken_manure_and_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.consortium </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.copper_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0228175 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.crab_shell </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.donald_stoner_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.ecoscraps_all_purpose_plant_food </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.farm_made_at_thimble </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.granite_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_compost_from_local_facility_mixed_with_peatmoss </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_earth_ag_and_turf </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.green_sand </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0396825 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.holiday_brook_farm </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0327381 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.imo_1_and_2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelp_meal </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0109127 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.kelplex </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0049603 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.manganese_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.menefee_humates </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.natural_mined_gypsum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0029762 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.nature_safe_5-6-6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0188492 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.own_tea </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.oyster_shell_flour </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.potash </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.pro_grow </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.proboost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.progro </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.purple_cow_activated_compost </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.quantum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0089286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.revita </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0069444 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seasalt </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.seashield </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_molybdate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sodium_selenate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0119048 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.solubor </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.spectrum </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0059524 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulfur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0178571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.sulphur </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0099206 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.tennessee_brown_rock_phosphate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0138889 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.whit_acres_mix </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.worm_power </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0019841 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zeolite </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0039683 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.zinc_sulfate </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0158730 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>
