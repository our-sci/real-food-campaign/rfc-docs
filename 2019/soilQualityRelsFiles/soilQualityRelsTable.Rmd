---
title: "Synthesis Tables: Soil Quality Relationships Analysis"
author: "Real Food Campaign"
date: "September 4, 2020"
output: html_document
---

# Introduction and Context

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of soil health. 
  This table shows only those practices and amendments for which at least one of the tests has yielded a relevant median shift compared to the median for the class of subjects which don't list any practices or amendments. On the ocasions were there wasn't strong enough evidence, the value has been quantized to 0 (as the null hypothesis on this tests is "There's a 0 median shift regarding the reference class").

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is interesting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting ones are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.

```{r input, echo=FALSE, warnings=FALSE}
options(knitr.table.format = "html")
library(tidyverse)
library(kableExtra)
amendmentsSynthesis <- read_rds( path="soilAmendmentsSynthesisTable.Rds" )
practicesSynthesis <- read_rds( path="soilPracticesSynthesisTable.Rds" )
relevants <- read_rds( path="./shiftsTable.Rds" )
sampleSizesTable <- read_rds( path="./sampleSizesTable.Rds" )

assignBackgroundNonVec <- function( value ) {
  if ( is.na(value) | typeof( value ) != "double" ) { return("none") }
  else if ( value == 0 ) { return("blue") }
  else if ( value > 0 ) { return("green") }
  else if ( value < 0 ) { return("red") }
}

assignBackground <- function( vecsValue ) {
  vecsValue %>% map_chr( assignBackgroundNonVec )
}

assignColorNonVec <- function( value ) {
  if ( is.na(value) | typeof( value ) != "double" ) { return("black") }
  else if ( value == 0 ) { return("white") }
  else if ( value > 0 ) { return("white") }
  else if ( value < 0 ) { return("white") }
}

assignColor <- function( vecsValue ) {
  vecsValue %>% map_chr( assignColorNonVec )
}

formatCols <- relevants %>% select( -"factor", -"family" ) %>%
  colnames %>%
  sort

allNArow <- function( rowInd, table, valueCols ) {
  table %>%
    slice(rowInd) %>%
    select( !!valueCols ) %>%
    is.na(.) %>%
    unlist %>%
      prod != 1
}

rowsNA <- map_lgl( 1:nrow(relevants), ~ allNArow( .x, relevants, formatCols ) ) 
indexesNA <- which( rowsNA == FALSE )

relevants <- relevants %>% slice( -indexesNA )
sampleSizesTable <- sampleSizesTable %>% slice( -indexesNA )

relevantSpecAmendments <- relevants %>% filter( family == "amendments" ) %>% select( - "family" ) 

sampleSizesAmendments <- sampleSizesTable %>% filter(  family == "amendments") %>% select( - "family" )

for ( col in formatCols ) {
  relevantSpecAmendments[[col]] <- cell_spec(
    relevantSpecAmendments[[col]],
    background = assignBackground( relevantSpecAmendments[[col]] ),
    tooltip = sampleSizesAmendments[[col]] %>% str_c( "Sample size: ", ., sep = "" ) ,
    color = assignColor( relevantSpecAmendments[[col]] ),
    bold = TRUE
               )
}

fullColorAmendments <- relevantSpecAmendments %>%
  filter( factor != "none" ) %>%
  mutate( factor = str_replace_all( factor, pattern = "_", replacement = " " ) %>% str_to_title( . ) ) %>%
  rename_all( ~ str_replace_all( ., pattern = "_", replacement = " " ) %>% str_to_title( . ) ) %>%
  kable( escape = FALSE, caption = "Percent of Variation over Median Value" ) %>%
  kable_styling( c( "hover", "striped", "condensed" ),
                full_width = FALSE,
                fixed_thead = TRUE
                ) %>%
  column_spec( 1, bold = TRUE, border_right = TRUE )


write_rds( fullColorAmendments, path="./soilAmendmentsShiftTable.Rds" )

relevantSpecPractices <- relevants %>% filter( family == "practices" ) %>% select( - "family" )

sampleSizesPractices <- sampleSizesTable %>% filter(  family == "practices") %>% select( - "family" )

for ( col in formatCols ) {
  relevantSpecPractices[[col]] <- cell_spec(
    relevantSpecPractices[[col]],
    background = assignBackground( relevantSpecPractices[[col]] ),
    tooltip = sampleSizesPractices[[col]] %>% str_c( "Sample size: ", ., sep = "" ) ,
    color = assignColor( relevantSpecPractices[[col]] ),
    bold = TRUE
               )
}

fullColorPractices <- relevantSpecPractices %>%
  filter( factor != "none" ) %>%
  mutate( factor = str_replace_all( factor, pattern = "_", replacement = " " ) %>% str_to_title( . ) ) %>%
  rename_all( ~ str_replace_all( ., pattern = "_", replacement = " " ) %>% str_to_title( . ) ) %>%
  kable( escape = FALSE, caption = "Percent of Variation over Median Value" ) %>%
  kable_styling( c( "hover", "striped", "condensed" ),
                full_width = FALSE,
                fixed_thead = TRUE
                ) %>%
  column_spec( 1, bold = TRUE, border_right = TRUE )


write_rds( fullColorPractices, path="./soilPracticesShiftTable.Rds" )

```

# Percentual Shift Over the Median Value for Farm Practices


```{r practices , echo=FALSE}
fullColorPractices
```


# Percentual Shift Over the Median Value for Soil Amendments


```{r amendments, echo=FALSE}
fullColorAmendments
```

