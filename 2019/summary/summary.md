---
title: "2019 Survey, General Summary"
author: "Real Food Campaign"
date: "12/09/2020"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
number_sectios: true
---



# Produce Summary Table

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Composition of Variable Families</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Carrot</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Cherry Tomato</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Grape</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Kale</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Lettuce</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Spinach</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 41.57 </td>
   <td style="text-align:right;"> 72.084 </td>
   <td style="text-align:right;"> 1025.840 </td>
   <td style="text-align:right;"> 183.092 </td>
   <td style="text-align:right;"> 1248.34 </td>
   <td style="text-align:right;"> 89.359 </td>
   <td style="text-align:right;"> 7071.13 </td>
   <td style="text-align:right;"> 124.175 </td>
   <td style="text-align:right;"> 296.91 </td>
   <td style="text-align:right;"> 270.618 </td>
   <td style="text-align:right;"> 175.160 </td>
   <td style="text-align:right;"> 364.075 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 8.10 </td>
   <td style="text-align:right;"> 11.254 </td>
   <td style="text-align:right;"> 6.200 </td>
   <td style="text-align:right;"> 9.198 </td>
   <td style="text-align:right;"> 18.50 </td>
   <td style="text-align:right;"> 6.889 </td>
   <td style="text-align:right;"> 8.80 </td>
   <td style="text-align:right;"> 3.850 </td>
   <td style="text-align:right;"> 3.60 </td>
   <td style="text-align:right;"> 7.769 </td>
   <td style="text-align:right;"> 6.400 </td>
   <td style="text-align:right;"> 5.190 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 10.62 </td>
   <td style="text-align:right;"> 228.064 </td>
   <td style="text-align:right;"> 40.865 </td>
   <td style="text-align:right;"> 33.943 </td>
   <td style="text-align:right;"> 111.75 </td>
   <td style="text-align:right;"> 130.141 </td>
   <td style="text-align:right;"> 232.07 </td>
   <td style="text-align:right;"> 50.139 </td>
   <td style="text-align:right;"> 31.28 </td>
   <td style="text-align:right;"> 136.125 </td>
   <td style="text-align:right;"> 209.105 </td>
   <td style="text-align:right;"> 24.772 </td>
  </tr>
</tbody>
</table>







# By Crop Summary Tables


## Carrot

### Summaries

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Carrots Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 41.57 </td>
   <td style="text-align:right;"> 3.9200 </td>
   <td style="text-align:right;"> 282.57 </td>
   <td style="text-align:right;"> 45.946402 </td>
   <td style="text-align:right;"> 72.084 </td>
   <td style="text-align:right;"> 444 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 8.10 </td>
   <td style="text-align:right;"> 1.3417 </td>
   <td style="text-align:right;"> 15.10 </td>
   <td style="text-align:right;"> 2.016387 </td>
   <td style="text-align:right;"> 11.254 </td>
   <td style="text-align:right;"> 376 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 10.62 </td>
   <td style="text-align:right;"> 1.0900 </td>
   <td style="text-align:right;"> 248.59 </td>
   <td style="text-align:right;"> 19.057512 </td>
   <td style="text-align:right;"> 228.064 </td>
   <td style="text-align:right;"> 489 </td>
  </tr>
</tbody>
</table>

### Histograms

![](summary_files/figure-html/histMatricesCarrot-1.png)<!-- -->

## Cherry Tomato

### Table

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Cherry Tomatos Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 1025.840 </td>
   <td style="text-align:right;"> 109.6000 </td>
   <td style="text-align:right;"> 20066.90 </td>
   <td style="text-align:right;"> 1912.222720 </td>
   <td style="text-align:right;"> 183.092 </td>
   <td style="text-align:right;"> 360 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 6.200 </td>
   <td style="text-align:right;"> 1.3373 </td>
   <td style="text-align:right;"> 12.30 </td>
   <td style="text-align:right;"> 1.611999 </td>
   <td style="text-align:right;"> 9.198 </td>
   <td style="text-align:right;"> 335 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 40.865 </td>
   <td style="text-align:right;"> 3.6900 </td>
   <td style="text-align:right;"> 125.25 </td>
   <td style="text-align:right;"> 16.447503 </td>
   <td style="text-align:right;"> 33.943 </td>
   <td style="text-align:right;"> 410 </td>
  </tr>
</tbody>
</table>

### Histograms

![](summary_files/figure-html/histMatricesCherry_Tomato-1.png)<!-- -->


## Grape

### Table

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Grapes Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 1248.34 </td>
   <td style="text-align:right;"> 294.49 </td>
   <td style="text-align:right;"> 26315.30 </td>
   <td style="text-align:right;"> 7089.41100 </td>
   <td style="text-align:right;"> 89.359 </td>
   <td style="text-align:right;"> 75 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 18.50 </td>
   <td style="text-align:right;"> 3.60 </td>
   <td style="text-align:right;"> 24.80 </td>
   <td style="text-align:right;"> 4.07388 </td>
   <td style="text-align:right;"> 6.889 </td>
   <td style="text-align:right;"> 69 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 111.75 </td>
   <td style="text-align:right;"> 9.32 </td>
   <td style="text-align:right;"> 1212.91 </td>
   <td style="text-align:right;"> 210.23897 </td>
   <td style="text-align:right;"> 130.141 </td>
   <td style="text-align:right;"> 85 </td>
  </tr>
</tbody>
</table>

### Histograms

![](summary_files/figure-html/histMatricesGrapes-1.png)<!-- -->



## Kale

### Table

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Kales Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 7071.13 </td>
   <td style="text-align:right;"> 311.12 </td>
   <td style="text-align:right;"> 38633.20 </td>
   <td style="text-align:right;"> 7364.813634 </td>
   <td style="text-align:right;"> 124.175 </td>
   <td style="text-align:right;"> 409 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 8.80 </td>
   <td style="text-align:right;"> 4.00 </td>
   <td style="text-align:right;"> 15.40 </td>
   <td style="text-align:right;"> 2.421197 </td>
   <td style="text-align:right;"> 3.850 </td>
   <td style="text-align:right;"> 306 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 232.07 </td>
   <td style="text-align:right;"> 20.32 </td>
   <td style="text-align:right;"> 1018.83 </td>
   <td style="text-align:right;"> 196.832316 </td>
   <td style="text-align:right;"> 50.139 </td>
   <td style="text-align:right;"> 517 </td>
  </tr>
</tbody>
</table>

### Histograms

![](summary_files/figure-html/histMatricesKale-1.png)<!-- -->



## Lettuce

### Table


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Lettuces Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 296.91 </td>
   <td style="text-align:right;"> 26.29 </td>
   <td style="text-align:right;"> 7114.55 </td>
   <td style="text-align:right;"> 916.434195 </td>
   <td style="text-align:right;"> 270.618 </td>
   <td style="text-align:right;"> 382 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 3.60 </td>
   <td style="text-align:right;"> 1.30 </td>
   <td style="text-align:right;"> 10.10 </td>
   <td style="text-align:right;"> 1.443739 </td>
   <td style="text-align:right;"> 7.769 </td>
   <td style="text-align:right;"> 342 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 31.28 </td>
   <td style="text-align:right;"> 4.95 </td>
   <td style="text-align:right;"> 673.82 </td>
   <td style="text-align:right;"> 68.433525 </td>
   <td style="text-align:right;"> 136.125 </td>
   <td style="text-align:right;"> 419 </td>
  </tr>
</tbody>
</table>

### Histograms

![](summary_files/figure-html/histMatricesLettuce-1.png)<!-- -->


## Spinach


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Spinachs Summary</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> standard_deviation </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sample_size </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 175.160 </td>
   <td style="text-align:right;"> 24.81 </td>
   <td style="text-align:right;"> 9032.71 </td>
   <td style="text-align:right;"> 1053.883060 </td>
   <td style="text-align:right;"> 364.075 </td>
   <td style="text-align:right;"> 143 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix </td>
   <td style="text-align:right;"> 6.400 </td>
   <td style="text-align:right;"> 2.10 </td>
   <td style="text-align:right;"> 10.90 </td>
   <td style="text-align:right;"> 1.712696 </td>
   <td style="text-align:right;"> 5.190 </td>
   <td style="text-align:right;"> 120 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 209.105 </td>
   <td style="text-align:right;"> 40.15 </td>
   <td style="text-align:right;"> 994.60 </td>
   <td style="text-align:right;"> 139.008417 </td>
   <td style="text-align:right;"> 24.772 </td>
   <td style="text-align:right;"> 178 </td>
  </tr>
</tbody>
</table>


### Histograms

![](summary_files/figure-html/histMatricesSpinach-1.png)<!-- -->


# Soil Summary Tables


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Summary of Soil Variables at Two Depths</caption>
 <thead>
<tr>
<th style="border-bottom:hidden" colspan="1"></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="5"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">0cm-10cm</div></th>
<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="5"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">10cm-20cm</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Standard D. </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Maximum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Minimum </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Ratio </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Standard D. </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> organic_carbon_percentage </td>
   <td style="text-align:right;"> 4.99 </td>
   <td style="text-align:right;"> 14.94 </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 14.94 </td>
   <td style="text-align:right;"> 2.72 </td>
   <td style="text-align:right;"> 4.12 </td>
   <td style="text-align:right;"> 14.81 </td>
   <td style="text-align:right;"> 0.98 </td>
   <td style="text-align:right;"> 15.11 </td>
   <td style="text-align:right;"> 2.26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil </td>
   <td style="text-align:right;"> 29.71 </td>
   <td style="text-align:right;"> 76.40 </td>
   <td style="text-align:right;"> 0.87 </td>
   <td style="text-align:right;"> 87.82 </td>
   <td style="text-align:right;"> 13.18 </td>
   <td style="text-align:right;"> 21.18 </td>
   <td style="text-align:right;"> 73.65 </td>
   <td style="text-align:right;"> 1.04 </td>
   <td style="text-align:right;"> 70.82 </td>
   <td style="text-align:right;"> 11.70 </td>
  </tr>
</tbody>
</table>

## Histograms

### 0cm to 10cm deep

![](summary_files/figure-html/soilHist-1.png)<!-- -->

### 10cm to 20cm deep

![](summary_files/figure-html/soilHist20-1.png)<!-- -->
