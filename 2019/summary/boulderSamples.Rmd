---
title: "2019 Survey, Flavor Scales Analysis"
author: "Real Food Campaign"
date: "July 1, 2020"
output:
  # pdf_document: default
  html_document: default
---

```{r preamble, echo = FALSE, errors=FALSE, warning=FALSE}
library(tidyverse)
library(kableExtra)
library(DescTools)

setwd("./2019")

dataframe <- read_rds( "./produceQualityRelationships/qualityRelsDataframe.Rds" )

boulderSamples <- dataframe %>%
  filter( county == "boulder" ) %>%
  select( sample_id ) %>%
  unique %>%
  unlist
```
