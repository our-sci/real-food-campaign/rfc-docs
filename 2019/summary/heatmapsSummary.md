---
title: "2019 Survey, Heatmaps for Soil and Crop Variables"
author: "Real Food Campaign"
date: "12/01/2020"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
number_sectios: true
---


```r
library(tidyverse)
library(kableExtra)

load( "../bigFiles/qualityRels.RSession" )

knitr::opts_chunk$set(
                    fig.width=12,
                    fig.height=18,
                    echo = FALSE,
                    warnigs=FALSE
                  )


squareHeatmap <- function( Xvarnames, Yvarnames, dataset, label, title ) {

  data <- dataset %>%
    select( Xvarnames, Yvarnames )

  correls <- data %>%
    cor( method = c( "spearman" ), use = "pairwise.complete.obs" )

  correlationsTable <- correls %>%
    as_tibble( rownames = "X" ) %>%
    pivot_longer(  - one_of(  "X" ), names_to = "Y", values_to = "correlation" ) %>%
    mutate(
      correlation = round( correlation,2 ),
      fontColor = ifelse( correlation < 0, "white", "black" )
    )

  visualization <- correlationsTable %>%
    filter( Y %in% Yvarnames ) %>%
    filter( X %in% Xvarnames ) %>%
    ggplot() +
    aes( x = X, y = Y, fill = correlation ) +
    geom_tile() +
    geom_text( aes( label = correlation, color = fontColor ), size = 5, fontface = "bold" ) +
    scale_fill_viridis_c( "Correlation", option = "plasma" ) +
    theme(
      axis.text.x = element_text( angle = 90, hjust = 1 ),
      legend.position = "top"
    ) +
    guides( color = "none" ) +
    scale_color_manual( values = c( "black", "white" ) ) +
    ggtitle( title )


  path <- paste0( "./graphics/heatmaps/Heatmap-", label ,".png" )

  ggsave( path , width=18, height=18 )

  output <- list(
    visualization = visualization,
    path = path,
    df = dataset,
    data = data
  )
}

triangleHeatmap <- function( varnames, dataset, label, title ) {

  data <- dataset %>%
    select( varnames )

  correls <- data %>%
    cor( method = c( "spearman" ), use = "pairwise.complete.obs" )

  varsOrder <- colnames(correls)

  correlationsTable <- correls %>%
    as_tibble( rownames = "X" ) %>%
    pivot_longer(  - one_of(  "X" ), names_to = "Y", values_to = "correlation" ) %>%
    mutate(
      correlation = round( correlation,2 ),
      fontColor = ifelse( correlation < 0, "white", "black" ),
      X = factor( X, levels = varsOrder, ordered = TRUE ),
      Y = factor( Y, levels = varsOrder, ordered = TRUE )
    ) %>%
    filter( X < Y )

  visualization <- correlationsTable %>%
    ggplot() +
    aes( x = X, y = Y, fill = correlation ) +
    geom_tile() +
    geom_text( aes( label = correlation, color = fontColor ), size = 5, fontface = "bold" ) +
    scale_fill_viridis_c( "Correlation", option = "plasma" ) +
    theme(
      axis.text.x = element_text( angle = 90, hjust = 1 ),
      legend.position = "top"
    ) +
    guides( color = "none" ) +
    scale_color_manual( values = c( "black", "white" ) ) +
    ggtitle( title )

  path <- paste0( "./graphics/heatmaps/Heatmap-", label ,".png" )

  ggsave( path , width=18, height=18 )

  output <- list(
    visualization = visualization,
    path = path,
    df = dataset,
    data = data
  )
}


soilQualityVariables10 <- c(
  "PH_soil_10cm",
  "respiration_soil_10cm",
  "organic_carbon_percentage_10cm"
)

soilQualityVariables20 <- c(
  "PH_soil_20cm",
  "respiration_soil_20cm",
  "organic_carbon_percentage_20cm"
)

soilMinerals10 <- dataframe %>%
  select( contains( "soil10.loi.xrf" ), - contains( "orig" ) ) %>%
  colnames

soilMinerals20 <- dataframe %>%
  select( contains( "soil20.loi.xrf" ), - contains( "orig" ) ) %>%
  colnames

cropMinerals <- dataframe %>%
  select( contains( "produce.xrf" ), - contains( "orig" ) ) %>%
  colnames

cropQuality <- c(
  "antioxidants",
  "polyphenols",
  "brix"
)

aggregatedData <- dataframe %>%
  select(
    cropQuality,
    cropMinerals,
    -contains("orig"),
    soilMinerals10,
    soilMinerals20,
    soilQualityVariables10,
    soilQualityVariables20
  ) %>%
  rename_at( vars( contains( "produce.xrf" ) ), ~ str_remove( ., "xrf." ) ) %>%
  rename_at( vars( contains( "loi.xrf" ) ), ~ str_remove( ., "loi.xrf." ) ) %>%
  mutate_at(
    vars( cropQuality, contains("produce") ),
    ~ vectorialECDF( .x )
  ) %>%
  rename_at(
    vars( cropQuality, contains("produce") ),
    ~ str_c( .x, "-Percentile" )
  )
```

# Correlation Between Crop Variables

## Carrot

![](heatmapsSummary_files/figure-html/cropCarrot-1.png)<!-- -->

## Cherry Tomato


![](heatmapsSummary_files/figure-html/cropCherryTomato-1.png)<!-- -->


## Grape

![](heatmapsSummary_files/figure-html/cropGrape-1.png)<!-- -->

## Kale

![](heatmapsSummary_files/figure-html/cropKale-1.png)<!-- -->

## Lettuce

![](heatmapsSummary_files/figure-html/cropLettuce-1.png)<!-- -->

## Spinach

![](heatmapsSummary_files/figure-html/cropSpinach-1.png)<!-- -->

# Correlation Between Soil Variables at Two Depths

## 0 to 10cm

![](heatmapsSummary_files/figure-html/soil10-1.png)<!-- -->

## 10 to 20cm

![](heatmapsSummary_files/figure-html/soil20-1.png)<!-- -->

# Aggregate Soil to Produce Correlations Heatmap




## 10 cm

![](heatmapsSummary_files/figure-html/unnamed-chunk-1-1.png)<!-- -->

## 20 cm

![](heatmapsSummary_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

# Crop Specific Soil To Produce Heatmaps

## Carrot

### 10cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapCarrot10-1.png)<!-- -->

### 20cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapCarrot20-1.png)<!-- -->

## Cherry Tomato

### 10cm


![](heatmapsSummary_files/figure-html/soilCropHeatmapCherry_Tomato10-1.png)<!-- -->


### 20cm


![](heatmapsSummary_files/figure-html/soilCropHeatmapCherry_Tomato20-1.png)<!-- -->

## Grape

### 10cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapGrape10-1.png)<!-- -->

### 20cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapGrape20-1.png)<!-- -->


## Kale

### 10cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapKale10-1.png)<!-- -->

### 20cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapKale20-1.png)<!-- -->

## Lettuce


### 10cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapLetuce10-1.png)<!-- -->

### 20cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapLetuce20-1.png)<!-- -->

## Spinach


### 10cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapSpinach10-1.png)<!-- -->

### 20cm

![](heatmapsSummary_files/figure-html/soilCropHeatmapSpinach20-1.png)<!-- -->
