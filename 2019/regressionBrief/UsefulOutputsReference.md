# Useful Objects Obtained During the Brief Generation

This list includes objects such as: complete lists of tables that were synthesized, dataframes from different instances of data agregation and organization, etc. 

## Some Things We can Provide Easily if Needed

* Specific formats for dataframes. We are currently outputting in CSV and RDS (R native format) formats. We are going to at least add JSON and probably some other format that might spare the user the work of parsing and assigning the right format to each column. If you need a not still avaiable fomart, don't hesitate to ask for help by generating an issue or asking for help on GOAT. 

## List

1. `resultsSynthesisTable` contains each random forest model with the name of its associated dataset and the observed performance metric (either *accuracy* for classifications or $R^2$) for regressions. The `mtry` hiperparameter is listed to increase reproducibility.
2. `fbpOutliers` is a list of those entries that have been identified as spectrical outliers for their respective crop. 
