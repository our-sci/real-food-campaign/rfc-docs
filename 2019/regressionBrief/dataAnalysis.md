---
title: "2019 Survey, Spectroscopy Data Analysis"
author: "Real Food Campaign"
date: "12/12/2020"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---





# Current State of Our Investigation and Available Outcomes

## Modelling Efforts

  Through application of varied techniques, we've steadily increased our predictive power, based on this same dataset. Currently, we've got fairly accurate models just for grapes and spinach ( both with $R^2$ around 0.7 ). For the other types of produce, we are usually below 0.5.
  We've searched for another useful application of our current efforts and developed classification models with three categories of content ( lower, middle and superior ). These ones are considerably more accurate and may probably find useful application as they exist right now or with a reasonable level of additional effort. We've got one model with 60% or more precission on category preddiction for almost every produce type, most of them require just the usage of the Our Sci reflectometer and perhaps a brixometer, so they are deployable and could find use for in-field testing.
  There's still **ongoing effort** in getting a better profit from the **SIWARE** data, which has required a substantial amount of cutting edge knowledge, specifical to the fields of Functional Data Analysis and Spectrometry. We may still find a way to squeeze more predictive power from this wavelengths, which have mostly been the least correlated data but have shown some promising results under heavy processing and with the usage of specialized methods. This is reasonable considering the extremely high dimensionality of the dataset and the complex nature of NIR spectroscopy data.

## Other Useful Outcomes

* We've organized and made available this dataset for **further investigations** from us or other interested parts.
* Provided a huge array of graphical information to make the task of a first insight over what's available rather easy and fast.
* Devised a big set of **specific tools** to help in that task.
* Developed several methods to **identify atypical spectrometry curves**, all of them **deployable on the field**, so a user can immediately know if they're under an unusual datum or a badly acquired measure.
* Pointed at the most successful modelling schemes and the most powerful variables for those models.


# Characterization of the Dataset

## Search For Outliers

Before any further analysis, a search for outrageously outlying data will be carried. As the amount of variables is overwhelming, boxplots graphics will serve the purpose of a fast glimpse in search of radically unusual points.

Our boxplots will show label IDs over the outliers to help identify the points that require more urgent consideration. While defining what an outlier is may be a contentious matter, we are not making decisions based on this criteria, just choosing a range of data to plot the labels and help making informed decisions later.

### Sample Composition
This is the sample composition by species:





Type             n()
--------------  ----
carrot           482
cherry_tomato    461
grape             81
kale             534
lettuce          482
other             11
spinach          164
NA               222

### Boxplots for Polyphenols content produce type.

It's important to notice that, as the distribution of this variables is drastically different among species, we will mostly be using free scales in this faceted graphics. Looking at two adjacent boxplots is not useful to compare their contents. We are using this visualization mostly in search of outliers inside every species.


#### Polyphenols Content by Produce, Far Outliers Labeled


![](dataAnalysis_files/figure-html/boxplotsP-1.png)<!-- -->

Here we see an interesting trend: outliers form clusters of a size reasonable enought to justify searching for a partition criteria on the population. This could perhaps lead us to interesting information.

We are going to add the far outlying points that are beyond any cluster into a table. This will be expanded with all suspicious points we find, so it will have a column for the reason of suspicion.


full_id    reason       
---------  -------------
2186_a     Polyphenols. 
2186_b     Polyphenols. 
4581_2_b   Polyphenols. 
2545_b     Polyphenols. 
3349       Polyphenols. 
3349       Polyphenols. 
3826_3     Polyphenols. 
3826_1     Polyphenols. 
3266       Polyphenols. 
3370       Polyphenols. 
3899       Polyphenols. 
2997       Polyphenols. 
2869       Polyphenols. 
2868       Polyphenols. 
2103_b     Polyphenols. 
2103_a     Polyphenols. 

### Boxplots for Antioxidants content by produce type.

#### Antioxidants Content by Produce, Far Outliers Labeled
![](dataAnalysis_files/figure-html/boxplotsAnti-1.png)<!-- -->

We see again outlying clusters and far outlying points. We tag the second group in a table, choosing by eyesight which points are far away even from the outliers cluster.


full_id   reason       
--------  -------------
3682      Antioxidants 
3256      Antioxidants 
3254_b    Antioxidants 
2545_b    Antioxidants 
2376_3    Antioxidants 
2026_a    Antioxidants 
2026_b    Antioxidants 
2024_b    Antioxidants 
UF007     Antioxidants 
4922_3    Antioxidants 
3255      Antioxidants 
3389      Antioxidants 
2102_b    Antioxidants 
2102_a    Antioxidants 

### Curves for juice spectra obtained with the Our Sci device.

Through the superimposed representation of each measured spectra, grouped by produce type, we are going to spot some undisputedly noisy patterns. 

We are classifying the observations by their variational behaviour. Our fisrt try at this would be just by the sign of variations, tough we could afterwards resort to clustering over an integral distance over observations if needed. This clustering is shown in color, over the folowing plots.

As there is much information to show, we want to point out that this graphics are drawn in very high definition  and can be zoomed for a more comphortable reading, besides being available as separate files in the repo.

#### Spectra for Juice Scans with OurSci's Device


![](dataAnalysis_files/figure-html/spaghettiJuiceOS-1.png)<!-- -->![](dataAnalysis_files/figure-html/spaghettiJuiceOS-2.png)<!-- -->![](dataAnalysis_files/figure-html/spaghettiJuiceOS-3.png)<!-- -->

This curves, at a first glance, look useful. Disctintive patterns emerge on a per cultivar basis and standard trajectories are apparently emerging.

We offer the clustering for two produce types besides the general plot, to show what kind of patterns are to be expected.

### Curves for OurSci device surface reflection Intensity

![](dataAnalysis_files/figure-html/spaghetti SurfaceOS-1.png)<!-- -->![](dataAnalysis_files/figure-html/spaghetti SurfaceOS-2.png)<!-- -->![](dataAnalysis_files/figure-html/spaghetti SurfaceOS-3.png)<!-- -->![](dataAnalysis_files/figure-html/spaghetti SurfaceOS-4.png)<!-- -->

This curves look problematic at first sight. The difference between products in shape is not evident, we'll have to check if a model can extract more information from them than their offset.

Tomattoes and grapes show the curious behaviour of having both curves that start increasing or decreasing, this pattern deserves deeper study.

A deeper analysis by variety shows that this curves contain more information than whats immediately apparent in the first visualization offered.


### Curves for SWIR juice intensity.

![](dataAnalysis_files/figure-html/juiceSWIRspaghetti-1.png)<!-- -->

#### Replot Without Far Outliers

![](dataAnalysis_files/figure-html/swir juice replot-1.png)<!-- -->

### Curves for SWIR surface intensity.

![](dataAnalysis_files/figure-html/surfaceSWIRspaghetti-1.png)<!-- -->

#### Replot Without Far Outliers

![](dataAnalysis_files/figure-html/swir surface clean-1.png)<!-- -->

### Some Observations About this Plots

On SIWARE curves, we tend to observe a consistent pattern of clustering. These show two groups of curves, clearly groupped by their amplitudes. 

#### Possible Explanations

* Some calibration adjustment at some momment in time. 
* Different practices used by different users of the instrument.
* Effect over the tissues of processing time.

This graphics aren't evidence enough because few points contains simultaneously all this information, so we ended up studying just a 10% of them. Nonetheless, if they are to be believed no pattern emerges when ordering by time in peak amplitude or nutrients content.

![](dataAnalysis_files/figure-html/time graphics-1.png)<!-- -->![](dataAnalysis_files/figure-html/time graphics-2.png)<!-- -->![](dataAnalysis_files/figure-html/time graphics-3.png)<!-- -->![](dataAnalysis_files/figure-html/time graphics-4.png)<!-- -->![](dataAnalysis_files/figure-html/time graphics-5.png)<!-- -->


## Distributional Plotting

This hasn't been the focus of our research, but we've produced spectral profiles ( which could be understood as the rainbow/spaghetti plots shown before, looking at the curves "from above" ) and joint density profiles for every produce types.

The joint densities are quite interesting and deserve further study. The spectrograms better reveal that SIWARE curves require heavy post processing.



![spectral profile polyphenols](./spectralPolyphenols.png)
![spectral profile antioxidants](./spectralAntioxidants.png)


```
## [[1]]
```

![](dataAnalysis_files/figure-html/densities-1.png)<!-- -->

```
## 
## [[2]]
```

![](dataAnalysis_files/figure-html/densities-2.png)<!-- -->

```
## 
## [[3]]
```

![](dataAnalysis_files/figure-html/densities-3.png)<!-- -->

```
## 
## [[4]]
```

![](dataAnalysis_files/figure-html/densities-4.png)<!-- -->

```
## 
## [[5]]
```

![](dataAnalysis_files/figure-html/densities-5.png)<!-- -->

```
## 
## [[6]]
```

![](dataAnalysis_files/figure-html/densities-6.png)<!-- -->

# Search for Outliers

  We've discarded far outlying points, but outliers still exist in our dataset. When dealing with curves, one can find two types of outliers: shape outliers and position outliers. The first category is exclusive of this kind of data and requires relatively novel techniques to be dealt with.
  The methods we tested and implented are all suited for in field use, most of them would need to store just a tiny amount of information to inform outlyingness of a new curve (for example, the boundaries of a bagplot of, for the most sofisticated methods, a set of vectors forming a best $q$ dimmensional approaching subspace where data should be projected). 





## Illustration of the Process


  For the better identification, we've used normalization and penalized smoothing over the whole dataset, intending to leave out just the curves that have really unusual shapes or amplitudes.
  In this subsection, we'll illustrate different algorithms using kale. Notice that we are plotting over the raw data, not the smoothed set.
  This graphics ( and a more detailed analysis on the amount and characteristics of the reported sets ) justify our definition of shape outlier.
  
  * We are classifying as shape outliers those points that are either removed by the functional boxplot cryteria on a first iteration, or after removing a first generation of outliers, smoothing and defining the boxplot again. 



![](dataAnalysis_files/figure-html/kale outliers-1.png)<!-- -->![](dataAnalysis_files/figure-html/kale outliers-2.png)<!-- -->![](dataAnalysis_files/figure-html/kale outliers-3.png)<!-- -->![](dataAnalysis_files/figure-html/kale outliers-4.png)<!-- -->![](dataAnalysis_files/figure-html/kale outliers-5.png)<!-- -->

### Bagplots on Raw Data

![](dataAnalysis_files/figure-html/box y bag, coles-1.png)<!-- -->![](dataAnalysis_files/figure-html/box y bag, coles-2.png)<!-- -->![](dataAnalysis_files/figure-html/box y bag, coles-3.png)<!-- -->![](dataAnalysis_files/figure-html/box y bag, coles-4.png)<!-- -->

###  Bagplots on Smoothed and Scaled Data


```
## [1] "siwareSurface grape fbplot"
```

![](dataAnalysis_files/figure-html/kale escalado-1.png)<!-- -->![](dataAnalysis_files/figure-html/kale escalado-2.png)<!-- -->![](dataAnalysis_files/figure-html/kale escalado-3.png)<!-- -->![](dataAnalysis_files/figure-html/kale escalado-4.png)<!-- -->



# Linear Model for Content in General


```
## # A tibble: 2,661 x 22
##    Juice_Scan_365nm Juice_Scan_385nm Juice_Scan_450nm Juice_Scan_500nm
##               <dbl>            <dbl>            <dbl>            <dbl>
##  1            -1.61            4.48             17.7              9.59
##  2            -1.61            4.48             17.7              9.59
##  3             0.88            6.96             18.4             12.9 
##  4            -3.26           -1.15              8                3.28
##  5            -3.26           -1.15              8                3.28
##  6            -3.06           -0.89              8.5              3.40
##  7            -3.42           -1.08              8.10             2.99
##  8            -3.42           -1.08              8.10             2.99
##  9            -3.09           -0.220             8.40             3.44
## 10            -3.15           -0.660             7.5              5.05
## # … with 2,651 more rows, and 18 more variables: Juice_Scan_530nm <dbl>,
## #   Juice_Scan_587nm <dbl>, Juice_Scan_632nm <dbl>, Juice_Scan_850nm <dbl>,
## #   Juice_Scan_880nm <dbl>, Juice_Scan_940nm <dbl>, Surface_Scan_365nm <dbl>,
## #   Surface_Scan_386nm <dbl>, Surface_Scan_450nm <dbl>,
## #   Surface_Scan_500nm <dbl>, Surface_Scan_530nm <dbl>,
## #   Surface_Scan_587nm <dbl>, Surface_Scan_632nm <dbl>,
## #   Surface_Scan_850nm <dbl>, Surface_Scan_880nm <dbl>,
## #   Surface_Scan_940nm <dbl>, Polyphenols <dbl>, Type <fct>
```



 r.squared   adj.r.squared      sigma   statistic   p.value   df
----------  --------------  ---------  ----------  --------  ---
 0.6682478       0.6649731   89.73684    204.0639         0   27

![](dataAnalysis_files/figure-html/LM-1.png)<!-- -->

This graphic shows a clear clusterization. This is raises naturally a hypothesis: each product belongs to a precise domain given its Polyphenols content and it's not unreasonable to try to classify them through a function over the available spectra. As the clusterization appears to happen above all by species, it would make great sense to take the variable idicating the produce species, "Type" and model considering it.

## ANCOVA Model, produce type as factor

![](dataAnalysis_files/figure-html/anova-1.png)<!-- -->![](dataAnalysis_files/figure-html/anova-2.png)<!-- -->![](dataAnalysis_files/figure-html/anova-3.png)<!-- -->

 r.squared   adj.r.squared      sigma   statistic   p.value    df
----------  --------------  ---------  ----------  --------  ----
 0.8207217       0.8105363   67.48294     80.5778         0   144

This graphic shows a quite prommising pattern. While we need to evaluate this through cross validation, we still haven't used any refinements as robust methods.

Apparently transforming the Polyphenols value should also "straighten" the model and give us some additional fitting.



# Models Over Different sub Datasets for Every Kind of Produce for Antioxidants and Polyphenols content.


For the sake of completeness and to be able to spot different behaviours, we are going to try naïve linear models over the whole dataset for every produce type. Each one would be modeled with different subsets of explaining variables: First separated sets of Surface and Juice OurSci spectra, then both sets, the same partition with SWIR scans, all the SWIR data, all SWIR scans on juice, all SWIR scans over the surface and finally the whole spectra availabe. 

We are going to proceed later with more sofisticated approaches in order to mitigate the problemas that preditably we are going to see with these combinations (if we model by produce type, oftentimes we are going to end up with more variables than samples). Probably we are dealing with a considerable amount of atypical data, also.

## Some general Details

* Our first target is to **predict Polyphenols and Antioxidants contents** with a reasonable and deployable model.

* We are going to use k-folding over our eficiency measures, with $K=15$. Those measures will be identified with the `CV` prefix. This means that for a certain produce category we are randomly subsampling into 15 subsets and every subset will be used in a train/test scheme against the remaining data for each subsequent trainig iteration. The metrics will be recorded on every measure and averaged, giving us a cross validated measure. This tipically increases strongly overfit immunity. Notice that for some models the metric is allways cross validated, so no `CV** prefix is added.

** Cross Validation should be carried out in such a way that repeated measures over the same ID always end up on the same set, be it train or test. Otherwise we'll get exagerated precision measures due to the existence of "almost" the same points on train and test sets.

* We are using succesive subsets of available variables in order to see where the more explicative power lies. These are pointed out in the `datasets` variable. To synthesize its contents: 10 OurSci wavelengths ( Our Sci reflectometer ) and 144 SWIR wavelengths ( siware device ) are available, iterated over juice and tissues of every subject. Besides, we will add a small amount of easily available metadata. Model are generated for every produce category, at least in this stage.

* We are not going to deal with NA's and *other* type produces, because there are insufficient meassures.

* The R objects available in the repo contain fundamental metrics of every model, but not the models, see below.

### Compositions of Evaluated Datasets


name                     description                                                         
-----------------------  --------------------------------------------------------------------
metadataDestructive      Brix,Source,Color                                                   
metadataNonDestructive   Source,Color                                                        
uvJuice                  ourSci's reflectometer on juice                                     
uvSurface                ourSci's reflectometer on surface                                   
uvSurfaceMeta            ourSci's reflectometer on surface and non destructive metadata.     
uvJuiceMeta              ourSci's reflectometer on juice and destructive metadata.           
uvAll                    ourSci in surface and juice,Brix,Source,Color                       
siwareJuice              SIWARE on juice                                                     
siwareSurface            SIWARE on surface                                                   
siwareSurfaceMetadata    SIWARE on surface and non destructive metadata.                     
siwareJuiceMetadata      SIWARE on juice and destructive metadata.                           
siwareAll                SIWARE on juice,SIWARE on surface,Brix,Source,Color                 
juiceAll                 SIWARE and OurSci measures over juice,Brix,Source,Color             
surfaceAll               SIWARE and OurSci measures over juice,Brix,Source,Color             
all                      SIWARE and OurSci measures over juice and surface,Brix,Source,Color 

### Results reproduction.

As resulting model tables are extremely heavy (in the order of 1/2GB each) code is available to generate them but they are not being uploaded into the repository. We are making the code available to reproduce our results, as for now it is inside this document. It is comented and replaced by reading versions of trimmed down objects that still contain most useful information. It will be soon moved to avoid the need of comenting/uncomenting after every re evaluation. To give a sense of the computational cost, it took 6 hours of uniterrupted work in an i7 3rd generation 4 core computer with 8gb of ram, on a fairly optimized linux distribution working exclusively over the modelling. As multi threading is implemented over the modelling process, this times starts to decrease sharply with increases in computing power. Anyone interested in the list of models can ask us for the complete set of models if it is more convenient than reproducing them.

### Relevant R objects Involved

In order to obtain a balace between a lightweight document and a resonably deep information available, objects are constantly mutated and trimmed.
For every subsection we are going to model over all defined sub datasets over every kind of produce and obtain a resulting "all" file. For *Polyphenols* random forest regression this would be `allPolyTree.Rds`. Relevant model information is subtracted from the model call into separate columns and the heavywiehgt column of model calls is removed, leading to the `lightweight` version of the same file, por example `lightPolyTree.Rds`. This is the file that is called in the making of the brief, columns are still being added as needed to allow for a better understanging of results. 

Finally, we are making a table with the five models that inform a better CV metric for every produce type and printing it. For deeper inspection, we recommend loading the `lightweight` objects or getting/generating the `all` object.

## Polyphenols

We are going to start with linear models and a simple random forest model. Random forest will be used both for regression and for classification. For classification: we will consider a vegetable is in the "High" category when it belongs to its superior quartile, reserving the "Low" category for the first quartile and the remanining two cuartiles for the "Medium" products.

### Linear Model





produce         dataset                cvRsquared
--------------  --------------------  -----------
grape           siwareJuiceMetadata        0.8052
grape           siwareAll                  0.7225
grape           all                        0.6950
spinach         metadataDestructive        0.5508
spinach         uvSurface                  0.5232
spinach         uvAll                      0.4770
carrot          uvAll                      0.4465
kale            uvJuiceMeta                0.4189
carrot          uvJuiceMeta                0.4163
kale            uvJuice                    0.4093
kale            uvAll                      0.4093
carrot          uvJuice                    0.3984
lettuce         uvJuice                    0.3321
lettuce         uvJuiceMeta                0.2711
cherry_tomato   siwareJuiceMetadata        0.2440
cherry_tomato   siwareAll                  0.2422
lettuce         metadataDestructive        0.2415
cherry_tomato   siwareJuice                0.2392



This table speaks volumes about the posibilities a naïve approach has of reaching a good fit and the consistent effect of overfitting given the generous number of available variables. We could also point out that it is showing how effective cross validation is in stripping our metrics of overfit bias.
A forward backward selection could probably give a benefit, especially in the case of grapes, which suffer the lack of a strong sample total.

#### Some Fit Analysis

Let's see some plots describing the fit behaviour for the most performing set , `uvJuice`.

![](dataAnalysis_files/figure-html/LM analysis-1.png)<!-- -->

Some homoskedasticity and a somewhat curved trajectory appears to emerge from this analysis, so we are going to search for a suitable variance reduction transformation.

#### Variance Stabilizing Transformation.

  We are going to search for a suitable power in an attempt to *straighten* the path our residuals seem to imply. The model will use the `uvJuice` dataset. This spectra is the more relevant to our ends because it measures the actual predictive power of our device and it has shown a better potential than `uvAll` in models we show in subsequent sections.
  We are not searching for a very precise optimization and with are not using a recursive function because the model should absorb any imprecisions in the calculation after a reasonable precision and because at the moment we do not have a chemical/phisical model justifying a belief in the existence of an actual, precise constant to search for, even if this could be a promising branch, worthy of investigation.

![](dataAnalysis_files/figure-html/power-1.png)<!-- -->

#### Regression over transformed variable

Apparently, our transformation does not have a substantial positive impact.
On our original QQ plot, the bigger Polyphenols content data was curved, this now has been fixed but we introduced the same pattern over the data with a lesser Polyphenols content (which was to be expected, on a second tought). The impact over `cvRsquared` is almost minimal.


Even though it would seem possible to choose between precision over prediction on high level contento or low level content, this is not really true as the sections clearly belong to different produce types. We could try to search one power for each prodce type also, but this line of investigation does not seem to be very promissing and there are others which appear to show more benefit with less unreasonable procedures.

![](dataAnalysis_files/figure-html/transformedLM-1.png)<!-- -->

produce         dataset                   cvRsquared
--------------  -----------------------  -----------
carrot          uvAll                         0.4377
carrot          uvJuiceMeta                   0.4070
carrot          uvSurfaceMeta                 0.3849
carrot          metadataNonDestructive        0.3760
carrot          uvJuice                       0.3524
cherry_tomato   juiceAll                      0.2927
cherry_tomato   all                           0.2449
cherry_tomato   siwareAll                     0.2328
cherry_tomato   surfaceAll                    0.2128
cherry_tomato   uvAll                         0.1897
grape           siwareJuiceMetadata           0.8549
grape           juiceAll                      0.8374
grape           siwareJuice                   0.7706
grape           metadataDestructive           0.7103
grape           uvJuice                       0.6127
kale            uvAll                         0.5277
kale            uvJuiceMeta                   0.4523
kale            uvJuice                       0.3787
kale            siwareAll                     0.2358
kale            siwareJuiceMetadata           0.2053
lettuce         uvJuiceMeta                   0.3923
lettuce         uvJuice                       0.3524
lettuce         uvAll                         0.2656
lettuce         surfaceAll                    0.2255
lettuce         siwareJuiceMetadata           0.1946
spinach         uvSurfaceMeta                 0.6297
spinach         all                           0.6183
spinach         uvAll                         0.4563
spinach         siwareJuiceMetadata           0.4515
spinach         uvSurface                     0.4433

#### Robust Regression

This linear model variant attemps to identify high leverage points in the dataset and give them a reduced weight in order to reduce their dragging effect over the general model, that is, it attempts to be robust against outliers through the calibration of special meand and variance measures, keeping the properties of regular lineal models.


![](dataAnalysis_files/figure-html/rob-1.png)<!-- -->

This function is not implemented over the framework we are using for cross validantion, but it is promising. The average error observed in our first graphic in this section is 2.1651323\times 10^{4} while in this last graphic it is 1.7951633\times 10^{4}.

An additional advantage of fitting this model is that we are finding outliers and it opens another way of exploration: we can use the model's weights over the dataset in other models, to use them in a robust fashion.


### Random Forest Regression



#### Performance


produce         dataset                   cvRsquared   mtry
--------------  -----------------------  -----------  -----
carrot          uvSurfaceMeta              0.5214296     12
carrot          uvAll                      0.5015027     12
carrot          metadataNonDestructive     0.4834262     12
carrot          uvSurface                  0.4781164      3
carrot          uvJuiceMeta                0.4391137      9
cherry_tomato   all                        0.4491917     23
cherry_tomato   siwareJuice                0.3690175      4
cherry_tomato   juiceAll                   0.3618634     30
cherry_tomato   siwareJuiceMetadata        0.3303351     38
cherry_tomato   uvAll                      0.2737234     18
grape           siwareJuiceMetadata        0.9998959     40
grape           juiceAll                   0.9635863     38
grape           siwareJuice                0.7582965     32
grape           all                        0.7301030     28
grape           metadataDestructive        0.7219916      5
kale            uvJuiceMeta                0.4777007     29
kale            uvAll                      0.3563686     25
kale            metadataDestructive        0.3321955      6
kale            siwareJuiceMetadata        0.3262857      1
kale            uvJuice                    0.3024275      4
lettuce         all                        0.3357903     33
lettuce         uvJuice                    0.3076625     12
lettuce         siwareAll                  0.2946972     16
lettuce         juiceAll                   0.2917412     20
lettuce         metadataNonDestructive     0.2864786     13
spinach         uvAll                      0.6689466      3
spinach         siwareJuiceMetadata        0.6342874     27
spinach         juiceAll                   0.5657848      3
spinach         uvJuiceMeta                0.5642101      6
spinach         uvJuice                    0.5507561      3

##### Random Forest Regression Over Tomato ( Worst Type for this Regression Family ), OurSci Juice Measures.



### Random Forest Classification by Quartiles

#### Available Metrics

If you run this R session (or plainly load the `allPolyTreeC.Rds` object) some important model information is available, especially the confusion matrix and error amounts by category on every model.








produce         dataset                    Accuracy   mtry
--------------  -----------------------  ----------  -----
carrot          uvJuiceMeta               0.5060194     22
carrot          uvAll                     0.4863942      8
carrot          uvSurfaceMeta             0.4842515     11
carrot          juiceAll                  0.4396800     39
carrot          metadataNonDestructive    0.4360728      9
cherry_tomato   juiceAll                  0.5092745     13
cherry_tomato   all                       0.4849131      2
cherry_tomato   siwareJuice               0.4579504     26
cherry_tomato   uvJuiceMeta               0.4366434     39
cherry_tomato   uvJuice                   0.4286607     35
grape           uvAll                     0.5784014     11
grape           juiceAll                  0.5000000     27
grape           uvSurfaceMeta             0.4955499     39
grape           all                       0.4888889     31
grape           metadataNonDestructive    0.4729978     39
kale            uvJuiceMeta               0.4976436      8
kale            uvAll                     0.4848673      7
kale            surfaceAll                0.4608724     30
kale            metadataDestructive       0.4386894     13
kale            uvSurfaceMeta             0.4162327     23
lettuce         surfaceAll                0.4651660     39
lettuce         uvAll                     0.4405469     24
lettuce         uvSurface                 0.3803240     31
lettuce         uvJuice                   0.3800775      1
lettuce         all                       0.3699743     25
spinach         uvJuiceMeta               0.5689064     39
spinach         siwareJuiceMetadata       0.5355429     31
spinach         juiceAll                  0.5018593     28
spinach         uvAll                     0.4827734      6
spinach         uvJuice                   0.4471796     27









## Antioxidants

### Random Forest Regression

Details are given in this section's preamble and in the equivalent evaluation over **Polyphenols**.





produce         dataset                   cvRsquared   mtry
--------------  -----------------------  -----------  -----
carrot          uvJuiceMeta                0.3500208     30
carrot          uvSurfaceMeta              0.3047597      3
carrot          metadataNonDestructive     0.2991036     12
carrot          uvAll                      0.2843664      1
carrot          juiceAll                   0.2771388     39
cherry_tomato   uvAll                      0.3175554     10
cherry_tomato   all                        0.2965751     40
cherry_tomato   juiceAll                   0.2792140     36
cherry_tomato   siwareJuiceMetadata        0.2785688     10
cherry_tomato   uvSurface                  0.2481747      8
grape           siwareJuice                0.8590628     36
grape           juiceAll                   0.8571438      3
grape           metadataDestructive        0.7015045     32
grape           all                        0.6760221     39
grape           uvSurfaceMeta              0.6213032      3
kale            juiceAll                   0.3003694     20
kale            all                        0.2983841     34
kale            siwareJuiceMetadata        0.2976557     13
kale            siwareJuice                0.2348188      1
kale            siwareAll                  0.2288321     27
lettuce         juiceAll                   0.3688783     37
lettuce         all                        0.3071553     40
lettuce         siwareJuiceMetadata        0.2875201      1
lettuce         uvJuiceMeta                0.2820167     18
lettuce         uvAll                      0.2814727     13
spinach         siwareJuiceMetadata        0.7218507     17
spinach         juiceAll                   0.6802400     20
spinach         uvJuiceMeta                0.6277774     40
spinach         all                        0.6114255     39
spinach         uvJuice                    0.5098761      2

We can see a simmilar goodness of fit as with Polyphenols and the model again appears to be useful for our needs.

### Classification by Quartiles





produce         dataset                    Accuracy   mtry
--------------  -----------------------  ----------  -----
carrot          uvSurfaceMeta             0.5178866      1
carrot          metadataDestructive       0.5124680      8
carrot          uvJuiceMeta               0.5048112      4
carrot          metadataNonDestructive    0.5038126     30
carrot          uvAll                     0.4997563      1
cherry_tomato   uvSurface                 0.5374064     40
cherry_tomato   juiceAll                  0.5362960     27
cherry_tomato   uvJuiceMeta               0.5278610      5
cherry_tomato   uvSurfaceMeta             0.5192876      9
cherry_tomato   uvAll                     0.5082010      3
grape           uvSurfaceMeta             0.6222964      8
grape           metadataDestructive       0.5976500     15
grape           all                       0.5678571     20
grape           uvAll                     0.5631674     17
grape           metadataNonDestructive    0.5574907     12
kale            metadataDestructive       0.5844488     30
kale            all                       0.5617609      1
kale            uvJuiceMeta               0.5551644      5
kale            siwareAll                 0.5537894      1
kale            juiceAll                  0.5523850      1
lettuce         uvAll                     0.5512757      5
lettuce         uvSurfaceMeta             0.5213654     39
lettuce         uvJuice                   0.5095734      4
lettuce         metadataNonDestructive    0.5012248      1
lettuce         juiceAll                  0.4991817     26
spinach         metadataNonDestructive    0.6084524     14
spinach         uvAll                     0.5951661      5
spinach         surfaceAll                0.5736706     30
spinach         uvSurfaceMeta             0.5644045      5
spinach         metadataDestructive       0.5137127      4

## Classifications by Three Categories.

Intending to increase predictive power, we've trained new models, classifying this time with three categories. For quartiles, this means that *inferior* is the first, *superior* the fourth one and te second and third are *middle*.
This is exposed with some additional depth in the brief `classification` on the repository. We've also attemped adding the *dry mass percentage* variable, it didn't yield an increase big enough to justify the big reduction in immediacy it implies ( exposed in the brief `classification2` ).

### Polyphenols


#### Performance



produce         dataset                   Accuracy   mtry
--------------  ----------------------  ----------  -----
carrot          uvAll                    0.6664174     27
carrot          siwareAll                0.6574250      1
carrot          juiceAll                 0.6558981     27
carrot          uvJuiceMeta              0.6528664      9
carrot          uvJuice                  0.6221741      2
cherry_tomato   juiceAll                 0.6486319     37
cherry_tomato   uvAll                    0.6397163     30
cherry_tomato   uvJuiceMeta              0.6172861     14
cherry_tomato   siwareAll                0.6097437      1
cherry_tomato   all                      0.6071207     35
grape           surfaceAll               0.8367347     38
grape           all                      0.6948052     23
grape           metadataDestructive      0.6848901     21
grape           uvAll                    0.6693901     20
grape           uvSurfaceMeta            0.6656463      4
kale            uvAll                    0.6790410     40
kale            uvJuiceMeta              0.6602730     29
kale            surfaceAll               0.6201475      1
kale            siwareSurfaceMetadata    0.5954380      1
kale            uvSurfaceMeta            0.5906913     11
lettuce         surfaceAll               0.6126742     39
lettuce         uvAll                    0.5833548      7
lettuce         uvSurfaceMeta            0.5689824     17
lettuce         uvJuiceMeta              0.5671527     14
lettuce         all                      0.5636952      8
spinach         siwareJuiceMetadata      0.7086350     19
spinach         uvJuiceMeta              0.7029035      8
spinach         uvAll                    0.6769116      5
spinach         siwareAll                0.6253307     30
spinach         siwareSurfaceMetadata    0.6201401      2

#### One of the Confusion Matrices

The new formulation has resulted in a reasonable increase over precission.

![](dataAnalysis_files/figure-html/poly quartiles spinach matrix-1.png)<!-- -->

#### Classification by Quintiles



##### Performance


produce         dataset                    Accuracy   mtry
--------------  -----------------------  ----------  -----
carrot          uvSurfaceMeta             0.5970325     24
carrot          uvAll                     0.5778888     14
carrot          uvJuiceMeta               0.5755232      8
carrot          uvSurface                 0.5592316      1
carrot          metadataDestructive       0.5350462     37
cherry_tomato   metadataDestructive       0.6137989     16
cherry_tomato   uvAll                     0.5927626     26
cherry_tomato   all                       0.5784074     24
cherry_tomato   uvJuice                   0.5698306     30
cherry_tomato   juiceAll                  0.5584880     35
grape           surfaceAll                0.8851371     35
grape           siwareSurfaceMetadata     0.7499389     36
grape           metadataDestructive       0.6534687     16
grape           uvAll                     0.6396651      4
grape           uvSurface                 0.6125731      1
kale            uvJuice                   0.6236365      1
kale            uvJuiceMeta               0.6167088     11
kale            uvAll                     0.6084472     38
kale            juiceAll                  0.5835787      2
kale            metadataDestructive       0.5626971     19
lettuce         uvAll                     0.6235495     16
lettuce         surfaceAll                0.5612913     36
lettuce         juiceAll                  0.5537247     28
lettuce         uvJuiceMeta               0.5515567      5
lettuce         metadataNonDestructive    0.5400300     39
spinach         uvJuice                   0.6265044      8
spinach         metadataDestructive       0.5812356     25
spinach         uvJuiceMeta               0.5660859     21
spinach         surfaceAll                0.5641680     30
spinach         uvSurface                 0.5322198     19

##### One of the Confusion Matrices ( Carrot, `uvJuice` )

![](dataAnalysis_files/figure-html/poly quintiles carrot matrix-1.png)<!-- -->

### Antioxidants

#### Quartiles



##### Performance


produce         dataset                    Accuracy   mtry
--------------  -----------------------  ----------  -----
carrot          uvSurfaceMeta             0.5178866      1
carrot          metadataDestructive       0.5124680      8
carrot          uvJuiceMeta               0.5048112      4
carrot          metadataNonDestructive    0.5038126     30
carrot          uvAll                     0.4997563      1
cherry_tomato   uvSurface                 0.5374064     40
cherry_tomato   juiceAll                  0.5362960     27
cherry_tomato   uvJuiceMeta               0.5278610      5
cherry_tomato   uvSurfaceMeta             0.5192876      9
cherry_tomato   uvAll                     0.5082010      3
grape           uvSurfaceMeta             0.6222964      8
grape           metadataDestructive       0.5976500     15
grape           all                       0.5678571     20
grape           uvAll                     0.5631674     17
grape           metadataNonDestructive    0.5574907     12
kale            metadataDestructive       0.5844488     30
kale            all                       0.5617609      1
kale            uvJuiceMeta               0.5551644      5
kale            siwareAll                 0.5537894      1
kale            juiceAll                  0.5523850      1
lettuce         uvAll                     0.5512757      5
lettuce         uvSurfaceMeta             0.5213654     39
lettuce         uvJuice                   0.5095734      4
lettuce         metadataNonDestructive    0.5012248      1
lettuce         juiceAll                  0.4991817     26
spinach         metadataNonDestructive    0.6084524     14
spinach         uvAll                     0.5951661      5
spinach         surfaceAll                0.5736706     30
spinach         uvSurfaceMeta             0.5644045      5
spinach         metadataDestructive       0.5137127      4

##### One of the Confusion Matrices ( Cherry Tomatto, `uvAll` )



#### Quintiles



#### Performance


produce         dataset                    Accuracy   mtry
--------------  -----------------------  ----------  -----
carrot          metadataNonDestructive    0.4900412     28
carrot          uvAll                     0.4840729     39
carrot          uvJuiceMeta               0.4727949      9
carrot          metadataDestructive       0.4710043      5
carrot          uvSurfaceMeta             0.4644501      6
cherry_tomato   siwareJuiceMetadata       0.5141440     39
cherry_tomato   uvSurface                 0.5141311     25
cherry_tomato   juiceAll                  0.5123476     22
cherry_tomato   uvAll                     0.5036097     38
cherry_tomato   metadataDestructive       0.5021831      6
grape           uvAll                     0.5769900     19
grape           siwareJuice               0.5764791     20
grape           all                       0.5536616     15
grape           uvJuiceMeta               0.5342602      3
grape           uvJuice                   0.5308233      8
kale            uvJuice                   0.5215696      1
kale            metadataDestructive       0.5107264     16
kale            uvSurfaceMeta             0.4843289     21
kale            uvAll                     0.4813814     31
kale            juiceAll                  0.4778268     38
lettuce         uvAll                     0.5557431     17
lettuce         uvSurfaceMeta             0.5398689     40
lettuce         metadataDestructive       0.5213786      6
lettuce         uvSurface                 0.5208221     18
lettuce         uvJuice                   0.5179952      6
spinach         juiceAll                  0.5336027     38
spinach         siwareAll                 0.5041209     27
spinach         metadataDestructive       0.4842716      9
spinach         siwareJuiceMetadata       0.4673759     13
spinach         uvSurface                 0.3994750      5

#### One of the Confusion Matrices ( Grape, `uvJuice` )

![](dataAnalysis_files/figure-html/poly quintiles grape matrix-1.png)<!-- -->


## Variable Importance in Forest Models


We'll measure importance metrics for our forest, profiting from the already calibrated *mtry* hyperparameters. In this new calculations, we'll also profit from our finer outlier filtering, so we expect to see better fits.







### Importance Metric



This metric illustrates the weight a variable does have over the predictive power of a model. It is obtained by replacing the variable by noise ( via a randomization of its existent values over the existent points ) and measuring the difference in predictive power for the model ( the *pseudo*$R^2$ ). It is **proper of random forest models**.

It's worth consideration that not all models have a reasonable $R^2$ and for that reason they are less indicative. What we've done, nonetheless, is adding the importance matrices of all similar models and checked at the 20 most important variables for each class of models. As with the other procedures, our functions and objects are available for deeper insight.

* On some of the tables, the amount of variables is overwhelming. This graphics are vectorized, so you can zoom in until the details you need to read are clear.

#### For Classification



##### Antioxidants


```
## [[1]]
```

![](dataAnalysis_files/figure-html/importance class antiox-1.png)<!-- -->

```
## 
## [[2]]
```

![](dataAnalysis_files/figure-html/importance class antiox-2.png)<!-- -->

```
## 
## [[3]]
```

![](dataAnalysis_files/figure-html/importance class antiox-3.png)<!-- -->

```
## 
## [[4]]
```

![](dataAnalysis_files/figure-html/importance class antiox-4.png)<!-- -->

```
## 
## [[5]]
```

![](dataAnalysis_files/figure-html/importance class antiox-5.png)<!-- -->

```
## 
## [[6]]
```

![](dataAnalysis_files/figure-html/importance class antiox-6.png)<!-- -->

```
## 
## [[7]]
```

![](dataAnalysis_files/figure-html/importance class antiox-7.png)<!-- -->

```
## 
## [[8]]
```

![](dataAnalysis_files/figure-html/importance class antiox-8.png)<!-- -->

```
## 
## [[9]]
```

![](dataAnalysis_files/figure-html/importance class antiox-9.png)<!-- -->

```
## 
## [[10]]
```

![](dataAnalysis_files/figure-html/importance class antiox-10.png)<!-- -->

```
## 
## [[11]]
```

![](dataAnalysis_files/figure-html/importance class antiox-11.png)<!-- -->

```
## 
## [[12]]
```

![](dataAnalysis_files/figure-html/importance class antiox-12.png)<!-- -->

```
## 
## [[13]]
```

![](dataAnalysis_files/figure-html/importance class antiox-13.png)<!-- -->

```
## 
## [[14]]
```

![](dataAnalysis_files/figure-html/importance class antiox-14.png)<!-- -->

```
## 
## [[15]]
```

![](dataAnalysis_files/figure-html/importance class antiox-15.png)<!-- -->

##### Polyphenols


```
## [[1]]
```

![](dataAnalysis_files/figure-html/importance class poly-1.png)<!-- -->

```
## 
## [[2]]
```

![](dataAnalysis_files/figure-html/importance class poly-2.png)<!-- -->

```
## 
## [[3]]
```

![](dataAnalysis_files/figure-html/importance class poly-3.png)<!-- -->

```
## 
## [[4]]
```

![](dataAnalysis_files/figure-html/importance class poly-4.png)<!-- -->

```
## 
## [[5]]
```

![](dataAnalysis_files/figure-html/importance class poly-5.png)<!-- -->

```
## 
## [[6]]
```

![](dataAnalysis_files/figure-html/importance class poly-6.png)<!-- -->

```
## 
## [[7]]
```

![](dataAnalysis_files/figure-html/importance class poly-7.png)<!-- -->

```
## 
## [[8]]
```

![](dataAnalysis_files/figure-html/importance class poly-8.png)<!-- -->

```
## 
## [[9]]
```

![](dataAnalysis_files/figure-html/importance class poly-9.png)<!-- -->

```
## 
## [[10]]
```

![](dataAnalysis_files/figure-html/importance class poly-10.png)<!-- -->

```
## 
## [[11]]
```

![](dataAnalysis_files/figure-html/importance class poly-11.png)<!-- -->

```
## 
## [[12]]
```

![](dataAnalysis_files/figure-html/importance class poly-12.png)<!-- -->

```
## 
## [[13]]
```

![](dataAnalysis_files/figure-html/importance class poly-13.png)<!-- -->

```
## 
## [[14]]
```

![](dataAnalysis_files/figure-html/importance class poly-14.png)<!-- -->

```
## 
## [[15]]
```

![](dataAnalysis_files/figure-html/importance class poly-15.png)<!-- -->

#### For Regression



##### Antioxidants


```
## [[1]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-1.png)<!-- -->

```
## 
## [[2]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-2.png)<!-- -->

```
## 
## [[3]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-3.png)<!-- -->

```
## 
## [[4]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-4.png)<!-- -->

```
## 
## [[5]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-5.png)<!-- -->

```
## 
## [[6]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-6.png)<!-- -->

```
## 
## [[7]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-7.png)<!-- -->

```
## 
## [[8]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-8.png)<!-- -->

```
## 
## [[9]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-9.png)<!-- -->

```
## 
## [[10]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-10.png)<!-- -->

```
## 
## [[11]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-11.png)<!-- -->

```
## 
## [[12]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-12.png)<!-- -->

```
## 
## [[13]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-13.png)<!-- -->

```
## 
## [[14]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-14.png)<!-- -->

```
## 
## [[15]]
```

![](dataAnalysis_files/figure-html/importance reg antiiox-15.png)<!-- -->

##### Polyphenols


```
## [[1]]
```

![](dataAnalysis_files/figure-html/importance reg poly-1.png)<!-- -->

```
## 
## [[2]]
```

![](dataAnalysis_files/figure-html/importance reg poly-2.png)<!-- -->

```
## 
## [[3]]
```

![](dataAnalysis_files/figure-html/importance reg poly-3.png)<!-- -->

```
## 
## [[4]]
```

![](dataAnalysis_files/figure-html/importance reg poly-4.png)<!-- -->

```
## 
## [[5]]
```

![](dataAnalysis_files/figure-html/importance reg poly-5.png)<!-- -->

```
## 
## [[6]]
```

![](dataAnalysis_files/figure-html/importance reg poly-6.png)<!-- -->

```
## 
## [[7]]
```

![](dataAnalysis_files/figure-html/importance reg poly-7.png)<!-- -->

```
## 
## [[8]]
```

![](dataAnalysis_files/figure-html/importance reg poly-8.png)<!-- -->

```
## 
## [[9]]
```

![](dataAnalysis_files/figure-html/importance reg poly-9.png)<!-- -->

```
## 
## [[10]]
```

![](dataAnalysis_files/figure-html/importance reg poly-10.png)<!-- -->

```
## 
## [[11]]
```

![](dataAnalysis_files/figure-html/importance reg poly-11.png)<!-- -->

```
## 
## [[12]]
```

![](dataAnalysis_files/figure-html/importance reg poly-12.png)<!-- -->

```
## 
## [[13]]
```

![](dataAnalysis_files/figure-html/importance reg poly-13.png)<!-- -->

```
## 
## [[14]]
```

![](dataAnalysis_files/figure-html/importance reg poly-14.png)<!-- -->

```
## 
## [[15]]
```

![](dataAnalysis_files/figure-html/importance reg poly-15.png)<!-- -->







# Comparisons Between Different Approaches Represented on the Available Data




## Performance Metrics for Antioxidants

### Regression


Table: Comparison of $R^2$ on antioxidants regressions.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
Bionutrient Meter Juice with Metadata      0.3500208       0.1590510   0.4513737   0.1948336   0.2820167   0.6277774
Bionutrient Meter Surface with Metadata    0.3047597       0.2197297   0.6213032   0.1525485   0.2218791   0.3793798
Non Destructive Sampling Metadata          0.2991036       0.1213982   0.5736808   0.1788969   0.1752343   0.4262921
Bionutrient Meter All                      0.2843664       0.3175554   0.5633048   0.1899187   0.2814727   0.4089084
All Juice                                  0.2771388       0.2792140   0.8571438   0.3003694   0.3688783   0.6802400
Bionutrient Meter Juice                    0.2764643       0.1170934   0.5205811   0.1443282   0.2000910   0.5098761
All surface                                0.2617546       0.1403095   0.5252477   0.1017291   0.2100306   0.3314751
Bionutrient Meter Surface                  0.2483645       0.2481747   0.6146406   0.1084877   0.1903008   0.2977075
SIWARE Juice with metadata                 0.2418163       0.2785688   0.5933140   0.2976557   0.2875201   0.7218507
All                                        0.2266418       0.2965751   0.6760221   0.2983841   0.3071553   0.6114255
Destructive Sampling Metadata              0.2153454       0.0748488   0.7015045   0.2149689   0.1808002   0.3454319
SIWARE Surface with Metadata               0.1951680       0.1268448   0.3518265   0.0721072   0.1171734   0.1343722
SIWARE All                                 0.1911287       0.1840444   0.6112810   0.2288321   0.1759849   0.5006950
SIWARE Juice                               0.1173631       0.2007674   0.8590628   0.2348188   0.1699274   0.3986818
SIWARE Surface                             0.0742549       0.0681423   0.4269459   0.0893326   0.1249239   0.2933898

### Classification

#### Quartiles


Table: Comparison of *Accuracy* on antioxidants classifications over quartiles.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
All                                        0.3954192       0.4749108   0.5678571   0.5617609   0.4536064   0.2749739
All Juice                                  0.4642596       0.5362960   0.4353175   0.5523850   0.4991817   0.3465963
Destructive Sampling Metadata              0.5124680       0.5039437   0.5976500   0.5844488   0.4891610   0.5137127
Non Destructive Sampling Metadata          0.5038126       0.4986628   0.5574907   0.5004417   0.5012248   0.6084524
SIWARE All                                 0.4392522       0.4837255   0.4742063   0.5537894   0.4828434   0.3925235
SIWARE Juice                               0.3742305       0.3421501   0.4693878   0.4374952   0.3457454   0.3196995
SIWARE Juice with metadata                 0.4070878       0.4375914   0.4888889   0.5460224   0.4328120   0.4314103
SIWARE Surface                             0.3865813       0.4672180   0.3959184   0.4055054   0.3381671   0.3187547
SIWARE Surface with Metadata               0.4615363       0.4898833   0.4064935   0.4994947   0.4748622   0.3912952
All surface                                0.4405584       0.5077892   0.5066616   0.4865889   0.4816840   0.5736706
Bionutrient Meter All                      0.4997563       0.5082010   0.5631674   0.5147881   0.5512757   0.5951661
Bionutrient Meter Juice                    0.4947258       0.4419786   0.5535233   0.4209423   0.5095734   0.3765771
Bionutrient Meter Juice with Metadata      0.5048112       0.5278610   0.5115702   0.5551644   0.4881604   0.5115127
Bionutrient Meter Surface                  0.4568305       0.5374064   0.4502653   0.4752692   0.4955665   0.4743114
Bionutrient Meter Surface with Metadata    0.5178866       0.5192876   0.6222964   0.5480466   0.5213654   0.5644045

#### Quintiles


Table: Comparison of *Accuracy* on antioxidants classifications quintiles.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
All                                        0.3966133       0.4890726   0.5536616   0.4347313   0.4671666   0.3523702
All Juice                                  0.3939711       0.5123476   0.5139860   0.4778268   0.4669155   0.5336027
Destructive Sampling Metadata              0.4710043       0.5021831   0.4616669   0.5107264   0.5213786   0.4842716
Non Destructive Sampling Metadata          0.4900412       0.3653073   0.3022263   0.3741410   0.4974250   0.3415318
SIWARE All                                 0.3612629       0.4249575   0.2642857   0.3663967   0.4146885   0.5041209
SIWARE Juice                               0.3532021       0.4992106   0.5764791   0.3649300   0.3784980   0.3750954
SIWARE Juice with metadata                 0.4112725       0.5141440   0.5102041   0.3256366   0.4399968   0.4673759
SIWARE Surface                             0.3867197       0.4170663   0.4843407   0.4162896   0.3892605   0.3448798
SIWARE Surface with Metadata               0.4073796       0.4287031   0.4954545   0.4262586   0.4798555   0.3550908
All surface                                0.3848495       0.4953344   0.3149351   0.4321998   0.4978186   0.3508266
Bionutrient Meter All                      0.4840729       0.5036097   0.5769900   0.4813814   0.5557431   0.2677760
Bionutrient Meter Juice                    0.4638528       0.4034310   0.5308233   0.5215696   0.5179952   0.2651052
Bionutrient Meter Juice with Metadata      0.4727949       0.4397455   0.5342602   0.4361215   0.4924854   0.3673265
Bionutrient Meter Surface                  0.4451220       0.5141311   0.4849765   0.4536259   0.5208221   0.3994750
Bionutrient Meter Surface with Metadata    0.4644501       0.5019255   0.4519130   0.4843289   0.5398689   0.3680982


## Performance Metrics for Polyphenols

### Regression


Table: Comparison of $R^2$ on polyphenols regressions.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
Bionutrient Meter Surface with Metadata    0.5214296       0.1443646   0.6679886   0.1923684   0.1233476   0.4225643
Bionutrient Meter All                      0.5015027       0.2737234   0.5354057   0.3563686   0.2419329   0.6689466
Non Destructive Sampling Metadata          0.4834262       0.1587202   0.6364848   0.1933895   0.2864786   0.4315521
Bionutrient Meter Surface                  0.4781164       0.1579415   0.4703631   0.1578066   0.1428737   0.3454850
Bionutrient Meter Juice with Metadata      0.4391137       0.1914253   0.7172921   0.4777007   0.2841492   0.5642101
Bionutrient Meter Juice                    0.4167565       0.1348529   0.5715049   0.3024275   0.3076625   0.5507561
All Juice                                  0.4016292       0.3618634   0.9635863   0.2475141   0.2917412   0.5657848
All                                        0.3784202       0.4491917   0.7301030   0.1811991   0.3357903   0.4692730
Destructive Sampling Metadata              0.3718954       0.1536798   0.7219916   0.3321955   0.2500430   0.5208406
All surface                                0.3623358       0.2312569   0.7200521   0.2274496   0.1567126   0.4489586
SIWARE Surface with Metadata               0.3300232       0.2034998   0.4840920   0.1691373   0.1163485   0.1909401
SIWARE Juice with metadata                 0.2806565       0.3303351   0.9998959   0.3262857   0.2757474   0.6342874
SIWARE All                                 0.2309082       0.2498474   0.6741210   0.1564203   0.2946972   0.4459078
SIWARE Juice                               0.2133437       0.3690175   0.7582965   0.2977655   0.1156249   0.3964988
SIWARE Surface                             0.0983888       0.1776636   0.2816980   0.0722716   0.0696306   0.2671888

### Classification

#### Quartiles


Table: Comparison of *Accuracy* on polyphenols classifications over quartiles.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
All                                        0.5847087       0.6071207   0.6948052   0.5135740   0.5636952   0.6097667
All Juice                                  0.6558981       0.6486319   0.5428571   0.5727592   0.4665102   0.6184349
Destructive Sampling Metadata              0.6201473       0.5511626   0.6848901   0.5447413   0.5056293   0.5775844
Non Destructive Sampling Metadata          0.6134488       0.5227088   0.6188395   0.5462107   0.5198020   0.4331508
SIWARE All                                 0.6574250       0.6097437   0.6187500   0.4821065   0.4756225   0.6253307
SIWARE Juice                               0.5842459       0.5478642   0.5622449   0.4296768   0.4353798   0.5477453
SIWARE Juice with metadata                 0.5957572       0.5273153   0.5159524   0.5112910   0.4354886   0.7086350
SIWARE Surface                             0.5359296       0.5569412   0.5740767   0.5324302   0.4669078   0.6154296
SIWARE Surface with Metadata               0.6008588       0.5520810   0.5922336   0.5954380   0.5054915   0.6201401
All surface                                0.5686539       0.5909590   0.8367347   0.6201475   0.6126742   0.5895306
Bionutrient Meter All                      0.6664174       0.6397163   0.6693901   0.6790410   0.5833548   0.6769116
Bionutrient Meter Juice                    0.6221741       0.5827909   0.5769391   0.5534076   0.5512640   0.5874061
Bionutrient Meter Juice with Metadata      0.6528664       0.6172861   0.6510989   0.6602730   0.5671527   0.7029035
Bionutrient Meter Surface                  0.5311899       0.5847276   0.6473012   0.5556687   0.5159347   0.5782170
Bionutrient Meter Surface with Metadata    0.6219392       0.5641228   0.6656463   0.5906913   0.5689824   0.5334257

#### Quintiles


Table: Comparison of *Accuracy* on polyphenols classifications quintiles.

datasetDisplay                                carrot   cherry_tomato       grape        kale     lettuce     spinach
----------------------------------------  ----------  --------------  ----------  ----------  ----------  ----------
All                                        0.5094600       0.5784074   0.5234385   0.5416343   0.5335173   0.3687037
All Juice                                  0.5076252       0.5584880   0.5617914   0.5835787   0.5537247   0.3475970
Destructive Sampling Metadata              0.5350462       0.6137989   0.6534687   0.5626971   0.5329283   0.5812356
Non Destructive Sampling Metadata          0.4928668       0.3937005   0.5937976   0.5352830   0.5400300   0.4754756
SIWARE All                                 0.4876457       0.5335067   0.5798186   0.5043128   0.5122303   0.3508210
SIWARE Juice                               0.4459249       0.4473383   0.5291667   0.4859818   0.4941178   0.4567513
SIWARE Juice with metadata                 0.5193832       0.4605148   0.4138889   0.5328880   0.5146523   0.3700237
SIWARE Surface                             0.4434684       0.5107477   0.4658009   0.4754039   0.4443305   0.4688195
SIWARE Surface with Metadata               0.4522954       0.5284774   0.7499389   0.5152637   0.4795175   0.4469644
All surface                                0.5156067       0.5069479   0.8851371   0.5316238   0.5612913   0.5641680
Bionutrient Meter All                      0.5778888       0.5927626   0.6396651   0.6084472   0.6235495   0.5124291
Bionutrient Meter Juice                    0.5281055       0.5698306   0.5030679   0.6236365   0.4836891   0.6265044
Bionutrient Meter Juice with Metadata      0.5755232       0.5502190   0.6095238   0.6167088   0.5515567   0.5660859
Bionutrient Meter Surface                  0.5592316       0.5121343   0.6125731   0.5154190   0.4613932   0.5322198
Bionutrient Meter Surface with Metadata    0.5970325       0.5109897   0.5911713   0.5364066   0.5102568   0.5102932



```
## [1] "finished at"
```

```
## [1] "Sat Dec 12 00:47:50 2020"
```
