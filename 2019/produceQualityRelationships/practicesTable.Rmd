---
title: "Relationships Between Farm Practices and Nutritional Quality"
author: "Real Food Campaign"
date: "August 4, 2020"
output: html_document
---

# Introduction and Context

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 
  This table shows only thos practices and amendments for which at least one of the tests has yielded a relevant median shift compared to the median for the class of subjects which don't list any practices or amendments. On the ocasions were there wasn't strong enough evidence, the value has been quantized to 0 (as the null hypothesis on this tests is "There's a 0 median shift regarding the reference class").

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is inetersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting ones are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.

```{r input, echo=FALSE, warnings=FALSE}

options(knitr.table.format = "html")
library(tidyverse)
library(kableExtra)

fullColorPractices <- read_rds( path="./practicesShiftTableProduceNutrients.Rds" )
codeKey <- read_rds( path="./pValuesCodeKey.Rds" )

```

# Percentual Shift Over the Median Value for Farm Practices

## Color Scale

```{r codekey, echo=FALSE}
codeKey
```

## Median Shifts on Farm Practices

```{r practices , echo=FALSE}
fullColorPractices
```
