# Hi!


  I copied all the variables I'm using to the tabbles in the tagging process. I mostly check which ones are available and pick the first one that's not an NA, which seems to work. 
  
  `fullTaggingProcess` is the dataframe, reduced to the columns that seemed relevant to me. 

  I sent a table for the succesfully tagged entries (around 2200) and another one for the missing ones, (arround 700). Of this ones, 211 have `sample_source==farm_market` and 500 are from unidentified farms. 
  
  There's not a single sample were we have tagging information but ignore the `sample_source`, so probably those with `sample_source==NA` are beyond any hope of identification. They are not terribly abundant anyway and we already tagged 75% of the samples, which might be enough.
  
  I might attemt reimputation through comparison of practices, or perhaps from geolocation data. 


