# Real Food Campaign, 2019

## Description of this Repository

  We are sharing two assets:
  
  1. The Real Food Campaign dataset, which contains a detailed and deep analysis of 6 crops over hundreds of variables, including antioxidants and polyphenols contents, register of farm practices, soil amendments, several aditional quality variables and detailed spectroscopy measures taken with two different devices.
  2. Our ongoing adata analysis efforts over this dataset. A great deal of them are intended as a bootstrap to empower any interested investigator and for that reason we recommend giving at least a superficial look at what's on offer, it migh save you a great amount of time or provide you with new hypotheses.

## About The Dataset ( `dataset` folder )

This dataset (avavilable in the `dataset` folder, where an additional `README` file clarifies versioning ) exposes

  1. Spectroscopy data obtained with two different instruments, (a siware NIR and an open source matrix reflectometer designed by Our Sci). Both instruments have been applied on tissue and juice of vegetables.
  2. Polyphenols and Antioxidants content, which are our proposed produce quality indicative variables.
  3. Farm practices details.
  4. Soil amendments details.
  5. Detailed location information, including details about climate and soil.
  5. Tasting scales for three different variables.
  5. Detailed lab intermediate steps of every wet chemistry measure obtained.
  6. XRF analysis for several elements ( coming soon ).
  
The merging of the dataset is achieved in the `mergingProcess` folder, which contains all related scripts. 

## Where to collaborate and discuss

  We are very interested in your ideas, hypotheses or in knowing the results of your own investigations. Please, [ let's keep that feedback inside this forum ](http://forum.goatech.org/t/data-analysis-rfc-2019-dataset/852) so everything stays organized and public in the spirit of open knowledge.
  Please let us know if you looked at this work, any impression is useful to keep improving it.

## Data Analysis

This repo also includes our in house analysis efforts. If you've got any feedback about them, don't hesitate to reach us. 

### Regression ( `regressioBrief` folder )

  This folder covers our efforts to predict nutritional quality based on spectra.
  This is a complex task and an ongoing effort, but we've done a lot of work probably useful for other parties interested in continuing the analysis or doing related research. The actual outcome of that investigation is the file `dataAnalysis`, on the top level of this folder. As of July 2, 2020, the files have been reorganized. Do notice some of the outputs are huge and demand considerable calculation effort, so they are commented out and replaced by synthesized outputs which do not contain the actual models considered, just their relevant statistics. Recalculating relevant models should be very fast, considering we offer the parameters we have found via cross validation and k folding.
  On the folder you'll find other `Rmd` files, these contain side analysis we did and didn't communicate either because they were not relevant our because they are still on the works. If you are interested on any of those, don't hesitate to ask for better comenting and organization.
  
  Please, if you have feedback, new ideas or any relevant comment: we encourage open discussion on this thread in [GOAT forum](http://forum.goatech.org/t/data-analysis-rfc-2019-dataset-nutritional-quality-regressions-over-spectral-profiles/852/7)

### Produce Quality Relationships ( `produceQualityRelationships` folder )

  On this occasion, we are looking for useful insights for the agricultors that can be obtained from the dataset such as performance comparisons over simmilar fields and reliable models to study the relationship between agricultural practices, natural factors (such as bioregions and climatic regions), crop types and varieties, time from harvest to consumption, etc. against some relevant nutriotional properties (polyphenols, antioxidants, brix), soil health (mostly carbon content) and yielding when possible. We hope to find other useful outcomes over the course of this exploration.
  The most interesting output of this investigation are probably the tables were we summarise all relevant observations on the effect of practices and amendments over our produce quality indivative variables, `amendmentsTable.html` and `practicesTable.html`.
  
  Please, if you have feedback, new ideas or any relevant comment: we encourage open discussion on this thread in [GOAT forum](http://forum.goatech.org/t/data-analysis-rfc-2019-relationships-between-food-soil-and-farm-management/886)

### Soil Quality Relationships ( `soilQualityRelsFiles` folder )

  This brief follows the structure of *Produce Quality Relationships* but focuses on organic matter content, PH and respiration for soil at two depths ( 10cm, 20cm ).
  Again, summary tables are available, `soilAmendmentsTable.html` and `soilPracticesTable.html`. 
  
### Flavor Scales ( `flavorsBrief` )

  We've analyzed tasting information and its impact over the median for some of our variables, testing at the same time the utility of this kind of data and it's predictive content.
