---
title: "2019 Survey, Spectroscopy Data Analysis"
author: "Real Food Campaign"
date: "01/08/2022"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---


```
## ── Attaching packages ──────────────────────────────────────────── tidyverse 1.3.0 ──
```

```
## ✔ ggplot2 3.3.3     ✔ purrr   0.3.4
## ✔ tibble  3.0.4     ✔ dplyr   1.0.2
## ✔ tidyr   1.1.2     ✔ stringr 1.4.0
## ✔ readr   1.4.0     ✔ forcats 0.5.0
```

```
## ── Conflicts ─────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```
## 
## ── Column specification ─────────────────────────────────────────────────────────────
## cols(
##   .default = col_double(),
##   lab_id = col_character(),
##   sample_id = col_character(),
##   batch_id = col_character(),
##   crop = col_character(),
##   proteins = col_logical(),
##   involved_surveys = col_character(),
##   organic_carbon_content_0to10cm = col_logical(),
##   organic_carbon_content_10to20cm = col_logical(),
##   sample_ID = col_character(),
##   produce = col_character(),
##   treatment = col_character(),
##   farm = col_character()
## )
## ℹ Use `spec()` for the full column specifications.
```

```
## # A tibble: 319 x 7
##    treatment    till  notill nobeam beam  conservation_till none 
##    <chr>        <lgl> <lgl>  <lgl>  <lgl> <lgl>             <lgl>
##  1 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  2 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  3 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  4 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  5 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  6 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  7 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  8 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  9 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
## 10 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
## # … with 309 more rows
```



