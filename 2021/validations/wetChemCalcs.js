const papa = require('papaparse');
const fs = require('fs');
const _ = require('lodash');

let data = JSON.parse( fs.readFileSync('../dataset/dataReviewCheckedData.json','utf8') );

let wetChemAttributes = [
    "antioxidants",
    "polyphenols",
    "supernatant",
    "proteins"
];

let calculationsControl;

calculationsControl =  data
    .filter( d => d.intake?.crop )  
    .map( row => {
    let newRow = {
        crop: row.intake?.crop,
        lab_id: row.lab_id?.replace( /\s/g, "" ),
        produce_approved: row.produce_check ? "yes" : "no",
        soil_approved: row.soil_check ? "yes" : "no",
        sample_id: row.sample_id?.replace( /\s/g, "" ),
        batch_id: row.batch_id?.replace( /\s/g, "" ),
        sample_wt: row.wet_chem_measured_values?.sample_wt,
        dry_weight_percentage: row.dry_weight?.weight,
        dry_weight_reference: row.wet_chem_reference_values?.standard_dry_weight,
        moisture_content: row.wet_chem_measured_values?.moisture_content,
        // this is 1 - standard_dry_weight * 0.01
        moisture_content_reference: row.wet_chem_reference_values?.moisture_content_ref,
        // orig is dilution multiplied by measurement. Fallback is 1.
        antioxidants_orig: row.antioxidants ? row.antioxidants[ "orig_sample" ] : undefined,
        antioxidants_dilution: row.antioxidants ? row.antioxidants.dilution : undefined,
        // raw is ( _orig * extractant ) / sample_wt. For proteins, there's an extra 100 factor, for polyphenols there an extra 0.1 factor
        antioxidants_raw: row.antioxidants ? row.antioxidants[ "raw_sample" ] : undefined,
        // _dry is _raw / dry_weight_percentage
        antioxidants_dry: row.antioxidants ? row.antioxidants[ "dry_sample" ] : undefined,
        // final  is  _dry * ( 1 - moisture_content )
        antioxidants_final: row.antioxidants ? row.antioxidants[ "final_sample" ] : undefined,
        polyphenols_orig: row.polyphenols ? row.polyphenols[ "orig_sample" ] : undefined,
        polyphenols_dilution: row.polyphenols ? row.polyphenols.dilution : undefined,
        polyphenols_raw: row.polyphenols ? row.polyphenols[ "raw_sample" ] : undefined,
        polyphenols_dry: row.polyphenols ? row.polyphenols[ "dry_sample" ] : undefined,
        polyphenols_final: row.polyphenols ? row.polyphenols[ "final_sample" ] : undefined,
        proteins_orig: row.proteins ? row.proteins[ "orig_sample" ] : undefined,
        proteins_dilution: row.proteins ? row.proteins.dilution : undefined,
        proteins_raw: row.proteins ? row.proteins[ "raw_sample" ] : undefined,
        proteins_dry: row.proteins ? row.proteins[ "dry_sample" ] : undefined,
        proteins_final: row.proteins ? row.proteins[ "final_sample" ] : undefined,
        proteins_percentage: row.proteins?.percentage

    };
        newRow.density = newRow.whole_crop_weight / newRow.volume;
    return newRow;
} );

fs.writeFileSync( "./wetChemCalcsValidation.csv", papa.unparse(calculationsControl), e => console.error(e) );
