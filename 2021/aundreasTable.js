const papa = require('papaparse');
const fs = require('fs');
const _ = require('lodash');

let data = JSON.parse( fs.readFileSync('../dataset/dataReviewDataChecked.json','utf8') );
let practices = papa.parse( fs.readFileSync( './chicoFarmPractices.csv', 'utf8' ), { header: true, dynamicTyping: true } )
    .data
;
// tagging tomattoes meant for canning
data
    .filter( d => d.sample_collection )
    .filter( d => d.sample_collection.variety?.value != null )
    .filter( d => /roma/i.test( d.sample_collection.variety.value[0] ) )
    .map( d => d.intake ? d.intake.crop = "roma tomato" : d.intake = { crop: "roma tomato" } )
;
// batch CCP081921 lacks sample_Colletion but is roma also
data
    .filter( d => d.batch_id?.replace(/\s/, "") == "CCP081921" )
    .filter( d => d.intake.crop == "tomato" )
    .map( d => d.intake ? d.intake.crop = "roma tomato" : d.intake = { crop: "roma tomato" } )
;


let wetChemAttributes = [
    "antioxidants",
    "polyphenols",
    "supernatant",
    "proteins"
];

let chicoData = data
    .filter( d => d.batch_id ? d.batch_id.match( /^CCP/ ) !== null : false )
;

let intakes = chicoData.map( d => d.intake.other_intake_data );
let densities = chicoData.map( d => d.intake.other_intake_data.intake.density );

let finalTable =  chicoData// getting entries just for california lab
    .map( row => {
        let whole_weight;
        let density;
        if ( row.intake.crop == 'butternut_squash' ) {
            whole_weight = row.intake?.other_intake_data?.intake.density_butternut.weight['1'].value;
        } else if ( row.intake?.other_intake_data?.intake?.size?.value ) {
            whole_weight = row.intake?.other_intake_data?.intake?.size?.value.average_weight;
            density = row.intake?.other_intake_data?.intake?.size?.value.density;
        } else {
            whole_weight = row.intake?.other_intake_data.intake.density['1'].value;
        }
        ;
    let newRow = {
        lab_id: row.lab_id.replace( /\s/g, "" ),
        sample_id: row.sample_id.replace( /\s/g, "" ),
        batch_id: row.batch_id.replace( /\s/g, "" ),
        crop: row.intake.crop,
        antioxidants: row.antioxidants ? row.antioxidants[ "final_sample" ] : undefined,
        polyphenols: row.polyphenols ? row.polyphenols[ "final_sample" ] : undefined,
        density: density,
        moisture_content: row.wet_chem_measured_values?.moisture_content,
        moisture_content_reference: row.wet_chem_reference_values?.moisture_content_ref,
        brix: row.intake.other_intake_data.intake.juice.brix.value.value,
        whole_crop_weight: whole_weight,
        proteins: row.proteins ? row.proteins[ "final_sample" ] : undefined,
        involved_surveys: row.involved_surveys,
        dry_weight_percentage: row.dry_weight?.weight,
        volume: row.intake.other_intake_data.intake.density.volume_end.value - row.intake.other_intake_data.intake.density.volume_start.value,
        organic_carbon_content_0to10cm: row.loi_0_10?.organic_carbon,
        organic_carbon_content_10to20cm: row.loi_10_20?.organic_carbon,
        length_1:row.intake.other_intake_data.intake.length['1'].value, 
        length_2:row.intake.other_intake_data.intake.length['2'].value, 
        length_3:row.intake.other_intake_data.intake.length['3'].value,
    };
        // if ( !newRow.density ) { newRow.density = newRow.whole_crop_weight / newRow.volume; };

        if ( newRow.crop == "beet" ) { newRow.density = newRow.whole_crop_weight / newRow.volume };
        
        let farmPracticesInfo = practices.find( d => d.sample_ID?.replace( /\s/g, "" ) == row.sample_id?.replace( /\s/g, "" ) );
        Object.assign( newRow, farmPracticesInfo );
    return newRow;
} );

let labIds = Array.from( new Set( finalTable.map( d => d.lab_id ) ) );

function sortFunction( a, b ) {
    let output;
    if ( a.batch_id !== b.batch_id ) {
        if ( a.batch_id < b.batch_id ) {
            output = -1;
        } else{
            output = 1;
        };
    } else if ( a.sample_id < b.sample_id ) {
        output = -1;
    } else {
        output = 1;
    };
    return output;
};

finalTable = finalTable
    .sort( sortFunction )
;


finalTable.map( d => d.antioxidants ).filter( d => d ).length;
finalTable.map( d => d.polyphenols ).filter( d => d ).length;
finalTable.map( d => d.organic_carbon_content_0to10cm ).filter( d => d ).length;
finalTable.map( d => d.organic_carbon_content_10to20cm ).filter( d => d ).length;


// swiss chard batches
let firstSwissChardFinal = finalTable.filter( d => /0427/.test( d.batch_id ) );
let firstSwissChardInitial = chicoData.filter( d => /0427/.test( d.batch_id ) );

// proxy value batches
let firstNoWeightFinal = finalTable.filter( d => /0504/.test( d.batch_id ) );
let firstNoWeightInitial = chicoData.filter( d => /0504/.test( d.batch_id ) );

//duplication
let duplicationFinal = finalTable.filter( d => /051721/.test( d.batch_id ) );
let duplicationInitial = chicoData.filter( d => /05172/.test( d.batch_id ) );

fs.writeFile( "../dataset/aundreasTable.csv", papa.unparse( finalTable ), err => console.log(err) );


let nationalValues;

nationalValues =  data
    .filter( d => d.lab_id )
    .map( row => {
        let whole_weight;
        let density;
        if ( row.intake?.crop == 'butternut_squash' ) {
            whole_weight = row.intake?.other_intake_data?.intake.density_butternut.weight['1'].value;
        } else if ( row.intake?.other_intake_data?.intake?.size?.value ) {
            whole_weight = row.intake?.other_intake_data?.intake?.size?.value.avearage_weight;
            density = row.intake?.other_intake_data?.intake?.size?.value.density;
        } else {
            whole_weight = row.intake?.other_intake_data?.intake.density['1'].value;
        }
        ;
    let newRow = {
        lab_id: row.lab_id?.replace( /\s/g, "" ),
        sample_id: row.sample_id?.replace( /\s/g, "" ),
        batch_id: row.batch_id?.replace( /\s/g, "" ),
        crop: row.intake?.crop,
        whole_crop_weight: whole_weight,
        antioxidants: row.antioxidants ? row.antioxidants[ "final_sample" ] : undefined,
        polyphenols: row.polyphenols ? row.polyphenols[ "final_sample" ] : undefined,
        density: density,
        proteins: row.proteins ? row.proteins[ "final_sample" ] : undefined,
        involved_surveys: row.involved_surveys,
        moisture_content: row.wet_chem_measured_values?.moisture_content,
        moisture_content_reference: row.wet_chem_reference_values?.moisture_content_ref,
        brix: row.intake.other_intake_data?.intake.juice.brix.value.value,
        dry_weight_percentage: row.dry_weight?.weight,
        volume: row.intake.other_intake_data?.intake.density.volume_end.value - row.intake.other_intake_data?.intake.density.volume_start.value,
        length_1:row.intake.other_intake_data?.intake.length['1'].value, 
        length_2:row.intake.other_intake_data?.intake.length['2'].value, 
        length_3:row.intake.other_intake_data?.intake.length['3'].value,
        organic_carbon_content_0to10cm: row.loi_0_10?.organic_carbon,
        organic_carbon_content_10to20cm: row.loi_10_20?.organic_carbon,
    };
        // if ( !newRow.density ) { newRow.density = newRow.whole_crop_weight / newRow.volume; };
        
        let farmPracticesInfo = practices.find( d => d.sample_ID?.replace( /\s/g, "" ) == row.sample_id?.replace( /\s/g, "" ) );
        Object.assign( newRow, farmPracticesInfo );
    return newRow;
} );


fs.writeFile( "../dataset/nationalValues.csv", papa.unparse( nationalValues ), err => console.log(err) );

nationalValues.map( d => d.organic_carbon_content_0to10cm ).filter( d => d ).length;
nationalValues.map( d => d.organic_carbon_content_10to20cm ).filter( d => d ).length;

let calculationsControl;

calculationsControl =  data
    .filter( d => d.intake?.crop )  
    .map( row => {
    let newRow = {
        crop: row.intake?.crop,
        antioxidants_orig: row.antioxidants ? row.antioxidants[ "orig_sample" ] : undefined,
        polyphenols_orig: row.polyphenols ? row.polyphenols[ "orig_sample" ] : undefined,
        antioxidants_raw: row.antioxidants ? row.antioxidants[ "raw_sample" ] : undefined,
        polyphenols_raw: row.polyphenols ? row.polyphenols[ "raw_sample" ] : undefined,
        antioxidants_dry: row.antioxidants ? row.antioxidants[ "dry_sample" ] : undefined,
        polyphenols_dry: row.polyphenols ? row.polyphenols[ "dry_sample" ] : undefined,
        antioxidants_final: row.antioxidants ? row.antioxidants[ "final_sample" ] : undefined,
        polyphenols_final: row.polyphenols ? row.polyphenols[ "final_sample" ] : undefined,
        moisture_content: row.wet_chem_measured_values?.moisture_content,
        moisture_content_reference: row.wet_chem_reference_values?.moisture_content_ref,
        dry_weight_percentage: row.dry_weight?.weight,
        dry_weight_reference: row.wet_chem_reference_values?.standard_dry_weight,

    };
        newRow.density = newRow.whole_crop_weight / newRow.volume;
        let farmPracticesInfo = practices.find( d => d.sample_ID == row.sample_id );
        Object.assign( newRow, farmPracticesInfo );
    return newRow;
} );

// practices in the whole dataset


let allPractices = data.map( d => d.sample_collection?.management?.practices?.value )
    .filter( d => d )
    .reduce( (a,b) => [ ...a, ...b ] )
;

//// given an array of scalars and get a table with the frequency of each value
function frequencyOfArrayElements( array ) {
    let values = _.uniq( array );
    let direct = new Object();
    let inverse = new Object();
    let output = { direct: direct, inverse: inverse };

    values.forEach( function( value ) {
        let frequency = array.filter( d => d == value ).length;
        direct[value] = frequency;
        if ( inverse[ frequency ] ) {
            inverse[frequency].push( value );
        } else {
            inverse[frequency] = [value];
        } ;
    }  );
    return output;
};
