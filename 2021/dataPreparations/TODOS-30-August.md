@OctavioDuarte see my notes from the latest round of data review


### 2019
1. crop_color is missing. However, “Color” is a parameter that is informed in the 2019 dataset 
2. In the 2019 dataset, the “class” field in the matrix is not properly parsing when masking practices. When the “sample_source” = “farm”, there should be 3 types of class options: 

* “data_partner”: “volunteer_type” = “data_partner”
* “farm_partner_planting”: “volunteer_type” = “farm_partner” AND “planting” is present
* “farm_partner_no_planting.”: “volunteer_type” = “farm_partner” and NO “planting” present
It looks like both “data_partner” and “farm_partner_no_planting” are getting masked to override all practices. I have added a “Class” column to the Management Practices sheet to help. For example, sample ID 2028 should be treated as a “data partner” and have organic, not certified = true. 
3. farm_market and store are not parsing any practices. All NA


### 2020
1. crop_color is missing. However, “Color” is a parameter that is informed in the dataset (sample_color_x)
2. Not parsing ‘notill’ properly. For example, sample ID `7252` has both “notill” and “tillage” marked as false, but does properly recognize that a planting IS present. In the planting, “broadforking” and “solarization” are selected, which should trigger “notill” = TRUE. Please re-check the no-till classification.
   - Clear ommision in 2020-data-merging/parseFarmPractices 
3. I made the following changes:

* that “tillage” = 0 when “farm_with_interest”
* “reference” = 0 when “garden”
4. It looks like “reference” is not always parsing properly. For example, “sample_ID” 4019 as both “farm_practices.certified_organic” and “farm_practices.reference” as TRUE. Please recheck the reference definition
  -- organic_certified vs certified_organic in parseFP
5. For “Organic – Not Certified” and “sample_source” = “farm_retail” and “orchard” I updated the parsing definition to:

* farm_market.practices = organic_not_cert         	 (not: ‘org_not_cert’)
* orchard.practices.value = organic_not_cert        	 (not: ‘org_not_cert’)

6. For Notill for “orchard” sample source, I added this option for parsing: “orchard.practices = notill” to reflect that there was a survey change midway through the season, so there are two possible options for parsing notill in orchards
7. For sample_source = store, ‘reference’ is not parsing correctly. Almost all are NA, but all should be parsed and many should be TRUE
-- missing mention to none in whitelist below
    // if the sample comes from a store, just the realistically acknowledgeable through a label practices are informed

### 2021
1. crop_color is not being merged
2. there is an issue with some of the “notill” and “tillage” parsing. For example, “sample_ID” 14030, should be defined as notill = TRUE, but is listed as NA
3. for “store” samples, samples that should have “reference = TRUE” are being displayed as “reference” = NA
