const papa = require('papaparse');
const fs = require('fs');



let data = JSON.parse( fs.readFileSync('./dataReviewData.json','utf8') );
let testData = JSON.parse( fs.readFileSync('./wet_chem_testData.json','utf8') );

let wetChemAttributes = [
    "antioxidants",
    "polyphenols",
    "supernatant",
    "proteins"
];

// CCP092521A/B/C and CCP092621A/B/C

const aundreasBatches = [
    "CCP092521A",   
    "CCP092521B",   
    "CCP092521C",   
    "CCP092621A",
    "CCP092621B",
    "CCP092621C",
];

// test surveys format is different to the one in production surveys. We need to rename the test_id attribute to lab_id and test_batch to batch id.
let eggplantTestData = testData
    .filter( d => d.batch_id.match( /CCP09(2521)|(2621)/ ) != null )
    .filter( d => d.test_id  )
;

let eggplantIDs = new Set( eggplantTestData.map( d => d.test_id ) );
let eggplantsProductionData = data.filter( d => eggplantIDs.has( d.lab_id ) );

// tranvserse the test data and copy the wet chem tests into main table
eggplantTestData.forEach( testEntry => {
    let mainTableEntry = eggplantsProductionData.find( d => d.lab_id == testEntry.test_id );
    if ( testEntry.antioxidants ) {
        mainTableEntry.antioxidants = testEntry.antioxidants;
    } else if (testEntry.polyphenols) {
        mainTableEntry.polyphenols = testEntry.polyphenols;
    };
}
);

let finalTable = eggplantsProductionData.map( row => {
    let newRow = {
        lab_id: row.lab_id,
        sample_id: row.sample_id,
        batch_id: row.batch_id,
        weight: row.dry_weight ? row.dry_weight.weight : undefined,
        antioxidants: row.antioxidants ? row.antioxidants[ "wet chem value" ] : undefined,
        polyphenols: row.polyphenols ? row.polyphenols[ "wet chem value" ] : undefined,
        supernatant: row.supernatant ? row.supernatant[ "wet chem value" ] : undefined,
        proteins: row.proteins ? row.proteins : undefined,
    };
    return newRow;
} );

fs.writeFile( "./aundreasEggPlantTable.csv", papa.unparse( finalTable ), err => console.log(err) );
