---
title: "2019 Survey, Spectroscopy Data Analysis"
author: "Real Food Campaign"
date: "05/20/2022"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---


```
## ── Attaching packages ───────────────────────────────────────────────────────────────────────────── tidyverse 1.3.1 ──
```

```
## ✔ ggplot2 3.3.5     ✔ purrr   0.3.4
## ✔ tibble  3.1.5     ✔ dplyr   1.0.7
## ✔ tidyr   1.1.4     ✔ stringr 1.4.0
## ✔ readr   2.0.2     ✔ forcats 0.5.1
```

```
## ── Conflicts ──────────────────────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```
## Rows: 272 Columns: 24
```

```
## ── Column specification ──────────────────────────────────────────────────────────────────────────────────────────────
## Delimiter: ","
## chr  (9): lab_id, sample_id, batch_id, crop, involved_surveys, sample_ID, pr...
## dbl (12): antioxidants, polyphenols, density, moisture_content, moisture_con...
## lgl  (3): proteins, organic_carbon_content_0to10cm, organic_carbon_content_1...
```

```
## 
## ℹ Use `spec()` to retrieve the full column specification for this data.
## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.
```

```
## # A tibble: 272 × 7
##    treatment    till  notill nobeam beam  conservation_till none 
##    <chr>        <lgl> <lgl>  <lgl>  <lgl> <lgl>             <lgl>
##  1 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  2 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  3 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  4 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  5 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  6 till/no BEAM TRUE  FALSE  TRUE   FALSE FALSE             FALSE
##  7 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  8 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
##  9 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
## 10 till/BEAM    TRUE  FALSE  FALSE  TRUE  FALSE             FALSE
## # … with 262 more rows
```



